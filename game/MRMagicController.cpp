//
// Created by Admin on 25.12.13.
//


#include "MRMagicController.h"
#include "MRGameController.h"
#include "MRTouchReceiver.h"

using namespace myth;
using namespace cocos2d;

void MagicController::setup() {
    m_idleMode = false;

    m_neurons.clear();
    ConfigReaderYAML *gesturesConf = ConfigReaderYAML::createWithFilePath("data/magicGestures.yaml");
    gesturesConf->retain();
    Dictionary *gestures = gesturesConf->fromPath("0")->toDictionary();
    DictElement *gesture;
    CCDICT_FOREACH(gestures, gesture) {
        string name = gesture->getStrKey();
        Array *segments = (Array*) gesture->getObject();
        m_segmentCount = segments->count();
        MagicNeuron neuron;
        neuron.m_name = name;
        neuron.m_memory.clear();
        Object *arrayElement;
        CCARRAY_FOREACH(segments, arrayElement) {
            Dictionary *segmentData = (Dictionary*) arrayElement;
            MagicSegment segment;
            segment.cosinus = ((String*)segmentData->objectForKey("cosinus"))->floatValue();
            segment.sinus = ((String*)segmentData->objectForKey("sinus"))->floatValue();
            neuron.m_memory.push_back(segment);
        }
        m_neurons.push_back(neuron);
    }
    gesturesConf->release();
    auto tl = EventListenerTouchOneByOne::create();

    EventDispatcher *dispatcher = Director::getInstance()->getEventDispatcher();
    tl->onTouchBegan = CC_CALLBACK_2(MagicController::touchBegan, this);
    tl->onTouchMoved = CC_CALLBACK_2(MagicController::touchMoved, this);
    tl->onTouchEnded = CC_CALLBACK_2(MagicController::touchEnded, this);
    dispatcher->addEventListenerWithFixedPriority((EventListener *) tl, m_config.touchPriority);
    m_touchListener = tl;

    //magic particles
    m_particles = ParticleGalaxy::create();
    m_config.parentNode->addChild(m_particles);
    m_particles->setEmissionRate(0.0);

    m_mask = cocos2d::ui::ImageView::create("gameScreen/ui/hud/magicIslandSelectMask.png", ui::Widget::TextureResType::PLIST);
    m_mask->setTouchEnabled(true);
    m_mask->retain();
    m_mask->setVisible(false);
    m_mask->setPosition(Tools::screenCenter());
    m_mask->setScale(4.0);
    m_config.gameController->m_uiNode->addChild(m_mask, 40);

    m_targetsNode = Node::create();
    m_config.gameController->m_uiNode->addChild(m_targetsNode, 41);
    for (int i = 0; i < 4; ++i)
        for (int j = 0; j < 4; ++j) {
            int index = i * 4 + j;
            auto target = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/magicIslandSelectTargetPassive.png");
            m_targetsNode->addChild(target, 1, index);
            target->setPosition(m_config.gameController->zones[i][j].center);
        }
    m_targetsNode->retain();
    auto counterBg = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/magicIslandSelectCounterBg.png");
    m_targetsNode->addChild(counterBg, 2);
    counterBg->setPosition(Point(117.0, 1024.0 - 161.0));
    auto label = Label::createWithBMFont("gameScreen/ui/islandSelectFont/font.fnt", "9", TextHAlignment::LEFT, 100.0, Point::ZERO);
    label->setPosition(Point(149.0, 1024.0 - 176.0));
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    m_targetsNode->addChild(label, 3, 555444);
    m_targetsNode->setVisible(false);
    m_touchAllowed = true;
}


bool MagicController::touchBegan(cocos2d::Touch* touch, cocos2d::Event* event) {
    if (!m_touchAllowed && event) return false;
    if (m_active == true) {
        m_capturing = true;
        float large = 10000.0;
        m_inputPointMin.x = m_inputPointMin.y = large;
        m_inputPointMax.x = m_inputPointMax.y = -large;
        m_inputPoints.clear();
        storeInputFromTouch(touch);

        startStreak(m_config.parentNode->convertTouchToNodeSpace(touch));

        m_config.gameController->magicDrawSymbolStarted();
        return true;

    }
    if (m_selectingIslands) {
        for (int i = 0; i < 4; ++i)
            for (int j = 0; j < 4; ++j) {
                Zone zone = m_config.gameController->zones[i][j];
                Point touchPos = m_config.gameController->m_gameNode->convertTouchToNodeSpace(touch);
                if (zone.center.getDistance(touchPos) < 50.0) {
                    selectIsland(i,j);
                }
            }
        return true;
    }
    return false;

}

void MagicController::touchMoved(cocos2d::Touch* touch, cocos2d::Event* event) {
    if (m_capturing) {
        if (!m_idleMode) storeInputFromTouch(touch);
        updateStreak(m_config.parentNode->convertTouchToNodeSpace(touch));
    }
}

void MagicController::touchEnded(cocos2d::Touch* touch, cocos2d::Event* event) {
    if (m_capturing) {
        filterInputData();
        m_capturing = false;
        _stopStreak();
        m_active = m_capturing = false;
        if (!m_idleMode) recognizeGesture();

    }

}

void MagicController::activate() {
    if (m_active || m_selectingIslands) return;
    m_active = true;
    auto castingMask = cocos2d::ui::ImageView::create("gameScreen/ui/castingMask.png");
    castingMask->setTouchEnabled(true);
    m_config.gameController->m_hudNode->addChild(castingMask, 10, 555666);
    castingMask->setPosition(Tools::screenCenter());
    m_config.gameController->setPaused(true);
    //m_config.gameController->dispatchDenyTouches((int)TouchReceiverFlag::magicButton);
    //m_config.gameController->dispatchAllowTouches((int)TouchReceiverFlag::magicGesture);

    // back button
    auto button = ui::Button::create();
    button->loadTextureNormal("commonUI/backButton.png", TextureResType::LOCAL);
    button->setTouchEnabled(true);
    button->addTouchEventListener([this](Ref* target, ui::Widget::TouchEventType type){
        if (type == ui::Widget::TouchEventType::ENDED) backButtonToucned();
    });
    button->setPosition(Point(100.0, 950.0));
    m_config.parentNode->addChild(button, 100);
    m_backButton = button;
}

void MagicController::storeInputFromTouch(Touch *pTouch) {
    MagicInputData inputData;
    inputData.point = pTouch->getLocation();
    if (inputData.point.x > m_inputPointMax.x) m_inputPointMax.x = inputData.point.x;
    if (inputData.point.y > m_inputPointMax.y) m_inputPointMax.y = inputData.point.y;
    if (inputData.point.x < m_inputPointMin.x) m_inputPointMin.x = inputData.point.x;
    if (inputData.point.y < m_inputPointMin.y) m_inputPointMin.y = inputData.point.y;
    if (m_inputPoints.size() == 0) {
        inputData.len = 0;
    } else {
        MagicInputData lastData = m_inputPoints[m_inputPoints.size()-1];
        inputData.len = lastData.len + inputData.point.getDistance(lastData.point);
    }
    m_inputPoints.push_back(inputData);
}

void MagicController::filterInputData() {
    normalizeInput();
    int curIndex = 0, prevIndex = 0, segmentCount = m_segmentCount;
    float segmentLen = m_inputPoints[m_inputPoints.size()-1].len / (float)segmentCount;
    Point segmentStart, segmentEnd;
    m_inputSegments.clear();
    for (int i = 0; i < segmentCount; ++i) {
        float searchLen = segmentLen * (i+1);
        MagicSegment segment;
        segmentStart =  (i == 0) ? m_inputPoints[0].point : segmentEnd;
        bool endPointFound = false;
        while (curIndex < m_inputPoints.size() && !endPointFound) {
            MagicInputData point = m_inputPoints[curIndex];
            MagicInputData pPoint = m_inputPoints[prevIndex];
            if (searchLen < point.len) {
                float lerpQ = ( searchLen - pPoint.len ) / (point.len - pPoint.len);
                segmentEnd = point.point.lerp(pPoint.point, 1.0-lerpQ);
                endPointFound = true;
            }
            else if (searchLen == point.len) { // almost impossible
                segmentEnd = point.point;
                endPointFound = true;
            }
            else {
                curIndex++;
                prevIndex = curIndex - 1;
            }
        }
        if (!endPointFound && i == segmentCount -1) { // the end
            segmentEnd = m_inputPoints[prevIndex].point;
        }
        Point diff = segmentEnd - segmentStart;
        segment.cosinus = cosf(atan2(diff.y, diff.x));
        segment.sinus = sinf(atan2(diff.y, diff.x));
        m_inputSegments.push_back(segment);
    }
}

void MagicController::recognizeGesture() {
    float maxNWeight = 0.0;
    int maxNWeightKey = 0;
    for (int i = 0; i < m_neurons.size(); ++i) {
        float weight = m_neurons[i].calculateWeight(&m_inputSegments, m_inputSegments.size());
        if (weight > maxNWeight) {
            maxNWeight = weight;
            maxNWeightKey = i;
        }
    }
    std::string text;
    MagicType type;
    if (m_neurons[maxNWeightKey].m_weight > m_config.successNeuronWeight) {
        auto name = m_neurons[maxNWeightKey].m_name;
        if (name == "fire" || name == "fireR") type = MagicType::fire;
        if (name == "air" || name == "airR")   type = MagicType::air;
        if (name == "water" || name == "waterR") type = MagicType::water;
        if (name == "earth" || name == "earthR") type = MagicType::earth;
        text = name;
    }
    else {
        type = MagicType::fail;
        text = "failed";
    }
    if (m_config.gameController->m_tutorial && type == MagicType::fail) {
        log("fail and tutorial");
        m_active = true;
        return;
    }
    else {
        auto castingMask = m_config.gameController->m_hudNode->getChildByTag(555666);
        castingMask->removeFromParentAndCleanup(true);
    }
    if (m_config.gameController->isBossTime()) {
        m_config.gameController->applyMagicBossTime(type);
        m_config.gameController->setPaused(false);
        hideBackButton();
    }
    else {
        if (m_config.gameController->callMagician(type)) {
            m_mask->setVisible(true);
            m_selectingIslands = true;
            m_currentType = type;
            //m_config.gameController->setPaused(true);
            ConfigReader* upgradeReader =
                    Tools::getUpgradeReader("magic", GD->m_model.getLevelProgress()->magicUpgradeIds[static_cast<int>(m_currentType)]);
            m_islandCountLimit = upgradeReader->fromPath("attackZones")->toInt();
            showTargets();
        }
        else {
            m_config.gameController->setPaused(false);
            hideBackButton();
            CCLOG("not enough mana");
        }
    }
}

void MagicController::normalizeInput() {
    float xBound = m_inputPointMax.x - m_inputPointMin.x;
    float yBound = m_inputPointMax.y - m_inputPointMin.y;
    for (int i = 0; i < m_inputPoints.size(); ++i) {
        m_inputPoints[i].point.x = (m_inputPoints[i].point.x - m_inputPointMin.x) / xBound;
        m_inputPoints[i].point.y = (m_inputPoints[i].point.y - m_inputPointMin.y) / yBound;
    }
}

void MagicController::selectIsland(int i, int j) {
    for (int k = 0; k < m_selectedIslands.size(); ++k)
        if (m_selectedIslands[k].first == i && m_selectedIslands[k].second == j)
            return;

    m_selectedIslands.push_back(std::pair<int,int>(i,j));
    auto target = static_cast<Sprite *>(m_targetsNode->getChildByTag(i * 4 + j));
    target->setSpriteFrame("gameScreen/ui/hud/magicIslandSelectTargetActive.png");
    updateCounterLabel(m_islandCountLimit - m_selectedIslands.size());

    if (m_selectedIslands.size() >= m_islandCountLimit) {
        m_selectingIslands = false;
        m_targetsNode->setVisible(false);
        m_mask->setVisible(false);
        m_config.gameController->applyMagic(m_currentType, m_selectedIslands);
        m_config.gameController->setPaused(false);
        m_selectedIslands.clear();
        hideBackButton();
    }
}


void MagicController::allowTouches(int flags) {
    if (flags & (int)TouchReceiverFlag::magicGesture)
        m_touchAllowed = true;
    if (flags & (int)TouchReceiverFlag::magicBack)
        m_backButtonAllowed = true;
}

void MagicController::denyTouches(int flags) {
    if (flags & (int)TouchReceiverFlag::magicGesture)
        m_touchAllowed = false;
    if (flags & (int)TouchReceiverFlag::magicBack)
        m_backButtonAllowed = false;
}

void MagicController::showTargets() {
    m_targetsNode->setVisible(true);
    for (int i = 0; i < 4; ++i)
        for (int j = 0; j < 4; ++j) {
            int index = i * 4 + j;
            auto target = static_cast<Sprite *>(m_targetsNode->getChildByTag(i * 4 + j));
            target->setSpriteFrame("gameScreen/ui/hud/magicIslandSelectTargetPassive.png");
        }
    updateCounterLabel(m_islandCountLimit);
}


void MagicController::updateCounterLabel(int count) {
    auto label = static_cast<Label*>(m_targetsNode->getChildByTag(555444));
    char islandsCount[2];
    sprintf(islandsCount, "%d", count);
    label->setString(islandsCount);
}

Node *MagicController::getIslandCounter() {
    return dynamic_cast<Node*>(m_targetsNode->getChildByTag(555444));;
}


void MagicController::hideBackButton() {
    m_backButton->removeFromParent();
}

void MagicController::backButtonToucned() {
    if (m_backButtonAllowed) {
        if (m_active) {
            m_active = false;
            auto castingMask = m_config.gameController->m_hudNode->getChildByTag(555666);
            castingMask->removeFromParentAndCleanup(true);
            hideBackButton();
            m_config.gameController->setPaused(false);
        }
        if (m_selectingIslands) {
            m_selectingIslands = false;
            m_targetsNode->setVisible(false);
            m_mask->setVisible(false);
            m_config.gameController->setPaused(false);
            hideBackButton();
        }
    }
}


MagicController::~MagicController() {
    m_targetsNode->release();
    m_mask->release();
    EventDispatcher *dispatcher = Director::getInstance()->getEventDispatcher();
    dispatcher->removeEventListener(m_touchListener);

}

void MagicController::setIdleMode(bool value) {
    m_idleMode = value;
}

void MagicController::startStreak(Vec2 pos) {
    m_particles->setPosition(pos);
    m_particles->setEmissionRate(100.0);
}

void MagicController::updateStreak(Vec2 pos) {
    m_particles->setPosition(pos);
}

void MagicController::_stopStreak() {
    m_particles->setEmissionRate(0);
}

void MagicController::stopStreak() {
    if (!m_capturing) _stopStreak();
}
