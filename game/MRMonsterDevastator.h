//
// Created by Admin on 20.08.14.
//


#ifndef __MRMonsterDevastator_H_
#define __MRMonsterDevastator_H_

#include "core/myth.h"
#include "MRMonster.h"

namespace myth {

    class MonsterDevastator : public Monster{

    public:
        virtual void makeBloodPaddle() override;

        virtual void fillAnimations(ConfigReader *reader) override;

        virtual void update(float dt) override;

        virtual void recieveDamage(float damage, DamageType type) override;
    };

}

#endif //__MRMonsterDevastator_H_
