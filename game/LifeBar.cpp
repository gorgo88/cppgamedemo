//
// Created by Admin on 19.12.13.
//


#include "LifeBar.h"

myth::LifeBar *myth::LifeBar::create() {
    LifeBar *instance = new LifeBar();
    instance->init();
    instance->autorelease();
    return instance;
}

bool myth::LifeBar::init() {
    auto fullSprite = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/LifeBarFull.png");
    auto emptySprite = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/LifeBarEmpty.png");
    auto borderSprite = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/LifeBarBorder.png");
    addChild(emptySprite);
    addChild(fullSprite);
    addChild(borderSprite);
    emptySprite->setAnchorPoint(Point(1.0,0.0));
    fullSprite->setAnchorPoint(Point());
    borderSprite->setAnchorPoint(Point());
    m_empty = emptySprite;
    m_empty->setPosition(Point(fullSprite->getContentSize().width, emptySprite->getPosition().y));
    m_full = fullSprite;
    return true;
}

void myth::LifeBar::setValue(float val) {
    if (val < 0.0) val = 0.0;
    if (val > 1.0) val = 1.0;
    m_full->setScaleX(val);
    m_empty->setScaleX(1.0-val);
}

