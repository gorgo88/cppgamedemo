//
// Created by Admin on 21.05.14.
//

#include "MRSpawnProgram.h"
#include "MRGameController.h"


void myth::SpawnProgram::setup(ConfigReader *programReader) {
    m_state = State::notStarted;
    fillDelay(programReader);
    fillMonsterData(programReader);
    fillQueue();
    fillInterval(programReader);
    m_spawnTimeout = m_delay + m_interval;
    m_queueItemIndex = 0;
}

void myth::SpawnProgram::update(float dt) {
    if (m_state == State::going && m_spawnTimeout > 0.0) {
        m_spawnTimeout -= dt;
        if (m_spawnTimeout <= 0.0) {
            spawnTime();
        }
    }
}

void myth::SpawnProgram::fillMonsterData(ConfigReader *reader) {
    Array *monstersArr = reader->fromPath("monsters")->toArray();
    for (int i = 0; i < monstersArr->count(); ++i) {
        char monsterKey[100];
        sprintf(monsterKey, "monsters.%d", i);
        ConfigReader *monsterReader = reader->getSubConfigByPath(monsterKey);
        MonsterData monster;
        monster.count = monsterReader->fromPath("count")->toInt();
        monster.price = -1;
        monster.attackDelay = -1.0;
        if (monsterReader->hasPath("price"))
            monster.price = monsterReader->fromPath("price")->toInt();
        if (monsterReader->hasPath("priceCrystal"))
            monster.priceCrystal = monsterReader->fromPath("priceCrystal")->toInt();
        if (monsterReader->hasPath("lifeMax"))
            monster.lifeMax = monsterReader->fromPath("lifeMax")->toInt();
        if (monsterReader->hasPath("attackDelay"))
            monster.attackDelay = monsterReader->fromPath("attackDelay")->toFloat();
        monster.id = monsterReader->fromPath("id")->toString()->_string;
        monster.configReader = monsterReader;
        monsterReader->retain();
        m_monsterDatas.push_back(monster);
    }
}

float myth::SpawnProgram::getTotalTime() {
    return m_delay + m_interval * getTotalMonsterCount();
}

float myth::SpawnProgram::getTotalMonsterCount() {
    int count = 0;
    for (int i = 0; i < m_monsterDatas.size(); ++i)
        count += m_monsterDatas[i].count;
    return count;
}

void myth::SpawnProgram::fillQueue() {
    m_queue.clear();

    //lets go: slice - repititive sequence of queueItems, roughly equaled
    int sliceCount = 1000;
    for (int i = 0; i < m_monsterDatas.size(); ++i)
        if (m_monsterDatas[i].count > 0 && m_monsterDatas[i].count < sliceCount) sliceCount = m_monsterDatas[i].count;
    for (int s = 0; s < sliceCount; ++s) {
        for (int m = 0; m < m_monsterDatas.size(); ++m) {
            if(m_monsterDatas[m].count <= 0) continue;
            QueueItem item;
            item.monsterDataIndex = m;
            item.spawned = 0;
            int regularItemTotal = (int)ceilf((float)m_monsterDatas[m].count / (float)sliceCount);
            if (s == sliceCount - 1)
                item.total = m_monsterDatas[m].count - (sliceCount - 1) * regularItemTotal;
            else
                item.total = regularItemTotal;
            if (item.total > 0) m_queue.push_back(item);
        }
    }
}

void myth::SpawnProgram::fillInterval(ConfigReader *reader) {
    if (!reader->hasPath("fillInto")) {
        m_type = TimingType::absolute;
        m_interval = reader->hasPath("interval")
                ? reader->fromPath("interval")->toFloat()
                : GD->defaultsConfig()->fromPath("monsterSpawnInterval")->toFloat();
        if (GD->m_model.getCurrentSlot()->difficulty == Difficulty::hard)
            m_interval = m_interval - m_interval * 0.17f; // issue #565
    }
    else {
        m_type = TimingType::relative;
        m_interval = (m_controller->
                getSpawnProgramById(reader->fromPath("fillInto")->toString()->_string)
                ->getTotalTime() - m_delay) / getTotalMonsterCount();
    }
}

void myth::SpawnProgram::fillDelay(ConfigReader *reader) {
    if (reader->hasPath("delay")) m_delay = reader->fromPath("delay")->toFloat();
    else m_delay = 0.0;
}

void myth::SpawnProgram::setController(myth::GameController *controller) {
    m_controller = controller;
}

void myth::SpawnProgram::spawnTime() {
    log("sp %s spawn time", m_id.c_str());
    QueueItem *item = &m_queue[m_queueItemIndex];
    if (item->total <= item->spawned) {
        if (m_queueItemIndex >= m_queue.size() - 1) {
            programComplete();
            return;
        }
        else {
            m_queueItemIndex++;
            item = &m_queue[m_queueItemIndex];
        }
    }
    MonsterData *mData = &m_monsterDatas[item->monsterDataIndex];

    log ("sp %s - %s spawned", m_id.c_str(), mData->id.c_str());
    if (m_spawnCallback) m_spawnCallback(mData, this);
    item->spawned ++;

    if (item->total <= item->spawned && m_queueItemIndex >= m_queue.size() - 1) {
        programComplete();
        return;
    }

    m_spawnTimeout = m_interval;
}

void myth::SpawnProgram::programComplete() {
    log("sp %s complete", m_id.c_str());

    m_state = State::complete;
    if (m_completeCallback) m_completeCallback(this);
}

void myth::SpawnProgram::start() {
    log("sp %s starts", m_id.c_str());
    m_state = State::going;
}

bool myth::SpawnProgram::isGoing() {
    return m_state == State::going;
}

bool myth::SpawnProgram::isComplete() {
    return m_state == State::complete;
}

void myth::SpawnProgram::forceComplete() {
    programComplete();
}
