
#ifndef __MRLocaton_H_
#define __MRLocaton_H_

namespace myth {
    enum class Location : int {
        desert,
        regular,
        snow,
        inferno
    };
}

#endif