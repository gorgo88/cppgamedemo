//
// Created by Admin on 07.01.14.
//



#ifndef __MRModel_H_
#define __MRModel_H_

#define LEVEL_COUNT 36

#include "MRLocations.h"
#include "MRComixEnum.h"
#include "cocos2d.h"
#include "MRMagicType.h"

namespace myth {

    enum class FlagIndices : int {
        newGameDialogComplete = 1<<0,  //unused
        firstLevelDialogPlayed = 1<<1,
        upgradesDialogPlayed = 1<<2,
        desertGreenDialogPlayed = 1<<3,
        greenSnowDialogPlayed = 1<<4,
        snowInfernoDialogPlayed = 1<<5,
        regularLocationOpened = 1<<6,
        snowLocationOpened = 1<<7,
        infernoLocationOpened = 1<<8,
        upgradesBridgesDialogPlayed = 1<<9,
        firstBossDialogPlayed = 1<<10,
        boss2_4DialogPlayed = 1<<11,
        fifthLevelDialogPlayed = 1<<12,
        towerUpgraded = 1<<13,
        slotNewGameEntered = 1<<14,
        sale0Used = 1<<15,
        sale1Used = 1<<16,
        sale2Used = 1<<17,
        sale0Shown = 1<<18,
        sale1Shown = 1<<19,
        sale2Shown = 1<<20,
    };


    enum class LevelAchievement : int {
        sniper = 1<<0,
        destroyer = 1<<1,
        mushroomer = 1<<2,
        lastLine = 1<<3,
        combo = 1<<4,
    };

    enum class Difficulty : int {
        normal,
        hard
    };

    class ModelGameProgress {
    public:
        int currentLevel;
        int bridgeUpgradeIds[16] = {0};
        int bulletUpgradeIds[4];
        int guillotineUpgradeIds[4];
        int towerUpgradeIds[4];
        int magicUpgradeIds[4];
        int completeLevels;   // last complete level id
        int gold;
        int crystals;
        int score;
        int flags;
        std::vector<std::string> shownMonsterCards;
        int mushroomCollected[LEVEL_COUNT];
        int levelAchievements[LEVEL_COUNT];
        int locationAchievements[4];
        Difficulty difficulty;
        int dailyAwardLastGrabedDayId;
        int dailyAwardLastGrabedAwardId;
        int totalGameTimeSeconds;
        float totalSlotTimeSeconds;
        int totalLoses;
        int salesShownMask;
        int salesUsedMask;
        bool shouldShowAfterBossWinSale; // doesnt require dumping
        bool shouldShowAfterBossLoseSale; // doesnt require dumping

        void clear() {
            currentLevel = 0;
            completeLevels = -1;
            gold = 0;
            crystals = 0;
            score = 0;
            for (int i = 0; i < 16; ++i) bridgeUpgradeIds[i] = 0;
            for (int i = 0; i < 4; ++i) bulletUpgradeIds[i] = 0;
            for (int i = 0; i < 4; ++i) guillotineUpgradeIds[i] = 0;
            for (int i = 0; i < 4; ++i) towerUpgradeIds[i] = 0;
            for (int i = 0; i < 4; ++i) magicUpgradeIds[i] = 0;
            magicUpgradeIds[(int)MagicType::water] = 1;
            for (int i = 0; i < LEVEL_COUNT; ++i) mushroomCollected[i] = 0;
            for (int i = 0; i < LEVEL_COUNT; ++i) levelAchievements[i] = 0;
            for (int i = 0; i < 4; ++i) locationAchievements[i] = 0;
            shownMonsterCards.clear();
            flags = 0;
            difficulty = Difficulty::normal;
            dailyAwardLastGrabedAwardId = -1;
            dailyAwardLastGrabedDayId = -1;
            totalGameTimeSeconds = 0;
            totalSlotTimeSeconds = 0.0f;
            totalLoses = 0;
            salesShownMask = 0;
            salesUsedMask = 0;
            shouldShowAfterBossWinSale = false;
            shouldShowAfterBossLoseSale = false;
        }

        bool getFlag(FlagIndices flag) {
            return (bool)((int)flag & flags);
        }

        void setFlag(FlagIndices flag, bool value) {
            if (value) flags = (int)flag | flags;
            else flags = ~(int)flag & flags;
        }

        bool getLevelAchievement(int levelIndex, LevelAchievement achievement) {
            return (int)achievement & levelAchievements[levelIndex];
        }

        void setLevelAchievement(int levelIndex, LevelAchievement achievement) {
            setLevelAchievement(levelIndex, achievement, true);
        }

        void setLevelAchievement(int levelIndex, LevelAchievement achievement, bool value) {
            if (value) levelAchievements[levelIndex] = (int)achievement | levelAchievements[levelIndex];
            else levelAchievements[levelIndex] = ~(int)achievement & levelAchievements[levelIndex];
        }

        bool getLocationAchievement(int locationIndex, LevelAchievement achievement) {
            return (int)achievement & locationAchievements[locationIndex];
        }

        void setLocationAchievement(int locationIndex, LevelAchievement achievement, bool value) {
            if (value) locationAchievements[locationIndex] = (int)achievement | locationAchievements[locationIndex];
            else locationAchievements[locationIndex] = ~(int)achievement & locationAchievements[locationIndex];
        }

        bool isSaleShowed(int saleId) {
            return (bool)(salesShownMask & (1<<saleId));
        }

        void setSaleShowed(int saleId, bool value) {
            if (value) salesShownMask = (1<<saleId) | salesShownMask;
            else salesShownMask = ~(1<<saleId) & salesShownMask;
        }

        bool isSaleUsed(int saleId) {
            return (bool)(salesUsedMask & (1<<saleId));
        }

        void setSaleUsed(int saleId, bool value) {
            if (value) salesUsedMask = (1<<saleId) | salesUsedMask;
            else salesUsedMask = ~(1<<saleId) & salesUsedMask;
        }

    };


    class TemporaryModelData {
    public:

        TemporaryModelData() :
                needsPlayNewLevelSound(false),
//                tutorialMode(false),
                lastLevelScreenInterstitialMinute(-1),
                currentSlot(-1) {}

        Location currentLocation;
        //bool tutorialMode;
        int currentSlot;
        ComixEnum currentComix;
        ModelGameProgress levelProgress;
        std::vector<int> levelCombos;
        bool needsPlayNewLevelSound;
        int lastComboPrice;
        int lastHeartPrice;
        int currentLevelSavedHearts;
        int currentLevelAchievements;
        int currentLevelLostHearts;
        int lastLevelScreenInterstitialMinute;

        void setLevelAchievement(LevelAchievement achievement, bool value) { //helper
            if (value) currentLevelAchievements = (int)achievement | currentLevelAchievements;
            else currentLevelAchievements = ~(int)achievement & currentLevelAchievements;
        }

        bool getLevelAchievement(LevelAchievement achievement) {
            return (int)achievement & currentLevelAchievements;
        }



    };

    class Model {

    public:
        static const int NO_SLOT = 3;

        enum class Version : int {
            v100,
            v107
        };
        Model() {
            clear();
        }
        static const Version VERSION = Version::v107;

        void clear() {
            soundVolume = 0.5;
            musicVolume = 0.5;
            isBloodEnabled = true;
            newGameDialogComplete = false;
            isAdDisablePurchased = false;
            for (int i = 0; i < 4; ++i) slots[i].clear();
        }

        ModelGameProgress slots[4];  // the fourth slot is only for unexpected tutorial, called from slotSelect menu
        float soundVolume;
        float musicVolume;
        bool isBloodEnabled;
        bool newGameDialogComplete;
        bool isAdDisablePurchased;

        TemporaryModelData temp; // out of dump

        void dumpToDisk();
        void restoreFromDisk();
        ModelGameProgress *getCurrentSlot();
        ModelGameProgress *getLevelProgress();
        void pushLevelProgress();
        void clearLevelProgress();

        void pushCurrentLevelAchievements();

        void filterLevelAchievements();

        void migrateVersions(cocos2d::ValueMap *pMap, Version const version, Version const version1);


    };
}


#endif //__MRModel_H_
