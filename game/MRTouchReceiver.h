//
// Created by Admin on 20.03.14.
//


#ifndef __MRTouchReceiver_H_
#define __MRTouchReceiver_H_

namespace myth {

enum class TouchReceiverFlag : int {
    catapult    = 1<<0,
    pauseButton = 1<<1,
    magicButton = 1<<2,
    upgradeButton = 1<<3,
    magicGesture = 1<<4,
    magicBack   = 1<<5,
    all             = 0xffffff
};

}

#endif //__MRTouchReceiver_H_
