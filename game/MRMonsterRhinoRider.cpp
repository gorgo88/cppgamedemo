//
// Created by Admin on 13.01.14.
//

#include "MRMonsterRhinoRider.h"
#include "MRGameController.h"
#include "audio/include/SimpleAudioEngine.h"

using namespace myth;
using namespace std;
using namespace CocosDenshion;

const string WALK_DOWN = "walkDown";
const string WALK_LEFT = "walkLeft";
const string WALK_RIGHT = "walkRight";
const string ATTACK = "attack";
const string DEATH = "death";
const string ARHINO = "arhino";
const string AFOOT = "afoot";

void MonsterRhinoRider::fillAnimations(ConfigReader *reader) {
    m_animations = Dictionary::create();
    m_animations->retain();

    vector<string> animationNames;
    animationNames.push_back(ATTACK);
    animationNames.push_back(DEATH);
    animationNames.push_back(WALK_DOWN);
    animationNames.push_back(WALK_LEFT);
    animationNames.push_back(WALK_RIGHT);

    vector<string> stateNames;
    stateNames.push_back(ARHINO);
    stateNames.push_back(AFOOT);

    Animation *anim;
    for (int s = 0; s < stateNames.size(); ++s)
        for (int i = 0; i < animationNames.size(); ++i) {
            anim = Animation::create();
            char pattern[255];
            char fpsKey[255];
            sprintf(
                    pattern,
                    "gameScreen/monsters/%s/%s/%s/%s.png",
                    m_config.data->id.c_str(),
                    stateNames[s].c_str(),
                    animationNames[i].c_str(),
                    "%04d");
            sprintf(fpsKey, "%s.animation.%s.fps", stateNames[s].c_str(), animationNames[i].c_str());
            Tools::addFramesToAnimation(anim, pattern, reader->fromPath(fpsKey)->toFloat());
            string animKey = animationNames[i];
            animKey.append(stateNames[s]);
            m_animations->setObject(anim, animKey);
        }

    switchAnimations(RhinoRiderState::arhino);
}

void MonsterRhinoRider::fillData(ConfigReader *reader) {
    //arhino
    //m_arhinoAttackPower = reader->fromPath("arhino.attackPower")->toFloat() + m_config.data->level;
    m_arhinoAttackPower.min = reader->fromPath("arhino.attackPower.min")->toFloat();
    m_arhinoAttackPower.max = reader->fromPath("arhino.attackPower.max")->toFloat();
    //m_arhinoLifeMax = reader->fromPath("arhino.lifeMax")->toFloat() * 0.25 * m_config.data->level;
    m_arhinoLifeMax = reader->fromPath("arhino.lifeMax")->toFloat();
    //afoot
    //m_afootAttackPower = reader->fromPath("afoot.attackPower")->toFloat() + m_config.data->level;
    m_afootAttackPower.min = reader->fromPath("afoot.attackPower.min")->toFloat();
    m_afootAttackPower.max = reader->fromPath("afoot.attackPower.max")->toFloat();
    //m_afootLifeMax = reader->fromPath("afoot.lifeMax")->toFloat() * 0.25 * m_config.data->level;
    m_afootLifeMax = reader->fromPath("afoot.lifeMax")->toFloat();

    m_attackPower   = m_arhinoAttackPower;
    m_lifeMax       = m_arhinoLifeMax;
    m_attackInt = 4.0;
    m_attackDelay = 0.5;

    m_rhinoState = RhinoRiderState::arhino;

}


void MonsterRhinoRider::fillSounds(ConfigReader *reader) {
    Monster::fillSounds(reader->getSubConfigByPath(AFOOT));
    m_afootAttackSounds = attackSounds;
    m_afootDeathSounds = deathSounds;
    Monster::fillSounds(reader->getSubConfigByPath(ARHINO));
}

void MonsterRhinoRider::dieMonster() {
    CCLOG("Die rhino rider");
    m_prevState = m_state;
    m_state = MonsterState::diying;
    if (m_rhinoState == RhinoRiderState::afoot)
        lifeBar->removeFromParent();
    playAnimation(DEATH, false);
    if (m_spriteHolder->getChildByTag(555))
        m_spriteHolder->getChildByTag(555)->removeFromParent();
}

void MonsterRhinoRider::deathAnimationComplete() {
    if (m_rhinoState == RhinoRiderState::afoot)
        Monster::deathAnimationComplete();
    else {
        m_config.controller->increaseScore(m_arhinoPriceScore, m_node->getPosition());
        switchAnimations(RhinoRiderState::afoot);

        // switch sounds
        attackSounds = m_afootAttackSounds;
        deathSounds = m_afootDeathSounds;

        m_rhinoState = RhinoRiderState::afoot;
        m_lifeMax = m_afootLifeMax;
        m_attackPower = m_afootAttackPower;
        m_life = m_lifeMax;
        updateLifeBar();
        m_state = m_prevState;

        if (deathSounds) {
            SimpleAudioEngine::getInstance()->playEffect(
                    deathSounds->sounds.pickNext().c_str(),
                    false, 1.0, 0.0, deathSounds->gain);
        }

        m_sprite->setSpriteFrame(static_cast<Animation*>(m_animations->objectForKey(ATTACK))->getFrames().at(0)->getSpriteFrame());
        if (m_prevState == MonsterState::walkDown || m_prevState == MonsterState::walkDown2)
            playAnimation(WALK_DOWN, true);
        else if (m_prevState == MonsterState::walkLet)
            playAnimation(WALK_LEFT, true);
        else if (m_prevState == MonsterState::walkRight)
            playAnimation(WALK_RIGHT, true);
    }
}

void MonsterRhinoRider::switchAnimations(RhinoRiderState state) {
    vector<string> animationNames;
    animationNames.push_back(ATTACK);
    animationNames.push_back(DEATH);
    animationNames.push_back(WALK_DOWN);
    animationNames.push_back(WALK_LEFT);
    animationNames.push_back(WALK_RIGHT);

    string targetStateName;

    if (state == RhinoRiderState::arhino)
        targetStateName = ARHINO;
    else
        targetStateName = AFOOT;

    for (int i = 0; i < animationNames.size(); ++i) {
        string stateKey = animationNames[i];
        stateKey.append(targetStateName);
        Object *anim = m_animations->
                objectForKey(stateKey);
        m_animations->setObject(anim, animationNames[i]);
    }

}

void MonsterRhinoRider::fillPrice(ConfigReader *reader) {
    Monster::fillPrice(reader);
    m_arhinoPriceScore = reader->fromPath("arhino.priceScore")->toInt();
    m_priceScore = reader->fromPath("afoot.priceScore")->toInt();
}
