//
// Created by Admin on 13.03.14.
//

#include "MRMonsterStaticWoodTarget.h"
#include "MRGameController.h"

using namespace myth;

void MonsterStaticWoodTarget::update(float dt) {
}

void MonsterStaticWoodTarget::setup(ConfigReader *reader) {
    MonsterWoodTarget::setup(reader);
    if (m_config.data->configReader->hasPath("topLeft"))
        m_zone = &m_config.controller->zones[3][0];
    else if (m_config.data->configReader->hasPath("bottomLeft"))
        m_zone = &m_config.controller->zones[2][0];
    else
        m_zone = m_config.controller->getRandFreeZone(std::pair<int,int>(0,3), std::pair<int,int>(0,3));
    m_node->setPosition(m_zone->center);

}

void MonsterStaticWoodTarget::fillAnimations(ConfigReader *reader) {

    MonsterWoodTarget::fillAnimations(reader);
}

vector <Zone *> MonsterStaticWoodTarget::getCurrentZones() {
    return vector<Zone*> {m_zone};
};

