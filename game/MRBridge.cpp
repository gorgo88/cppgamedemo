//
// Created by Admin on 10.12.13.
//


#include "MRBridge.h"
#include "MRGameController.h"
#include "core/CheetahAtlasHelper.h"

using namespace myth;

void Bridge::setup() {
    m_node = Node::create();
    String *path = String::createWithFormat("Bridge%d%d", m_config.row, m_config.col);
    GD->ggc()->applyTransform(m_node, path->_string);
    m_config.parentNode->addChild(m_node, m_config.zOrder);

    //SpriteFrameCache::getInstance()->addSpriteFramesWithFile("gameScreen/bridges/bridges.plist");
    m_sprite = Sprite::create();
    m_node->addChild(m_sprite);
    m_state = BridgeState::up;

    m_getUpAnimation = nullptr;
    m_getDownAnimation = nullptr;

    m_damageAnimation = nullptr;

    char upgradeKey[100];
    sprintf(upgradeKey, "bridgeUpgrades.upgrade%02d", m_config.startUpgrade);
    ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(upgradeKey);
    applyUpgrade(m_config.startUpgrade, upgradeReader);

    lifeBar = LifeBar::create();
    m_node->addChild(lifeBar);
    lifeBar->setPosition(-30.0, -10.0);
    lifeBar->setScale(0.5);
    lifeBar->setRotation(-90.0);
    updateLifeBar();

    for (int i = 0; i < ALLOWED_MONSTERS_TO_ATTACK; ++i)
        m_attackingMonsters[i] = nullptr;
}


void Bridge::updateLifeBar() {
    lifeBar->setValue(getLifeScale());
}


float Bridge::getLifeScale() {
    return m_life / m_lifeMax;
}

void Bridge::applyUpgrade(int upgradeIndex, ConfigReader *reader) {
    m_lifeMax = reader->fromPath("lifeMax")->toFloat();
    //m_life = m_lifeMax;
    fillAnimations(upgradeIndex);
    if (m_sprite->getNumberOfRunningActions() > 0) {
        m_sprite->stopAllActions();
        m_sprite->runAction(Animate::create(m_state == BridgeState::down ? m_getDownAnimation : m_getUpAnimation));
    }
}



void Bridge::fillAnimations(int upgradeIndex) {
    if (m_getDownAnimation) m_getDownAnimation->release();
    if (m_getUpAnimation) m_getUpAnimation->release();
    m_getUpAnimation = Animation::create();
    m_getDownAnimation = Animation::create();
    char framePattern[60];
    sprintf(framePattern, "gameScreen/bridges/%02d/grow/%s.png", upgradeIndex, "%04d");
    Tools::addFramesToAnimation(m_getUpAnimation, framePattern, 24.0);
    sprintf(framePattern, "gameScreen/bridges/%02d/fall/%s.png", upgradeIndex, "%04d");
    Tools::addFramesToAnimation(m_getDownAnimation, framePattern, 24.0);
    m_getDownAnimation->retain();
    m_getUpAnimation->retain();
    if (m_sprite->getNumberOfRunningActions() <=0 ) {
        auto animation = (m_state == BridgeState::down) ? m_getUpAnimation : m_getDownAnimation;
            m_sprite->setSpriteFrame(animation->getFrames().at(0)->getSpriteFrame());
    }
    if (m_damageAnimation) m_damageAnimation->release();
    m_damageAnimation = Animation::create();
    Tools::addFramesToAnimation(m_damageAnimation, "gameScreen/bridges/splinters/%04d.png", 24.0);
    m_damageAnimation->retain();
}

void Bridge::recieveDamage(float damage) {
    m_life -= damage;
    if (m_life <= 0) {
        m_life = 0;

        getDown();
    }
    updateLifeBar();

    auto damageSprite = Sprite::create();
    damageSprite->runAction(Sequence::createWithTwoActions(
            Animate::create(m_damageAnimation),
            CallFunc::create([damageSprite](){damageSprite->removeFromParentAndCleanup(true);}))
    );
    m_node->addChild(damageSprite, -1);
    damageSprite->setPosition(Point(0.0, 20.0));
}

void Bridge::getDown() {
    m_sprite->stopAllActions();
    m_sprite->runAction(Animate::create(m_getDownAnimation));
    m_state = BridgeState::down;
    m_node->setZOrder(2); //todo: redo
    m_fallTimeout = m_config.fallTimeout;
    CCLOG("bridge fail");
}

void Bridge::getUp() {
    m_sprite->stopAllActions();
    m_sprite->runAction(Animate::create(m_getUpAnimation));
    m_state = BridgeState::up;
    m_life = m_lifeMax;
    m_node->setZOrder(6);
    updateLifeBar();
}

bool Bridge::isInAttackZone(cocos2d::Point point) {
    Rect zone = m_sprite->getBoundingBox();
    zone.origin.y += 20;
    point = m_node->convertToNodeSpace(point);
    return zone.containsPoint(point);
}

void Bridge::update(float dt) {
    if (m_state == BridgeState::down) {
        if (m_fallTimeout <= 0) {
            if (m_config.controller->canBridgeUp(this)) {
                getUp();
            }
        } else {
            m_fallTimeout -= dt;
        }
    }

}

Bridge::~Bridge() {
    if (m_getUpAnimation) m_getUpAnimation->release();
    if (m_getDownAnimation) m_getDownAnimation->release();
    if (m_damageAnimation) m_damageAnimation->release();
}
