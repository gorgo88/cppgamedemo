//
// Created by Admin on 13.02.14.
//

#include "MRTutorialController.h"
#include "MRGameController.h"
#include "strutil/strutil.h"
#include "MRDialogManager.h"
#include "MRTouchReceiver.h"

#define ISSUE_853_MASK_TAG 2113

using namespace myth;
using namespace ui;

static const int HAND_TAG = 2211;

void TutorialController::setup() {
    m_pausableNodes = new Vector<Node*>();


    //m_controller->setPaused(true);

//    vector<string> statements;
//    statements.push_back(_("Now we will undergo training at the combat outpost. We have four catapults for shooting; each one is located opposite to one of the four caves."));
//    DialogManager::getInstance()->setAutoCompleteMode(false);
//    DialogManager::getInstance()->show(m_controller->m_uiNode, 30, Point(200.0, 512.0));
//    DialogManager::getInstance()->createDialog(statements, 3.0, [this](){talkSlide();});

    //highlihgtMagicScale();
    //talkManySymbols();
    //talkWhatAboutMagic();
    m_waitForDrawMagicSymbol = false;
    m_waitForCatapultTouched = false;
    m_waitForClickMagicButton = false;
    m_waitForClickMagicButton2 = false;
    m_waitForFirstMonsterAttack = false;
    m_magicStreakCapturing = false;

    m_waitABitTimer = -1.0f;
    m_slideHintShowed = false;

    /*
    auto bg = Sprite::create("gameScreen/ui/tutorial/talkerBackground.png");
    m_controller->m_uiNode->addChild(bg);
    bg->setPosition(Point(330.0, 400.0));
    bg->setTag(987);
    int fontSize = 13;
    int lineInterval = 15;
    if (Director::getInstance()->getContentScaleFactor() > 1.0) {
        fontSize *= 2;
        lineInterval *= 2;
    }
    auto tf = Label::createWithTTF(" ", "IZH.ttf", fontSize, lineInterval, TextHAlignment::LEFT , GlyphCollection::CUSTOM , firstText.c_str(), false);
    tf->setTag(876);
    //tf->setString(firstText);
    //tf->setAnchorPoint(Point(0.0, 1.0));
    tf->setColor(Color3B::BLACK);
    tf->setPosition(Point(210.0,450.0));
    tf->setAnchorPoint(Point(0.0,1.0));
    m_controller->m_uiNode->addChild(tf);
    m_textField = tf;

    m_currentText = str::to_wcs(firstText);
    m_currentTextLen = m_currentText.length();
    m_currientLetter = 0;
    m_letterTimeout = 0.022;
    m_letterTimer = 100000.0;//m_letterTimeout;
    m_readyToClose = false;


    auto targetPoint = bg->getPosition();
    bg->setPosition(targetPoint - Point(1000.0, 0.0));
    bg->runAction(Sequence::createWithTwoActions(
            EaseSineOut::create(MoveTo::create(1.0, targetPoint)),
            CallFunc::create(CC_CALLBACK_0(TutorialController::animationComplete, this))
    )); */

    int agents =
            (int) TouchReceiverFlag::catapult
                    | (int) TouchReceiverFlag::magicButton
                    | (int) TouchReceiverFlag::magicGesture
                    | (int) TouchReceiverFlag::upgradeButton;
    m_controller->dispatchDenyTouches(agents);
    
    m_catapultSliding = false;
    m_sampleSymbolCapturing = false;
    m_pointerForCatapult = nullptr;
    m_sampleSymbolCaptureTarget = nullptr;



    m_wave = 5;
    m_controller->spawnTutorialEnemies(m_wave);
    //m_waitForFirstMonsterAttack = true;

    talkTryMakeAShoot();
    catapultShot();
}


void TutorialController::monsterAttackedBridge() {
    if (m_waitForFirstMonsterAttack) {
        m_waitForFirstMonsterAttack = false;
        talkTryMakeAShoot();
        catapultShot();
    }

}


/*

void TutorialController::animationComplete () {
    m_letterTimer = m_letterTimeout;
}*/

void TutorialController::pointCaves() {
    //DialogManager::getInstance()->moveTo(Point::ZERO);
    auto sprite = Sprite::create("gameScreen/ui/hand.png");
    sprite->setAnchorPoint(Point::ANCHOR_TOP_LEFT);
    m_controller->m_uiNode->addChild(sprite);
    sprite->setPosition(m_controller->zones[3][0].center);
    float targetY = 856.0;
    Vector<FiniteTimeAction*> actions;
    actions.pushBack(Spawn::createWithTwoActions(FadeIn::create(0.2), MoveTo::create(0.4, Point(m_controller->zones[3][0].center.x, targetY))));
    actions.pushBack(DelayTime::create(0.5));

    for (int i = 1; i < 4; ++i) {
        actions.pushBack(DelayTime::create(0.5));
        actions.pushBack(EaseSineInOut::create(MoveTo::create(0.9, Point(m_controller->zones[3][i].center.x, targetY))));
    }
    actions.pushBack(DelayTime::create(1.3));
    actions.pushBack(Spawn::createWithTwoActions(MoveTo::create(0.4, m_controller->zones[3][3].center), FadeOut::create(0.2)));
    actions.pushBack(CallFunc::create([this, sprite](){sprite->removeFromParentAndCleanup(true);
        talkIslands();}));
    sprite->runAction(Sequence::create(actions));
    m_pausableNodes->pushBack(sprite);
}

void TutorialController::talkIslands() {
    vector<string> statemens;
    statemens.push_back(_("When the enemy comes out of the cave he can move only straight forward across four islands."));
    pointIslands();
    DialogManager::getInstance()->createDialog(statemens, 3.0, [this](){stopPointIslands();talkSlide();});
}

void TutorialController::pointIslands() {
    auto target = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/target.png");
    m_controller->m_uiNode->addChild(target);
    target->setPosition(m_controller->zones[3][0].center);
    Vector<FiniteTimeAction*> actions;
    for (int i = 2; i >= 0; --i) {
        actions.pushBack(EaseSineInOut::create(MoveTo::create(0.9, m_controller->zones[i][0].center)));
    }
    for (int i = 1; i <= 3; ++i) {
        actions.pushBack(EaseSineInOut::create(MoveTo::create(0.9, m_controller->zones[i][0].center)));
    }
    target->runAction(RepeatForever::create(Sequence::create(actions)));
    target->runAction(FadeIn::create(0.2));
    target->setTag(4455);
    m_pausableNodes->pushBack(target);

}


void TutorialController::stopPointIslands() {
    m_controller->m_uiNode->getChildByTag(4455)->stopAllActions();
    m_controller->m_uiNode->getChildByTag(4455)->removeFromParent();

}


void TutorialController::talkSlide() {
    //DialogManager::getInstance()->moveTo(Point(200.0, 0.0));

    vector<string> statemens;
    statemens.push_back(_("Our task is to define the island where the monster is and shoot at him usinga sliding motion in the target zone."));
    DialogManager::getInstance()->createDialog(statemens, 3.5, [this](){
        pointSlide();
        DialogManager::getInstance()->hide();
    });

    m_wave = 4;
    m_controller->spawnTutorialEnemies(m_wave);
    m_controller->getCatapult(0)->forceTargetingVisible(true);
}

void TutorialController::pointSlide() {
    auto sprite = Sprite::create("gameScreen/ui/hand.png");
    sprite->setAnchorPoint(Point::ANCHOR_TOP_LEFT);
    m_controller->m_uiNode->addChild(sprite);
    auto catapult =m_controller->getCatapult(0);
    sprite->setPosition(catapult->m_node->getPosition() + Point(0.0, 100.0));
    Vector<FiniteTimeAction*> actions;
    actions.pushBack(FadeIn::create(0.2));
    actions.pushBack(EaseSineInOut::create(MoveBy::create(2.5, Point(0.0, -150.0))));
    actions.pushBack(DelayTime::create(2.2));
    actions.pushBack(FadeOut::create(0.2));
    actions.pushBack(CallFunc::create([this, sprite](){
        sprite->removeFromParentAndCleanup(true);
        finishSlide();
        talkTryMakeAShoot();}));
    sprite->runAction(Sequence::create(actions));
    m_pausableNodes->pushBack(sprite);

    catapult->m_targetingNode->setVisible(true);
    /*Node *node = catapult->m_targetingNode;
    catapult->m_targetingNode->runAction(Sequence::createWithTwoActions(
                                         DelayTime::create(2.5)         , Hide::create())
                                //CallFunc::create([node](){node->setVisible(true);})
                                                          );
                                                          */
    m_pointerForCatapult = sprite;
    m_catapultSliding = true;

    /*Point v;
    v.set(100.0, 200.0);
    log("v %f, %f", v.x, v.y);
    Point gl = Director::getInstance()->convertToGL(v);
    log("gl %f, %f", gl.x, gl.y);
    Point ui = Director::getInstance()->convertToUI(v);
    log("ui %f, %f", ui.x, ui.y);
    v = Director::getInstance()->convertToGL(Director::getInstance()->convertToUI(v));
    log("v conv %f, %f", v.x, v.y);

    Point vd = gl - v;
    //Vec2 vd = Vec2(100.0, 200.0) - Vec2(100.0, 800.0);
    m_glDiff = gl - v;
    log("diff %f, %f", vd.x, vd.y);
    log("diff %f, %f", m_glDiff.x, m_glDiff.y);
    */
    //m_touch = Touch();
    /*Point nonGl = m_glDiff + (
            m_pointerForCatapult->getParent()->convertToWorldSpace(m_pointerForCatapult->getPosition()));
    log ("local %f, %f, ui %f, %f", m_pointerForCatapult->getPosition().x, m_pointerForCatapult->getPosition().y, nonGl.x, nonGl.y);
    m_touch.setTouchInfo(2223323, nonGl.x, nonGl.y);
    log ("touch %f, %f", m_touch.getLocation().x, m_touch.getLocation().y);
    //catapult->touchBegan(&m_touch, nullptr);*/
    catapult->touchBegan(m_pointerForCatapult->getParent()->convertToWorldSpace(m_pointerForCatapult->getPosition()));

}

void TutorialController::finishSlide() {
    auto catapult = m_controller->getCatapult(0);
    catapult->touchEnded();
    m_catapultSliding = false;
    m_pointerForCatapult = nullptr;
}

void TutorialController::talkTryMakeAShoot() {
    vector<string> statemens;
    statemens.push_back(_("it is training level, \nhere we will learn to \noperate an outpost"));
    statemens.push_back(_("make a shot by \nsliding movement"));
    Vec2 place = Vec2(200,300);
    if (GD->getDeviceFamily() == DeviceFamily::iphone) place.x = 490;
    DialogManager::getInstance()->setAutoCompleteMode(true);
    DialogManager::getInstance()->show(m_controller->m_uiNode , 30, place);
    DialogManager::getInstance()->createDialog(statemens, 4.1f, [this](){});
    m_waitForCatapultTouched = true;


}

void TutorialController::firstCatapultTouched() {
    log("catapult touch !");
    if (m_waitForCatapultTouched) {
        m_waitForCatapultTouched = false;
        DialogManager::getInstance()->skipAll();
        DialogManager::getInstance()->hide();
        m_controller->m_uiNode->getChildByTag(HAND_TAG)->removeFromParent();
        m_controller->getCatapult(0)->touchEnded();
        m_controller->getCatapult(0)->setIdleMode(false);
        m_pointerForCatapult = nullptr;
        m_catapultSliding = false;
    }
}


void TutorialController::catapultShot() {
    if (m_wave == 5) {
        if (m_waitForCatapultTouched && !m_slideHintShowed) {
            m_slideHintShowed = true;
            m_controller->dispatchAllowTouches((int)TouchReceiverFlag::catapult);

            auto hand = getHand();
            m_controller->m_uiNode->addChild(hand);
            auto catapult = m_controller->getCatapult(0);
            hand->setPosition(catapult->m_node->getPosition() + Point(0.0, 160.0));
            Vector<FiniteTimeAction*> actions;
            actions.pushBack(MoveTo::create(1.5f, hand->getPosition() + Vec2(0.0f, -200.0f)));
            actions.pushBack(Spawn::createWithTwoActions(MoveTo::create(0.4f, hand->getPosition()),
                Sequence::createWithTwoActions(EaseSineOut::create(ScaleTo::create(0.2f, 1.5f)), EaseSineIn::create(ScaleTo::create(0.2f, 1.0f)))
            ));
            hand->runAction(RepeatForever::create(Sequence::create(actions)));
            hand->setTag(HAND_TAG);
            m_pausableNodes->pushBack(hand);


            m_pointerForCatapult = hand;
            m_catapultSliding = true;

            m_controller->getCatapult(0)->setIdleMode(true);
            m_waitForCatapultTouched = false;
            m_controller->getCatapult(0)->
                    touchBegan(m_pointerForCatapult->getParent()->convertToWorldSpace(m_pointerForCatapult->getPosition()));
            m_waitForCatapultTouched = true; // OMG! :()
        }

    }
}


void TutorialController::talkTryStatic() {
    m_controller->getCatapult(0)->forceTargetingVisible(false);
    vector<string> statemens;
    statemens.push_back(_("Now try to exterminate several targets"));
    DialogManager::getInstance()->show(m_controller->m_uiNode, 30, Point(200.0, 512.0));
    DialogManager::getInstance()->createDialog(statemens, 3.0, [this](){
        DialogManager::getInstance()->hide();
        placeStaticTarget();});
}


void TutorialController::placeStaticTarget() {
    m_controller->dispatchAllowTouches((int)TouchReceiverFlag::catapult);
    m_wave = 0;
    m_controller->spawnTutorialEnemies(m_wave);
}

void TutorialController::touched(Object *sender, TouchEventType type) {
    if (type == TouchEventType::TOUCH_EVENT_ENDED) {
        auto btn = dynamic_cast<Node*>(sender);

    }
}

void TutorialController::allMonstersKilled() {
    if (m_wave == 0) talkGoodJob();
    //else if (m_wave == 1) talkManyDynamicTargets(); // issue #636
    else if (m_wave == 2) talkGoodCatapult();//waitJustABit(3.0f);
    else if (m_wave == 3) talkWellDone();
    else if (m_wave == 5) placeManyDynamicTargets();//talkGoodJob();
}

void TutorialController::talkGoodCatapult() {
    //m_controller->dispatchDenyTouches((int)TouchReceiverFlag::catapult);

    vector<string> statemens;
    statemens.push_back(_("You perfectly manage the catapults!"));
    DialogManager::getInstance()->setAutoCompleteMode(false);
    DialogManager::getInstance()->show(m_controller->m_uiNode, 30, Point(200.0, 512.0));
    DialogManager::getInstance()->createDialog(statemens, 3.0, [this](){
        DialogManager::getInstance()->hide();
        DialogManager::getInstance()->setAutoCompleteMode(true);
        waitJustABit(5.0f);
        placeRestOfEnemies();

    });

}


void TutorialController::waitJustABit(float time) {
    m_waitABitTimer = time;
}

void TutorialController::waitJustABitComplete() {
    talkActivateMagicMode();
}


void TutorialController::update(float dt) {
    if(m_catapultSliding) {
        /*Point nonGl = (
                m_pointerForCatapult->getParent()->convertToWorldSpace(m_pointerForCatapult->getPosition()));
        m_touch.setTouchInfo(2223323, nonGl.x, nonGl.y);
        log ("local %f, %f, ui %f, %f", m_pointerForCatapult->getPosition().x, m_pointerForCatapult->getPosition().y, nonGl.x, nonGl.y);
        */
        m_controller->getCatapult(0)->
                touchMoved(m_pointerForCatapult->getParent()->convertToWorldSpace(m_pointerForCatapult->getPosition()));
    }

    if (m_waitABitTimer > 0) {
        m_waitABitTimer -= dt;
        if (m_waitABitTimer <= 0) {
            m_waitABitTimer = -1.0f;
            waitJustABitComplete();
        }
    }

    /*
    if (m_sampleSymbolCapturing) {
        Point nonGl = Director::getInstance()->convertToUI(m_sampleSymbolCaptureTarget->getPosition());
        m_touch.setTouchInfo(222333, nonGl.x, nonGl.y);
        m_controller->getMagicController()->touchMoved(&m_touch, nullptr);
    } */

    if (m_magicStreakCapturing) {
        m_controller->getMagicController()->updateStreak(m_magicStreakTarget->getPosition());
    }
}

/*
void TutorialController::update(float dt) {
if (m_letterTimer <= 0.0) {
while (m_letterTimer < 0) {
m_letterTimer += m_letterTimeout;
printLetter();
}
}
else m_letterTimer -= dt;
}
  *//*
void TutorialController::printLetter() {
    if (m_currientLetter < m_currentTextLen) {
        m_textField->setString(
                str::to_utf8(m_currentText.substr(0, m_currientLetter+1)));
        m_currientLetter+=1;
    }
    else
        m_readyToClose = true;
}
            */

void TutorialController::placeOneDynamicTargets() {
    m_controller->dispatchAllowTouches((int)TouchReceiverFlag::catapult);

    m_wave = 1;
    m_controller->spawnTutorialEnemies(m_wave);
}

void TutorialController::takeTouches() {

    //auto dispatcher = Director::getInstance()->getEventDispatcher();
    //dispatcher->addEventListenerWithFixedPriority(m_touchBlocker, -500);
    int agents =
            (int) TouchReceiverFlag::catapult
            | (int) TouchReceiverFlag::magicButton
            | (int) TouchReceiverFlag::upgradeButton;
    m_controller->dispatchDenyTouches(agents);

}

bool TutorialController::touchBegan(cocos2d::Touch* touch, cocos2d::Event* event) {
    return true;
}

void TutorialController::releaseTouches() {
    //auto dispatcher = Director::getInstance()->getEventDispatcher();
    //dispatcher->removeEventListener(m_touchBlocker);
    int agents =
            (int) TouchReceiverFlag::catapult
            | (int) TouchReceiverFlag::magicButton
            | (int) TouchReceiverFlag::upgradeButton;

    m_controller->dispatchAllowTouches(agents);

}

void TutorialController::talkDynamicTarget() {
    m_controller->dispatchDenyTouches((int)TouchReceiverFlag::catapult);

    vector<string> statemens;
    statemens.push_back(_("Good job!"));
    statemens.push_back(_("And now try to hit the moving target"));

    DialogManager::getInstance()->show(m_controller->m_uiNode, 30, Point(200.0, 512.0));
    DialogManager::getInstance()->createDialog(statemens, 3.0, [this](){
        DialogManager::getInstance()->hide();
        placeOneDynamicTargets();});
}

void TutorialController::talkGoodJob() {
    m_controller->dispatchDenyTouches((int)TouchReceiverFlag::catapult);

    vector<string> statemens;
//    statemens.push_back(_("Excellent!"));
//    statemens.push_back(_("What about several targets simultaneously?"));
    //statemens.push_back(_("Good job!"));
    statemens.push_back(_("You are doing great!"));
    //statemens.push_back(_("Let’s crash the remained opponents"));
    //statemens.push_back(_("And now try to hit the moving targets"));
    DialogManager::getInstance()->setAutoCompleteMode(false);
    DialogManager::getInstance()->show(m_controller->m_uiNode, 30, Point(200.0, 512.0));
    DialogManager::getInstance()->createDialog(statemens, 3.0, [this](){
        DialogManager::getInstance()->hide();
        DialogManager::getInstance()->setAutoCompleteMode(true);
        placeManyDynamicTargets();});
}

void TutorialController::placeManyDynamicTargets() {
    m_controller->dispatchAllowTouches((int)TouchReceiverFlag::catapult);
    m_wave = 2;
    m_controller->spawnTutorialEnemies(m_wave);

}

void TutorialController::talkWhatAboutMagic() {
    m_controller->dispatchDenyTouches((int)TouchReceiverFlag::catapult);

    vector<string> statemens;
    statemens.push_back(_("You`ve brilliantly coped with the task and learned to operate catapults. What about magic?"));

    DialogManager::getInstance()->show(m_controller->m_uiNode, 30, Point(200.0, 512.0));
    DialogManager::getInstance()->createDialog(statemens, 3.0, [this](){
        placeRestOfEnemies();
        talkMagicScale();});
}

void TutorialController::placeRestOfEnemies() {
    m_wave = 3;
    m_controller->spawnTutorialEnemies(m_wave);
    //m_controller->startSpawnProgramsBySection("tutorialMushrooms"); // mushrooms
}

void TutorialController::talkMagicScale() {
    vector<string> statemens;
    statemens.push_back(_("To use magic you need to make sure that the magicscaleis full."));

    DialogManager::getInstance()->createDialog(statemens, 3.0, [this](){
        highlihgtMagicScale();});
}


void TutorialController::highlihgtMagicScale() {

    auto waterScale = m_controller->getUIController()->m_magicProgessBars[(int)MagicType::water];
    //  auto dupli

    for (int i = 0; i < 4; ++i) {
        auto scale = m_controller->getUIController()->m_magicProgessBars[i];
        Vector<FiniteTimeAction*> scaleActions;
        scaleActions.pushBack(DelayTime::create(0.7));
        scaleActions.pushBack(Blink::create(2.5, 5));
        scaleActions.pushBack(DelayTime::create(0.7));
        scale->runAction(Sequence::create(scaleActions));
        m_pausableNodes->pushBack(scale);
    }

    auto hand = getHand();
    m_controller->m_uiNode->addChild(hand);
    hand->setScaleX(-1.0);
    hand->setPosition(waterScale->getPosition() + Point(-50.0,-20.0));
    Vector<FiniteTimeAction*> handActions;
    handActions.pushBack(EaseSineOut::create(MoveBy::create(0.5, Point(30.0,30.0))));
    handActions.pushBack(DelayTime::create(3.7));
    handActions.pushBack(EaseSineIn::create(MoveBy::create(0.5, Point(-30.0,-30.0))));
    handActions.pushBack(FadeOut::create(0.2));
    handActions.pushBack(CallFunc::create([hand, this](){
        hand->removeFromParentAndCleanup(true);
        talkActivateMagicMode();
    }));
    hand->runAction(Sequence::create(handActions));
    m_pausableNodes->pushBack(hand);

}



void TutorialController::talkActivateMagicMode() {
    //m_controller->dispatchDenyTouches((int)TouchReceiverFlag::catapult);
    m_controller->setPaused(true);
    auto mask = ui::Layout::create();
    mask->setContentSize(Size(768,1024));
    mask->setBackGroundColorType(ui::Layout::BackGroundColorType::SOLID);
    mask->setBackGroundColor(Color3B::BLACK);
    mask->setBackGroundColorOpacity(155);
    mask->setTag(ISSUE_853_MASK_TAG);
    m_controller->m_gameNode->addChild(mask, 200);
    vector<string> statemens;
    //statemens.push_back(_("You perfectly manage the catapults!"));
    statemens.push_back(_("Hero! Now  we can use magic"));

    DialogManager::getInstance()->show(m_controller->m_uiNode, 30, Point(200.0, 512.0));
    DialogManager::getInstance()->setAutoCompleteMode(false);
    DialogManager::getInstance()->createDialog(statemens, 3.0, [this](){
        highlightMagicButton();
        m_waitForClickMagicButton = true;
        m_controller->dispatchAllowTouches((int)TouchReceiverFlag::magicButton);
        DialogManager::getInstance()->hide();
    });

}


void TutorialController::highlightMagicButton() {
    auto magicButton = m_controller->getUIController()->m_magicActivateButton;

    auto hand = getHand();
    m_controller->m_uiNode->addChild(hand, 101);
    hand->setPosition(magicButton->getPosition() + Point(30.0,-50.0));
    Vector<FiniteTimeAction*> handActions;
    handActions.pushBack(EaseSineInOut::create(MoveBy::create(0.5, Point(-30.0,50.0))));
    handActions.pushBack(DelayTime::create(0.7));
    //handActions.pushBack(ScaleTo::create(0.5, 0.7));
    //handActions.pushBack(CallFunc::create([this, magicButton](){
    //    m_controller->getMagicController()->activate();}));
    //handActions.pushBack(ScaleTo::create(0.5, 1.0));
    handActions.pushBack(DelayTime::create(0.7));
//    handActions.pushBack(EaseSineInOut::create(MoveBy::create(0.5, Point(30.0,-50.0))));
//    handActions.pushBack(FadeOut::create(0.2));
//    handActions.pushBack(CallFunc::create([hand, this](){
//        hand->removeFromParentAndCleanup(true);
//        //talkDrawSymbol();
//    }));
    hand->runAction(Sequence::create(handActions));
    hand->runAction(RepeatForever::create(
            Sequence::createWithTwoActions(TintTo::create(0.4f,150,150,150),TintTo::create(0.3f, 255,255,255))));
    hand->setTag(HAND_TAG);
    m_pausableNodes->pushBack(hand);

}

void TutorialController::magicButtonClicked() {
    if (m_waitForClickMagicButton) {
        m_waitForClickMagicButton = false;
        highlightAllMagicScales(false);
        talkDrawSymbol();
        m_controller->setPaused(false); //release pause, issue #853
        m_controller->m_gameNode->removeChildByTag(ISSUE_853_MASK_TAG);
        //m_controller->dispatchDenyTouches((int)TouchReceiverFlag::magicGesture);
    }
    else if (m_waitForClickMagicButton2) {  // this is old
        m_waitForClickMagicButton2 = false;
    }
    if (m_controller->m_uiNode->getChildByTag(HAND_TAG))
        m_controller->m_uiNode->getChildByTag(HAND_TAG)->removeFromParent();
}

void TutorialController::talkDrawSymbol() {
    vector<string> statemens;
    //statemens.push_back("а теперь чертим магический символ любой стихии");
    statemens.push_back(_("try to draw any of these runes in the center of the screen"));
    DialogManager::getInstance()->show(m_controller->m_uiNode, 500, Vec2(200.0, 10.0));
    DialogManager::getInstance()->setAutoCompleteMode(true);
    DialogManager::getInstance()->createDialog(statemens, 3.0, [this](){
        //DialogManager::getInstance()->hide();
    });
    DrawSampleSymbol();
    m_waitForDrawMagicSymbol = true;
    m_controller->dispatchAllowTouches((int)TouchReceiverFlag::magicGesture);
}

void TutorialController::DrawSampleSymbol() {
    auto sprite = getHand();
    sprite->setAnchorPoint(Point::ANCHOR_TOP_LEFT);
    m_controller->m_uiNode->addChild(sprite);
    sprite->setPosition(Tools::screenCenter()+ Point(0.0, 150.0));
    Vector<FiniteTimeAction*> actions;
    actions.pushBack(FadeIn::create(0.5));
    actions.pushBack(CallFunc::create([this, sprite](){
        m_waitForDrawMagicSymbol = true;
        m_controller->dispatchAllowTouches((int)TouchReceiverFlag::magicGesture);
    }));
    actions.pushBack(DelayTime::create(1.0));
    //actions.pushBack(CallFunc::create([sprite, this](){drawSampleSymbolCaptureStart(sprite);}));
    actions.pushBack(CallFunc::create([this, sprite](){
        m_controller->getMagicController()->startStreak(sprite->getPosition());
    }));
    actions.pushBack(EaseSineInOut::create(MoveBy::create(1.0, Point(-200, -200))));
    actions.pushBack(EaseSineInOut::create(MoveBy::create(1.0, Point(400, 0))));
    actions.pushBack(EaseSineInOut::create(MoveBy::create(1.0, Point(-200, 200))));
    actions.pushBack(DelayTime::create(0.5));
    actions.pushBack(FadeOut::create(0.2));
    actions.pushBack(CallFunc::create([this, sprite](){
        sprite->removeFromParentAndCleanup(true);
        m_controller->getMagicController()->stopStreak();

    }));
    sprite->runAction(Sequence::create(actions));
    sprite->setTag(HAND_TAG);

    m_pausableNodes->pushBack(sprite);

    m_magicStreakCapturing = true;
    m_magicStreakTarget = sprite;
}

void TutorialController::magicUserDrawSymbolStarted() {
    if (m_waitForDrawMagicSymbol && !m_magicSymbolDrawStarted) {
        DialogManager::getInstance()->skip();
        DialogManager::getInstance()->hide();
        if (m_controller->m_uiNode->getChildByTag(HAND_TAG))
            m_controller->m_uiNode->getChildByTag(HAND_TAG)->removeFromParentAndCleanup(true);
        m_magicSymbolDrawStarted = true;
        m_magicStreakCapturing = false;
    }
}


void TutorialController::drawSampleSymbolCaptureStart(Sprite *pSprite) {
    m_sampleSymbolCapturing = true;
    m_touch = Touch();
    m_sampleSymbolCaptureTarget = pSprite;
    Point nonGl = Director::getInstance()->convertToUI(pSprite->getPosition());
    m_touch.setTouchInfo(222333, nonGl.x, nonGl.y);
    m_controller->getMagicController()->touchBegan(&m_touch, nullptr);
}

void TutorialController::drawSampleSymbolCaptureStop() {
    m_sampleSymbolCapturing = false;
    m_controller->getMagicController()->touchEnded(&m_touch, nullptr);
}

void TutorialController::magicSymbolDrawn() {
    if (m_waitForDrawMagicSymbol) {
        if(m_controller->m_uiNode->getChildByTag(HAND_TAG))
            m_controller->m_uiNode->getChildByTag(HAND_TAG)->removeFromParent();
        m_waitForDrawMagicSymbol = false;
        m_controller->dispatchDenyTouches((int)TouchReceiverFlag::magicGesture | (int) TouchReceiverFlag::magicButton);
        //talkCastIslandCount();
        talkCastIslandsDefine();
    }
}

void TutorialController::talkCastIslandCount() {
    vector<string> statemens;
    statemens.push_back(_("At the first level of development each wizard can attack not more than three times."));

    DialogManager::getInstance()->show(m_controller->m_uiNode, 500, Vec2(200.0, 512.0));
    DialogManager::getInstance()->setAutoCompleteMode(false);
    DialogManager::getInstance()->createDialog(statemens, 3.0, [this](){
        PointCastIslandCount();

    });
}

void TutorialController::PointCastIslandCount() {
    auto hand = getHand();
    m_controller->m_uiNode->addChild(hand, 101);
    auto counter = m_controller->getMagicController()->getIslandCounter();
    hand->setPosition(counter->getPosition() + Point(70.0,-50.0));
    Vector<FiniteTimeAction*> handActions;
    handActions.pushBack(EaseSineInOut::create(MoveBy::create(0.5, Point(-30.0,30.0))));
    handActions.pushBack(CallFunc::create([counter](){counter->runAction(Blink::create(2.0, 2));}));
    handActions.pushBack(DelayTime::create(2.0));
    handActions.pushBack(CallFunc::create([hand, this](){
        hand->removeFromParentAndCleanup(true);
        talkCastIslandsDefine();

    }));
    hand->runAction(Sequence::create(handActions));
    m_pausableNodes->pushBack(hand);

}


void TutorialController::talkCastIslandsDefine() {
    vector<string> statemens;
    statemens.push_back(_("Define 3 places where you would like to use magic"));
    DialogManager::getInstance()->show(m_controller->m_uiNode, 500, Vec2(200.0, 512.0));
    DialogManager::getInstance()->setAutoCompleteMode(false);
    DialogManager::getInstance()->createDialog(statemens, 3.0, [this](){
        DialogManager::getInstance()->hide();
        //showMagicIslandsSelect();
        m_controller->dispatchAllowTouches((int)TouchReceiverFlag::magicGesture);
        m_magicFirstTime = true;
    });
}

void TutorialController::showMagicIslandsSelect() {

    auto hand = getHand();
    m_controller->m_uiNode->addChild(hand, 101, HAND_TAG);
    hand->setPosition(m_controller->zones[0][2].center + Point(0.0, -50.0));
    Vector<FiniteTimeAction*> handActions;
    handActions.pushBack(EaseSineInOut::create(MoveBy::create(0.5, Point(-30.0,50.0))));
    for (int i = 0; i < 3; ++i) {
        handActions.pushBack(EaseSineInOut::create(MoveTo::create(0.5, m_controller->zones[i][2].center)));
        handActions.pushBack(ScaleTo::create(0.2, 0.7));
        handActions.pushBack(CallFunc::create([i, this](){
            m_controller->getMagicController()->selectIsland(i, 2);
        }));
        handActions.pushBack(ScaleTo::create(0.2, 1.0));
    }
    handActions.pushBack(CallFunc::create([hand, this](){
        hand->removeFromParentAndCleanup(true);
        talkTryYourMagic();
    }));
    hand->runAction(Sequence::create(handActions));
    m_pausableNodes->pushBack(hand);

}


void TutorialController::talkTryYourMagic() {
    vector<string> statemens;
    statemens.push_back(_("Now destroy the remained enemies!"));

    DialogManager::getInstance()->show(m_controller->m_uiNode, 500, Vec2(10.0f, 512.0f));
    DialogManager::getInstance()->createDialog(statemens, 3.0, [this](){
        m_waitForClickMagicButton2 = true;
        m_controller->dispatchAllowTouches((int)TouchReceiverFlag::magicButton | (int)TouchReceiverFlag::magicGesture);
        highlightMagicButton();
        DialogManager::getInstance()->hide();

    });

}

void TutorialController::magicAppliedAndSomeoneIsAlive() {
    if (m_magicFirstTime) {talkTryYourMagic(); m_magicFirstTime = false;}
    else if (!m_controller->m_uiNode->getChildByTag(HAND_TAG)) highlightMagicButton();

}



void TutorialController::talkManySymbols() {
    m_controller->dispatchDenyTouches((int)TouchReceiverFlag::magicButton);
    m_controller->dispatchDenyTouches((int)TouchReceiverFlag::magicGesture);

    vector<string> statemens;
    statemens.push_back(_("Excellent! Now you`ve mastered the system of drawing runes."));
    statemens.push_back(_("Remember that every element has its own rune and the way of drawing it."));

    DialogManager::getInstance()->show(m_controller->m_uiNode, 500, Vec2(200.0f, 512.0f));
    DialogManager::getInstance()->createDialog(statemens, 4.0, [this](){
        highlightAllMagicScales(true);
    });
}

void TutorialController::highlightAllMagicScales(bool finish) {
    string labels[4] = {"Earth", "Water", "Air", "Fire"};
    for (int i = 0; i < 4; ++i)
    {
        char frame[64];
        sprintf(frame, "gameScreen/ui/hud/rune%s.png", labels[i].c_str());
        char geomKey[64];
        sprintf(geomKey, "Rune%s", labels[i].c_str());
        auto sprite = Sprite::createWithSpriteFrameName(frame);
        if (finish) sprite->setBlendFunc(BlendFunc::ADDITIVE);
        m_controller->m_uiNode->addChild(sprite, 1500);
        GD->ggc()->applyTransformAP(sprite, geomKey);
        Vector<FiniteTimeAction*> actions;
        if (finish)
            actions.pushBack(Repeat::create(Sequence::createWithTwoActions(
                FadeIn::create(0.3),FadeOut::create(0.3)), 3));
        else
            actions.pushBack(Repeat::create(Sequence::createWithTwoActions(
                    ScaleTo::create(0.3, 1.5),ScaleTo::create(0.3, 1.0)), 20));
        //m_controller->getUIController()->m_magicProgessBars[i]->runAction(Blink::create(2.0, 3));
        if (finish && i == 3) {
            actions.pushBack(DelayTime::create(3.0));
            actions.pushBack(CallFunc::create([this](){talkWellDone();}));
        }
        actions.pushBack(CallFunc::create([sprite](){sprite->removeFromParentAndCleanup(true);}));
        sprite->runAction(Sequence::create(actions));
        m_pausableNodes->pushBack(sprite);

    }
}

void TutorialController::talkWellDone() {

    m_controller->m_uiNode->removeChildByTag(HAND_TAG);

    vector<string> statemens;
    statemens.push_back(_(" My congratulations, Hero! You reflected the first wave of aggressors. You are doing great!"));

    DialogManager::getInstance()->show(m_controller->m_uiNode, 500, Vec2(200.0f, 512.0f));
    DialogManager::getInstance()->createDialog(statemens, 3.0, [this](){

        DialogManager::getInstance()->setAutoCompleteMode(true);
        DialogManager::getInstance()->hide();
        if (GD->m_model.temp.currentSlot  == Model::NO_SLOT)
            GD->openScreen(Screens::menu);
        else
            m_controller->successLevel();
    });

}

void TutorialController::skip() {
    GD->openScreen(Screens::level);
}

TutorialController::~TutorialController() {
    delete m_pausableNodes;
}

Sprite *TutorialController::getHand() {
    auto sprite = Sprite::create("gameScreen/ui/hand.png");
    sprite->setAnchorPoint(Point::ANCHOR_TOP_LEFT);
    return sprite;
}


void TutorialController::setPaused(bool value) {
    DialogManager::getInstance()->setPaused(value);
    for (int i = 0; i < m_pausableNodes->size(); ++i) {
        if (value) m_pausableNodes->at(i)->pause(); else m_pausableNodes->at(i)->resume();
    }
}

