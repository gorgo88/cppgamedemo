//
// Created by Admin on 12.02.14.
//


#ifndef __MRWoodenTargetMonster_H_
#define __MRWoodenTargetMonster_H_

#include "MRMonster.h"


namespace myth {
    class MonsterWoodTarget : public Monster {
    public:
        virtual void makeBloodPaddle() override;
        virtual void fillAnimations(ConfigReader *reader) override;

        virtual void update(float dt) override;

        virtual void setup(ConfigReader *reader) override;
    };

}


#endif //__MRWoodenTargetMonster_H_
