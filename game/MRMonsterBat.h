//
// Created by Admin on 10.03.14.
//


#ifndef __MRMonsterBat_H_
#define __MRMonsterBat_H_

#include "MRMonster.h"

namespace myth {
    class GameController;
    class MonsterBat : public Monster {

    public:
        virtual void fillAnimations(ConfigReader *reader) override;

        virtual void update(float dt) override;

        virtual void fillData(ConfigReader *reader) override;

        virtual void makeBloodPaddle() override;

        virtual bool isGoing() override;
};

}

#endif //__MRMonsterBat_H_
