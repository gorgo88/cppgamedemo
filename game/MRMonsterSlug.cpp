//
// Created by Admin on 14.01.14.
//

#include "MRMonsterSlug.h"
#include "MRGameController.h"

using namespace myth;

void MonsterSlug::recieveDamage(float damage) {
    if (m_isJumping) return;
    Monster::recieveDamage(damage);
    if (!m_isDuplicated && !m_isClone && !(
            m_state == MonsterState::walkDown2
            || m_state == MonsterState::walkLet
            || m_state == MonsterState::walkRight
    )) {
        duplicate();
    }
}

void MonsterSlug::duplicate() {
    m_isDuplicated = true;
    m_config.controller->duplicateSlug(this);
}


void MonsterSlug::fillPrice(ConfigReader *reader) {
    Monster::fillPrice(reader);
    if (m_isClone) m_price = reader->fromPath("priceClone")->toInt();
    if (m_isClone) m_priceScore = reader->fromPath("priceScoreClone")->toInt();


}

void MonsterSlug::fillData(ConfigReader *reader) {
    if (m_isClone) {
        //m_attackPower   = reader->fromPath("attackPowerClone")->toFloat() + m_config.data->level;
        m_attackPower.min   = reader->fromPath("attackPowerClone.min")->toFloat();
        m_attackPower.max   = reader->fromPath("attackPowerClone.max")->toFloat();
        //m_lifeMax       = reader->fromPath("lifeMaxClone")->toFloat() * 0.25 * m_config.data->level;
        m_lifeMax       = reader->fromPath("lifeMaxClone")->toFloat();
        // price already setted in controller
    }
    else {
        //m_attackPower   = reader->fromPath("attackPower")->toFloat() + m_config.data->level;
        m_attackPower.min   = reader->fromPath("attackPower.min")->toFloat();
        m_attackPower.max   = reader->fromPath("attackPower.max")->toFloat();
        //m_lifeMax       = reader->fromPath("lifeMax")->toFloat() * 0.25 * m_config.data->level;
        m_lifeMax       = reader->fromPath("lifeMax")->toFloat();
    }
    m_attackInt = 3.0;
    m_attackDelay = 0.5;


}

void MonsterSlug::update(float dt) {
    if (m_isJumping) return;
    Monster::update(dt);
}

void MonsterSlug::jump() {
    Zone nextZone;
    bool zoneFound = false;
    for (int i = 0; i < 4; ++i) {
        if (m_node->getPosition().y > m_config.controller->zones[i][0].center.y + 20.0) {
            nextZone = m_config.controller->zones[i][m_config.column];
            zoneFound = true;
        }
    }
    float targetY = 0;
    if (zoneFound)
        targetY = nextZone.center.y;
    else
        targetY = m_config.tunt1Y;
    m_node->runAction(Sequence::createWithTwoActions(
            MoveTo::create(0.4, Point(m_node->getPosition().x, targetY)),
            CallFunc::create(CC_CALLBACK_0(MonsterSlug::jumpComplete, this))
    ));
    m_sprite->runAction(Sequence::createWithTwoActions(
            ScaleTo::create(0.2, 1.5),
            ScaleTo::create(0.2, 0.7)
    ));
    m_isJumping = true;

}

void MonsterSlug::jumpComplete() {
    m_isJumping = false;
}


