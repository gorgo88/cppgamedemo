//
// Created by Admin on 14.01.14.
//



#ifndef __MRMonsterGinor_H_
#define __MRMonsterGinor_H_

#include "MRMonster.h"
#include "MRMonsterUnmovable.h"


namespace myth {

class Zone;


enum class GinorState {
    entering,
    leaving,
    waitingForAttack,
    attaking,
    hiding,
    ginorDie
};

class MonsterGinor : public Monster, public MonsterUnmovable {
public:
    GinorState m_ginorState;
    float m_enterInterval;
    float m_attackDelay;
    float m_timeout;

    Zone *m_lastZone;

    virtual void fillAnimations(ConfigReader *reader) override;

    virtual void update(float dt) override;

    virtual void fillData(ConfigReader *reader) override;

    void ginorEnter();
    void enterAnimationComplete();
    void ginorAttack();
    void attackAnimationComplete();
    void leaveAnimationComplete();

    virtual void animationComplete() override;

    virtual void setup(ConfigReader *reader) override;

    virtual void dieMonster() override;

    virtual bool isActive() override;

    virtual void recieveDamage(float damage) override;

    virtual std::vector <Zone *> getCurrentZones();

    virtual bool isGoing() override;
};

}
#endif //__MRMonsterGinor_H_
