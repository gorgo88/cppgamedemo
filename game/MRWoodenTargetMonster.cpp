//
// Created by Admin on 12.02.14.
//

#include "MRWoodenTargetMonster.h"
#include "MRGameController.h"

using namespace myth;

const std::string WALK_DOWN = "walkDown";
const std::string DEATH = "death";


void MonsterWoodTarget::makeBloodPaddle() {
    return;
}

void MonsterWoodTarget::fillAnimations(ConfigReader *reader) {
    m_animations = Dictionary::create();
    m_animations->retain();

    vector<string> animationNames;
    animationNames.push_back(DEATH);
    animationNames.push_back(WALK_DOWN);

    string textureId = m_config.data->id;
    if (reader->hasPath("textureId"))
        textureId = reader->fromPath("textureId")->toString()->_string;

    Animation *anim;
    for (int i = 0; i < animationNames.size(); ++i) {
        anim = Animation::create();
        char pattern[255];
        char fpsKey[255];
        sprintf(pattern, "gameScreen/monsters/%s/%s/%s.png", textureId.c_str(), animationNames[i].c_str(), "%04d");
        sprintf(fpsKey, "animation.%s.fps", animationNames[i].c_str());
        Tools::addFramesToAnimation(anim, pattern, reader->fromPath(fpsKey)->toFloat());
        m_animations->setObject(anim, animationNames[i]);
    }


}

void MonsterWoodTarget::setup(ConfigReader *reader) {
    Monster::setup(reader);
    m_sprite->setPosition(m_sprite->getPosition() + Point(0.0, 20.0));
    if (m_config.data->configReader->hasPath("weak")) {
        m_lifeMax = m_life = 10;
        updateLifeBar();
    }
}

void MonsterWoodTarget::update(float dt) {
    float ds = dt*m_speed;
    if (m_slowing) {
        m_speed += m_speed*0.05;
        if (m_speed >= m_initSpeed) {
            m_speed = m_initSpeed;
            m_slowing = false;
        }
    }
    Point pos = m_node->getPosition();
    if (!m_config.controller->canMonsterWalk(this)) {
        if (
                m_state == MonsterState::walkDown
                        ||m_state == MonsterState::walkLet
                        ||m_state == MonsterState::walkRight
                        ||m_state == MonsterState::walkDown2) m_bridge->recieveDamage(100500);
        return;
    }


    if (m_state == MonsterState::walkDown) {
        pos.y -= ds;
        m_node->setPosition(pos);

        if (pos.y <= m_config.controller->zones[0][0].center.y) {
                m_state = MonsterState::attack; // means stop walking
                m_sprite->stopAllActions();
            return;
        }
    }


}
