//
// Created by Admin on 09.12.13.
//


#include "MRMonster.h"
#include "MRGameController.h"
#include "audio/include/SimpleAudioEngine.h"
#include "core/CheetahAtlasHelper.h"
#include "../../cocos2d/external/websockets/include/linux/libwebsockets.h"

using namespace myth;
using namespace CocosDenshion;

const std::string WALK_DOWN = "walkDown";
const std::string WALK_LEFT = "walkLeft";
const std::string WALK_RIGHT = "walkRight";
const std::string ATTACK = "attack";
const std::string DEATH = "death";

float Monster::getLifeScale() {
    return m_life / m_lifeMax;
}

void Monster::setup(ConfigReader *reader) {
    m_blood = nullptr;
    m_speed = m_config.speed;
    m_slowing = false;
    m_initSpeed = 0.0;
    fillData(reader);

    m_life = m_lifeMax;


    m_node = Node::create();
    m_spriteHolder = Node::create();
    m_sprite = Sprite::create();

    m_config.parentNode->addChild(m_node, m_config.zOrder);
    m_node->addChild(m_spriteHolder,1);
    m_spriteHolder->addChild(m_sprite);
    m_node->setPosition(m_config.initPosition);
    m_state = MonsterState::walkDown;

    lifeBar = LifeBar::create();

    Sprite *shadow = Sprite::create("gameScreen/monsterShadow.png");
    m_spriteHolder->addChild(shadow, -1, 555);
    shadow->setPosition(Vec2(0,-18.0f));

    m_node->addChild(lifeBar);
    lifeBar->setPosition(-50.0, -50.0);
    lifeBar->setRotation(-90.0);
    updateLifeBar();

    m_isDamageEmited = false;
    m_isAttackPlaying = false;


    fillAnimations(reader);
    fillSounds(reader);
    fillPrice(reader);

    CheetahAtlasHelper::getInstance()->add("gameScreen/bloodPaddle.atlas");
    if (AnimationCache::getInstance()->getAnimation("bloodPaddle") == NULL) {
        Animation* anim = Animation::create();
        Tools::addFramesToAnimation(anim, "bloodPaddle/%04d.png", 24.0);
        AnimationCache::getInstance()->addAnimation(anim, "bloodPaddle");
    }

    playAnimation(WALK_DOWN, true);

    if (reader->hasPath("nodeScale"))  {
        float scale = reader->fromPath("nodeScale")->toFloat();
        m_node->setScale(scale);
    }
}


void Monster::fillData(ConfigReader *reader) {
    //m_attackPower   = reader->fromPath("attackPower")->toFloat() + m_config.data->level;
    m_attackPower.min   = reader->fromPath("attackPower.min")->toFloat();
    m_attackPower.max   = reader->fromPath("attackPower.max")->toFloat();
    //m_lifeMax       = reader->fromPath("lifeMax")->toFloat() * 0.25 * m_config.data->level;
    m_lifeMax       = m_config.data->lifeMax != -1 ? m_config.data->lifeMax : reader->fromPath("lifeMax")->toFloat();
    if (reader->hasPath("speed")) m_speed = reader->fromPath("speed")->toFloat();
    m_attackInt = 3.0;
    m_attackDelay = 0.5;
}

vector<string> Monster::getAllStandardAnimations() {
    vector<string> animationNames;
    animationNames.push_back(ATTACK);
    animationNames.push_back(DEATH);
    animationNames.push_back(WALK_DOWN);
    animationNames.push_back(WALK_LEFT);
    animationNames.push_back(WALK_RIGHT);
    return animationNames;
}

void Monster::fillAnimations(ConfigReader *reader) {
    m_animations = Dictionary::create();
    m_animations->retain();

    vector<string> animationNames = getAllStandardAnimations();

    Animation *anim;
    for (int i = 0; i < animationNames.size(); ++i) {
        anim = Animation::create();
        char pattern[255];
        char fpsKey[255];
        sprintf(pattern, "gameScreen/monsters/%s/%s/%s.png", m_config.data->id.c_str(), animationNames[i].c_str(), "%04d");
        sprintf(fpsKey, "animation.%s.fps", animationNames[i].c_str());
        Tools::addFramesToAnimation(anim, pattern, reader->fromPath(fpsKey)->toFloat());
        m_animations->setObject(anim, animationNames[i]);
    }

    // override death
    if (shouldUseCommonDeathAnimation()) {
        anim = Animation::create();
        Tools::addFramesToAnimation(anim, "gameScreen/monsterExplode/%04d.png", 24.0);
        m_animations->setObject(anim, DEATH);
    }
}


void Monster::fillSounds(ConfigReader *reader) {
    if (reader->getObjectByPath("sounds.attack")) {
        attackSounds = new MonsterSoundSet();
        Array *variants = reader->fromPath("sounds.attack.variants")->toArray();
        Object *variant;
        CCARRAY_FOREACH(variants, variant) {
                attackSounds->sounds.add(dynamic_cast<String*>(variant)->getCString());
            }
        attackSounds->gain = reader->fromPath("sounds.attack.gain")->toFloat();
    }
    if (reader->getObjectByPath("sounds.death")) {
        deathSounds = new MonsterSoundSet();
        Array *variants = reader->fromPath("sounds.death.variants")->toArray();
        Object *variant;
        CCARRAY_FOREACH(variants, variant) {
                deathSounds->sounds.add(dynamic_cast<String*>(variant)->getCString());
            }
        deathSounds->gain = reader->fromPath("sounds.death.gain")->toFloat();
    }
}


void Monster::fillPrice(ConfigReader *reader) {
    m_price = 0;
    m_priceCrystal = 0;
    m_priceScore = 0;
    if (reader->hasPath("price")) m_price = reader->fromPath("price")->toInt();
    if (m_config.data->price != -1) m_price = m_config.data->price;
    if (reader->hasPath("priceCrystal")) m_priceCrystal = reader->fromPath("priceCrystal")->toInt();
    if (m_config.data->priceCrystal != -1) m_priceCrystal = m_config.data->priceCrystal;
    if (reader->hasPath("priceScore")) m_priceScore = reader->fromPath("priceScore")->toInt();
}

void Monster::update(float dt) {
    float ds = dt*m_speed;
    if (m_slowing) {
        m_speed += m_speed*0.05;
        if (m_speed >= m_initSpeed) {
            m_speed = m_initSpeed;
            m_slowing = false;
        }
    }
    Point pos = m_node->getPosition();
    if (m_state == MonsterState::attack) {
        m_attackTimeout -= dt;
        if (m_attackTimeout <= 0) {
            m_attackTimeout = m_attackInt;
            attackTime();
        }
        if (!m_isDamageEmited && m_attackTimeout <= m_attackInt - m_attackDelay) {
            emitDamage();
            m_isDamageEmited = true;
        }

    }

    if (!m_config.controller->canMonsterWalk(this)) {
        if (
                m_state == MonsterState::walkDown
                ||m_state == MonsterState::walkLet
                ||m_state == MonsterState::walkRight
                ||m_state == MonsterState::walkDown2) setStateAttack();
        return;
    }  else {
        if (m_state == MonsterState::attack && !m_isAttackPlaying) {
            freeBridgeFromMyself();
            m_state = MonsterState::walkDown;
            playAnimation(WALK_DOWN, true);
        }
    }



    if (m_state == MonsterState::walkDown) {
        pos.y -= ds;
        m_node->setPosition(pos);

        if (pos.y <= m_config.tunt1Y) {
            if (m_config.column < 2) {
                m_state = MonsterState::walkRight;
                playAnimation(WALK_RIGHT, true);

            } else {
                m_state = MonsterState::walkLet;
                playAnimation(WALK_LEFT, true);

            }
            return;
        }
    }

    if (m_state == MonsterState::walkLet) {
        pos.x -= ds;
        m_node->setPosition(pos);

        if (pos.x <= m_config.turn2X) {
            m_state = MonsterState::walkDown2;
            playAnimation(WALK_DOWN, true);

            return;
        }
    }

    if (m_state == MonsterState::walkRight) {
        pos.x += ds;
        m_node->setPosition(pos);

        if (pos.x >= m_config.turn2X) {
            m_state = MonsterState::walkDown2;
            playAnimation(WALK_DOWN, true);

            return;
        }
    }

    if (m_state == MonsterState::walkDown2) {
        pos.y -= ds;
        m_node->setPosition(pos);

        if (pos.y <= m_config.endY) {
            m_state = MonsterState::dead;
            hasLeft();
            return;
        }
    }



}

void Monster::playAnimation(std::string key, bool loop) {
    playAnimation(key, loop, m_sprite);
}

void Monster::playAnimation(std::string key, bool loop, Sprite *target) {
    playAnimationCallback(key, loop, target, nullptr);
}

void Monster::playAnimationCallback(std::string key, bool loop, Sprite *target, std::function<void()> callback) {
    Animation *anim = dynamic_cast<Animation*>(m_animations->objectForKey(key));
    auto action = Animate::create(anim);
    target->stopAllActions();
    Sequence *sequence;
    if (callback) {
        sequence = Sequence::createWithTwoActions(
                action,
                CallFunc::create(callback)
        );
    }
    else {
        sequence = Sequence::createWithTwoActions(
                action,
                CallFunc::create(CC_CALLBACK_0(Monster::animationComplete, this))
        );
    }
    target->runAction((loop ? (Action *) RepeatForever::create(sequence) : sequence));
    target->setSpriteFrame(anim->getFrames().at(0)->getSpriteFrame());
}

void Monster::updateLifeBar() {
    lifeBar->setValue(getLifeScale());
}

void Monster::recieveDamage(float damage) {
    recieveDamage(damage, DamageType::bullet);
}


void Monster::recieveDamage(float damage, DamageType type) {
    if (!isActive() || m_state == MonsterState::diying) return;
    m_life -= damage;
    if (m_life <= 0) {dieMonster();return;}
    updateLifeBar();
}

void Monster::setStateAttack() {
    m_state = MonsterState::attack;
    m_attackTimeout = 0.0; //immediate call attackTime ()
}

void Monster::setBridge(Bridge *pBridge) {
    m_bridge = pBridge;
}



void Monster::attackTime() {
    if (!checkIsBridgeFree()) {
        m_sprite->stopAllActions();
        m_sprite->setSpriteFrame(dynamic_cast<Animation *>(m_animations->objectForKey(ATTACK))
                ->getFrames().at(0)->getSpriteFrame());
        return;
    }

    playAnimation(ATTACK, false);
    m_isDamageEmited = false;
    m_isAttackPlaying = true;
}


bool Monster::checkIsBridgeFree() {
    for (int i = 0; i < ALLOWED_MONSTERS_TO_ATTACK; ++i) {
        if (!m_bridge->m_attackingMonsters[i]
            || !m_bridge->m_attackingMonsters[i]->isActive()
            || m_bridge->m_attackingMonsters[i] == this){
            m_bridge->m_attackingMonsters[i] = this;
            return true;
        }
    }
    return false;
}


void Monster::freeBridgeFromMyself() {
    for (int i = 0; i < ALLOWED_MONSTERS_TO_ATTACK; ++i) {
        if (m_bridge->m_attackingMonsters[i] == this){
            m_bridge->m_attackingMonsters[i] = nullptr;
        }
    }
}



void Monster::animationComplete() {
    if (m_state == MonsterState::attack) {
        m_isAttackPlaying = false;
    }
    if (m_state == MonsterState::diying) {
        deathAnimationComplete();

    }
}


void Monster::deathAnimationComplete() {
    m_state = MonsterState::dead;
    m_sprite->runAction(Sequence::createWithTwoActions(
            FadeOut::create(3.0),
            CallFunc::create(CC_CALLBACK_0(Monster::hidedCallback, this))
    ));
    m_config.controller->monsterWasKilled(this);
}

void Monster::hidedCallback() {
    m_node->removeFromParent();
    m_node = nullptr;
}


void Monster::emitDamage() {
    m_config.controller->monsterAttackedBridge(this, m_bridge);

    //sound
    playSoundFromSet(attackSounds);

}


void Monster::playSoundFromSet(MonsterSoundSet *soundSet) {
    if (soundSet) {
        SimpleAudioEngine::getInstance()->playEffect(
                soundSet->sounds.pickNext().c_str(),
                false, 1.0, 0.0, soundSet->gain);
    }
}


void Monster::dieMonster() {
    CCLOG("Die monster");
    m_deathPosition = m_node->getPosition();
    m_state = MonsterState::diying;
    lifeBar->removeFromParent();
    m_spriteHolder->getChildByTag(555)->removeFromParent(); // shadow
    playAnimation(DEATH, false);
    //sound
    playSoundFromSet(deathSounds);
    makeBloodPaddle();
}


void Monster::makeBloodPaddle() {
    if (!GD->m_model.isBloodEnabled) return;
    m_blood = Sprite::create();
    m_blood->setRotation(Tools::randInt(0, 359));
    m_blood->setScale(Tools::randFloat()*0.5+0.5);
    m_blood->setTag(111);
    m_node->addChild(m_blood,-1);
    Vector<FiniteTimeAction*> actions;
    actions.pushBack(Animate::create(AnimationCache::getInstance()->getAnimation("bloodPaddle")));
    actions.pushBack(FadeOut::create(5.0));
    actions.pushBack(CallFunc::create([this](){m_blood->removeFromParentAndCleanup(true);}));
    m_blood->runAction(Sequence::create(actions));
    m_blood->retain();
}


void Monster::hasLeft() {
    m_node->removeFromParent();
    m_node = nullptr;
    m_config.controller->monsterHasLeft(this);
}


bool Monster::isActive() {
    return m_state == MonsterState::attack
            || m_state == MonsterState::diying
            || m_state == MonsterState::walkDown2
            || m_state == MonsterState::walkDown
            || m_state == MonsterState::walkLet
            || m_state == MonsterState::walkRight;
}

void Monster::setUniqueId(std::string id) {
    m_uniqueId = id;
}

float Monster::getAttackPower() {
    return Tools::randFloat() * (m_attackPower.max - m_attackPower.min) + m_attackPower.min;
}


bool Monster::isDiying() {
    return m_state == MonsterState::diying || m_state == MonsterState::dead;
}

Monster::~Monster() {
    CheetahAtlasHelper::getInstance()->remove("gameScreen/bloodPaddle.atlas");
    if (m_blood) {
        m_blood->stopAllActions();
        m_blood->release();
    }


    delete deathSounds;
    delete attackSounds;
    m_animations->release();

}

bool Monster::isGoing() {
    return m_state == MonsterState::walkDown
            || m_state == MonsterState::walkDown2
            || m_state == MonsterState::walkLet
            || m_state == MonsterState::walkRight ;
}

void Monster::slowDown() {
    if (m_slowing) return;
    m_slowing = true;
    m_initSpeed = m_speed;
    m_speed = m_speed * 0.5;
}

Vec2 Monster::getDeathPosition() {
    if (m_deathPosition != Vec2::ZERO)
        return m_deathPosition;
    else if (m_node)
        return m_node->getPosition();
    else
        return Vec2::ZERO;
}

bool Monster::shouldUseCommonDeathAnimation() {
    return true;
}
