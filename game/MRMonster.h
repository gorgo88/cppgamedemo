//
// Created by Admin on 09.12.13.
//



#ifndef __Monster_H_
#define __Monster_H_

#include "core/myth.h"
#include "LifeBar.h"
#include "MRBridge.h"
#include "core/MRStringPicker.h"
#include "MRSpawnProgram.h"

namespace myth {

class GameController;
class MonsterData;

enum class MonsterState {
    walkDown,
    walkDown2,
    walkRight,
    walkLet,
    attack,
    diying,
    dead,
    none // for inherited unusual monsters
};

class MonsterSoundSet {
public:
    StringPicker sounds;
    float gain;
};

class MonsterAttackRange {
public:
    float min;
    float max;
};

class MonsterConfig {
public:
    GameController *controller;
    Point initPosition;
    int column;
    Node *parentNode;
    int zOrder;
    float speed;
    float tunt1Y;
    float turn2X;
    float endY;
    MonsterData *data;
    SpawnProgram *spawnProgram;
};

class Monster : public EasyConfig {
public:
    enum class DamageType {
        bullet,
        magic
    };

    float m_life;
    float m_lifeMax;
    MonsterAttackRange m_attackPower;
    int m_price;
    int m_priceCrystal;
    int m_priceScore;

    MonsterState m_state;
    MonsterConfig m_config;
    LifeBar *lifeBar;

    virtual void recieveDamage(float damage);
    virtual void recieveDamage(float damage, DamageType type);

    float getLifeScale();

    virtual void setup(ConfigReader *reader);

    virtual void fillAnimations(ConfigReader *reader);
    virtual void fillPrice(ConfigReader *reader);

    virtual void update (float dt);

    virtual bool isActive();
    Node *m_node;
    Node *m_spriteHolder;
    Sprite *m_sprite;
    Dictionary *m_animations;
    MonsterSoundSet *attackSounds = nullptr;
    MonsterSoundSet *deathSounds = nullptr;


    void playAnimation(std::string key, bool loop);
    void playAnimationCallback(std::string key, bool loop, Sprite *target, std::function<void()> callback = nullptr);
    void playAnimation(std::string key, bool loop, Sprite *target);

    void updateLifeBar();

    void setStateAttack();

    void setBridge(Bridge * pBridge);

    Bridge *m_bridge;


    void attackTime();
    void emitDamage();

    virtual void animationComplete();
    void hidedCallback();

    virtual void dieMonster();

    virtual ~Monster();

    float m_attackInt;
    float m_attackTimeout;
    float m_attackDelay;
    bool m_isDamageEmited;
    bool m_isAttackPlaying;
    bool m_isDeaf;

    void hasLeft();

    virtual void fillSounds(ConfigReader *reader);

    virtual void fillData(ConfigReader *reader);

    virtual void deathAnimationComplete();

    virtual void makeBloodPaddle();


    float m_speed;
    Sprite *m_blood;

    bool checkIsBridgeFree();

    void freeBridgeFromMyself();

    void setUniqueId(std::string id);

    std::string m_uniqueId = "";

    float getAttackPower();

    bool isDiying();

    void playSoundFromSet(MonsterSoundSet * soundSet);

    // use only for slow down procedure, issue #568
    virtual bool isGoing();

    void slowDown();

    float m_initSpeed;
    bool m_slowing;

    virtual std::vector<std::string> getAllStandardAnimations();

    Vec2 getDeathPosition();

    Vec2 m_deathPosition;

    virtual bool shouldUseCommonDeathAnimation();
};

}

#endif //__Monster_H_
