//
// Created by Admin on 07.01.14.
//

#include "MRModel.h"
#include "cocos2d.h"

using namespace myth;
using namespace cocos2d;

static std::string DUMP_FILENAME = "modelDump";

void Model::dumpToDisk() {
    ValueMap dump;

    dump["version"]             = Value(Value((int)VERSION).asString());
    dump["soundVolume"]         = Value(Value(soundVolume).asString());
    dump["musicVolume"]         = Value(Value(musicVolume).asString());
    dump["isBloodEnabled"]      = Value(Value(isBloodEnabled).asString());
    dump["newGameDialogComplete"]   = Value(Value(newGameDialogComplete).asString());
    dump["isAdDisablePurchased"]    = Value(Value(isAdDisablePurchased).asString());

    for (int i = 0; i < 3; ++i) {
        ValueMap slot;
        slot["currentLevel"]        = Value(Value(slots[i].currentLevel).asString());
        slot["completeLevels"]      = Value(Value(slots[i].completeLevels).asString());
        slot["gold"]                = Value(Value(slots[i].gold).asString());
        slot["crystals"]            = Value(Value(slots[i].crystals).asString());
        slot["score"]               = Value(Value(slots[i].score).asString());
        slot["flags"]               = Value(Value(slots[i].flags).asString());
        slot["difficulty"]          = Value(Value((int)(slots[i].difficulty)).asString());
        slot["dailyAwardLastGrabedDayId"]   = Value(Value(slots[i].dailyAwardLastGrabedDayId).asString());
        slot["dailyAwardLastGrabedAwardId"] = Value(Value(slots[i].dailyAwardLastGrabedAwardId).asString());
        slot["totalGameTimeSeconds"] = Value(Value(slots[i].totalGameTimeSeconds).asString());
        slot["totalSlotTimeSeconds"] = Value(Value(slots[i].totalSlotTimeSeconds).asString());
        slot["totalLoses"]           = Value(Value(slots[i].totalLoses).asString());
        slot["salesShownMask"]       = Value(Value(slots[i].salesShownMask).asString());
        slot["salesUsedMask"]        = Value(Value(slots[i].salesUsedMask).asString());

        ValueVector _bridgeUpgradeIds;
        for (int j = 0; j < 16; ++j) _bridgeUpgradeIds.push_back(Value(Value(slots[i].bridgeUpgradeIds[j]).asString()));
        slot["bridgeUpgradeIds"]    = _bridgeUpgradeIds;

        ValueVector _bulletUpgradeIds;
        for (int j = 0; j < 4; ++j) _bulletUpgradeIds.push_back(Value(Value(slots[i].bulletUpgradeIds[j]).asString()));
        slot["bulletUpgradeIds"]    = _bulletUpgradeIds;

        ValueVector _guillotineUpgradeIds;
        for (int j = 0; j < 4; ++j) _guillotineUpgradeIds.push_back(Value(Value(slots[i].guillotineUpgradeIds[j]).asString()));
        slot["guillotineUpgradeIds"]    = _guillotineUpgradeIds;

        ValueVector _towerUpgradeIds;
        for (int j = 0; j < 4; ++j) _towerUpgradeIds.push_back(Value(Value(slots[i].towerUpgradeIds[j]).asString()));
        slot["towerUpgradeIds"]    = _towerUpgradeIds;

        ValueVector _magicUpgradeIds;
        for (int j = 0; j < 4; ++j) _magicUpgradeIds.push_back(Value(Value(slots[i].magicUpgradeIds[j]).asString()));
        slot["magicUpgradeIds"]    = _magicUpgradeIds;

        ValueVector _mushroomCollected;
        for (int j = 0; j < LEVEL_COUNT; ++j) _mushroomCollected.push_back(Value(Value(slots[i].mushroomCollected[j]).asString()));
        slot["mushroomCollected"]    = _mushroomCollected;

        ValueVector _levelAchievements;
        for (int j = 0; j < LEVEL_COUNT; ++j) _levelAchievements.push_back(Value(Value(slots[i].levelAchievements[j]).asString()));
        slot["levelAchievements"]    = _levelAchievements;

        ValueVector _locationAchievements;
        for (int j = 0; j < 4; ++j) _locationAchievements.push_back(Value(Value(slots[i].locationAchievements[j]).asString()));
        slot["locationAchievements"]    = _locationAchievements;


        ValueVector _shownMonsterCards;
        for (int j = 0; j < slots[i].shownMonsterCards.size(); ++j)
            _shownMonsterCards.push_back(Value(slots[i].shownMonsterCards[j]));
        slot["shownMonsterCards"]    = _shownMonsterCards;


        char slotId[16];
        sprintf(slotId, "slot%d", i);
        dump[slotId] = slot;
    }


    auto filename = FileUtils::getInstance()->getWritablePath().append(DUMP_FILENAME);
    //CCLOG("dump filename %s", filename.c_str());
    FileUtils::getInstance()->writeToFile(dump, filename.c_str());
}

void Model::restoreFromDisk() {
    auto filename = FileUtils::getInstance()->getWritablePath().append(DUMP_FILENAME);
    if (!FileUtils::getInstance()->isFileExist(filename)) return;
    log("model path: %s", FileUtils::getInstance()->fullPathForFilename(filename).c_str());
    ValueMap dump = FileUtils::getInstance()->getValueMapFromFile(filename);

    Version version;
    if (dump.find("version") == dump.end())
        version = Version::v100;
    else
        version = (Version)dump["version"].asInt();
    if ((int) VERSION > (int) version)
        migrateVersions(&dump, version, VERSION);

    soundVolume         = dump["soundVolume"].asFloat();
    musicVolume         = dump["musicVolume"].asFloat();
    isBloodEnabled      = dump["isBloodEnabled"].asBool();
    newGameDialogComplete   = dump["newGameDialogComplete"].asBool();
    isAdDisablePurchased    = dump["isAdDisablePurchased"].asBool();

    for (int i = 0; i < 3; ++i) {
        char slotId[16];
        sprintf(slotId, "slot%d", i);
        ValueMap slot = dump[slotId].asValueMap();
        slots[i].currentLevel        = slot["currentLevel"].asInt();
        slots[i].completeLevels      = slot["completeLevels"].asInt();
        slots[i].gold                = slot["gold"].asInt();
        slots[i].crystals            = slot["crystals"].asInt();
        slots[i].score               = slot["score"].asInt();
        slots[i].flags               = slot["flags"].asInt();
        slots[i].difficulty          = (Difficulty)slot["difficulty"].asInt();
        slots[i].dailyAwardLastGrabedAwardId = slot["dailyAwardLastGrabedAwardId"].asInt();
        slots[i].dailyAwardLastGrabedDayId   = slot["dailyAwardLastGrabedDayId"].asInt();
        slots[i].totalGameTimeSeconds        = slot["totalGameTimeSeconds"].asInt();
        slots[i].totalSlotTimeSeconds        = slot["totalSlotTimeSeconds"].asFloat();
        slots[i].totalLoses          = slot["totalLoses"].asInt();
        slots[i].salesShownMask      = slot["salesShownMask"].asInt();
        slots[i].salesUsedMask       = slot["salesUsedMask"].asInt();

        ValueVector _bridgeUpgradeIds = slot["bridgeUpgradeIds"].asValueVector();
        for (int j = 0; j < 16; ++j) slots[i].bridgeUpgradeIds[j] = _bridgeUpgradeIds[j].asInt();

        ValueVector _bulletUpgradeIds = slot["bulletUpgradeIds"].asValueVector();
        for (int j = 0; j < 4; ++j) slots[i].bulletUpgradeIds[j] = _bulletUpgradeIds[j].asInt();

        ValueVector _guillotineUpgradeIds = slot["guillotineUpgradeIds"].asValueVector();
        for (int j = 0; j < 4; ++j) slots[i].guillotineUpgradeIds[j] = _guillotineUpgradeIds[j].asInt();

        ValueVector _towerUpgradeIds = slot["towerUpgradeIds"].asValueVector();
        for (int j = 0; j < 4; ++j) slots[i].towerUpgradeIds[j] = _towerUpgradeIds[j].asInt();

        ValueVector _magicUpgradeIds = slot["magicUpgradeIds"].asValueVector();
        for (int j = 0; j < 4; ++j) slots[i].magicUpgradeIds[j] = _magicUpgradeIds[j].asInt();

        ValueVector _mushroomCollected = slot["mushroomCollected"].asValueVector();
        for (int j = 0; j < _mushroomCollected.size(); ++j) slots[i].mushroomCollected[j] = _mushroomCollected[j].asInt();

        ValueVector _levelAchievements = slot["levelAchievements"].asValueVector();
        for (int j = 0; j < _levelAchievements.size(); ++j) slots[i].levelAchievements[j] = _levelAchievements[j].asInt();

        ValueVector _locationAchievements = slot["locationAchievements"].asValueVector();
        for (int j = 0; j < _locationAchievements.size(); ++j) slots[i].locationAchievements[j] = _locationAchievements[j].asInt();

        ValueVector _shownMonsterCards = slot["shownMonsterCards"].asValueVector();
        slots[i].shownMonsterCards.clear();
        for (int j = 0; j < _shownMonsterCards.size(); ++j)
            slots[i].shownMonsterCards.push_back(_shownMonsterCards[j].asString());
    }


}

ModelGameProgress *Model::getCurrentSlot() {
    if (temp.currentSlot >= 0) return &slots[temp.currentSlot];
    else return nullptr;
}

ModelGameProgress *Model::getLevelProgress() {
    return &temp.levelProgress;
}

void Model::pushLevelProgress() {
    for (int i = 0; i < 16; ++i)
        getCurrentSlot()->bridgeUpgradeIds[i] = getLevelProgress()->bridgeUpgradeIds[i];
    for (int i = 0; i < 4; ++i)
        getCurrentSlot()->bulletUpgradeIds[i] = getLevelProgress()->bulletUpgradeIds[i];
    for (int i = 0; i < 4; ++i)
        getCurrentSlot()->guillotineUpgradeIds[i] = getLevelProgress()->guillotineUpgradeIds[i];
    for (int i = 0; i < 4; ++i)
        getCurrentSlot()->towerUpgradeIds[i] = getLevelProgress()->towerUpgradeIds[i];
    for (int i = 0; i < 4; ++i)
        getCurrentSlot()->magicUpgradeIds[i] = getLevelProgress()->magicUpgradeIds[i];
    getCurrentSlot()->gold = getLevelProgress()->gold;
    getCurrentSlot()->crystals = getLevelProgress()->crystals;
    getCurrentSlot()->score = getLevelProgress()->score;
}

void Model::clearLevelProgress() {
    for (int i = 0; i < 16; ++i)
        getLevelProgress()->bridgeUpgradeIds[i] = getCurrentSlot()->bridgeUpgradeIds[i];
    for (int i = 0; i < 4; ++i)
        getLevelProgress()->bulletUpgradeIds[i] = getCurrentSlot()->bulletUpgradeIds[i];
    for (int i = 0; i < 4; ++i)
        getLevelProgress()->guillotineUpgradeIds[i] = getCurrentSlot()->guillotineUpgradeIds[i];
    for (int i = 0; i < 4; ++i)
        getLevelProgress()->towerUpgradeIds[i] = getCurrentSlot()->towerUpgradeIds[i];
    for (int i = 0; i < 4; ++i)
        getLevelProgress()->magicUpgradeIds[i] = getCurrentSlot()->magicUpgradeIds[i];
    getLevelProgress()->gold = getCurrentSlot()->gold;
    getLevelProgress()->crystals = getCurrentSlot()->crystals;
    getLevelProgress()->score = getCurrentSlot()->score;

}

void Model::pushCurrentLevelAchievements() {
    int items[5] = {
            (int)LevelAchievement::sniper,
            (int)LevelAchievement::destroyer,
            (int)LevelAchievement::mushroomer,
            (int)LevelAchievement::lastLine,
            (int)LevelAchievement::combo,
    };
    for (int i = 0; i < 5; ++i) {
        if (temp.getLevelAchievement((LevelAchievement)items[i]))
            getCurrentSlot()->setLevelAchievement(getCurrentSlot()->currentLevel, (LevelAchievement)items[i]);

    }
}

void Model::filterLevelAchievements() {
    LevelAchievement items[5] = {
            LevelAchievement::sniper,
            LevelAchievement::destroyer,
            LevelAchievement::mushroomer,
            LevelAchievement::lastLine,
            LevelAchievement::combo,
    };
    for (int i = 0; i < 5; ++i) {
        if (getCurrentSlot()->getLevelAchievement(getCurrentSlot()->currentLevel, items[i])
                && temp.getLevelAchievement(items[i]))
            temp.setLevelAchievement(items[i], false);
    }

}

void Model::migrateVersions(ValueMap *dump, Version const old, Version const current) {
    if (old == Version::v100 && current == Version::v107) {
        for (int i = 0; i < 3; ++i) {
            char slotId[16];
            sprintf(slotId, "slot%d", i);
            ValueMap slot = dump->at(slotId).asValueMap();
            int completeLevels = slot["completeLevels"].asInt();
            int completeLocations = completeLevels / 12;
            int completeLevelsLoc = completeLevels % 12;
            if (completeLevelsLoc > 8) completeLevelsLoc = 8;
            completeLevels = 9 * completeLocations + completeLevelsLoc;
            slot["completeLevels"] = Value(completeLevels);
            slot["dailyAwardLastGrabedAwardId"] = Value(-1);
            slot["dailyAwardLastGrabedDayId"]   = Value(0);
            slot["totalGameTimeSeconds"]        = Value(0);
            slot["totalSlotTimeSeconds"]        = Value(0.0f);
            slot["totalLoses"]                  = Value(0);
            slot["salesUsedMask"]               = Value(0);
            slot["salesShownMask"]              = Value(0);
            dump->at(slotId) = slot;

        }
        dump->insert({"isAdDisablePurchased", Value(false)});
    }
}
