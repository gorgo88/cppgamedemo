//
// Created by Admin on 04.12.13.
//



#ifndef __GameController_H_
#define __GameController_H_

#include "cocos2d.h"
#include "core/myth.h"
#include "MRCatapult.h"
#include "MRMonster.h"
#include "MRBridge.h"
#include "MRMagicController.h"
#include "MRGameUIController.h"
#include "MRMonsterGinor.h"
#include "MRMonsterSlug.h"
#include "MRMagicType.h"
#include "MRTutorialController.h"
#include "MRMonsterBat.h"
#include "MRSpawnProgram.h"
#include "MRMonsterData.h"
#include "MRSpawnColumnSelector.h"


namespace myth {

class Zone {
public:
    int colIndex;
    int rowIndex;
    Point center;
};

    static const int LIFE_COST = 50;


    class GameController : public Object {
    class LevelStatistics {
    public:
        LevelStatistics() : shotsTotal(0), shotsSucceed(0) {}
        int shotsTotal;
        int shotsSucceed;
    };
    LevelStatistics m_levelStatistics;
public:
    void setup(Screen * screen);
    GameController();

    virtual ~GameController();

    Screen *m_screen;
    Node *m_uiNode;
    Node *m_gameNode;
    Node *m_hudNode;

    Zone zones[4][4];
    float m_mana[4] = {100.0}; // indexed by MagicType enum
    int m_life;
    int m_lifeMax;
    void bulletLanded(CatapultBullet *bullet);
    bool canMonsterWalk(Monster *monster);
    bool canBridgeUp(Bridge *bridge);
    void applyMagic(MagicType type, std::vector<std::pair<int,int>> islands);
    void activateMagic();
    void monsterWasKilled(Monster *monster);
    bool m_magicianIsDying;
    vector<Sprite*> m_ginorBullets;
    vector<Sprite*> m_dragonBullets;

    void update(float dt);

    void onClose();


    void monsterHasLeft(Monster * monster);

    void ginorAttack(MonsterGinor * pGinor);
    void dragonAttack(Monster *pDragon, Zone* zone);
    void duplicateSlug(MonsterSlug* slug);

    void setPaused(bool paused);
    void pauseNodeRecursive(Node *node, bool paused);


    void upgradeBridge(int bridgeIndex, int upgradeIndex);
    void upgradeBullet(int towerIndex, int bulletUpgradeIndex);
    void upgradeTower(int towerIndex, int towerUpgradeIndex);
    void upgradeGuillotine(int towerIndex, int guillotineUpgradeIndex);

    bool canBuyUpgrade(UpgradeType type, int index);


    void upgradeMoney(int upgradeIndex, std::function<void()> successCallback, std::function<void()> failureCallback);


    void bossAttack(myth::Monster *boss);

    bool isAnyMonsterOppositeGuillotine(Catapult *catapult);

    void guillotineAttack(Catapult * catapult);


    void applyMagicBossTime(MagicType type);

    bool callMagician(MagicType type);

    void upgradeMagic(int upgradeId, MagicType magicType);

    Zone* getRandFreeZone(std::pair<int,int> rowLimits, std::pair<int,int> colLimits);
    std::vector<float> m_mushroomTimes;

    void batAttack(MonsterBat * bat);
    Catapult *getCatapult(int index); //in a case
    GameUIController *getUIController();
    void spawnTutorialEnemies(int kind);
    bool m_tutorial;
    void dispatchAllowTouches(int flags);

    void dispatchDenyTouches(int flags);
    MagicController *m_magicController;

    MagicController *getMagicController();

    void monsterAttackedBridge(Monster * pMonster, Bridge * pBridge);
    void increaseScore(int score, Point monsterPos);

    std::vector<SpawnProgram*> m_spawnPrograms;
    int m_buyLifeTries;

        void leaveTheScreen();

    private:
    std::vector<Catapult*> m_pCatapults;
    std::vector<Monster*> m_pMonsters;
    std::vector<Bridge*> m_pBridges;

    void addDecorAnimation(std::string animationName, std::string transformLabel);


    void initMagicController();

    bool touchBegan(Touch *touch, Event *event);
    void touchMoved(Touch *touch, Event *event);
    void touchEnded(Touch *touch, Event *event);

    EventListener *m_touchListener;

    SpawnColumnSelector m_columnSelector;

    void allMonstersKilled();


    bool m_isWon;
    float m_leaveTheScreen;


    GameUIController *m_uiController;
    bool m_paused;


    void debetPriceAuto(int priceGold);

    void debitPriceCrystals(int priceCrystals);

    RenderTexture *m_gameNodeRenderTex;
    Sprite *m_magician;

    void magicianAnimationComplete();

    MagicType m_magicianType;
    int m_magicianLife;

    Sprite *m_shields[4];

    TutorialController *m_tutorialController = nullptr;

    void increaseMana(int percent);


    void increaseGold(int price, Point monsterPos);

    void ginorBulletsUpdate(float dt);
    void dragonBulletsUpdate(float dt);

    void sortZOrders();

    void checkForShowMonsterCard(string id);

    void setupBackAnimations();

    void cleanBackAnimations();

    void playFirstLevelDialog();

    void playUpgradeDialog();

    void playWinBossDialog();

    void mushroomWasKilled(Monster * pMonster);

    void increaseCrystal(int crystal, Point monsterPos);

    void calculateLevelAchievements();

    void checkCollectBossCrystals(Monster *monster);

        void setupSpawnPrograms();

        void showLostLifeSale();
public:
    SpawnProgram* getSpawnProgramById(string id);

    void spawnMonster(MonsterData * monsterData, SpawnProgram *program);

    void loadMonsterAtlases();

    void scanForBoss();

    MonsterData *m_bossData;

    int m_comboKills;

    void applyCombo(int kills);

    bool isCurrentLevelCompleteBefore();

    bool isSpawnProgramKilled(SpawnProgram * program);

    void checkForCurrentSpawnSectionKilled();

    void bossTime();

    std::string m_currentSpawnSectionId;

    void startSpawnProgramsBySection(string sectionId);

    void addMushroomsSpawnProgram();

    bool isBossTime();

    void repair();

    void magicianDeath();

    bool m_isAskedForBuyMagicianLife;

    void playUpgradeBridgeDialog();

    void playFirstBossDialog();

    void playGinorDialog();

    void askForBuyLife(function<void()> failureCallback);

    void play2_4BossDialog();

    void playFifthLevelDialog();

    void playTowerUpgradeDialog();
private:
    int m_pauseQueryCount;
public:
    bool m_superBulletFlag;
    double m_gameTime;

        void repair(int lives);

        int m_bossGoldFund;

        void shakeAndLaught();

        bool m_isAskForBuyLifeWasDenied;

        void quitFromTutorial();

        function<void()> m_upgradeMoneySuccessCallback;

        void avoidMonsterStuck();

        void playDevastatorDialog();

        void checkForKillAllBats(Monster *boss);

        bool isEnoughCrystalsForBuyLifes();

        void magicDrawSymbolStarted();

        bool isCurrentSpawnSectionKilled();

        bool isCurrentSpawnSectionKilledOrDying();

        void catapultTouched(int index);

        int m_tutorSpecialColumnCounter;

        Zone *getRandFreeZoneDragon();


        void successLevel();
    };

}

#endif //__GameController_H_
