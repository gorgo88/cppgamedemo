//
// Created by Admin on 10.02.14.
//

#include "core/myth.h"
#include "MRPauseMenuPanel.h"
#include "MRGameUIController.h"

using namespace myth;
using namespace mgui;

static TextureResType plistType = TextureResType::PLIST;

PauseMenuPanel *PauseMenuPanel::create(GameUIController *controller) {
    PauseMenuPanel *widget = new PauseMenuPanel();
    if (widget && widget->init()) {
        widget->m_controller = controller;
        widget->setup();
        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

void PauseMenuPanel::setup() {
    auto bgPanel = Node::create();
    auto bgSprite = Sprite::createWithSpriteFrameName("gameScreen/ui/pauseMenu/background.png");
    GD->ggc()->applyTransformAP(bgPanel, "PauseMenuPanel");
    addChild(bgPanel);
    bgPanel->addChild(bgSprite, -1);

    //resume button
    auto resumeButton = ui::Button::create();
    resumeButton->loadTextureNormal("gameScreen/ui/pauseMenu/resumeButton.png", plistType);
    resumeButton->setTouchEnabled(true);
    resumeButton->setTag(4);
    resumeButton->addTouchEventListener(this, toucheventselector(PauseMenuPanel::buttonTouched));
    bgPanel->addChild(resumeButton);
    GD->ggc()->applyTransform(resumeButton, "PauseMenuPanel.ResumeButton");

    //restart button
    auto restartButton = ui::Button::create();
    restartButton->loadTextureNormal("gameScreen/ui/pauseMenu/restartButton.png", plistType);
    restartButton->setTouchEnabled(true);
    restartButton->setTag(5);
    restartButton->addTouchEventListener(this, toucheventselector(PauseMenuPanel::buttonTouched));
    bgPanel->addChild(restartButton);
    GD->ggc()->applyTransform(restartButton, "PauseMenuPanel.RestartButton");

    //exit button
    auto exitButton = ui::Button::create();
    exitButton->loadTextureNormal("gameScreen/ui/pauseMenu/exitButton.png", plistType);
    exitButton->setTouchEnabled(true);
    exitButton->setTag(6);
    exitButton->addTouchEventListener(this, toucheventselector(PauseMenuPanel::buttonTouched));
    bgPanel->addChild(exitButton);
    GD->ggc()->applyTransform(exitButton, "PauseMenuPanel.ExitButton");

    //help button
//    auto helpButton = ui::Button::create();
//    helpButton->loadTextureNormal("gameScreen/ui/pauseMenu/helpButton.png", plistType);
//    helpButton->setTouchEnabled(true);
//    helpButton->setTag(7);
//    helpButton->addTouchEventListener(this, toucheventselector(PauseMenuPanel::buttonTouched));
//    bgPanel->addChild(helpButton);
//    GD->ggc()->applyTransform(helpButton, "PauseMenuPanel.HelpButton");

    //soundOn button
    auto soundOnButton = ui::Button::create();
    soundOnButton->loadTextureNormal("gameScreen/ui/pauseMenu/soundOnButton.png", plistType);
    soundOnButton->setTouchEnabled(true);
    soundOnButton->setTag(8);
    soundOnButton->addTouchEventListener(this, toucheventselector(PauseMenuPanel::buttonTouched));
    bgPanel->addChild(soundOnButton);
    GD->ggc()->applyTransform(soundOnButton, "PauseMenuPanel.SoundOnButton");

    //soundOff button
    auto soundOffButton = ui::Button::create();
    soundOffButton->loadTextureNormal("gameScreen/ui/pauseMenu/soundOffButton.png", plistType);
    soundOffButton->setTouchEnabled(true);
    soundOffButton->setTag(9);
    soundOffButton->addTouchEventListener(this, toucheventselector(PauseMenuPanel::buttonTouched));
    bgPanel->addChild(soundOffButton);
    GD->ggc()->applyTransform(soundOffButton, "PauseMenuPanel.SoundOffButton");

    //musicOn button
    auto musicOnButton = ui::Button::create();
    musicOnButton->loadTextureNormal("gameScreen/ui/pauseMenu/musicOnButton.png", plistType);
    musicOnButton->setTouchEnabled(true);
    musicOnButton->setTag(10);
    musicOnButton->addTouchEventListener(this, toucheventselector(PauseMenuPanel::buttonTouched));
    bgPanel->addChild(musicOnButton);
    GD->ggc()->applyTransform(musicOnButton, "PauseMenuPanel.MusicOnButton");

    //musicOff button
    auto musicOffButton = ui::Button::create();
    musicOffButton->loadTextureNormal("gameScreen/ui/pauseMenu/musicOffButton.png", plistType);
    musicOffButton->setTouchEnabled(true);
    musicOffButton->setTag(11);
    musicOffButton->addTouchEventListener(this, toucheventselector(PauseMenuPanel::buttonTouched));
    bgPanel->addChild(musicOffButton);
    GD->ggc()->applyTransform(musicOffButton, "PauseMenuPanel.MusicOffButton");
/*
    //bloodSwitch
    auto bloodSwitch = ControlSwitch::create(
            Sprite::create("gameScreen/ui/switchMask.png"),
            Sprite::createWithSpriteFrameName("gameScreen/ui/pauseMenu/switchBar.png"),
            Sprite::create(),
            Sprite::createWithSpriteFrameName("gameScreen/ui/pauseMenu/switchHandler.png"), Label::create(), Label::create()
    );
    bloodSwitch->setTag(1);
//    bloodSwitch->addTargetWithActionForControlEvents(this, [](Ref* sender, Control::EventType type){log("yeahh");}, Control::EventType::VALUE_CHANGED);
    bloodSwitch->addTargetWithActionForControlEvents(this,
            static_cast<cocos2d::extension::Control::Handler>(&PauseMenuPanel::controlValueChanged),
            Control::EventType::VALUE_CHANGED);
    bgPanel->addChild(bloodSwitch);
    //bloodSwitch->setOn(GD->m_model.isBloodEnabled);
    GD->ggc()->applyTransform(bloodSwitch, "PauseMenuPanel.BloodSwitchBar");
*/
    //soundSlider
    auto soundSlider = ui::Slider::create();
    soundSlider->loadBarTexture("gameScreen/ui/pauseMenu/sliderBarEmpty.png", ui::Widget::TextureResType::PLIST);
    soundSlider->loadSlidBallTextures("gameScreen/ui/pauseMenu/sliderHandler.png", "", "", ui::Widget::TextureResType::PLIST);
    soundSlider->loadProgressBarTexture("gameScreen/ui/pauseMenu/sliderBar.png",ui::Widget::TextureResType::PLIST);
    soundSlider->setPercent(int(GD->m_model.soundVolume * 100.0f));
    soundSlider->addEventListener([this, soundSlider](Ref* sender, ui::Slider::EventType type){
        m_controller->setSoundVolume(soundSlider->getPercent() * 0.01f);
    });
    bgPanel->addChild(soundSlider);
    GD->ggc()->applyTransform(soundSlider, "PauseMenuPanel.SoundSliderBar");
    m_soundSlider = soundSlider;

    //musicSlider
    auto musicSlider = Slider::create();
    musicSlider->loadBarTexture("gameScreen/ui/pauseMenu/sliderBarEmpty.png", ui::Widget::TextureResType::PLIST);
    musicSlider->loadSlidBallTextures("gameScreen/ui/pauseMenu/sliderHandler.png", "", "", ui::Widget::TextureResType::PLIST);
    musicSlider->loadProgressBarTexture("gameScreen/ui/pauseMenu/sliderBar.png",ui::Widget::TextureResType::PLIST);
    musicSlider->setPercent(int(GD->m_model.musicVolume * 100.0f));
    musicSlider->addEventListener([this, musicSlider](Ref* sender, ui::Slider::EventType type){
        m_controller->setMusicVolume(musicSlider->getPercent() * 0.01f);
    });
    bgPanel->addChild(musicSlider);
    GD->ggc()->applyTransform(musicSlider, "PauseMenuPanel.MusicSliderBar");
    m_musicSlider = musicSlider;


}

void PauseMenuPanel::buttonTouched(Object *sender, cocos2d::ui::TouchEventType type) {
    Button *button = static_cast<Button*>(sender);
    if (type == cocos2d::ui::TouchEventType::TOUCH_EVENT_ENDED)
    switch (button->getTag()) {
        case 4: {m_controller->closePauseMenu();break;}
        case 5: {m_controller->restartLevel();break;}
        case 6: {m_controller->exitToMainMenu();break;}
        case 7: {m_controller->getHelp();break;}
        case 8: {m_controller->setSoundVolume(1.0);m_soundSlider->setPercent(100);break;}
        case 9: {m_controller->setSoundVolume(0.0);m_soundSlider->setPercent(0);break;}
        case 10: {m_controller->setMusicVolume(1.0);m_musicSlider->setPercent(100);break;}
        case 11: {m_controller->setMusicVolume(0.0);m_musicSlider->setPercent(0);break;}

    }
}
