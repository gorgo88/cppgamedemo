//
// Created by Admin on 04.12.13.
//

#include <iostream>
#include "MRGameController.h"
#include "ui/CocosGUI.h"
#include "audio/include/SimpleAudioEngine.h"
#include "core/CheetahAtlasHelper.h"
#include "MRMonsterRhinoRider.h"
#include "MRMonsterGinor.h"
#include "MRMonsterSlug.h"
#include "ui/CocosGUI.h"
#include "MRGameUIController.h"
#include "MRZoomedCircle.h"
#include "MRMonsterDeath.h"
#include "MRWoodenTargetMonster.h"
#include "MRTutorialController.h"
#include "MRMonsterMushroom.h"
#include "MRMonsterBat.h"
#include "MRMonsterStaticWoodTarget.h"
#include "MRTouchReceiver.h"

#include "TestNode.h"
#include "MRDialogManager.h"
#include "MRSpawnProgram.h"
#include "platform/MRPlatformTools.h"
#include "MRMonsterDevastator.h"
#include "MRMonsterDragon.h"

using namespace cocos2d;
using namespace CocosDenshion;

using namespace myth;

static const int PLAYER_LIFE_MAX_NORMAL = 5;
static const int PLAYER_LIFE_MAX_HARD = 3;
static const int COMBO_MAX = 90;
static const float SNIPER_ACHIEVEMENT_RATIO = 0.8;
static const int LOST_LIFE_SALE_ID = 5;

GameController::GameController () :
        m_pauseQueryCount(0),
        m_bossGoldFund(0),
        m_isAskForBuyLifeWasDenied(false),
        m_tutorSpecialColumnCounter(0){}




void GameController::setup(Screen *screen) {
    CheetahAtlasHelper::getInstance()->add("gameScreen/ui/hud_1.atlas");
    CheetahAtlasHelper::getInstance()->add("gameScreen/ui/hud_2.atlas");
    CheetahAtlasHelper::getInstance()->add("gameScreen/monsterExplode/monsterExplode.atlas");

    GD->m_model.clearLevelProgress();
    GD->m_model.temp.currentLevelAchievements = 0;
    GD->m_model.temp.setLevelAchievement(LevelAchievement::lastLine,true);
    m_screen = screen;
    m_isWon = false;
    m_leaveTheScreen = false;
    m_magicianIsDying = false;
    m_isAskedForBuyMagicianLife = false;
    m_paused = false;
    m_comboKills = 0;
    GD->m_model.temp.levelCombos.clear();
    m_buyLifeTries = 3;

    m_uiNode = Node::create();
    m_gameNode = Node::create();
    m_hudNode = Node::create();
    m_gameNode->retain();

    m_tutorial = false;
    if (GD->m_model.getCurrentSlot()->currentLevel == 0) {
        m_tutorialController = new TutorialController(this);
        m_tutorial = true;


    }

    for (int i = 0; i < 4; ++i) {
        if (GD->m_model.getLevelProgress()->magicUpgradeIds[i] > 0 || m_tutorial)
            m_mana[i] = 100.0;
        else
            m_mana[i] = 0.0;
    }

    m_gameNodeRenderTex = RenderTexture::create(768, 1024, Texture2D::PixelFormat::RGB888);
    m_gameNodeRenderTex->retain();
    m_gameNodeRenderTex->getSprite()->setAnchorPoint(Point(0.,0.)); // 0,0
    m_gameNodeRenderTex->addChild(m_gameNode);
    m_gameNodeRenderTex->setAutoDraw(true);
    m_gameNodeRenderTex->setClearFlags(0);
    m_gameNodeRenderTex->setPosition(-m_screen->rootNode->getPosition()*0.5f);

    m_screen->rootNode->addChild(m_gameNodeRenderTex, 1);
    m_screen->rootNode->addChild(m_hudNode, 2);
    m_screen->rootNode->addChild(m_uiNode, 3);


    m_uiController = new GameUIController();
    m_uiController->m_config.controller = this;
    m_uiController->m_config.parentNode = m_uiNode;
    m_uiController->setup();

    Sprite *background;
    Sprite *caveMasks;
    std::string locId;

    //if (m_tutorial) GD->m_model.temp.currentLocation = Location::snow;

    switch (GD->m_model.temp.currentLocation) {
        default:
        case Location::desert:  locId = "desert"; break;
        case Location::regular: locId = "regular"; break;
        case Location::snow:    locId = "snow"; break;
        case Location::inferno: locId = "inferno"; break;
    }
    Texture2D::setDefaultAlphaPixelFormat(Texture2D::PixelFormat::RGB888);
    background = Sprite::create(StringUtils::format("gameScreen/%s/background.png", locId.c_str()));
    Texture2D::setDefaultAlphaPixelFormat(Texture2D::PixelFormat::RGBA8888);
    caveMasks = Sprite::create(StringUtils::format("gameScreen/%s/caveMasks.png", locId.c_str()));

    background->setAnchorPoint(Point(0.0f, 0.0f));
    m_gameNode->addChild(background, 0);
    GD->ggc()->applyTransformAP(caveMasks, "CavesMask");
    m_gameNode->addChild(caveMasks, 49);


    setupBackAnimations();




    for (int i = 0; i < 4; ++i) {
        String *textureName = String::createWithFormat("gameScreen/regular/caveLight%d.png", i);
        String *geometryLabel = String::createWithFormat("CaveLight%d", i);
        Sprite *lightSprite = Sprite::create(textureName->_string);
        m_gameNode->addChild(lightSprite,1);
        GD->ggc()->applyTransformAP(lightSprite, geometryLabel->_string);
        lightSprite->setOpacity(0.0);
        Sequence *sequence = Sequence::createWithTwoActions(
                FadeTo::create(1.0, 128),
                FadeTo::create(1.0, 0.0)
        );
        auto action = RepeatForever::create(sequence);
        lightSprite->runAction(action);
        lightSprite->setBlendFunc(BlendFunc::ADDITIVE);
    }




    // init zones
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            String *cat = String::createWithFormat("Catapult%dNode",j);
            String *land = String::createWithFormat("LandCenter%d",i);
            Point colPoint = GD->ggc()->getPoint(cat->_string);
            Point rowPoint = GD->ggc()->getPoint(land->_string);
            zones[i][j].colIndex = j;
            zones[i][j].rowIndex = i;
            zones[i][j].center = Point(colPoint.x, rowPoint.y);
        }
    }


    // shields
    CheetahAtlasHelper::getInstance()->add("gameScreen/shields/shields.atlas");
    for (int i = 0; i < 4; ++i) {
        m_shields[i] = Sprite::createWithSpriteFrameName("gameScreen/shields/passive/0000.png");
        m_gameNode->addChild(m_shields[i], 41);
        m_shields[i]->setPosition(Point(zones[0][i].center.x, 250.0));
    }
    Animation *shieldAnim = Animation::create();
    Tools::addFramesToAnimation(shieldAnim, "gameScreen/shields/active/%04d.png", 10.0);
    shieldAnim->setLoops(-1);
    AnimationCache::getInstance()->addAnimation(shieldAnim, "shieldActive");

    // create catapults
    Node *bulletNode = Node::create();
    m_gameNode->addChild(bulletNode, 60);
    CheetahAtlasHelper::getInstance()->add("gameScreen/towers/towers.atlas");
    CheetahAtlasHelper::getInstance()->add("gameScreen/catapult/decor.atlas");
    CheetahAtlasHelper::getInstance()->add("gameScreen/bullets/bullets_1.atlas");
    CheetahAtlasHelper::getInstance()->add("gameScreen/bullets/bullets_2.atlas");

    for (int i = 0; i < 4; ++i) {
        Catapult *catapult = new Catapult();
        catapult->m_config.parentNode = m_gameNode;
        catapult->m_config.touchPriority = 10;
        catapult->m_config.targetingHandleTexture = "gameScreen/catapult/targetingHandle.png";
        catapult->m_config.targetingMaskTexture = "gameScreen/catapult/targetingMask.png";
        catapult->m_config.controller = this;
        catapult->m_config.zoneCol = i;
        catapult->m_config.bulletParentNode = bulletNode;
        catapult->m_config.startTowerUpgrade = GD->m_model.getLevelProgress()->towerUpgradeIds[i];
        catapult->m_config.startBulletUpgrade = GD->m_model.getLevelProgress()->bulletUpgradeIds[i];
        catapult->m_config.startGuillotineUpgrade = GD->m_model.getLevelProgress()->guillotineUpgradeIds[i];
        catapult->m_config.hudNode = m_hudNode;
        catapult->m_config.zoomerTexture = m_gameNodeRenderTex->getSprite()->getTexture();
        catapult->setup();
        m_pCatapults.push_back(catapult);
    }


    // create bridges
    float cols[4] = {
            GD->ggc()->getPoint("Bridge00").x,
            GD->ggc()->getPoint("Bridge01").x,
            GD->ggc()->getPoint("Bridge02").x,
            GD->ggc()->getPoint("Bridge03").x};
    float rows[4] = {
            GD->ggc()->getPoint("Bridge00").y,
            GD->ggc()->getPoint("Bridge10").y,
            GD->ggc()->getPoint("Bridge20").y,
            GD->ggc()->getPoint("Bridge30").y};

    CheetahAtlasHelper::getInstance()->add("gameScreen/bridges/bridges_1.atlas");
    CheetahAtlasHelper::getInstance()->add("gameScreen/bridges/bridges_2.atlas");

    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            Bridge *bridge = new Bridge();
            bridge->m_config.parentNode = m_gameNode;
            bridge->m_config.zOrder = 5; // doesnt matter, sort z order every frame
            bridge->m_config.col = i;
            bridge->m_config.row = j;
            bridge->m_config.controller = this;
            bridge->m_config.startUpgrade = GD->m_model.getLevelProgress()->bridgeUpgradeIds[i*4+j];
            bridge->setup();
            bridge->getUp();
            m_pBridges.push_back(bridge);
        }
    }

    if (m_tutorial) {
        for (int i = 0; i < 4; ++i) {
            int lvl = 4;
            ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(StringUtils::format("towerUpgrades.upgrade%02d", lvl));
            m_pCatapults[i]->applyTowerUpgrade(lvl, upgradeReader);
        }
        for (int i = 0; i < 4; ++i) {
            int lvl = 13;
            ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(StringUtils::format("bulletUpgrades.upgrade%02d", lvl));
            m_pCatapults[i]->applyBulletUpgrade(lvl, upgradeReader);

        }
        for (int i = 0; i < 16; ++i) {
            int lvl = 2;
            ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(StringUtils::format("bridgeUpgrades.upgrade%02d", lvl));
            m_pBridges[i]->applyUpgrade(lvl, upgradeReader);

        }

    }

    setupSpawnPrograms();
    addMushroomsSpawnProgram();
    loadMonsterAtlases();
    scanForBoss();
    if (!m_tutorial)
        startSpawnProgramsBySection("main");

    if (m_bossData) {
        CheetahAtlasHelper::getInstance()->add("gameScreen/bossCrystals/bossCrystals.atlas");

        if (m_bossData->id == "death" || m_bossData->id == "ginorMaster")
            CheetahAtlasHelper::getInstance()->add(StringUtils::format(
                    "gameScreen/bossAttack/%s/%s.atlas", m_bossData->id.c_str(), m_bossData->id.c_str()
            ));
    }

    CheetahAtlasHelper::getInstance()->add("gameScreen/magic/magic.atlas");

    Director::getInstance()->getScheduler()->scheduleUpdate(this, 10, false);

    // explode prepare
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("gameScreen/explodes/explode.plist");
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("gameScreen/decorations/trees.plist");
    
    Animation *anim = Animation::create();
    Tools::addFramesToAnimation(anim, "30%02d.png", 1, 20, 24.0);
    AnimationCache::getInstance()->addAnimation(anim, "explode");

    anim = Animation::create();
    Tools::addFramesToAnimation(anim, "01_%02d.png", 2, 10, 5.0);
    anim->setLoops(-1);
    AnimationCache::getInstance()->addAnimation(anim, "tree0");

    anim = Animation::create();
    Tools::addFramesToAnimation(anim, "02_%02d.png", 1, 10, 5.0);
    anim->setLoops(-1);
    AnimationCache::getInstance()->addAnimation(anim, "tree1");

    anim = Animation::create();
    Tools::addFramesToAnimation(anim, "03_%02d.png", 1, 15, 5.0);
    anim->setLoops(-1);
    AnimationCache::getInstance()->addAnimation(anim, "tree2");

    if (GD->m_model.temp.currentLocation == Location::regular)
    for (int i = 0; i < 10; ++i) {
        String *path = String::createWithFormat("Tree%02d", i);
        std::string animName = i < 4 ? "tree0" : (i < 7 ? "tree1" : "tree2");
        addDecorAnimation(animName, path->_string);
    }

    std::string magicIds[4] = {"air", "earth", "water", "fire"};
    for (int i = 0; i < 4; ++i) {
        anim = Animation::create();
        char pattern[64], cacheId[16];
        sprintf(pattern, "gameScreen/magic/%s/%s.png", magicIds[i].c_str(), "%04d");
        sprintf(cacheId, "%sMagic", magicIds[i].c_str());
        Tools::addFramesToAnimation(anim, pattern, 20.0);
        AnimationCache::getInstance()->addAnimation(anim, cacheId); //TODO: clear on destroy
    }

    initMagicController();
    auto tl = EventListenerTouchOneByOne::create();
    EventDispatcher *dispatcher = Director::getInstance()->getEventDispatcher();
    tl->onTouchBegan = CC_CALLBACK_2(GameController::touchBegan, this);
    tl->onTouchMoved = CC_CALLBACK_2(GameController::touchMoved, this);
    tl->onTouchEnded = CC_CALLBACK_2(GameController::touchEnded, this);
    dispatcher->addEventListenerWithFixedPriority((EventListener *) tl, -10);  //todo: get away to delegates
    m_touchListener = tl;

    SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
    SimpleAudioEngine::getInstance()->playBackgroundMusic("gameScreen/music.mp3", true);


    // magician

    CheetahAtlasHelper::getInstance()->add("gameScreen/magicians/magicians_1.atlas");
    CheetahAtlasHelper::getInstance()->add("gameScreen/magicians/magicians_2.atlas");
    m_magician = Sprite::create();
    Animation *mAnim = Animation::create();
    Tools::addFramesToAnimation(mAnim, "gameScreen/magicians/air/cast/%04d.png", 16.0);
    AnimationCache::getInstance()->addAnimation(mAnim, "airMagician");
    mAnim = Animation::create();
    Tools::addFramesToAnimation(mAnim, "gameScreen/magicians/earth/cast/%04d.png", 16.0);
    AnimationCache::getInstance()->addAnimation(mAnim, "earthMagician");
    mAnim = Animation::create();
    Tools::addFramesToAnimation(mAnim, "gameScreen/magicians/water/cast/%04d.png", 16.0);
    AnimationCache::getInstance()->addAnimation(mAnim, "waterMagician");
    mAnim = Animation::create();
    Tools::addFramesToAnimation(mAnim, "gameScreen/magicians/fire/cast/%04d.png", 16.0);
    AnimationCache::getInstance()->addAnimation(mAnim, "fireMagician");
    m_magician->setVisible(false);
    m_magician->setPosition(Point(384.0, 250.0));
    m_gameNode->addChild(m_magician, 4);

    // lives
    m_lifeMax = GD->m_model.getCurrentSlot()->difficulty == Difficulty::hard
            ? PLAYER_LIFE_MAX_HARD
            : PLAYER_LIFE_MAX_NORMAL;
    m_life = m_lifeMax;
    m_uiController->updateStats();

    if (m_tutorial) {
        m_tutorialController->setup();
    }

    if (!m_tutorial) {
        char levelTabloText[16];
        sprintf(levelTabloText, _("Wave %d").c_str(), GD->m_model.getCurrentSlot()->currentLevel +1);
        TTFConfig conf;
        conf.fontFilePath = "IZH.ttf";
        conf.fontSize = 60;
        conf.glyphs = GlyphCollection::CUSTOM;
        conf.customGlyphs = "0123456789Wave ";
        auto levelTablo = Label::createWithTTF(conf, levelTabloText, TextHAlignment::CENTER , 200);
        levelTablo->setAnchorPoint(Point(0.5,0.5));
        Vector<FiniteTimeAction*> actions;
        actions.pushBack(DelayTime::create(2.0));
        actions.pushBack(ScaleTo::create(0.5, 0.0));
        actions.pushBack(CallFunc::create([levelTablo](){levelTablo->removeAllChildrenWithCleanup(true);}));
        levelTablo->runAction(Sequence::create(actions));
        m_uiNode->addChild(levelTablo, 10);
        levelTablo->setPosition(Tools::screenCenter() + Point(0.0, 200.0));
    }

    if (!m_tutorial
            && GD->m_model.getCurrentSlot()->currentLevel == 1 // issue #863
            && !GD->m_model.getCurrentSlot()->getFlag(FlagIndices::firstLevelDialogPlayed)) {
        GD->m_model.getCurrentSlot()->setFlag(FlagIndices::firstLevelDialogPlayed, true);
        GD->m_model.dumpToDisk();
        playFirstLevelDialog();
    }
    if (!m_tutorial
            && GD->m_model.getCurrentSlot()->currentLevel == 4
            && !GD->m_model.getCurrentSlot()->getFlag(FlagIndices::fifthLevelDialogPlayed)) {
        GD->m_model.getCurrentSlot()->setFlag(FlagIndices::fifthLevelDialogPlayed, true);
        GD->m_model.dumpToDisk();
        playFifthLevelDialog();
    }


    CheetahAtlasHelper::getInstance()->add("gameScreen/ginorBullet/ginorBullet.atlas");

    if (m_bossData && m_bossData->id == "dragon")
        CheetahAtlasHelper::getInstance()->add("gameScreen/dragonBullet/dragonBullet.atlas");



    PlatformTools::sendTapjoyEvent("enterLevel", StringUtils::format("%d", GD->m_model.getCurrentSlot()->currentLevel+1));
    PlatformTools::postGAEvent("game", "enter_level", "",
            GD->m_model.getCurrentSlot()->currentLevel+1);
    PlatformTools::hideGADBanner();
    m_gameTime = 0.0;

//    if (m_tutorial)
//        m_uiController->placeSkipTutorialButton(); // issue #854

    if (!m_tutorial)
        PlatformTools::preloadGADInterstitial(false);

    GD->m_model.getCurrentSlot()->setSaleShowed(LOST_LIFE_SALE_ID, false);
}

void GameController::dispatchAllowTouches(int flags) {
    if (m_uiController) m_uiController->allowTouches(flags);
    if (m_magicController) m_magicController->allowTouches(flags);
    for (int i = 0; i < m_pCatapults.size(); ++i)
        m_pCatapults[i]->allowTouches(flags);

}

void GameController::dispatchDenyTouches(int flags) {
    if (m_uiController) m_uiController->denyTouches(flags);
    if (m_magicController) m_magicController->denyTouches(flags);
    for (int i = 0; i < m_pCatapults.size(); ++i)
        m_pCatapults[i]->denyTouches(flags);

}


void GameController::update (float dt) {
    // something above pause and something before...
    if (m_tutorialController) m_tutorialController->update(dt);

    if (m_paused) return;
    m_gameTime += dt;

    if (m_leaveTheScreen > 0) {
        m_leaveTheScreen -= dt;
        if (m_leaveTheScreen <= 0) {
            m_leaveTheScreen = -1.0;
            calculateLevelAchievements();
            GD->m_model.temp.currentLevelSavedHearts = m_life;
            GD->m_model.temp.currentLevelLostHearts = m_lifeMax - m_life;
            PlatformTools::postGATiming("game", (int)round(m_gameTime * 1000), "level_time",
                    StringUtils::format("level_%d", GD->m_model.getCurrentSlot()->currentLevel + 1));
            GD->m_model.getCurrentSlot()->totalGameTimeSeconds += (int)m_gameTime;
            if (m_isWon) {
                GD->levelComplete();
            }
            else {
                GD->levelFailed();
            }

            return;
        }
    }


    for (auto item = m_pCatapults.begin(); item != m_pCatapults.end(); ++item) {
        (*item)->update(dt);
    }

    for (auto item = m_pMonsters.begin(); item != m_pMonsters.end(); ++item) {
        if ((*item)->isActive()) (*item)->update(dt);
    }

    for (auto item = m_pBridges.begin(); item != m_pBridges.end(); ++item) {
        (*item)->update(dt);
    }


    ginorBulletsUpdate(dt);
    dragonBulletsUpdate(dt);
    sortZOrders();

    for (int i = 0; i < m_spawnPrograms.size(); ++i) {
        m_spawnPrograms[i]->update(dt);
    }

    avoidMonsterStuck();

    GD->check35MinSale([this](){m_uiController->updateStats();});

}



void GameController::bulletLanded(CatapultBullet *bullet) {
    if (m_tutorial) m_tutorialController->catapultShot();
    Sprite *exSprite = Sprite::createWithSpriteFrameName("3001.png");
    exSprite->runAction(Sequence::createWithTwoActions(
            Animate::create(AnimationCache::getInstance()->getAnimation("explode")),
            CallFunc::create([exSprite](){exSprite->removeFromParentAndCleanup(true);}))
    );
    m_gameNode->addChild(exSprite,42);
    exSprite->setPosition(zones[bullet->m_targetZoneRow][bullet->m_targetZoneCol].center);

    std::vector<Zone*> attackZones;
    attackZones.push_back( & zones[bullet->m_targetZoneRow][bullet->m_targetZoneCol]);

//    //superExplodeTest
//
//    if (false && bullet->m_targetZoneCol == 2) {
//        m_superBulletFlag = !m_superBulletFlag;
//        if (m_superBulletFlag) {
//            auto sexSprite = Sprite::create(); // yeah, sex!
//            auto sexAnim = Animation::create();
//            Tools::addFramesToAnimation(sexAnim, "gameScreen/superExplode/00/%04d.png", 24);
//            auto sexAction = Sequence::createWithTwoActions(Repeat::create(Animate::create(sexAnim), 1), CallFunc::create([sexSprite](){
//                sexSprite->removeFromParent();
//            }));
//            sexSprite->runAction(sexAction);
//            sexSprite->setPosition(zones[bullet->m_targetZoneRow][bullet->m_targetZoneCol].center);
//            sexSprite->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
//            m_gameNode->addChild(sexSprite, 41);
//
//
//            // extra zones
//            if (bullet->m_targetZoneCol < 3) {
//                attackZones.push_back( & zones[bullet->m_targetZoneRow + 1][bullet->m_targetZoneCol]);
//                if (bullet->m_targetZoneRow > 0)
//                    attackZones.push_back( & zones[bullet->m_targetZoneRow + 1][bullet->m_targetZoneCol - 1]);
//                if (bullet->m_targetZoneRow < 3)
//                    attackZones.push_back( & zones[bullet->m_targetZoneRow + 1][bullet->m_targetZoneCol + 1]);
//            }
//        }
//    }



    float threshold = 35.0;
    m_levelStatistics.shotsTotal ++;
    bool succesed = false;
    int oldComboKills = m_comboKills;
    for (int z = 0; z < attackZones.size(); ++z) {
        for (int i = 0; i < m_pMonsters.size(); ++i) {
            Monster *monster = m_pMonsters[i];
            if (monster->isActive()) {
                if (auto multiNode = dynamic_cast<MonsterMultiNode*>(monster)) {
                    auto nodes = multiNode->getNodes();
                    for (int n = 0; n < nodes.size(); ++n) {
                        if (nodes[n]->getPosition().getDistance(attackZones[z]->center) < threshold) {
                            multiNode->receiveDamage(bullet->m_attackPower, nodes[n]);
                            if (monster->isDiying()) m_comboKills++;
                            succesed = true;
                            break;
                        }
                    }
                }
                else if (monster->m_node->getPosition().getDistance(attackZones[z]->center) < threshold) {
                    monster->recieveDamage(bullet->m_attackPower);
                    if (monster->isDiying()) m_comboKills++;
                    succesed = true;
                }
            }
        }
    }

    if (succesed) {
        m_levelStatistics.shotsSucceed ++;
        SimpleAudioEngine::getInstance()->playEffect("gameScreen/bulletLanded.wav", false, 1.0, 0.0, 1.0);
    }
    if (!m_tutorial) {
        if (m_comboKills == oldComboKills) {
            if (m_comboKills > 1) {
                applyCombo(m_comboKills);
            }
            m_comboKills = 0;
        }
        else {
            if (m_comboKills > COMBO_MAX) {
                applyCombo(m_comboKills);
                m_comboKills = 0;
            }
        }
        m_uiController->displayComboCounter(m_comboKills);
    }
    //sound
    int sNum = Tools::randInt(1, 4);
    float explodeGain = 1.0;
    __String *s = __String::createWithFormat("gameScreen/explode%02d.wav", sNum);
    SimpleAudioEngine::getInstance()->playEffect(s->getCString(), false, 1.0, 0.0, explodeGain);

}


void GameController::guillotineAttack(Catapult *catapult) {
    Rect guilloRect = catapult->getGuillotineAttackRect();
    Sprite *bloodSprite = nullptr;
    for (int i = 0; i < m_pMonsters.size(); ++i) {
        Monster *monster = m_pMonsters.at(i);
        if (monster->isActive()
            && monster->m_config.data != m_bossData
            && guilloRect.containsPoint(monster->m_node->getPosition())) {
            monster->recieveDamage(catapult->m_guilloPower);
            if (!bloodSprite && monster->m_config.data->id != "devastator") {
                bloodSprite = Sprite::createWithSpriteFrameName("gameScreen/guillotines/blood/0006.png");
                Animation *anim = Animation::create();
                Tools::addFramesToAnimation(anim, "gameScreen/guillotines/blood/%04d.png", 10);
                bloodSprite->runAction(Sequence::createWithTwoActions(Animate::create(anim),
                        CallFunc::create ([bloodSprite](){
                    bloodSprite->removeFromParent();
                })));
                m_gameNode->addChild(bloodSprite, 70);
                bloodSprite->setPosition(monster->m_node->getPosition());

            }
        }
    }
}


bool GameController::isAnyMonsterOppositeGuillotine(Catapult *catapult) {
    Rect guilloRect = catapult->getGuillotineAttackRect();
    for (int i = 0; i < m_pMonsters.size(); ++i) {
        Monster *monster = m_pMonsters.at(i);
        if (monster->isActive()
            && monster->m_config.data != m_bossData
            && guilloRect.containsPoint(monster->m_node->getPosition())) {

            return true;
        }
    }
    return false;
}


void GameController::spawnMonster(MonsterData *monsterData, SpawnProgram *program) {

    ConfigReader *defaultsReader = GD->defaultsConfig()->getSubConfigByPath(
            StringUtils::format("monster.%s", monsterData->id.c_str()));

    int spawnCol = m_columnSelector.getColumn();
    // issue #636
    if (m_tutorial) {
        if (m_currentSpawnSectionId == "wave3" || m_currentSpawnSectionId == "wave4")
            spawnCol = m_tutorSpecialColumnCounter++;
            // issue #846
        if (monsterData->configReader->hasPath("topLeft")
            || monsterData->configReader->hasPath("bottomLeft"))
            spawnCol = 0;

    }

    Monster *monster;
    if (monsterData->id == "rhinoRider")
        monster = new MonsterRhinoRider();
    else if (monsterData->id == "ginor" || monsterData->id == "ginorPurple" || monsterData->id == "ginorRed")
        monster = new MonsterGinor();
    else if (monsterData->id == "slug")
        monster = new MonsterSlug();
    else if (monsterData->id == "woodTarget")
        monster = new MonsterWoodTarget();
    else if (monsterData->id == "staticWoodTarget")
        monster = new MonsterStaticWoodTarget();
    else if (monsterData->id == "mushroom")
        monster = new MonsterMushroom();
    else if (monsterData->id == "bat")
        monster = new MonsterBat();
    else if (monsterData->id == "death" || monsterData->id == "ginorMaster")
        monster = new MonsterDeath();
    else if (monsterData->id == "dragon")
        monster = new MonsterDragon();
    else if (monsterData->id == "devastator")
        monster = new MonsterDevastator();
    else
        monster = new Monster();
    float spawnY = GD->ggc()->getPoint("MonsterSpawnHorizontal").y;
    monster->m_config.controller = this;
    monster->m_config.initPosition = Point(zones[0][spawnCol].center.x, spawnY);
    monster->m_config.zOrder = 42;   // doesnt matter since auto z-order sorting in update
    monster->m_config.column = spawnCol;
    monster->m_config.parentNode = m_gameNode;
    monster->m_config.speed = 65.0f;
    monster->m_config.tunt1Y = 210.0;
    monster->m_config.turn2X = 768.0f / 2.0f;
    monster->m_config.endY = -20.0f;
    monster->m_config.data = monsterData;
    monster->m_config.spawnProgram = program;
    monster->setup(defaultsReader);
    m_pMonsters.push_back(monster);
    monster->setUniqueId(StringUtils::format("%s_%d", monsterData->id.c_str(), m_pMonsters.size()));
    checkForShowMonsterCard(monsterData->id);

    log("spawn monster with id = %s, time = %d", monster->m_uniqueId.c_str(), m_gameTime);
}

void GameController::spawnTutorialEnemies(int kind) {
    startSpawnProgramsBySection(StringUtils::format("wave%d", kind+1)); // why its +1 ??
}


void GameController::monsterWasKilled(Monster *monster) {
    if (monster->m_config.data == m_bossData) {
        m_bossGoldFund += monster->m_price;
        checkCollectBossCrystals(monster);
        checkForKillAllBats(monster);
    }
    log("monster %s was killed", monster->m_uniqueId.c_str());

    increaseMana(1);
    if (monster->m_price > 0) {
        int price = monster->m_price;
        if (monster->m_config.data->id == "mushroom"
                && GD->m_model.getCurrentSlot()->mushroomCollected[GD->m_model.getCurrentSlot()->currentLevel] >= 3) {
            price = 1;
        }
        if (GD->m_model.getCurrentSlot()->currentLevel <= GD->m_model.getCurrentSlot()->completeLevels) {
            price = (int)floorf((float)price*0.15) + 1;
        }
        increaseGold(price, monster->getDeathPosition());
    }
    //if (monster->m_priceCrystal > 0) // if its boss, thats bad idea
    //    increaseCrystal(monster->m_priceCrystal, monster->getDeathPosition());
    if (monster->m_priceScore > 0) {
        int priceScore = monster->m_priceScore;
        if (GD->m_model.getCurrentSlot()->difficulty == Difficulty::hard) priceScore += priceScore * 0.35f; // issue #565
        if (GD->m_model.getCurrentSlot()->currentLevel <= GD->m_model.getCurrentSlot()->completeLevels) {
            priceScore = 1;
        }
        increaseScore(priceScore, monster->getDeathPosition());
    }

    if (monster->m_config.data->id == "mushroom") {
        mushroomWasKilled(monster);
    }

    checkForCurrentSpawnSectionKilled();

}

void GameController::mushroomWasKilled(Monster *pMonster) {
    int curLevel = GD->m_model.getCurrentSlot()->currentLevel;
    GD->m_model.getCurrentSlot()->mushroomCollected[curLevel] ++;
    if (GD->m_model.getCurrentSlot()->mushroomCollected[curLevel] > 3) {
        GD->m_model.getCurrentSlot()->mushroomCollected[curLevel] = 3;
    }
}


void GameController::monsterHasLeft(Monster *monster) {
    CCLOG("monster %s has left", monster->m_uniqueId.c_str());
    if (!IMMORTAL) m_life--;
    if (!GD->m_model.getCurrentSlot()->isSaleShowed(LOST_LIFE_SALE_ID) && m_lifeMax - m_life >= 3) {
        GD->m_model.getCurrentSlot()->setSaleShowed(LOST_LIFE_SALE_ID, true); // clears when the level start
        if (Tools::randFloat() > 0.5f) showLostLifeSale();
    }
    m_uiController->updateStats();
    if (m_life <= 0 && !isBossTime() && !m_isAskForBuyLifeWasDenied) {
        askForBuyLife([this](){
            m_isAskForBuyLifeWasDenied = true;
            leaveTheScreen();
            shakeAndLaught();
        });
        return;
    } else {
        shakeAndLaught();
    }
    if (monster->m_config.data == m_bossData) {
        leaveTheScreen();
        return;
    }
    if (m_life > 0)
        checkForCurrentSpawnSectionKilled();

}


void GameController::shakeAndLaught() {
    if (m_gameNode->getNumberOfRunningActions() > 0) return;
    Vec2 speed = Vec2(0.2, 0.3);
    Vec2 amplitude = Vec2(3.0, 3.3);
    m_gameNode->runAction(
            Repeat::create(
                    Sequence::createWithTwoActions(
                            MoveTo::create(1.0*speed.y, Vec2(0.0, -amplitude.y)),
                            MoveTo::create(1.0*speed.y, Vec2(0.0, amplitude.y))), 3));
    m_gameNode->runAction(
            Repeat::create(
                    Sequence::createWithTwoActions(
                            MoveTo::create(1.0*speed.x, Vec2(-amplitude.x, 0.0)),
                            MoveTo::create(1.0*speed.x, Vec2(amplitude.x, 0.0))), 3));
    SimpleAudioEngine::getInstance()->playEffect("gameScreen/monsterLeftLaugh.wav", false, 1.0, 0.0, 1.0);
}



void GameController::allMonstersKilled() {
    if (m_tutorial)
        m_tutorialController->allMonstersKilled();
    else {
        successLevel();
    }
}

void GameController::successLevel() {
    m_isWon = true;
    if (m_currentSpawnSectionId == "boss") {
        playWinBossDialog();
    }
    else {
        leaveTheScreen();
    }
    if (m_comboKills >= 2) {
        applyCombo(m_comboKills);
        m_comboKills = 0;
    }
}

void GameController::ginorAttack(MonsterGinor *pGinor) {
    auto ginorBullet = Sprite::create();
    auto animationStart = Animation::create();
    Tools::addFramesToAnimation(animationStart, "gameScreen/ginorBullet/start/%04d.png", 16.0);
    auto animationLoop = Animation::create();
    Tools::addFramesToAnimation(animationLoop, "gameScreen/ginorBullet/loop/%04d.png", 16.0);
    animationLoop->setLoops(-1);
    Vector<FiniteTimeAction*> actions;
    actions.pushBack(Animate::create(animationStart));
    actions.pushBack(CallFunc::create([this, ginorBullet](){m_ginorBullets.push_back(ginorBullet);}));

    actions.pushBack(Animate::create(animationLoop));
    ginorBullet->runAction(Sequence::create(actions));
    ginorBullet->setPosition(pGinor->m_node->getPosition() + Point(0.0, -70.0));
    ginorBullet->setTag(pGinor->m_lastZone->colIndex);
    m_gameNode->addChild(ginorBullet, 42);


}

void GameController::dragonAttack(Monster *pDragon, Zone* zone) {
    auto ginorBullet = Sprite::create();
    auto animationStart = Animation::create();
    Tools::addFramesToAnimation(animationStart, "gameScreen/dragonBullet/start/%04d.png", 16.0);
    auto animationLoop = Animation::create();
    Tools::addFramesToAnimation(animationLoop, "gameScreen/dragonBullet/loop/%04d.png", 16.0);
    animationLoop->setLoops(-1);
    Vector<FiniteTimeAction*> actions;
    actions.pushBack(Animate::create(animationStart));
    actions.pushBack(CallFunc::create([this, ginorBullet](){m_dragonBullets.push_back(ginorBullet);}));

    actions.pushBack(Animate::create(animationLoop));
    ginorBullet->runAction(Sequence::create(actions));
    ginorBullet->setPosition(zone->center + Point(0.0, -70));
    ginorBullet->setTag(zone->colIndex);
    m_gameNode->addChild(ginorBullet, 42);

    ginorBullet->setScale(1.6f);
}

void GameController:: bossAttack(Monster *boss) {
    if (m_magicianLife <=0 ) return;
    m_magicianLife--;

    if (boss) {// if not = its the bat
        auto bossEffect = Sprite::create();
        auto magicianEffect = Sprite::create();
        bossEffect->setPosition(boss->m_node->getPosition());
        magicianEffect->setPosition(m_magician->getPosition());
        m_gameNode->addChild(bossEffect, 9);
        m_gameNode->addChild(magicianEffect, 9);
        auto bossAnim = Animation::create();
        auto magicianAnim = Animation::create();
        char bossPattern[128];
        char magicianPattern[128];
        sprintf(bossPattern, "gameScreen/bossAttack/%s/boss/%s.png", m_bossData->id.c_str(), "%04d");
        sprintf(magicianPattern, "gameScreen/bossAttack/%s/magician/%s.png", m_bossData->id.c_str(), "%04d");
        Tools::addFramesToAnimation(bossAnim, bossPattern, 16.0);
        Tools::addFramesToAnimation(magicianAnim, magicianPattern, 16.0);
        bossEffect->runAction(Sequence::createWithTwoActions(
                Animate::create(bossAnim),
                CallFunc::create([bossEffect](){bossEffect->removeFromParentAndCleanup(true);})
        ));
        magicianEffect->runAction(Sequence::createWithTwoActions(
                Animate::create(magicianAnim),
                CallFunc::create([magicianEffect](){magicianEffect->removeFromParentAndCleanup(true);})
        ));
    }

    if (m_magicianLife > 0) {
        char labelText[32];
        sprintf (labelText, "%d", m_magicianLife);
        auto label = dynamic_cast<Label*>(m_hudNode->getChildByTag(321));
        label->setString(labelText);
        label->runAction(Sequence::createWithTwoActions(
                ScaleTo::create(0.3, 1.5),
                ScaleTo::create(0.3, 1.0)
        ));
    }
    else if (!m_magicianIsDying) { //TODO check the logick
        askForBuyLife([this](){
            magicianDeath();
        });
        m_magicianIsDying = true;

    }
}


void GameController::batAttack(MonsterBat *bat) {
    bossAttack(nullptr);
}


void GameController::duplicateSlug(MonsterSlug *slug) {
    ConfigReader *defaultsReader = GD->defaultsConfig()->getSubConfigByPath("monster.slug");
    MonsterSlug *clone = new MonsterSlug();
    clone->m_config = slug->m_config; // clone configs
    clone->m_config.initPosition = slug->m_node->getPosition();
    clone->m_isClone = true;
    //clone->m_price = defaultsReader->fromPath("priceClone")->toInt();

    clone->setup(defaultsReader);
    clone->jump();
    m_pMonsters.push_back(clone);
}


bool GameController::canMonsterWalk(Monster *monster) {
    for (int i = 0; i < m_pBridges.size(); ++i) {
        Bridge *bridge = m_pBridges[i];
        if (bridge->isInAttackZone(monster->m_node->getParent()->convertToWorldSpace(monster->m_node->getPosition()))
                && bridge->m_state == BridgeState::up) {
            monster->setBridge(bridge);
            return false;
        }

    }
    return true;
}

bool GameController::canBridgeUp(Bridge *bridge) {
    float threshold = 20.0f;
    for (int i = 0; i < m_pMonsters.size(); ++i) {
        Monster *monster = m_pMonsters[i];

        if (monster->isActive()
                && bridge->m_node->getPosition().getDistance(monster->m_node->getPosition()) < threshold) {
            return false;
        }
    }

    return true;
}

void GameController::addDecorAnimation(std::string animationName, std::string transformLabel) {
    Sprite *sprite = Sprite::create();
    m_gameNode->addChild(sprite);
    GD->ggc()->applyTransformAP(sprite, transformLabel);
    sprite->runAction(Animate::create(AnimationCache::getInstance()->getAnimation(animationName)));

}

void GameController::initMagicController() {

    m_magicController = new MagicController();
    m_magicController->m_config.parentNode = m_uiNode;
    m_magicController->m_config.touchPriority = -1; // just above magic masks
    m_magicController->m_config.gameController = this;
    m_magicController->setup();

}


bool GameController::touchBegan(cocos2d::Touch* touch, cocos2d::Event* event) {
    return false;
}

void GameController::touchMoved(cocos2d::Touch* touch, cocos2d::Event* event) {

}

void GameController::touchEnded(cocos2d::Touch* touch, cocos2d::Event* event) {

}

void GameController::activateMagic() {
    //dispatchDenyTouches((int)TouchReceiverFlag::all);
    //dispatchAllowTouches((int)TouchReceiverFlag::magicGesture);
    m_magicController->activate();
    if (m_tutorial) m_tutorialController->magicButtonClicked();
}

void GameController::applyMagic(MagicType type, std::vector<std::pair<int,int>> islands) {
    //dispatchAllowTouches((int)TouchReceiverFlag::all);

    if (type == MagicType::fail) return;

    int oldComboKills = m_comboKills;

    SimpleAudioEngine::getInstance()->playEffect(
            StringUtils::format("gameScreen/magicAttack/%s.wav", Tools::getMagicId(type).c_str()).c_str(),
            false, 1,0,1);

    for (int i = 0; i < m_pMonsters.size(); ++i) {
        Monster *monster = m_pMonsters[i];
        if (!monster->isActive()) continue;

        Point monsterPos = monster->m_node->getPosition();
        for (int j = 0; j < islands.size(); ++j) {
            Zone zone = zones[islands.at(j).first][islands.at(j).second];
            if (zone.center.getDistance(monsterPos) < 50.0)  {
                monster->recieveDamage(20000.0, Monster::DamageType::magic);
                if (monster->isDiying()) m_comboKills++;

                //magic effect
                Sprite *sprite = Sprite::create();
                m_gameNode->addChild(sprite, 70);
                sprite->setAnchorPoint(Point(0.5,0.5));
                sprite->setPosition(Point(monsterPos.x, monsterPos.y + (type == MagicType::air ? 150.0 : 0.0)));

                char cacheId[32];
                sprintf(cacheId, "%sMagic", Tools::getMagicId(type).c_str());
                sprite->runAction(Sequence::createWithTwoActions(
                        Animate::create(AnimationCache::getInstance()->getAnimation(cacheId)),
                        CallFunc::create([sprite] () {sprite->removeFromParentAndCleanup(true);} )
                ));
                // hardcode:
                if (type == MagicType::earth) sprite->setAnchorPoint(Point(0.5, 0.3));
                break;
            }
        }
    }

    if (m_tutorial && !isCurrentSpawnSectionKilledOrDying()) {
        m_tutorialController->magicAppliedAndSomeoneIsAlive();
    }

    if (!m_tutorial) {
        if (m_comboKills == oldComboKills) {
            if (m_comboKills > 1) {
                applyCombo(m_comboKills);
            }
            m_comboKills = 0;
        }
        else {
            if (m_comboKills > COMBO_MAX) {
                applyCombo(m_comboKills);
                m_comboKills = 0;
            }
        }
        m_uiController->displayComboCounter(m_comboKills);
    }

    if (!m_tutorial) m_mana[static_cast<int>(type)] = 0.0;
    m_uiController->updateStats();
}


bool GameController::callMagician(MagicType type) {
    if (!m_tutorial && (m_mana[(int)type] < 100.0
            || GD->m_model.getLevelProgress()->magicUpgradeIds[(int)type] == 0)) {
        //dispatchAllowTouches((int)TouchReceiverFlag::all);
        return false;
    }
    char animName[32];
    sprintf(animName, "%sMagician", Tools::getMagicId(type).c_str());
    Animation *anim = AnimationCache::getInstance()->getAnimation(animName);
    m_magician->setVisible(true);
    m_magician->stopAllActions();
    m_magician->runAction(Sequence::createWithTwoActions(
            Animate::create(anim),
            CallFunc::create(CC_CALLBACK_0(GameController::magicianAnimationComplete, this))
    ));
    m_magician->setSpriteFrame(anim->getFrames().at(0)->getSpriteFrame());
    char sound[128];
    sprintf(sound, "gameScreen/magicians/%s.wav", Tools::getMagicId(type).c_str());
    SimpleAudioEngine::getInstance()->playEffect(sound, false, 1.0, 0.0, 1.0);
    if (m_tutorial) m_tutorialController->magicSymbolDrawn();
    return true;
}


void GameController::applyMagicBossTime(MagicType type) {
    if (m_magicianType == type) {
        m_magicianLife++;
        char labelText[32];
        sprintf(labelText, "%d", m_magicianLife);
        dynamic_cast<Label*>(m_hudNode->getChildByTag(321))->setString(labelText);
    }
}

void GameController::magicianAnimationComplete () {
    m_magician->setVisible(false);
}

void GameController::setPaused(bool paused) {
    int queryCountOld = m_pauseQueryCount;
    m_pauseQueryCount += paused ? 1 : -1;
    log(" pause , old = %d, qq = %d!", queryCountOld, m_pauseQueryCount );

    if ((queryCountOld <=0 && m_pauseQueryCount > 0)
            || (queryCountOld > 0 && m_pauseQueryCount <= 0)) {
        log(" realy unpaused %i!", paused);

        m_paused = paused;
        pauseNodeRecursive(m_gameNode, paused);
        if (m_tutorial) m_tutorialController->setPaused(paused);
        //m_gameNodeRenderTex->setAutoDraw(paused);
    }
}

void GameController::pauseNodeRecursive(Node *node, bool paused) {
    if (paused) node->pause();
    else node->resume();
    for (int i = 0; i < node->getChildrenCount(); ++i) {
        Node *child = node->getChildren().at(i);
        pauseNodeRecursive(child, paused);
    }
}



void GameController::upgradeBridge(int bridgeIndex, int upgradeIndex) {
    if (GD->m_model.getLevelProgress()->bridgeUpgradeIds[bridgeIndex] != upgradeIndex) {
        char upgradeKey[100];
        sprintf(upgradeKey, "bridgeUpgrades.upgrade%02d", upgradeIndex);
        ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(upgradeKey);
        int priceGold = upgradeReader->fromPath("priceGold")->toInt();
        debetPriceAuto(priceGold);
        m_pBridges[bridgeIndex]->applyUpgrade(upgradeIndex, upgradeReader);
        GD->m_model.getLevelProgress()->bridgeUpgradeIds[bridgeIndex] = upgradeIndex;
    }
    SimpleAudioEngine::getInstance()->playEffect("gameScreen/buy.wav", false, 1.0, 0.0, 1.0);

    int line = (bridgeIndex % 4) + 1;
    PlatformTools::postGAEvent("upgrades", "upgraded_bridge", StringUtils::format("line_%d", line),
            upgradeIndex);
}

void GameController::debetPriceAuto(int priceGold) {
    int creditGold = GD->m_model.getLevelProgress()->gold - priceGold;
    int debetCrystals = 0;
    if (creditGold < 0) {
        debetCrystals = static_cast<int>(floorf( (-((float) creditGold) * 0.25 ) + 1.0));
    }
    GD->m_model.getLevelProgress()->gold = creditGold + debetCrystals * 4;
    GD->m_model.getLevelProgress()->crystals -= debetCrystals;
    m_uiController->updateStats();
}


void GameController::debitPriceCrystals(int priceCrystals) {
    GD->m_model.getLevelProgress()->crystals -= priceCrystals;
    m_uiController->updateStats();

}

void GameController::upgradeMoney(
        int upgradeIndex,
        std::function<void()> successCallback,
        std::function<void()> failureCallback) {
    GD->requestMoneyUpgrade(upgradeIndex, [this, successCallback](){
        SimpleAudioEngine::getInstance()->playEffect("gameScreen/buy.wav", false, 1.0, 0.0, 1.0);
        m_uiController->updateStats();
        if (successCallback) successCallback();
    }, failureCallback);
}

void GameController::upgradeBullet(int towerIndex, int bulletUpgradeIndex) {
    if (GD->m_model.getLevelProgress()->bulletUpgradeIds[towerIndex] != bulletUpgradeIndex) {
        char upgradeKey[100];
        sprintf(upgradeKey, "bulletUpgrades.upgrade%02d", bulletUpgradeIndex);
        ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(upgradeKey);
        if (bulletUpgradeIndex < 13) {
            int priceGold = upgradeReader->fromPath("priceGold")->toInt();
            debetPriceAuto(priceGold);
        }
        else {
            int priceCrystals = upgradeReader->fromPath("priceCrystal")->toInt();
            debitPriceCrystals(priceCrystals);
        }
        m_pCatapults[towerIndex]->applyBulletUpgrade(bulletUpgradeIndex, upgradeReader);
        GD->m_model.getLevelProgress()->bulletUpgradeIds[towerIndex] = bulletUpgradeIndex;
    }
    SimpleAudioEngine::getInstance()->playEffect("gameScreen/buy.wav", false, 1.0, 0.0, 1.0);

    PlatformTools::postGAEvent("upgrades", "upgraded_bullet", StringUtils::format("tower_%d", towerIndex + 1),
            bulletUpgradeIndex);
}

void GameController::upgradeTower(int towerIndex, int towerUpgradeIndex) {
    if (GD->m_model.getLevelProgress()->towerUpgradeIds[towerIndex] != towerUpgradeIndex) {
        char upgradeKey[100];
        sprintf(upgradeKey, "towerUpgrades.upgrade%02d", towerUpgradeIndex);
        ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(upgradeKey);
        int priceGold = upgradeReader->fromPath("priceGold")->toInt();
        debetPriceAuto(priceGold);
        m_pCatapults[towerIndex]->applyTowerUpgrade(towerUpgradeIndex, upgradeReader);

        GD->m_model.getLevelProgress()->towerUpgradeIds[towerIndex] = towerUpgradeIndex;
        if (!GD->m_model.getCurrentSlot()->getFlag(FlagIndices::towerUpgraded)) {
            GD->m_model.getCurrentSlot()->setFlag(FlagIndices::towerUpgraded, true);
            GD->m_model.dumpToDisk();
            playTowerUpgradeDialog();
        }

    }
    SimpleAudioEngine::getInstance()->playEffect("gameScreen/buy.wav", false, 1.0, 0.0, 1.0);
    PlatformTools::postGAEvent("upgrades", "upgraded_tower", StringUtils::format("tower_%d", towerIndex + 1),
            towerUpgradeIndex);

}

void GameController::upgradeGuillotine(int towerIndex, int guillotineUpgradeIndex) {
    if (GD->m_model.getLevelProgress()->guillotineUpgradeIds[towerIndex] != guillotineUpgradeIndex) {
        char upgradeKey[100];
        sprintf(upgradeKey, "guillotineUpgrades.upgrade%02d", guillotineUpgradeIndex);
        ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(upgradeKey);
        int priceGold = upgradeReader->fromPath("priceGold")->toInt();
        debetPriceAuto(priceGold);
        m_pCatapults[towerIndex]->applyGuillotineUpgrade(guillotineUpgradeIndex, upgradeReader);
        GD->m_model.getLevelProgress()->guillotineUpgradeIds[towerIndex] = guillotineUpgradeIndex;
    }
    SimpleAudioEngine::getInstance()->playEffect("gameScreen/buy.wav", false, 1.0, 0.0, 1.0);
    PlatformTools::postGAEvent("upgrades", "upgraded_guillotine", StringUtils::format("tower_%d", towerIndex + 1),
            guillotineUpgradeIndex);

}


void GameController::increaseMana(int percent) {
    for (int i = 0; i < 4; ++i) {
        float q = 0.5f; //issue#609
        float value = q * (float)percent * (100.0 / (float)(100 - 10 * GD->m_model.getLevelProgress()->magicUpgradeIds[i]));
        m_mana[i] += value;
        if (m_mana[i] > 100) m_mana[i] = 100;
    }
    m_uiController->updateStats();
}

void GameController::upgradeMagic(int upgradeId, MagicType magicType) {
    if (GD->m_model.getLevelProgress()->magicUpgradeIds[static_cast<int>(magicType)] != upgradeId) {
        char upgradeKey[100];
        sprintf(upgradeKey, "magicUpgrades.upgrade%02d", upgradeId);
        ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(upgradeKey);
        int priceGold = upgradeReader->fromPath("priceGold")->toInt();
        debetPriceAuto(priceGold);
        GD->m_model.getLevelProgress()->magicUpgradeIds[static_cast<int>(magicType)] = upgradeId;
        m_mana[(int)magicType] = 100.0f;
        m_uiController->updateStats();
    }
    SimpleAudioEngine::getInstance()->playEffect("gameScreen/buy.wav", false, 1.0, 0.0, 1.0);
    PlatformTools::postGAEvent("upgrades", "upgraded_magic", Tools::getMagicId(magicType),
            upgradeId);

}

bool GameController::canBuyUpgrade(UpgradeType type, int index) {
    char upgradeKey[100];
    sprintf(upgradeKey, "%sUpgrades.upgrade%02d", Tools::getUpgradeLabel(type).c_str(), index);
    ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(upgradeKey);

    if (type == UpgradeType::bullet && index >= 13) {
        int priceCrystals = upgradeReader->fromPath("priceCrystal")->toInt();
        return priceCrystals <= GD->m_model.getLevelProgress()->crystals;
    }

    int priceGold = upgradeReader->fromPath("priceGold")->toInt();
    int creditGold = GD->m_model.getLevelProgress()->gold - priceGold;
    int creditCrystals = 0;
    if (creditGold < 0) {
        creditCrystals = (int)(floorf( (-((float) creditGold) * 0.25 ) + 1.0));
    }
    if (GD->m_model.getLevelProgress()->crystals - creditCrystals < 0) return false;
    return true;
}



void GameController::onClose() {
    EventDispatcher *dispatcher = Director::getInstance()->getEventDispatcher();
    dispatcher->removeEventListener( m_touchListener);
    Director::getInstance()->getScheduler()->unscheduleAllForTarget(this);
    delete m_magicController;


    for (int s = 0; s < m_spawnPrograms.size(); ++s) {
        for (int m = 0; m < m_spawnPrograms[s]->m_monsterDatas.size(); ++m) {
            string monsterId = m_spawnPrograms[s]->m_monsterDatas[m].id;
            auto reader = GD->defaultsConfig()->getSubConfigByPath(
                    StringUtils::format("monster.%s", monsterId.c_str()));
            string textureId = reader->hasPath("textureId")
                    ? reader->fromPath("textureId")->toString()->_string
                    : monsterId;
            CheetahAtlasHelper::getInstance()->remove(
                    StringUtils::format("gameScreen/monsters/%s.atlas", textureId.c_str()));
        }
    }
    m_gameNode->release();
    m_gameNodeRenderTex->removeFromParentAndCleanup(true);
    m_gameNodeRenderTex->release();
    CheetahAtlasHelper::getInstance()->remove("gameScreen/towers/towers.atlas");
    CheetahAtlasHelper::getInstance()->remove("gameScreen/bridges/bridges_1.atlas");
    CheetahAtlasHelper::getInstance()->remove("gameScreen/bridges/bridges_2.atlas");
    CheetahAtlasHelper::getInstance()->remove("gameScreen/bullets/bullets_1.atlas");
    CheetahAtlasHelper::getInstance()->remove("gameScreen/bullets/bullets_2.atlas");
    CheetahAtlasHelper::getInstance()->remove("gameScreen/magicians/magicians_1.atlas");
    CheetahAtlasHelper::getInstance()->remove("gameScreen/magicians/magicians_2.atlas");
    CheetahAtlasHelper::getInstance()->remove("gameScreen/shields/shields.atlas");
    CheetahAtlasHelper::getInstance()->remove("gameScreen/magic/magic.atlas");
    CheetahAtlasHelper::getInstance()->remove("gameScreen/ui/hud_1.atlas");
    CheetahAtlasHelper::getInstance()->remove("gameScreen/ui/hud_2.atlas");
    CheetahAtlasHelper::getInstance()->remove("gameScreen/monsterExplode/monsterExplode.atlas");

    CheetahAtlasHelper::getInstance()->remove("gameScreen/monsters/mushroom.atlas");
    CheetahAtlasHelper::getInstance()->remove("gameScreen/ginorBullet/ginorBullet.atlas");
    if (m_bossData) {
        CheetahAtlasHelper::getInstance()->remove("gameScreen/bossCrystals/bossCrystals.atlas");

        if (m_bossData->id == "death" || m_bossData->id == "ginorMaster")
            CheetahAtlasHelper::getInstance()->remove(StringUtils::format(
                "gameScreen/bossAttack/%s/%s.atlas", m_bossData->id.c_str(), m_bossData->id.c_str()
            ));
    }

    if (m_bossData && m_bossData->id == "dragon")
        CheetahAtlasHelper::getInstance()->remove("gameScreen/dragonBullet/dragonBullet.atlas");


    cleanBackAnimations();

}


Zone* GameController::getRandFreeZone(std::pair<int,int> rowLimits, std::pair<int,int> colLimits) {
    int tries = 0;
    Zone *zone = nullptr;
    while (zone == nullptr) {
        Zone *candidate = &zones[Tools::randInt(rowLimits.first, rowLimits.second)][Tools::randInt(colLimits.first, colLimits.second)];
        bool isFree = true;
        for (int i = 0; i < m_pMonsters.size(); ++i) {
            Monster *monster = m_pMonsters[i];
            if (monster->isActive()) {
                MonsterUnmovable *uMonster = dynamic_cast<MonsterUnmovable*>(monster);
                if (uMonster) {
                    vector<Zone*> monsterZones = uMonster->getCurrentZones();
                    for (int z = 0; z < monsterZones.size(); ++z)
                        if (monsterZones[z] == candidate) {
                            isFree = false;
                            break;
                        }
                    if (!isFree) break;
                }
            }
        }
        if (isFree || tries++ > 10) zone = candidate;
    }
    return zone;
}

Zone* GameController::getRandFreeZoneDragon() { // deprecated
    int tries = 0;
    std::pair<int,int> rowLimits(1,3);
    std::pair<int,int> colLimits(0,3);
    Zone *zone = nullptr;
    while (zone == nullptr) {
        Zone *candidate = &zones[Tools::randInt(rowLimits.first, rowLimits.second)][Tools::randInt(colLimits.first, colLimits.second)];
        Zone *subCandidate = &zones[candidate->rowIndex - 1][candidate->colIndex];
        bool isFree = true;
        for (int i = 0; i < m_pMonsters.size(); ++i) {
            Monster *monster = m_pMonsters[i];
            if (monster->isActive()) {
                MonsterUnmovable *uMonster = dynamic_cast<MonsterUnmovable*>(monster);
                if (uMonster) {
                    vector<Zone*> monsterZones = uMonster->getCurrentZones();
                    for (int z = 0; z < monsterZones.size(); ++z)
                        if (monsterZones[z] == candidate || monsterZones[z] == subCandidate) {
                            isFree = false;
                            break;
                        }
                    if (!isFree) break;
                }
            }
        }
        if (isFree || tries++ > 10) zone = candidate;
    }
    return zone;
};

void GameController::increaseGold(int price, Point pos) {
    char labelText[16];
    sprintf (labelText, "+%d gold", price);
    TTFConfig conf;
    conf.fontFilePath = "Square_rough.ttf";
    conf.fontSize  = 20;
    conf.glyphs = GlyphCollection::ASCII;
    auto label = Label::createWithTTF(conf, labelText, TextHAlignment::CENTER , 200);
    m_hudNode->addChild(label);
    label->setColor(Color3B(232,204,21));
    label->setPosition(pos + Point(-20.0, 30.0));
    label->runAction(Sequence::createWithTwoActions(
            Spawn::createWithTwoActions(MoveBy::create(1.0, Point(0.0, 30.0)), FadeIn::create(0.2)),
            CallFunc::create([label](){label->removeFromParentAndCleanup(true);})
    ));
    label->enableShadow(Color4B::BLACK, Size(-1.0,1.0), 0);

    GD->m_model.getLevelProgress()->gold += price;
    m_uiController->updateStats();
    if (!GD->m_model.getCurrentSlot()->getFlag(FlagIndices::upgradesDialogPlayed)
            && GD->m_model.getLevelProgress()->gold > 300 && !isBossTime() && !m_tutorial) {
        GD->m_model.getCurrentSlot()->setFlag(FlagIndices::upgradesDialogPlayed, true);
        GD->m_model.dumpToDisk();
        playUpgradeDialog();

    }

    if (!GD->m_model.getCurrentSlot()->getFlag(FlagIndices::upgradesBridgesDialogPlayed)
            && GD->m_model.getLevelProgress()->gold > 400 && !isBossTime() && !m_tutorial) {
        GD->m_model.getCurrentSlot()->setFlag(FlagIndices::upgradesBridgesDialogPlayed, true);
        GD->m_model.dumpToDisk();
        playUpgradeBridgeDialog();

    }
}


void GameController::increaseCrystal(int crystal, Point monsterPos) {
    TTFConfig conf;
    conf.fontFilePath = "Square_rough.ttf";
    conf.fontSize  = 20;
    conf.glyphs = GlyphCollection::ASCII;
    auto label = Label::createWithTTF(conf, StringUtils::format("+%d crystals", crystal), TextHAlignment::CENTER , 200);
    m_hudNode->addChild(label);
    label->setPosition(monsterPos + Point(-20.0, 80.0));
    label->runAction(Sequence::createWithTwoActions(
            Spawn::createWithTwoActions(MoveBy::create(1.0, Point(0.0, 30.0)), FadeIn::create(0.2)),
            CallFunc::create([label](){label->removeFromParentAndCleanup(true);})
    ));
    GD->m_model.getLevelProgress()->crystals += crystal;
    m_uiController->updateStats();
}

void GameController::increaseScore(int score, Point monsterPos) {
    TTFConfig conf;
    conf.fontFilePath = "Square_rough.ttf";
    conf.fontSize  = 20;
    conf.glyphs = GlyphCollection::ASCII;
    
    auto label = Label::createWithTTF(conf, StringUtils::format("+%d score", score), TextHAlignment::CENTER , 200);
    m_hudNode->addChild(label);
    label->setPosition(monsterPos + Point(-20.0, 55.0));
    label->runAction(Sequence::createWithTwoActions(
            Spawn::createWithTwoActions(MoveBy::create(1.0, Point(0.0, 30.0)), FadeIn::create(0.2)),
            CallFunc::create([label](){label->removeFromParentAndCleanup(true);})
    ));
    GD->m_model.getLevelProgress()->score += score;
    m_uiController->updateStats();
}


Catapult *GameController::getCatapult(int index) {
    return m_pCatapults[index];
}



GameUIController *GameController::getUIController() {
    return m_uiController;
}

void GameController::ginorBulletsUpdate(float dt) {
    float ds = dt * 200.0;
    vector<int> toRemove;
    toRemove.clear();
    for (int i = 0; i < m_ginorBullets.size(); ++i) {
        auto bullet = m_ginorBullets[i];
        bullet->setPosition(bullet->getPosition() + Point(0.0, -ds));
        if (bullet->getPosition().y < 100.0) {
            toRemove.push_back(i);
        }
    }
    for (int i = 0; i < toRemove.size(); ++i) {
        Catapult *catapult = m_pCatapults[m_ginorBullets[toRemove[i]]->getTag()];
        catapult->recieveFog();

        m_ginorBullets[toRemove[i]]->stopAllActions();
        m_ginorBullets[toRemove[i]]->removeFromParentAndCleanup(true);
        m_ginorBullets.erase(m_ginorBullets.begin() + toRemove[i]);
        for(int j = i; j < toRemove.size(); ++j) {
            if (toRemove[j]>= toRemove[i])
                toRemove[j] --;
        }
    }
}

void GameController::dragonBulletsUpdate(float dt) {
    float ds = dt * 200.0f;
    vector<int> toRemove;
    toRemove.clear();
    for (int i = 0; i < m_dragonBullets.size(); ++i) {
        auto bullet = m_dragonBullets[i];
        bullet->setPosition(bullet->getPosition() + Point(0.0, -ds));
        if (bullet->getPosition().y < 270.0) {
            toRemove.push_back(i);
        }
    }
    for (int i = 0; i < toRemove.size(); ++i) {
        //action
        bossAttack(nullptr);

        m_dragonBullets[toRemove[i]]->stopAllActions();
        m_dragonBullets[toRemove[i]]->removeFromParentAndCleanup(true);
        m_dragonBullets.erase(m_dragonBullets.begin() + toRemove[i]);
        for(int j = i; j < toRemove.size(); ++j) {
            if (toRemove[j]>= toRemove[i])
                toRemove[j] --;
        }
    }
}


void GameController::sortZOrders() {
    float yMin = 100.0,
        yMax = 900.0;
    int zMin = 4,
        zMax = 40;
    float dy = yMax - yMin;
    int dz = zMax - zMin;
    auto nsort = [](Node*node, float yMin, int zMax, float dy, int dz){
        node->setZOrder(zMax - roundf(float(dz)*(node->getPositionY() - yMin)/dy));
    };
    for (int i = 0; i < m_pMonsters.size(); ++i) {
        if (m_pMonsters[i]->isActive() && dynamic_cast<MonsterBat*>(m_pMonsters[i]) == nullptr) {
            if (MonsterMultiNode *multiNode = dynamic_cast<MonsterMultiNode*> (m_pMonsters[i])) {
                auto nodes = multiNode->getNodes();
                for (int n = 0; n < nodes.size(); ++n) {
                    nsort(nodes[n], yMin, zMax, dy, dz);
                }
            }
            else
                nsort(m_pMonsters[i]->m_node, yMin, zMax, dy, dz);
        }
    }
    for (int i = 0; i < m_pBridges.size(); ++i) {
        nsort(m_pBridges[i]->m_node, yMin, zMax, dy, dz);        //TODO: those are static, no need to do it in every frame
    }
    for (int i = 0; i < 4; ++i)
        nsort(m_shields[i], yMin, zMax, dy, dz);
}


MagicController *GameController::getMagicController() {
    return m_magicController;
}


void GameController::setupBackAnimations() {
    auto handler = [this] (std::string *ids, int *counts, bool *isAdditive, int count, bool *isDelayed, int *zOrders) {
        for (int i = 0; i < count; ++i) {
            CheetahAtlasHelper::getInstance()->add(
                    StringUtils::format("gameScreen/backAnimations/%s.%s", ids[i].c_str(), "atlas"));

            auto anim = Animation::create();
            Tools::addFramesToAnimation(anim, StringUtils::format(
                    "gameScreen/backAnimations/%s/%s.%s", ids[i].c_str(), "%04d", isAdditive[i] ? "jpg" : "png"), 20.0);
            anim->setLoops(1);

            for (int j = 0; j < counts[i]; ++j) {
                auto sprite = Sprite::create();
                GD->ggc()->applyTransformAP(sprite, StringUtils::format("BA_%s_%02d", ids[i].c_str(), j));
                m_gameNode->addChild(sprite, zOrders[i]);
                Vector<FiniteTimeAction*> actions;
                if (isDelayed[i]) actions.pushBack(DelayTime::create(Tools::randFloat() * 2.0 + 1.0));
                actions.pushBack(Animate::create(anim));
                sprite->runAction(RepeatForever::create(Sequence::create(actions)));
                sprite->setSpriteFrame(anim->getFrames().at(5)->getSpriteFrame());
                if (isAdditive[i]) sprite->setBlendFunc(BlendFunc::ADDITIVE);
            }

        }
    };
    if (GD->m_model.temp.currentLocation == Location::desert) {
        std::string ids[4] = {"fontain", "scorpions", "snake", "eyes"};
        int counts[4] = {3, 4, 2, 3};
        int zOrders[4] = {1, 1, 1, 1};
        bool isAdditive[4] = {false, false, false, false};
        bool isDelayed[4] = {true, true, true, true};
        handler(ids, counts, isAdditive, 4, isDelayed, zOrders);
    }
    if (GD->m_model.temp.currentLocation == Location::inferno) {
        std::string ids[5] = {"sparks", "cracks", "fireBoom", "shine", "bats"};
        int counts[5] = {4, 6, 6, 7, 5};
        int zOrders[5] = {1, 1, 1, 51, 2};
        bool isAdditive[5] = {true, true, true, true, false};
        bool isDelayed[5] = {false, true, true, false, true};
        handler(ids, counts, isAdditive, 5, isDelayed, zOrders);
    }
}


void GameController::cleanBackAnimations() {
    auto handler = [this] (std::string *ids, int count) {
        for (int i = 0; i < count; ++i) {
            CheetahAtlasHelper::getInstance()->remove(
                    StringUtils::format("gameScreen/backAnimations/%s.%s", ids[i].c_str(), "png"));
        }
    };
    if (GD->m_model.temp.currentLocation == Location::desert) {
        std::string ids[4] = {"fontain", "scorpions", "snake", "eyes"};
        handler(ids, 4);
    }
    if (GD->m_model.temp.currentLocation == Location::inferno) {
        std::string ids[5] = {"sparks", "cracks", "fireBoom", "shine", "bats"};
        handler(ids, 5);
    }
}

void GameController::checkForShowMonsterCard(string id) {
    if (m_tutorial) return;
    auto shownCards = &GD->m_model.getCurrentSlot()->shownMonsterCards;
    if (std::find(shownCards->begin(), shownCards->end(), id) == shownCards->end()) {
        shownCards->push_back(id);
        char cardPath[64];
        sprintf(cardPath, "gameScreen/monsterCards/%s.png", id.c_str());
        if (FileUtils::getInstance()->isFileExist(FileUtils::getInstance()->fullPathForFilename(cardPath)))
            m_uiController->showMonsterCard(id);
    }
}

void GameController::playFirstLevelDialog() {
    if (DialogManager::getInstance()->m_isTelling) return;

    setPaused(true);
    dispatchDenyTouches((int)TouchReceiverFlag::all);
    DialogManager::getInstance()->show(m_uiNode, 60, Point(0.0, 480.0));
    vector<string> statements;
    statements.push_back(_("It`s time for the real battle, let us show the enemy that we are able to counter his threats"));
    DialogManager::getInstance()->createDialog(statements, 30.0, [this](){
        setPaused(false);
        DialogManager::getInstance()->hide();
        dispatchAllowTouches((int)TouchReceiverFlag::all);
    });
}


void GameController::playFifthLevelDialog() {
    if (DialogManager::getInstance()->m_isTelling) return;

    setPaused(true);
    dispatchDenyTouches((int)TouchReceiverFlag::all);
    DialogManager::getInstance()->show(m_uiNode, 60, Point(0.0, 480.0));
    vector<string> statements;
    statements.push_back(_("Commander, don't forget to upgrade defensive knives! They activate at the approach of the enemy and not once can save the citadel from invaders"));
    DialogManager::getInstance()->createDialog(statements, 30.0, [this](){
        setPaused(false);
        DialogManager::getInstance()->hide();
        dispatchAllowTouches((int)TouchReceiverFlag::all);
    });
}


void GameController::playUpgradeDialog() {
    if (DialogManager::getInstance()->m_isTelling) return;

    setPaused(true);
    dispatchDenyTouches((int)TouchReceiverFlag::all);
    DialogManager::getInstance()->show(m_uiNode, 60, Point(0.0, 480.0));
    vector<string> statements;
    statements.push_back(_("Don`t forget to strengthen the defense buildings in the menu «upgrades»"));
    DialogManager::getInstance()->createDialog(statements, 23.0, [this](){
        setPaused(false);
        DialogManager::getInstance()->hide();
        dispatchAllowTouches((int)TouchReceiverFlag::all);
    });
    auto pointer = Sprite::create("gameScreen/ui/hand.png");
    Point target = m_uiController->m_upgradeButton->getPosition();
    pointer->setPosition(target + Point(240.0,-80.0));
    Vector<FiniteTimeAction*> actions;
    actions.pushBack(MoveBy::create(0.5, Point(-50.0,50.0)));
    actions.pushBack(DelayTime::create(2.0));
    actions.pushBack(MoveBy::create(0.5, Point(50.0,-50.0)));
    actions.pushBack(CallFunc::create([pointer](){pointer->removeFromParentAndCleanup(true);}));
    pointer->runAction(Sequence::create(actions));
    m_uiNode->addChild(pointer, 105);
}

void GameController::playTowerUpgradeDialog() {
    if (DialogManager::getInstance()->m_isTelling) return;

    setPaused(true);
    dispatchDenyTouches((int)TouchReceiverFlag::all);
    DialogManager::getInstance()->show(m_uiNode, 160, Point(0.0, 480.0));
    vector<string> statements;
    statements.push_back(_("Commander, upgrading of the towers allows to deploy new shots and increases the speed of the tower attack!"));
    DialogManager::getInstance()->createDialog(statements, 30.0, [this](){
        setPaused(false);
        DialogManager::getInstance()->hide();
        dispatchAllowTouches((int)TouchReceiverFlag::all);
    });
}


void GameController::playWinBossDialog() {
    if (DialogManager::getInstance()->m_isTelling) return;

    setPaused(true);
    dispatchDenyTouches((int)TouchReceiverFlag::all);
    DialogManager::getInstance()->show(m_uiNode, 60, Point(0.0, 480.0));
    vector<string> statements;
    int cl = GD->m_model.getCurrentSlot()->currentLevel;
    int locationId = cl / GD->getLevelsPerLocation();
    int cll = cl % GD->getLevelsPerLocation();
    if (locationId == 0) {
        if (cll == bossLevel::FIRST_BOSS) {
            statements.push_back(_("Hmm... You are cool! You broke best army of darkness scouts."));
            statements.push_back(_("I am afraid the enemy won't leave it just like that..."));
        }
        else if (cll == bossLevel::SECOND_BOSS) {
            statements.push_back(_("You start disturbing dark troops."));
            statements.push_back(_("They got used that nobody dares to follow their ways."));
            statements.push_back(_("This time, they sent the advanced troops to take our fortress!"));
        }
        else if (cll == bossLevel::LAST_BOSS) {
            statements.push_back(_("It`s gorgeous! We  rescued the south part of the island from the invasion of foes!"));
            statements.push_back(_("Congratulations, hero! The south gateway is again opened for caravans and trade."));
            //new one
            statements.push_back(_("The first groups of enemy are gone to help by a new wave of evil"));
        }

    }
    else if (locationId == 1) {
        if (cll == bossLevel::FIRST_BOSS) {
            statements.push_back(_("You don't cease to surprise me!"));
            statements.push_back(_("With such commander the victory will be for us!"));
        }
        else if (cll == bossLevel::SECOND_BOSS) {
            statements.push_back(_("Perfectly, the snake is overthrow, the enemy runs, now they know how to put on our lands"));
        }
        else if (cll == bossLevel::LAST_BOSS) {
            statements.push_back(_("Excellent! We crushed the enemy and strengthened the west frontier, now the enemy cannot take it."));
            statements.push_back(_("You`ve brilliantly coped with all tasks!"));
            // new one
            //statements.push_back(_("The first groups of enemy are gone to help by a new wave of evil"));
        }
    }
    else if (locationId == 2) {
        if (cll == bossLevel::FIRST_BOSS) {
            statements.push_back(_("I start thinking that you - one of the most brave heroes whom I know!"));
            statements.push_back(_("Advance forces of enemy are completely broken."));
            statements.push_back(_("The enemy will remember this lesson for a long time!"));
        }
        else if (cll == bossLevel::SECOND_BOSS) {
            statements.push_back(_("And again success, accept my congratulations, hero!"));
            statements.push_back(_("People start putting songs about your feats"));
        }
        else if (cll == bossLevel::LAST_BOSS) {
            statements.push_back(_("It was a hard battle, without your expertise we would have lost."));
            statements.push_back(_("My best congratulations to you, Hero!"));
            // new one
            //statements.push_back(_("The first groups of enemy are gone to help by a new wave of evil"));
        }

    }
    else if (locationId == 3) {
        if (cll == bossLevel::FIRST_BOSS) {
            statements.push_back(_("You are a great soldier!"));
            statements.push_back(_("Even fighting vehicles of goblins aren't capable to pass through walls of your fortifications!"));
        }
        else if (cll == bossLevel::SECOND_BOSS) {
            statements.push_back(_("Strengths of the enemy on an outcome! You really hit them down."));
            statements.push_back(_("And after all at the beginning nobody believed that we will manage to stop such powerful opponent"));
        }
        else if (cll == bossLevel::LAST_BOSS) {
            statements.push_back(_("We managed to save the island andrescued it from the arms of death."));
            statements.push_back(_("Thanks to you we`ve done it! Mitrandia recovered its independence and got your reliable protection."));
            statements.push_back(_("Please, accept our best thanks on the behalf of the Council and all inhabitants of the island."));
            // new one
            //statements.push_back(_("The first groups of enemy are gone to help by a new wave of evil"));
        }
    }
    if (statements.size() == 0) // might be debug situation
        statements.push_back("debuG mode");
    DialogManager::getInstance()->createDialog(statements, 23.0, [this](){
        setPaused(false);
        DialogManager::getInstance()->hide();
        dispatchAllowTouches((int)TouchReceiverFlag::all);
        leaveTheScreen();
    });
}

void GameController::calculateLevelAchievements () {
    // sniper
    GD->m_model.temp.setLevelAchievement(
            LevelAchievement::sniper,
            (float) m_levelStatistics.shotsSucceed / (float) m_levelStatistics.shotsTotal >= SNIPER_ACHIEVEMENT_RATIO);

    // destroyer
    GD->m_model.temp.setLevelAchievement(
            LevelAchievement::destroyer,
            m_life == m_lifeMax);

    // mushroomer
    GD->m_model.temp.setLevelAchievement(
            LevelAchievement::mushroomer,
            GD->m_model.getCurrentSlot()->mushroomCollected[GD->m_model.getCurrentSlot()->currentLevel] == 3);

    // last line
    // no magic - set during game
}

void GameController::monsterAttackedBridge(Monster *pMonster, Bridge *pBridge) {
    if (!(m_tutorial && pBridge->m_config.row == 0))
        pBridge->recieveDamage(pMonster->getAttackPower());

    if (pBridge->m_config.row == 0)
        GD->m_model.temp.setLevelAchievement(LevelAchievement::lastLine,false);
    if (m_tutorial)
        m_tutorialController->monsterAttackedBridge();
}

void GameController::checkCollectBossCrystals(Monster *monster) {
    int priceCrystal = 0;
    if (!isSpawnProgramKilled(monster->m_config.spawnProgram)
            || isCurrentLevelCompleteBefore()) return;
    priceCrystal = monster->m_priceCrystal;
    if (priceCrystal < 0) return;
    increaseCrystal(priceCrystal, monster->getDeathPosition());

    //anim
    auto sprite = Sprite::create();
    sprite->setPosition(monster->getDeathPosition());
    m_gameNode->addChild(sprite, 50);
    Animation *anim = Animation::create();
    Tools::addFramesToAnimation(anim, "gameScreen/bossCrystals/%04d.png", 24.0);
    sprite->runAction(Sequence::createWithTwoActions(Animate::create(anim), FadeOut::create(1.0)));
    sprite->setScale(1.5);

    auto labelsNode = Node::create();
    labelsNode->setPosition(Tools::screenCenter() + Vec2(0.0,-150.0));
    m_hudNode->addChild(labelsNode);
    Vector<FiniteTimeAction*> actions;
    actions.pushBack(MoveBy::create(2.0, Vec2(0.0, 50.0)));
    actions.pushBack(CallFunc::create([labelsNode](){labelsNode->removeFromParent();}));
    labelsNode->runAction(Sequence::create(actions));

    auto goldTitle = Sprite::create("gameScreen/bossPrice/titles/gold.png");
    auto crystalsTitle = Sprite::create("gameScreen/bossPrice/titles/crystals.png");
    labelsNode->addChild(goldTitle);
    labelsNode->addChild(crystalsTitle);
    goldTitle->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    crystalsTitle->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
    goldTitle->setPosition(0.0, 120.0);
    crystalsTitle->setPosition(0.0, 70.0);
    Label* goldDigits = Label::createWithBMFont("gameScreen/bossPrice/digitFonts/gold/font.fnt",
            StringUtils::format("%d", m_bossGoldFund), TextHAlignment::RIGHT , 500);
    Label* crystalDigits = Label::createWithBMFont("gameScreen/bossPrice/digitFonts/crystals/font.fnt",
            StringUtils::format("%d", priceCrystal), TextHAlignment::RIGHT , 500);
    labelsNode->addChild(goldDigits);
    labelsNode->addChild(crystalDigits);
    goldDigits->setPosition(Vec2(50.0, 120.0));
    crystalDigits->setPosition(Vec2(50.0, 70.0));
    goldDigits->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    crystalDigits->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);

}


void GameController::leaveTheScreen() {
    m_leaveTheScreen = 3.0;

}

void GameController::showLostLifeSale() {
    if (!PlatformTools::isPurchaseAllowed()) return;
    setPaused(true);
    const int saleId = LOST_LIFE_SALE_ID;
    mgui::Sale *panel = mgui::Sale::create(saleId);
    panel->setName("salePanel");
    GD->m_model.getCurrentSlot()->setSaleShowed(saleId, true);

    // if ok
    GameController *controller = this;

    panel->m_onSaleClicked = [controller](int _saleId) {
        GameController *c2 = controller; // somehow this is nesessary
        c2->m_uiNode->getChildByName("salePanel")->removeFromParent();
        c2->setPaused(false);
        int upgradeId = GD->defaultsConfig()->fromPath(StringUtils::format("sales.sale%d.moneyUpgradeId", _saleId))->toInt();

        GD->requestMoneyUpgrade(upgradeId, [c2, _saleId](){
            GameController *c3 = c2;
            SimpleAudioEngine::getInstance()->playEffect("gameScreen/buy.wav", false, 1.0, 0.0, 1.0);
            c3->m_uiController->updateStats();
            GD->m_model.getCurrentSlot()->setSaleUsed(_saleId, true);

        }, nullptr);
    };


    // if not
    panel->m_onCloseClicked = [this]() {
        GameController *c1 = this;
        c1->m_uiNode->getChildByName("salePanel")->removeFromParent();
        c1->setPaused(false);
        log("unpaused!");
    };
    m_uiNode->addChild(panel, 200);
}


GameController::~GameController() {
    for (int i = 0; i < m_pCatapults.size(); ++i) {
        auto catapult = m_pCatapults[i];
        delete catapult;
    }
    m_pCatapults.clear();
    for (int i = 0; i < m_pBridges.size(); ++i) {
        auto bridge = m_pBridges[i];
        delete bridge;
    }
    m_pBridges.clear();
    for (int i = 0; i < m_pMonsters.size(); ++i) {
        auto monster = m_pMonsters[i];
        delete monster;
    }
    m_pMonsters.clear();
    m_uiController->release();
    if (m_tutorialController) delete m_tutorialController;
    CCLOG("GameController destruct");
}

/*

level1:
  spawnPrograms:
    main:
      - monsters:
          - id: goblinAx
            count: 2
          - id: slug
            count: 4
        interval: 1.5

      - fillInto: main.1
        monsters:
          - id: ginor
            count: 10
            price:
    boss:
      - interval: 0.5
        monsters:
          - id: death
            count: 2
      - monsters:
          - id: ginorRed
            count: 20
            price: 0
        delay: 3.0
      - monsters:
          - id: bat
            count: 10


 */
void GameController::setupSpawnPrograms() {
    ConfigReader *levelReader;
    if (m_tutorial) {
        levelReader = GD->defaultsConfig()->getSubConfigByPath("levels.tutorial");
    }
    else {
        levelReader = GD->defaultsConfig()->getSubConfigByPath(
                StringUtils::format("levels.level%02d", GD->m_model.getCurrentSlot()->currentLevel + 1));
    }
    Array *sections = levelReader->fromPath("spawnPrograms")->toDictionary()->allKeys();
    for (int s = 0; s < sections->count(); ++s) {
        std::string sectionName = dynamic_cast<String*>(sections->getObjectAtIndex(s))->_string;
        Array *sectionPrograms =
                levelReader->fromPath(StringUtils::format("spawnPrograms.%s", sectionName.c_str()))->toArray();
        for (int i = 0; i < sectionPrograms->count(); ++i) {
            auto programReader = levelReader->getSubConfigByPath(
                    StringUtils::format("spawnPrograms.%s.%d",sectionName.c_str(),i));
            auto program = new SpawnProgram(StringUtils::format("%s.%d",sectionName.c_str(), i+1));
            program->m_sectionId = sectionName;
            program->setController(this);

            using namespace placeholders;
            program->m_spawnCallback = std::bind(&GameController::spawnMonster, this, _1, _2);

            m_spawnPrograms.push_back(program);

            program->setup(programReader);
        }
    }
}

SpawnProgram* GameController::getSpawnProgramById(std::string id) {
    for (int i = 0; i < m_spawnPrograms.size(); ++i) {
        if (m_spawnPrograms[i]->m_id == id) return m_spawnPrograms[i];
    }
    return nullptr;
}

void GameController::loadMonsterAtlases() {
    for (int s = 0; s < m_spawnPrograms.size(); ++s) {
        for (int m = 0; m < m_spawnPrograms[s]->m_monsterDatas.size(); ++m) {
            std::string monsterId = m_spawnPrograms[s]->m_monsterDatas[m].id;
            auto reader = GD->defaultsConfig()->getSubConfigByPath(
                    StringUtils::format("monster.%s", monsterId.c_str()));
            std::string textureId = reader->hasPath("textureId")
                    ? reader->fromPath("textureId")->toString()->_string
                    : monsterId;
            CheetahAtlasHelper::getInstance()->add(
                    StringUtils::format("gameScreen/monsters/%s.atlas", textureId.c_str()));
        }
    }
}

void GameController::scanForBoss() {
    for (int s = 0; s < m_spawnPrograms.size(); ++s) {
        for (int m = 0; m < m_spawnPrograms[s]->m_monsterDatas.size(); ++m) {
            std::string monsterId = m_spawnPrograms[s]->m_monsterDatas.at(m).id;
            if (monsterId == "death" || monsterId == "ginorMaster" || monsterId == "dragon") {
                m_bossData = &m_spawnPrograms[s]->m_monsterDatas.at(m);
                return;
            }
            else m_bossData = nullptr;
        }
    }
}

void GameController::applyCombo(int kills) {
    //std::string titles[10] = {"double kill", "triple kill", "ultra kill", "monster kill", "exterminator",
    //        "Godlike", "Godlike", "Godlike", "Godlike"};
    GD->m_model.temp.levelCombos.push_back(kills);
    int locId = GD->m_model.getCurrentSlot()->currentLevel / GD->getLevelsPerLocation();
    int achieveKills[4] = {5,6,7,8};
    if (kills >= achieveKills[locId])
        GD->m_model.temp.setLevelAchievement(LevelAchievement::combo, true);
    int labelId = kills < 2 ? 2 : (kills > 7 ? 7 : kills);


    TTFConfig conf;
    conf.fontSize = 35;
    conf.glyphs = GlyphCollection::CUSTOM;
    conf.customGlyphs = " goldx0123456789";
    conf.fontFilePath = "Square_rough.ttf";

    int gold = GD->getComboGold(kills);
    GD->m_model.getLevelProgress()->gold += gold;
    m_uiController->updateStats();

    auto labelScore = Label::createWithTTF(conf, StringUtils::format("x%d", GD->getComboScore(kills)), TextHAlignment::CENTER , 500);
    auto labelGold = Label::createWithTTF(conf, StringUtils::format("gold %d", gold), TextHAlignment::CENTER , 500);
    auto label = Sprite::createWithSpriteFrameName(StringUtils::format("gameScreen/ui/hud/combo/titles/%d.png", labelId));
    auto labelNode = Node::create();
    m_hudNode->addChild(labelNode, 20);
    label->setAnchorPoint(Point::ANCHOR_MIDDLE_RIGHT);
    labelGold->setAnchorPoint(Point::ANCHOR_MIDDLE_RIGHT);
    labelScore->setAnchorPoint(Point::ANCHOR_MIDDLE_RIGHT);
    labelNode->addChild(label);
    labelNode->addChild(labelScore);
    labelNode->addChild(labelGold);
    labelNode->setPosition(Point(label->getContentSize().width + 780.0, 700.0));
    labelScore->setPosition(Vec2(-30.0, -50.0));
    labelGold->setPosition(Vec2(-30.0, -100.0));
    labelGold->setColor(Color3B(232,204,21));
    labelGold->enableShadow(Color4B::BLACK, Size(-2.0f,-2.0f), 0);
    Vector<FiniteTimeAction*> actions;
    //label->setColor(Color3B(122,25,21));
    float dx = label->getContentSize().width + 100.0;
    actions.pushBack(EaseBackOut::create(MoveBy::create(0.5, Point(-dx, 0.0))));
    actions.pushBack(DelayTime::create(1.0));
    actions.pushBack(Spawn::createWithTwoActions(EaseSineIn::create(MoveBy::create(0.2, Point(dx, 0.0))), FadeOut::create(0.2)));
    actions.pushBack(CallFunc::create([label](){label->removeFromParent();}));
    labelNode->runAction(Sequence::create(actions));
    labelNode->runAction(FadeIn::create(0.1));

    //sound
    /*if (kills >= 3) {
        int soundId = kills >= 8 ? 8 : kills;
        SimpleAudioEngine::getInstance()->playEffect(StringUtils::format("gameScreen/combo/%d.wav", soundId).c_str(), false, 1.0, 0.0, 1.0);
    } */

    //another sound
    SimpleAudioEngine::getInstance()->playEffect("gameScreen/combo.wav", false, 1.0, 0.0, 1.0);

}

bool GameController::isCurrentLevelCompleteBefore() {
    return GD->m_model.getCurrentSlot()->completeLevels >= GD->m_model.getCurrentSlot()->currentLevel;
}

bool GameController::isSpawnProgramKilled(SpawnProgram *program) {
    if (!program->isComplete()) return false;
    for (int i = 0; i < m_pMonsters.size(); ++i)
        if (m_pMonsters[i]->m_config.spawnProgram == program && m_pMonsters[i]->isActive())
            return false;
    return true;
}

void GameController::checkForCurrentSpawnSectionKilled() {
    if (isCurrentSpawnSectionKilled()) {
        // so, all killed!
        log ("all killed!");
        if (m_currentSpawnSectionId == "main" && m_bossData) bossTime();
        else allMonstersKilled();
    }
}


bool GameController::isCurrentSpawnSectionKilled() {
    log("check sp %s killed", m_currentSpawnSectionId.c_str());
    for (int s = 0; s < m_spawnPrograms.size(); ++s) {
        auto sp = m_spawnPrograms[s];
        if (sp->m_sectionId == m_currentSpawnSectionId) {
            if (!sp->isComplete()) {log ("sp %s incomplete", sp->m_id.c_str()); return false;}
            else {
                for (int m = 0; m < m_pMonsters.size(); ++m)
                    if (m_pMonsters[m]->isActive() && m_pMonsters[m]->m_config.spawnProgram == sp) {
                        log ("monster %s of sp %s is active", m_pMonsters[m]->m_uniqueId.c_str(), sp->m_id.c_str());return false;}
            }
        }
    }
    return true;
}

bool GameController::isCurrentSpawnSectionKilledOrDying() {
    for (int s = 0; s < m_spawnPrograms.size(); ++s) {
        auto sp = m_spawnPrograms[s];
        if (sp->m_sectionId == m_currentSpawnSectionId) {
            if (!sp->isComplete()) {
                log("sp %s incomplete", sp->m_id.c_str());
                return false;
            }
            else {
                for (int m = 0; m < m_pMonsters.size(); ++m)
                    if (m_pMonsters[m]->isActive() && !m_pMonsters[m]->isDiying() && m_pMonsters[m]->m_config.spawnProgram == sp) {
                        log("monster %s of sp %s is active", m_pMonsters[m]->m_uniqueId.c_str(), sp->m_id.c_str());
                        return false;
                    }
            }
        }
    }
    return true;
}



void GameController::bossTime() {
    startSpawnProgramsBySection("boss");

    m_magician->setVisible(true);
    MagicType magicianType = MagicType::fail;
    char animKey[32];
    int levelMax = -1;
    for (int i = 0; i < 4; ++i) {
        int level = GD->m_model.getLevelProgress()->magicUpgradeIds[i];
        if (level > levelMax) {
            levelMax = level;
            magicianType = static_cast<MagicType>(i);
            sprintf(animKey, "%sMagician", Tools::getMagicId(magicianType).c_str());
        }
    }
    m_magicianType = magicianType;
    int magicianUpgradeId = GD->m_model.getLevelProgress()->magicUpgradeIds[static_cast<int>(m_magicianType)];
    auto magicianUpgradeReader = Tools::getUpgradeReader("magic", magicianUpgradeId);
    Animation *anim = AnimationCache::getInstance()->getAnimation(animKey);
    m_magician->stopAllActions();
    m_magician->runAction(RepeatForever::create(Animate::create(anim)));
    if (!GD->m_model.getCurrentSlot()->getFlag(FlagIndices::firstBossDialogPlayed)) {
        m_magician->runAction(Sequence::createWithTwoActions(DelayTime::create(1.0), CallFunc::create([this](){playFirstBossDialog();})));
        GD->m_model.getCurrentSlot()->setFlag(FlagIndices::firstBossDialogPlayed, true);
    }
    int locId = GD->m_model.getCurrentSlot()->currentLevel / GD->getLevelsPerLocation();
    if (!GD->m_model.getCurrentSlot()->getFlag(FlagIndices::boss2_4DialogPlayed)
            && locId == 1) {
        m_magician->runAction(Sequence::createWithTwoActions(DelayTime::create(1.0), CallFunc::create([this](){play2_4BossDialog();})));
        GD->m_model.getCurrentSlot()->setFlag(FlagIndices::boss2_4DialogPlayed, true);
    }
    m_magicianLife = magicianUpgradeReader->fromPath("magicianLives")->toInt();
    TTFConfig conf;
    conf.fontSize = 42;
    conf.fontFilePath = "arial.ttf";
    auto mLifeLabel = Label::createWithTTF(conf, StringUtils::format("%d", m_magicianLife), TextHAlignment::CENTER, 50);
    mLifeLabel->setTag(321);
    m_hudNode->addChild(mLifeLabel);
    mLifeLabel->setPosition(m_magician->getPosition() + Point(-40.0, 0.0));

    auto lifeIcon = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/magicianHeartIcon.png");
    lifeIcon->setPosition(Point(-30.0, 0.0) + mLifeLabel->getPosition());
    m_hudNode->addChild(lifeIcon);
    lifeIcon->setTag(322);

    for (int i = 0; i < 4; ++i) {
        m_shields[i]->runAction(Animate::create(AnimationCache::getInstance()->getAnimation("shieldActive")));
    }

    SimpleAudioEngine::getInstance()->playBackgroundMusic("gameScreen/boss.mp3", true);
}

void GameController::startSpawnProgramsBySection(std::string sectionId) {
    m_currentSpawnSectionId = sectionId;
    for (int s = 0; s < m_spawnPrograms.size(); ++s) {
        auto sp = m_spawnPrograms[s];
        if (sp->m_sectionId == sectionId)
            sp->start();
    }
}

void GameController::addMushroomsSpawnProgram() {
    std::string readerPath = m_tutorial ? "levels.mushroomSpawnProgramTutorial" : "levels.mushroomSpawnProgram";
    auto programReader = GD->defaultsConfig()->getSubConfigByPath(readerPath);
    auto program = new SpawnProgram("main.mushroom");
    if (m_tutorial) program->m_sectionId = "wave4"; // mega hardcode!!!
    else program->m_sectionId = "main";
    program->setController(this);

    using namespace placeholders;
    program->m_spawnCallback = std::bind(&GameController::spawnMonster, this, _1, _2);

    m_spawnPrograms.push_back(program);
    program->setup(programReader);
}

bool GameController::isBossTime() {
    return m_currentSpawnSectionId == "boss";
}

void GameController::repair() {
    repair(1);
}


void GameController::repair(int lives) {
    if (isBossTime()) {
        m_magicianLife ++;
        m_magicianIsDying = false;
    }
    else {
        m_life += lives;
        m_uiController->updateStats();
        m_leaveTheScreen = -1.0;
        m_isAskedForBuyMagicianLife = false;

    }
    GD->m_model.getLevelProgress()->crystals -= LIFE_COST * lives;
    m_uiController->updateStats();
    checkForCurrentSpawnSectionKilled();

}


void GameController::magicianDeath() {
    SimpleAudioEngine::getInstance()->playEffect("gameScreen/magicianDeath.wav", false, 1.0, 0.0, 1.0);

    for (int j = 0; j < m_pMonsters.size(); ++j) {
        if (m_pMonsters[j]->m_config.data == m_bossData && m_pMonsters[j]->isActive())
            dynamic_cast<IMonsterBoss*>(m_pMonsters[j])->winDefile();
    }
    Animation *deathAnim = Animation::create();
    std::string magicianId = Tools::getMagicId(m_magicianType);
    char pattern [64];
    sprintf (pattern, "gameScreen/magicians/%s/death/%s.png", magicianId.c_str(), "%04d");
    Tools::addFramesToAnimation(deathAnim, pattern, 16.0);
    m_magician->stopAllActions();
    m_magician->runAction(Animate::create(deathAnim));
    for (int i = 0; i < 4; ++i) {
        m_shields[i]->stopAllActions();
        m_shields[i]->setSpriteFrame("gameScreen/shields/passive/0000.png");
    }
    auto label = dynamic_cast<Label*>(m_hudNode->getChildByTag(321));
    label->removeFromParent();

}

void GameController::playUpgradeBridgeDialog() {
    if (DialogManager::getInstance()->m_isTelling) return;
    setPaused(true);
    dispatchDenyTouches((int)TouchReceiverFlag::all);
    DialogManager::getInstance()->show(m_uiNode, 60, Point(0.0, 480.0));
    vector<string> statements;
    statements.push_back(_("Walls upgrading allows to hold the monsters and denies their quick movement from one sector to another"));
    DialogManager::getInstance()->createDialog(statements, 23.0, [this](){
        setPaused(false);
        DialogManager::getInstance()->hide();
        dispatchAllowTouches((int)TouchReceiverFlag::all);
    });
    auto pointer = Sprite::create("gameScreen/ui/hand.png");
    Point target = m_uiController->m_upgradeButton->getPosition();
    pointer->setPosition(target + Point(240.0,-80.0));
    Vector<FiniteTimeAction*> actions;
    actions.pushBack(MoveBy::create(0.5, Point(-50.0,50.0)));
    actions.pushBack(DelayTime::create(2.0));
    actions.pushBack(MoveBy::create(0.5, Point(50.0,-50.0)));
    actions.pushBack(CallFunc::create([pointer](){pointer->removeFromParentAndCleanup(true);}));
    pointer->runAction(Sequence::create(actions));
    m_uiNode->addChild(pointer, 105);

}

void GameController::playFirstBossDialog() {
    if (DialogManager::getInstance()->m_isTelling) return;

    setPaused(true);
    dispatchDenyTouches((int)TouchReceiverFlag::all);
    DialogManager::getInstance()->show(m_uiNode, 60, Point(0.0, 480.0));
    vector<string> statements;
    statements.push_back(_("Commander, be careful, during the battle with the Boss"));
    statements.push_back(_("all powers of your magic defender are expended in guarding the barrier"));
    statements.push_back(_("and you can't use magic"));
    DialogManager::getInstance()->createDialog(statements, 23.0, [this](){
        setPaused(false);
        DialogManager::getInstance()->hide();
        dispatchAllowTouches((int)TouchReceiverFlag::all);
    });
}

void GameController::playGinorDialog() {
    if (DialogManager::getInstance()->m_isTelling) return;

    setPaused(true);
    dispatchDenyTouches((int)TouchReceiverFlag::all);
    DialogManager::getInstance()->show(m_uiNode, 60, Point(0.0, 480.0));
    vector<string> statements;
    statements.push_back(_("Be careful! If ginor has enough time to attack, the tower will lose its firing ability for some time."));
    DialogManager::getInstance()->createDialog(statements, 23.0, [this](){
        setPaused(false);
        DialogManager::getInstance()->hide();
        dispatchAllowTouches((int)TouchReceiverFlag::all);
    });
}

void GameController::playDevastatorDialog() {
    if (DialogManager::getInstance()->m_isTelling) return;

    setPaused(true);
    dispatchDenyTouches((int)TouchReceiverFlag::all);
    DialogManager::getInstance()->show(m_uiNode, 60, Point(0.0, 480.0));
    vector<string> statements;
    statements.push_back(_("Commander, be ready! This time it will be very serious, goblins brought siege instruments. They have an excellent armor and very powerful attack skills!"));
    DialogManager::getInstance()->createDialog(statements, 23.0, [this](){
        setPaused(false);
        DialogManager::getInstance()->hide();
        dispatchAllowTouches((int)TouchReceiverFlag::all);
    });
}


void GameController::askForBuyLife(function<void()> failureCallback) { // TODO: that all needs serious refactoring.
    if (m_buyLifeTries > 0) {
        m_buyLifeTries --;
        m_uiController->askForBuyMagicianLife(failureCallback, !isEnoughCrystalsForBuyLifes());
    }
    else {
        m_uiController->disappearAskForBuyLifeDialog();
        failureCallback();
    }
}

bool GameController::isEnoughCrystalsForBuyLifes() {
    return GD->m_model.getLevelProgress()->crystals >=
            (LIFE_COST + LIFE_COST * m_uiController->m_askForBuyMagicianLifeQueriesCount);
}

void GameController::play2_4BossDialog() {
    if (DialogManager::getInstance()->m_isTelling) return;

    setPaused(true);
    dispatchDenyTouches((int)TouchReceiverFlag::all);
    DialogManager::getInstance()->show(m_uiNode, 60, Point(0.0, 480.0));
    vector<string> statements;
    statements.push_back(_("Commander, take heed! I don't know what they have crammed their ginors with, but they've become much tougher"));
    DialogManager::getInstance()->createDialog(statements, 23.0, [this](){
        setPaused(false);
        DialogManager::getInstance()->hide();
        dispatchAllowTouches((int)TouchReceiverFlag::all);
    });
}

void GameController::quitFromTutorial() {
    GD->exitFromPauseMenu();

}

void GameController::avoidMonsterStuck() {
    Vec2 positions[200];
    int count = 0;
    float threshold = 30.0;
    for (auto item = m_pMonsters.begin(); item != m_pMonsters.end(); ++item) {
        Monster *monster = *item;
        if(monster->isGoing()) {
            for (int i = 0; i < count; ++i) {
                if (monster->m_node->getPosition().getDistanceSq(positions[i]) < threshold) {
                    monster->slowDown();
                }
            }
            positions[count] = monster->m_node->getPosition();
            if (count < 200) count++;
        }
    }
}

void GameController::checkForKillAllBats(Monster* boss) {
    if (isSpawnProgramKilled(boss->m_config.spawnProgram)) {
        for (auto item = m_pMonsters.begin(); item != m_pMonsters.end(); ++item) {
            Monster *monster = *item;
            if (monster->m_config.data->id == "bat") {
                monster->recieveDamage(999999);

            }
        }
    }
    for (int i = 0; i < m_spawnPrograms.size(); ++i) {
        if (m_spawnPrograms[i]->isComplete()) continue;
        for (int j = 0; j < m_spawnPrograms[i]->m_monsterDatas.size(); ++j) {
            if (m_spawnPrograms[i]->m_monsterDatas[j].id == "bat") {
                m_spawnPrograms[i]->forceComplete();
                break;
            }
        }
    }
}


void GameController::magicDrawSymbolStarted() {
    if (m_tutorial) m_tutorialController->magicUserDrawSymbolStarted();
}



void GameController::catapultTouched(int index) {
    if (m_tutorial && index == 0)
        m_tutorialController->firstCatapultTouched();
}
