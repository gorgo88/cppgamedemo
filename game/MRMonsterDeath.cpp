//
// Created by Admin on 03.02.14.
//

#include "MRMonsterDeath.h"
#include "MRGameController.h"
#include "audio/include/SimpleAudioEngine.h"

using namespace myth;
using namespace CocosDenshion;

const std::string WALK_DOWN = "walkDown";
const std::string WALK_LEFT = "walkLeft";
const std::string WALK_RIGHT = "walkRight";
const std::string ATTACK = "attack";
const std::string DEATH = "death";

// don't worry! its a copy->paste from MonsterGinor

void MonsterDeath::setup(ConfigReader *reader) {
    Monster::setup(reader);
    m_deathState = DeathState::hiding;
    lifeBar->setVisible(false);
    m_timeout = 0.0;
    if (m_config.data->id == "ginorMaster") m_sprite->setPosition(m_sprite->getPosition() + Point(0.0, 20.0));
}

void MonsterDeath::fillData(ConfigReader *reader) {
    //m_lifeMax       = reader->fromPath("lifeMax")->toFloat() * 0.25 * m_config.data->level;
    m_lifeMax       = m_config.data->lifeMax != -1 ? m_config.data->lifeMax : reader->fromPath("lifeMax")->toFloat();

    //m_attackDelay   = reader->fromPath("attackDelay")->toFloat() / m_config.data->level;
    m_attackDelay   = m_config.data->attackDelay > 0.0 ? m_config.data->attackDelay : reader->fromPath("attackDelay")->toFloat();
    m_enterInterval = 3.0;

}

void MonsterDeath::fillAnimations(ConfigReader *reader) {
    m_animations = Dictionary::create();
    m_animations->retain();

    vector<string> animationNames;
    animationNames.push_back(ATTACK);
    animationNames.push_back(DEATH);
    animationNames.push_back(WALK_DOWN);
    animationNames.push_back(WALK_LEFT);
    animationNames.push_back(WALK_RIGHT);

    Animation *anim;
    for (int i = 0; i < animationNames.size(); ++i) {
        anim = Animation::create();
        char pattern[255];
        char fpsKey[255];
        sprintf(pattern, "gameScreen/monsters/%s/%s/%s.png", m_config.data->id.c_str(), animationNames[i].c_str(), "%04d");
        //sprintf(fpsKey, "animation.%s.fps", animationNames[i].c_str());
        Tools::addFramesToAnimation(anim, pattern, /*reader->fromPath(fpsKey)->toFloat()*/16.0);
        m_animations->setObject(anim, animationNames[i]);
    }

}

void MonsterDeath::update(float dt) {
    //Monster::update(dt);
    m_timeout-= dt;
    if (m_state == MonsterState::diying || m_state == MonsterState::dead) return;
    if (m_deathState == DeathState::goOut) {
        Monster::update(dt);
        return;
    }
    if (m_timeout <= 0 && m_state != MonsterState::diying) {
        if (m_isDeaf) {
            m_isDeaf = false;
            leave();
        } else if (m_deathState == DeathState::hiding)  {
            bossEnter();
        } else if (m_deathState == DeathState::waitingForAttack) {
            bossAttack();
        }

    }

}

void MonsterDeath::bossEnter() {
    m_lastZone = m_config.controller->getRandFreeZone(std::pair<int,int>(0,3), std::pair<int,int>(0,3));

    m_node->setPosition(m_lastZone->center);
    lifeBar->setVisible(true);

    m_deathState = DeathState::entering;
    m_sprite->setVisible(true);

    //shadow
    m_spriteHolder->getChildByTag(555)->setVisible(true);
    m_spriteHolder->getChildByTag(555)->setScale(0);
    m_spriteHolder->getChildByTag(555)->runAction(ScaleTo::create(2.0, 1.0));


    playAnimation(WALK_DOWN, true);
    m_sprite->setScale(0.0);
    auto scaleAction = ScaleTo::create(2.0, 1.0);
    m_sprite->runAction(Sequence::createWithTwoActions(
            scaleAction,
            CallFunc::create(CC_CALLBACK_0(MonsterDeath::enterAnimationComplete, this))
    ));
}

void MonsterDeath::bossAttack() {
    m_deathState = DeathState::attaking;
    playAnimation(ATTACK, false);
    if (attackSounds) {
        SimpleAudioEngine::getInstance()->playEffect(
                attackSounds->sounds.pickNext().c_str(),
                false, 1.0, 0.0, attackSounds->gain);
    }

}

void MonsterDeath::enterAnimationComplete() {
    if (m_isDeaf) return;
    if (m_isDefile) {
        m_deathState = DeathState::goOut;
        m_state = MonsterState::walkDown2;
    }
    else {
        m_deathState = DeathState::waitingForAttack;
        m_timeout = m_attackDelay;
    }
}

void MonsterDeath::animationComplete() {
    Monster::animationComplete();
    if (m_state == MonsterState::walkDown && m_deathState == DeathState::attaking)
        attackAnimationComplete();
}

void MonsterDeath::attackAnimationComplete() {
    //if (m_isDeaf) return; // what it mean?
    m_config.controller->bossAttack(this);
    leave();

}


void MonsterDeath::dieMonster() {
    Monster::dieMonster();
    m_sprite->setScale(1.0); // if dies during leave

}

void MonsterDeath::leaveAnimationComplete() {
    if (m_isDefile) {

        m_node->setPosition(Point(384, 200.0));

        m_deathState = DeathState::entering;
        m_sprite->setVisible(true);

        playAnimation(WALK_DOWN, true);
        m_sprite->setScale(0.0);
        Vector<FiniteTimeAction*> actions;
        actions.pushBack(DelayTime::create(Tools::randFloat()*2.5));
        actions.pushBack(ScaleTo::create(2.0, 1.0));
        actions.pushBack(CallFunc::create(CC_CALLBACK_0(MonsterDeath::enterAnimationComplete, this)));
        m_sprite->runAction(Sequence::create(actions));
        //shadow
        m_spriteHolder->getChildByTag(555)->setScale(0);
        m_spriteHolder->getChildByTag(555)->runAction(ScaleTo::create(2.0, 1.0));

    }
    else {
        m_deathState = DeathState::hiding;
        m_sprite->setVisible(false);
        lifeBar->setVisible(false);
        m_spriteHolder->getChildByTag(555)->setVisible(false);

        m_timeout = m_enterInterval;
    }
}

void MonsterDeath::recieveDamage(float damage) {
    if (m_deathState == DeathState::hiding)
        return;
    Monster::recieveDamage(damage);
    if (m_state != MonsterState::diying && isActive() && !m_isDeaf) {
        m_isDeaf = true;
        m_timeout = 0.1;
    }
}

bool MonsterDeath::isActive() {
    return Monster::isActive();
}

void MonsterDeath::leave() {
    m_deathState = DeathState::leaving;
    playAnimation(WALK_DOWN, true);
    auto scaleAction = ScaleTo::create(2.0, 0.0);
    m_sprite->runAction(Sequence::createWithTwoActions(
            scaleAction,
            CallFunc::create(CC_CALLBACK_0(MonsterDeath::leaveAnimationComplete, this))
    ));
    //shadow
    m_spriteHolder->getChildByTag(555)->runAction(ScaleTo::create(2.0, 0.0));

}

void MonsterDeath::winDefile() {
    m_isDefile = true;
    if (m_deathState != DeathState::leaving) {
        leave();
    }

}

vector <Zone *> MonsterDeath::getCurrentZones() {
    return vector<Zone*> {m_lastZone};
};


bool MonsterDeath::isGoing() {
    return false;
}
