//
// Created by Admin on 12.03.15.
//

#include "MRMonsterDragon.h"
#include "MRGameController.h"
#include <core/CheetahAtlasHelper.h>
#include <audio/include/SimpleAudioEngine.h>

using namespace myth;
using namespace std;
using namespace CocosDenshion;

const std::string HEAD_IN = "headIn";
const std::string HEAD_ATTACK = "headAttack";
const std::string HEAD_OUT = "headOut";
const std::string TAIL_IN = "tailIn";
const std::string TAIL_OUT= "tailOut";
const std::string BODY = "body";
const std::string DEATH = "death";
const std::string SHAKE = "shake";
const std::string SCREAM = "scream";



vector<string> MonsterDragon::getAllStandardAnimations() {
    vector<string> animationNames = {
            HEAD_IN,
            HEAD_ATTACK,
            HEAD_OUT,
            TAIL_IN,
            TAIL_OUT,
            BODY,
            DEATH,
            SHAKE,
            SCREAM
    };
    return animationNames;
}


void MonsterDragon::setup(ConfigReader *reader) {
    m_blood = nullptr;
    m_speed = m_config.speed;
    m_slowing = false;
    m_initSpeed = 0.0;
    fillData(reader);

    m_life = m_lifeMax;


    m_node = nullptr;
    m_spriteHolder = nullptr;
    m_sprite = nullptr;

    m_state = MonsterState::none;

    lifeBar = LifeBar::create();

    m_config.parentNode->addChild(lifeBar, m_config.zOrder + 1);
    lifeBar->setPosition(-50.0, -50.0);
    lifeBar->setRotation(-90.0);
    updateLifeBar();
    lifeBar->setVisible(false);

    m_isDamageEmited = false;
    m_isAttackPlaying = false;

    m_vulnerabilityHead = false;
    m_vulnerabilityTail = false;

    m_isDefile = -1;

    fillAnimations(reader);
    fillSounds(reader);
    fillPrice(reader);
/*
*
*   ( * ) - damageNode
*    ---
*   ( 8 ) - node // forget. now its the same island nodes
*/
    float nodeScale = 1.0f;
    if (reader->hasPath("nodeScale"))  {
        nodeScale = reader->fromPath("nodeScale")->toFloat();
    }
    for (int i = 0; i < SEGMENT_COUNT; ++i) {
        Node *node = Node::create();
        Node *damageNode = Node::create();
        Node *spriteHolder = Node::create();
        Sprite *sprite = Sprite::create();
        node->addChild(spriteHolder);
        spriteHolder->addChild(sprite);
        m_segments[i].node = node;
        m_segments[i].damageNode = damageNode;
        m_segments[i].sprite = sprite;
        m_segments[i].spriteHolder = spriteHolder;
        m_config.parentNode->addChild(node, m_config.zOrder);
        m_config.parentNode->addChild(damageNode, m_config.zOrder);
        m_segments[i].node->setVisible(false);
        node->setScale(1.0f);
        m_segments[i].spriteHolder->setPosition(Vec2(-4, 7));
    }
    headInSegment(0);

}


void MonsterDragon::headInSegment(int segmentIndex) {
    using namespace std;
    if (m_isDefile > 2) return;
    m_headSegmentIndex = segmentIndex;
    Zone *zone;
    if (m_isDefile == -1) {
        zone = m_config.controller->getRandFreeZone(std::pair<int,int>(0,3), std::pair<int,int>(0,3));
        m_segments[segmentIndex].damageNode->setPosition(zone->center);
    }
    else {
        m_segments[segmentIndex].damageNode->setPosition(Vec2(m_config.turn2X, m_config.tunt1Y - 100));
        zone = nullptr;
        m_isDefile ++;
    }
    // issue #748
    //float dy = m_config.controller->zones[1][0].center.y - m_config.controller->zones[0][0].center.y;
    m_segments[segmentIndex].node->setPosition(m_segments[segmentIndex].damageNode->getPosition()/* + Vec2(0, -dy)*/);
    playAnimationCallback(HEAD_IN, false, m_segments[segmentIndex].sprite, [this, segmentIndex](){
        if (m_isDefile == -1) {
            m_segments[segmentIndex].attackTimer = m_attackInt;
            m_vulnerabilityHead = true;
        }
        else forceHeadOut();
    });
    int tailIndex = offsetSegmentIndex(segmentIndex, -2);
    lifeBar->setPosition(m_segments[tailIndex].damageNode->getPosition() + Vec2(-50,-50));
    playAnimationCallback(TAIL_IN, false, m_segments[tailIndex].sprite, [this, segmentIndex](){
        // stop body animating
        int bodyIndex = offsetSegmentIndex(segmentIndex, -1);
        m_segments[bodyIndex].sprite->stopAllActions();
        m_vulnerabilityTail = true;
        lifeBar->setVisible(true);

    });
    m_segments[segmentIndex].node->setVisible(m_isDefile <= 1);
    m_segments[segmentIndex].zone = zone;
}

int MonsterDragon::offsetSegmentIndex(int old, int offset) {
    return (old + offset + SEGMENT_COUNT * 3) % SEGMENT_COUNT; // % works only with positive
}


void MonsterDragon::animationComplete() {
    //Monster::animationComplete();
}

void MonsterDragon::fillData(ConfigReader *reader) {
    m_attackInt = reader->fromPath("attackDelay")->toFloat();
    if (m_config.data->attackDelay != -1) m_attackDelay = m_config.data->attackDelay;
    m_attackDelay = reader->fromPath("attackDelayAnimation")->toFloat();

    m_idleTime = reader->fromPath("idleTime")->toFloat();
    m_deafTime = reader->fromPath("deafTime")->toFloat();
    m_lifeMax  = m_config.data->lifeMax != -1
            ? m_config.data->lifeMax
            : reader->fromPath("lifeMax")->toFloat();

}


void MonsterDragon::fillSounds(ConfigReader *reader) {
    Monster::fillSounds(reader);
    m_attackSnd.first = reader->fromPath("sounds.attack.variants.0")->toString()->_string;
    m_attackSnd.second = reader->fromPath("sounds.attack.gain")->toFloat();

    m_deafSnd.first = reader->fromPath("sounds.deaf.variants.0")->toString()->_string;
    m_deafSnd.second = reader->fromPath("sounds.deaf.gain")->toFloat();

    m_damageSnd.first = reader->fromPath("sounds.damage.variants.0")->toString()->_string;
    m_damageSnd.second = reader->fromPath("sounds.damage.gain")->toFloat();
}

bool updateTimer(float *timer, float dt) {
    if (*timer > 0) {
        *timer -= dt;
        if (*timer <= 0) {
            *timer = -1;
            return true;
        }
    }
    return false;
}

void MonsterDragon::update(float dt) {
    for (int i = 0; i < SEGMENT_COUNT; ++i) {
        if (updateTimer(&m_segments[i].attackTimer, dt)) {
            m_segments[i].attackDelayTimer = m_attackDelay;
            m_vulnerabilityHead = false;
            playAnimationCallback(HEAD_ATTACK, false, m_segments[i].sprite, [this, i](){
                forceHeadOut();
            });
        }
        if (updateTimer(&m_segments[i].attackDelayTimer, dt)) {
            //actual attack
            m_config.controller->dragonAttack(this, m_segments[i].zone);
            SimpleAudioEngine::getInstance()->playEffect(m_attackSnd.first.c_str(), false, 1, 0, m_attackSnd.second);

        }
        if (updateTimer(&m_segments[i].idleTimer, dt)) {
            headInSegment(offsetSegmentIndex(i, 1));
        }
        if (updateTimer(&m_segments[i].deafTimer, dt)) {
            forceHeadOut();
        }
    }
}


vector<Node *> MonsterDragon::getNodes() {
    vector<Node*> nodes;
    for (int i = 0; i < SEGMENT_COUNT; ++i)
        if (isSegmentPlaced(i)) {
            nodes.push_back(m_segments[i].damageNode);
            nodes.push_back(m_segments[i].node);
        }
    return nodes;
}

vector<Zone*> MonsterDragon::getCurrentZones() {
    vector<Zone*> zones;
    for (int i = 0; i < SEGMENT_COUNT; ++i)
        if (isSegmentPlaced(i)) {
            zones.push_back(m_segments[i].zone);
            //zones.push_back(&m_config.controller->zones[m_segments[i].zone->rowIndex - 1][m_segments[i].zone->colIndex]);
        }
    return zones;
}

MonsterDragon::~MonsterDragon() {
//    for (int i = 0; i < SEGMENT_COUNT; ++i) {
//        m_segments[i].node->retain();
//    }
}

bool MonsterDragon::isSegmentPlaced(int index) {
    return m_segments[index].zone != nullptr;
}

void MonsterDragon::receiveDamage(float damage, cocos2d::Node *targetNode) {
    // determine segment
    for (int i = 0; i < SEGMENT_COUNT; ++i) {
        if (m_segments[i].damageNode == targetNode) {
            if (m_stunCount < 3 && m_headSegmentIndex == i && m_vulnerabilityHead) {
                m_stunCount += 1;
                stun();
                //forceHeadOut();
            }
            if (offsetSegmentIndex(m_headSegmentIndex, -2) == i && m_vulnerabilityTail) {

                recieveDamage(damage);
                SimpleAudioEngine::getInstance()->playEffect(m_damageSnd.first.c_str(), false, 1, 0, m_damageSnd.second);

                if (m_life > 0 && (m_segments[m_headSegmentIndex].attackTimer > 0 || m_segments[m_headSegmentIndex].deafTimer > 0))
                    playAnimation(SCREAM, false, m_segments[m_headSegmentIndex].sprite);
            }
            break;
        }
    }
}

void MonsterDragon::stun() {
    playAnimation(SCREAM, false, m_segments[m_headSegmentIndex].sprite);
    m_segments[m_headSegmentIndex].deafTimer = m_deafTime;
    m_segments[m_headSegmentIndex].attackTimer = -1;
    m_segments[m_headSegmentIndex].attackDelayTimer = -1;
    SimpleAudioEngine::getInstance()->playEffect(m_deafSnd.first.c_str(), false, 1, 0, m_deafSnd.second);
//    float deg = 10;
//    float t = 0.25f;
//    Vector<FiniteTimeAction*> actions;
//    actions.pushBack(RotateTo::create(t, deg));
//    actions.pushBack(RotateTo::create(t, -deg));
//    actions.pushBack(RotateTo::create(t, deg / 2));
//    actions.pushBack(RotateTo::create(t, 0));
//    m_segments[m_headSegmentIndex].spriteHolder->runAction(Sequence::create(actions));
//
//    SimpleAudioEngine::getInstance()->playEffect(m_deafSnd.first.c_str(), false, 1, 0, m_deafSnd.second);
}

void MonsterDragon::forceHeadOut(bool endless) {
    m_vulnerabilityHead = false;
    int i = m_headSegmentIndex;

    m_stunCount = 0;

    m_segments[i].attackTimer = -1;
    m_segments[i].attackDelayTimer = -1;
    playAnimationCallback(HEAD_OUT, false, m_segments[i].sprite, [this, i, endless](){
        playAnimation(BODY, true, m_segments[i].sprite);
        m_segments[i].idleTimer = m_idleTime;

    });

    playAnimation(BODY, true, m_segments[offsetSegmentIndex(i, -1)].sprite);

    m_vulnerabilityTail = false;
    lifeBar->setVisible(false);
    playAnimationCallback(TAIL_OUT, false, m_segments[offsetSegmentIndex(i, -2)].sprite, [this, i](){
        m_segments[offsetSegmentIndex(i, -2)].node->setVisible(false);
        if (m_isDefile > 2)
            m_config.controller->monsterHasLeft(this); // end point
    });
}

bool MonsterDragon::isActive() {
    return m_life > 0;
}

void MonsterDragon::dieMonster() {
    m_deathPosition = m_segments[m_headSegmentIndex].node->getPosition();
    m_state = MonsterState::diying;
    lifeBar->removeFromParent();
    for (int i = 0; i < SEGMENT_COUNT; ++i) {
        if (i == 0)
            playAnimationCallback(DEATH, false, m_segments[i].sprite, [this](){
                deathAnimationComplete();
            });
        else
            playAnimation(DEATH, false, m_segments[i].sprite);
    }
    //sound
    playSoundFromSet(deathSounds);
}

bool MonsterDragon::shouldUseCommonDeathAnimation() {
    return false;
}

void MonsterDragon::deathAnimationComplete() {
    m_state = MonsterState::dead;
    m_config.controller->monsterWasKilled(this);
    //todo: remove nodes from graph
}

void MonsterDragon::winDefile() {
    m_isDefile = 0;
    m_idleTime = 0.2f; // something short
    //forceHeadOut();
}
