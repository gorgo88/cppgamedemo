//
// Created by Admin on 14.01.14.
//

#include "MRMonsterGinor.h"
#include "MRGameController.h"
#include "audio/include/SimpleAudioEngine.h"

using namespace myth;
using namespace cocos2d;
using namespace CocosDenshion;

const std::string WALK_DOWN = "walkDown";
const std::string ATTACK = "attack";
const std::string DEATH = "death";

void MonsterGinor::setup(ConfigReader *reader) {
    Monster::setup(reader);
    m_ginorState = GinorState::hiding;
    lifeBar->setVisible(false);
    m_spriteHolder->getChildByTag(555)->setScale(0);
}

void MonsterGinor::fillAnimations(ConfigReader *reader) {
    m_animations = Dictionary::create();
    m_animations->retain();

    vector<string> animationNames;
    animationNames.push_back(ATTACK);
    animationNames.push_back(WALK_DOWN);
    animationNames.push_back(DEATH);

    Animation *anim;
    for (int i = 0; i < animationNames.size(); ++i) {
        anim = Animation::create();
        char pattern[255];
        char fpsKey[255];
        sprintf(pattern, "gameScreen/monsters/%s/%s/%s.png", m_config.data->id.c_str(), animationNames[i].c_str(), "%04d");
        sprintf(fpsKey, "animation.%s.fps", animationNames[i].c_str());
        Tools::addFramesToAnimation(anim, pattern, reader->fromPath(fpsKey)->toFloat());
        m_animations->setObject(anim, animationNames[i]);
    }

}

void MonsterGinor::fillData(ConfigReader *reader) {
    //m_lifeMax = reader->fromPath("lifeMax")->toFloat() * 0.25 * m_config.data->level;
    m_lifeMax       = m_config.data->lifeMax != -1 ? m_config.data->lifeMax : reader->fromPath("lifeMax")->toFloat();

    m_enterInterval = reader->hasPath("enterInterval") ? reader->fromPath("enterInterval")->toFloat() : 5.0;
    m_attackDelay = reader->hasPath("attackDelay") ? reader->fromPath("attackDelay")->toFloat() : 2.0;
    m_timeout = 0.0;
}

void MonsterGinor::update(float dt) {
    m_timeout-= dt;
    if (m_timeout <= 0) {
        if (m_ginorState == GinorState::hiding)  {
            ginorEnter();
        } else if (m_ginorState == GinorState::waitingForAttack) {
            ginorAttack();
        }

    }
}

void MonsterGinor::ginorEnter() {
    m_lastZone = m_config.controller->getRandFreeZone(std::pair<int,int>(0,2), std::pair<int,int>(0,3));

    m_node->setPosition(m_lastZone->center);
    lifeBar->setVisible(true);

    m_ginorState = GinorState::entering;
    m_sprite->setVisible(true);

    //shadow
    m_spriteHolder->getChildByTag(555)->setVisible(true);


    playAnimation(WALK_DOWN, true);
    m_sprite->setScale(0.0);
    auto scaleAction = ScaleTo::create(5.0, 1.0);
    m_sprite->runAction(Sequence::createWithTwoActions(
            scaleAction,
            CallFunc::create(CC_CALLBACK_0(MonsterGinor::enterAnimationComplete, this))
    ));
    //shadow
    m_spriteHolder->getChildByTag(555)->setScale(0);
    m_spriteHolder->getChildByTag(555)->runAction(ScaleTo::create(5.0, 1.0));

}

void MonsterGinor::enterAnimationComplete() {
    m_ginorState = GinorState::waitingForAttack;
    m_timeout = m_attackDelay;
}

void MonsterGinor::ginorAttack() {
    m_ginorState = GinorState::attaking;
    playAnimation(ATTACK, false);
}

void MonsterGinor::attackAnimationComplete() {
    m_ginorState = GinorState::leaving;
    playAnimation(WALK_DOWN, true);
    auto scaleAction = ScaleTo::create(5.0, 0.0);
    m_sprite->runAction(Sequence::createWithTwoActions(
            scaleAction,
            CallFunc::create(CC_CALLBACK_0(MonsterGinor::leaveAnimationComplete, this))
    ));
    m_spriteHolder->getChildByTag(555)->runAction(ScaleTo::create(5.0, 0));

    m_config.controller->ginorAttack(this);
}

void MonsterGinor::leaveAnimationComplete() {
    m_ginorState = GinorState::hiding;
    m_sprite->setVisible(false);
    m_spriteHolder->getChildByTag(555)->setVisible(false);

    lifeBar->setVisible(false);

    m_timeout = m_enterInterval;
}

void MonsterGinor::animationComplete() {
    //Monster::animationComplete();
    if (m_ginorState == GinorState::attaking)
        attackAnimationComplete();
    if (m_ginorState == GinorState::ginorDie)
        deathAnimationComplete();
}

void MonsterGinor::dieMonster() {
    CCLOG("Die ginor");
    m_spriteHolder->getChildByTag(555)->removeFromParent(); //shadow
    m_state = MonsterState::diying;
    m_ginorState = GinorState::ginorDie;
    lifeBar->removeFromParent();
    playAnimation(DEATH, false);
    if (deathSounds) {
        SimpleAudioEngine::getInstance()->playEffect(
                deathSounds->sounds.pickNext().c_str(),
                false, 1.0, 0.0, deathSounds->gain);
    }
}


void MonsterGinor::recieveDamage(float damage) {
    if (m_ginorState == GinorState::hiding)
        return;
    Monster::recieveDamage(damage);
    if (m_life > 0 &&
            (m_ginorState == GinorState::waitingForAttack
                    || m_ginorState == GinorState::attaking
                    || m_ginorState == GinorState::entering)) {
        m_ginorState = GinorState::leaving;
        playAnimation(WALK_DOWN, true);
        auto scaleAction = ScaleTo::create(5.0, 0.0);
        m_sprite->runAction(Sequence::createWithTwoActions(
                scaleAction,
                CallFunc::create(CC_CALLBACK_0(MonsterGinor::leaveAnimationComplete, this))
        ));
        //shadow
        m_spriteHolder->getChildByTag(555)->runAction(ScaleTo::create(5.0, 0));
    }

}

bool MonsterGinor::isActive() {
    //return m_ginorState != GinorState::ginorDie;
    return Monster::isActive();
}

vector <Zone *> MonsterGinor::getCurrentZones() {
    return vector<Zone*> {m_lastZone};
};

bool MonsterGinor::isGoing() {
    return false;
}
