//
// Created by gorgo on 28.03.15.
//

#ifndef _MYTHRANDIA_MRIMONSTERBOSS_H_
#define _MYTHRANDIA_MRIMONSTERBOSS_H_

namespace myth {
    class IMonsterBoss {
    public:
        virtual void winDefile() = 0;
    };
}

#endif //_MYTHRANDIA_MRIMONSTERBOSS_H_
