//
// Created by Admin on 19.12.13.
//



#ifndef __LifeBar_H_
#define __LifeBar_H_

#include "cocos2d.h"
using namespace cocos2d;

namespace myth {
class LifeBar : public Node{
public:
    virtual ~LifeBar() {
        //log("life bar destruct");
    }

    static LifeBar *create();
    void setValue(float val);
private:
    virtual bool init() override;

    Sprite *m_empty;
    Sprite *m_full;
};

}
#endif //__LifeBar_H_
