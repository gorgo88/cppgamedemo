//
// Created by Admin on 16.01.14.
//

#include "core/myth.h"
#include "MRUIWidgets.h"
#include "MRGameUIController.h"
#include "MRGameController.h"

#include "audio/include/SimpleAudioEngine.h"

using namespace myth;
using namespace mgui;
using namespace cocos2d;
using namespace CocosDenshion;
using namespace ui;
using namespace upgrades;

static const TextureResType plistTex = TextureResType::PLIST;


std::string getBulletFrameName(int upgradeIndex) {
    char frameName[100];
    if (upgradeIndex < 13) {
        int i = upgradeIndex - 1;
        int rowIndex = i % 4;
        int colIndex = static_cast<int>(floorf(static_cast<float>(i) / 4.0));
        sprintf(frameName, "gameScreen/ui/upgrades/tech/bullets/item%02d.png", i);
    }
    else {
        int i = upgradeIndex - 13;
        sprintf(frameName, "gameScreen/ui/upgrades/tech/bullets/super/item%02d.png", i);
    }
    return std::string(frameName);
}

bool mgui::Button::init() {
    if (cocos2d::ui::Button::init()) {
        loadTextureNormal("commonUI/dialog/buttonActive.png", plistTex);
        loadTexturePressed("commonUI/dialog/buttonInactive.png", plistTex);
        TTFConfig conf;
        conf.fontSize = 20;
        conf.fontFilePath = "IZH.ttf";
        m_label = Label::createWithTTF(conf, "", TextHAlignment::CENTER , 100);
        m_label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
        m_label->setPosition(getSize()*0.5);
        addProtectedChild(m_label);
        setTouchEnabled(true);
        return true;
    }

    return false;
}

void mgui::Button::setTitleTextCustom(const std::string text) {
    //Button::setTitleText(text);
    m_label->setString(text);
}

mgui::Button* mgui::Button::create(std::string label) {
    mgui::Button *widget = new mgui::Button();
    if (widget && widget->init()) {
        widget->setTitleTextCustom(label);
        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

bool DialogBox::init(int textFontSize) {
    if (Layout::init()){
        setLayoutType(LayoutType::RELATIVE);
        setName("dialogPanel");
        auto lParam = RelativeLayoutParameter::create();
        lParam->setRelativeName("dialogPanel");
        setLayoutParameter(lParam);
        setBackGroundImage("commonUI/dialog/panel.png", plistTex);
        //auto bgImage = ui::ImageView::create("commonUI/dialog/panel.png", plistTex);
        //addChild(bgImage);
        setContentSize(_backGroundImageTextureSize);
        //setContentSize(bgImage->getContentSize());
        mgui::Button *okBtn = mgui::Button::create("ok");
        mgui::Button *cancelBtn = mgui::Button::create("cancel");
        auto okLParam = RelativeLayoutParameter::create();
        auto cancelLParam = RelativeLayoutParameter::create();
        okBtn->setTag(0);
        cancelBtn->setTag(1);
        Margin margin;
        margin.bottom = 20.0;
        margin.left = 20.0;
        margin.right = 20.0;
        okLParam->setMargin(margin);
        okLParam->setAlign(RelativeAlign::PARENT_LEFT_BOTTOM);
        okLParam->setRelativeToWidgetName("dialogPanel");
        okLParam->setRelativeName("okButton");
        cancelLParam->setMargin(margin);
        cancelLParam->setAlign(RelativeAlign::PARENT_RIGHT_BOTTOM);
        cancelLParam->setRelativeToWidgetName("dialogPanel");
        cancelLParam->setRelativeName("cancelButton");


        okBtn->setLayoutParameter(okLParam);
        cancelBtn->setLayoutParameter(cancelLParam);
        addChild(okBtn);
        addChild(cancelBtn);
        m_okButton = okBtn;
        m_cancelButton = cancelBtn;

        TTFConfig conf;
        conf.fontFilePath = "IZH.ttf";
        conf.fontSize = textFontSize;
        auto label = Label::createWithTTF(conf, "", TextHAlignment::CENTER, getSize().width);
        label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
        label->setPosition(Vec2(getSize() * 0.5) + Vec2(0.0, 20.0));
        addChild(label);
        m_titleField = label;
        return true;
    }
    return false;
}

DialogBox *DialogBox::create(std::string title, std::string okLabel, std::string cancelLabel) {
    DialogBox *widget = DialogBox::create(33);
    widget->m_okButton->setTitleTextCustom(okLabel);
    widget->m_cancelButton->setTitleTextCustom(cancelLabel);
    widget->m_titleField->setString(title);
    return widget;
}

DialogBox *DialogBox::create(std::string title, std::string okLabel, std::string cancelLabel, int textFontSize) {
    DialogBox *widget = DialogBox::create(textFontSize);
    widget->m_okButton->setTitleTextCustom(okLabel);
    widget->m_cancelButton->setTitleTextCustom(cancelLabel);
    widget->m_titleField->setString(title);
    return widget;
}

DialogBox *DialogBox::create(int textFontSize) {
    DialogBox *widget = new DialogBox();
    if (widget && widget->init(textFontSize)) {

        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

Item *Item::create() {
    Item *widget = new Item();
    if (widget && widget->init()) {

        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}


Item *Item::create(std::string iconFrameName, float priceGold, float priceCrystal) {
    Item *widget = Item::create();
    widget->setup(iconFrameName, priceGold, priceCrystal, "%.0f");
    return widget;
}

Item *Item::create(std::string iconFrameName, float priceGold, float priceCrystal, char const *priceGoldFormat) {
    Item *widget = Item::create();
    widget->setup(iconFrameName, priceGold, priceCrystal, priceGoldFormat);
    return widget;
}


void Item::setup(std::string iconFrameName, float priceGold, float priceCrystal, char const *priceGoldFormat) {
    setSize(Size(100.0, 100.0));
    setLayoutType(LayoutType::RELATIVE);
    //setBackGroundColor(Color3B::BLUE);
    //setBackGroundColorType(LayoutBackGroundColorType::LAYOUT_COLOR_SOLID);
    auto lParam = RelativeLayoutParameter::create();

    //lParam->setRelativeName(
    //        static_cast<std::string>("itemLayout").append(std::to_string(_ID)).c_str());
    lParam->setRelativeName("itemLayout");
    m_icon = ui::Button::create();
    m_icon->loadTextureNormal(iconFrameName.c_str(), plistTex);
    m_icon->loadTexturePressed(iconFrameName.c_str(), plistTex);
    m_icon->setTouchEnabled(true);

    m_icon->addTouchEventListener(this, toucheventselector(Item::iconTouched));
    auto iconLParam = RelativeLayoutParameter::create();
    iconLParam->setAlign(RelativeAlign::PARENT_BOTTOM_CENTER_HORIZONTAL);
    iconLParam->setRelativeToWidgetName(lParam->getRelativeName());
    iconLParam->setRelativeName("iconButton");

    m_icon->setLayoutParameter(iconLParam);
    addChild(m_icon, 2);

    auto priceBg = ImageView::create();
    priceBg->loadTexture("gameScreen/ui/upgrades/itemPriceBack.png", plistTex);
    priceBg->setTag(1);
    priceBg->setLayoutParameter(RelativeLayoutParameter::create());
    addChild(priceBg, -3);

    setPriceBottomMargin (-17.0);


    TTFConfig conf;
    conf.glyphs = GlyphCollection::ASCII;
    conf.fontSize = 36;
    conf.fontFilePath = "IZH.ttf";
    m_priceGold = Label::createWithTTF(conf, "00", TextHAlignment::CENTER , 50);
    m_priceGold->setScale(0.5f);
    m_priceGold->setPosition(Point(priceBg->getSize()) * 0.5 + Point(4.0,-15.0));
    m_priceGold->setAnchorPoint(Point::ANCHOR_MIDDLE_BOTTOM);
    char priceGoldStr[50];
    sprintf(priceGoldStr, priceGoldFormat, priceGold);
    m_priceGold->setString(priceGoldStr);
    priceBg->addProtectedChild(m_priceGold, 1);





    /*m_priceCrystal = TextField::create();

    m_priceCrystal->setFontName("Marker Felt.ttf");
    m_priceCrystal->setFontSize(9);
    m_priceCrystal->setPosition(Point(7.0,-7.0));
    m_priceCrystal->setAnchorPoint(Point(0.0,0.5));
    char priceCrystalStr[50];
    sprintf(priceCrystalStr, "%.0f", priceCrystal);
    m_priceCrystal->setText(priceCrystalStr);
    priceBg->addChild(m_priceCrystal, 2);
    */

}


void Item::setValue(std::string string) {
    setValue(string, ValueType::attack);
}

void Item::setValue(std::string string, ValueType type) {
    if (type != ValueType::none) {
        auto backSprite = type == ValueType::attack
                ? "gameScreen/ui/upgrades/itemValueBack.png" :
                "gameScreen/ui/upgrades/itemValueBackShield.png";
        auto bgSprite = ui::ImageView::create(backSprite, TextureResType::PLIST);
        bgSprite->setScaleX(1.2);
        addChild(bgSprite, -200);
        bgSprite->getLayoutParameter(LayoutParameter::Type::RELATIVE)->setMargin(Margin(5.0,-16.0,0.0,.0));

    }

    TTFConfig conf;
    conf.customGlyphs = "0123456789-";
    conf.fontFilePath = "IZH.ttf";
    conf.glyphs = GlyphCollection::CUSTOM;
    conf.fontSize = 30;
    auto label = Label::createWithTTF(conf, string.c_str(), TextHAlignment::LEFT , 90);
    addProtectedChild(label,1);
    label->setColor(Color3B::BLACK);
    label->setScale(0.5f);
    label->setPosition(Point(52.0, 102.0) + Point(-25.0,-6.0));
    label->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
}


void Item::addCrystalIcon() {
    // crystalIcon
    ImageView *priceBg = static_cast<ImageView*>(getChildByTag(1));
    auto crystalIcon = ImageView::create();
    crystalIcon->loadTexture("gameScreen/ui/hud/crystalIcon2x.png", plistTex);
    priceBg->addChild(crystalIcon, 1);
    crystalIcon->setPosition(Point(priceBg->getSize()) * 0.5 + Point(-22.0, -5.0));
    crystalIcon->setScale(0.6f); //issue #658
    m_crystalIcon = crystalIcon;
}

void Item::addGoldIcon() {
    //goldIcon
    ImageView *priceBg = static_cast<ImageView*>(getChildByTag(1));
    auto goldIcon = ImageView::create();
    goldIcon->loadTexture("gameScreen/ui/hud/goldIcon2x.png", plistTex);
    priceBg->addChild(goldIcon, 1);
    goldIcon->setPosition(Point(priceBg->getSize()) * 0.5 + Point(-22.0, -5.0));
    goldIcon->setScale(0.4);
    m_goldIcon = goldIcon;
}

void Item::setPriceBottomMargin (float margin) {
    auto priceBg = dynamic_cast<ImageView*>(getChildByTag(1));

    auto priceBgLParam = dynamic_cast<RelativeLayoutParameter*>(priceBg->getLayoutParameter(LayoutParameterType::RELATIVE));
    priceBgLParam->setMargin(Margin(0.0,0.0,0.0,margin));
    priceBgLParam->setAlign(RelativeAlign::PARENT_BOTTOM_CENTER_HORIZONTAL);
    priceBgLParam->setRelativeName("priceBgImage");
    priceBg->setLayoutParameter(priceBgLParam);
}

void Item::iconTouched(Object *sender, TouchEventType type) {
    if (type == TouchEventType::BEGAN) {
        m_icon->setScale(0.9);
    }
    else if (type == TouchEventType::ENDED
            || type == TouchEventType::CANCELED) {
        m_icon->setScale(1.0);
    }

}

void Item::setHighlight(bool isHighlighted) {
    //setBrightStyle(ui::BrightStyle::BRIGHT_HIGHLIGHT);
    //setBright(isHighlighted);
    if (!isHighlighted) {
        function<void(ui::Widget*)>handler = [&] (ui::Widget* w) {
            w->setColor(Color3B(60,60,60));
            for (int i = 0; i < w->getChildren().size(); ++i) {
                handler(dynamic_cast<ui::Widget*>(w->getChildren().at(i)));
            }
        };
        handler(this);
    }
    Widget *equipedMask = dynamic_cast<ui::Widget*>(m_icon->getChildByName("equipedMask"));
    if (equipedMask) equipedMask->setColor(Color3B::WHITE);

}

void Item::addArrow() {
    //arrow
    ImageView *arrow = ui::ImageView::create();
    arrow->loadTexture("gameScreen/ui/upgrades/tech/upArrow.png", plistTex);
    addChild(arrow,10);
    arrow->getLayoutParameter(LayoutParameter::Type::RELATIVE)->setMargin(Margin(50.0f,35.0f,0.0f,.0f));
    arrow->setTouchEnabled(false);

    arrow->setScale(0.6f);
    arrow->runAction(RepeatForever::create(
            Sequence::createWithTwoActions(
                    EaseSineOut::create(ScaleTo::create(0.4, 0.6)),
                    EaseSineIn::create(ScaleTo::create(0.4, 0.45))
            )));
}


TypeSelectPanel *TypeSelectPanel::create(GameUIController *controller) {
    TypeSelectPanel *widget = new TypeSelectPanel();
    if (widget && widget->init()) {
        widget->m_controller = controller;
        widget->setup();
        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

void TypeSelectPanel::setup() {
    setName("typeSelect");
    setSize(Size(300.0,300.0));
    cocos2d::ui::Button *magicIcon, *moneyIcon, *towerIcon, *bridgeIcon;
    magicIcon = ui::Button::create();
    magicIcon->loadTextureNormal("gameScreen/ui/upgrades/magicIcon.png", plistTex);
    magicIcon->loadTexturePressed("gameScreen/ui/upgrades/magicIcon.png", plistTex);
    magicIcon->setPosition(Point(250.0,500.0));
    magicIcon->setTouchEnabled(true);
    magicIcon->setTag(static_cast<int>(UIUpgradeType::magic));
    addChild(magicIcon);

    moneyIcon = ui::Button::create();
    moneyIcon->loadTextureNormal("gameScreen/ui/upgrades/moneyIcon.png", plistTex);
    moneyIcon->loadTexturePressed("gameScreen/ui/upgrades/moneyIcon.png", plistTex);
    moneyIcon->setPosition(Point(530.0,490.0));
    moneyIcon->setTouchEnabled(true);
    moneyIcon->setTag(static_cast<int>(UIUpgradeType::money));

    addChild(moneyIcon);

    towerIcon = ui::Button::create();
    towerIcon->loadTextureNormal("gameScreen/ui/upgrades/techIcon.png", plistTex);
    towerIcon->loadTexturePressed("gameScreen/ui/upgrades/techIcon.png", plistTex);
    towerIcon->setPosition(Point(530.0,720.0));
    towerIcon->setTouchEnabled(true);
    towerIcon->setTag(static_cast<int>(UIUpgradeType::tower));

    addChild(towerIcon);

    bridgeIcon = ui::Button::create();
    bridgeIcon->loadTextureNormal("gameScreen/ui/upgrades/wallIcon.png", plistTex);
    bridgeIcon->loadTexturePressed("gameScreen/ui/upgrades/wallIcon.png", plistTex);
    bridgeIcon->setPosition(Point(250.0,720.0));
    bridgeIcon->setTouchEnabled(true);
    bridgeIcon->setTag(static_cast<int>(UIUpgradeType::bridge));

    addChild(bridgeIcon);

    magicIcon->addTouchEventListener(this, toucheventselector(TypeSelectPanel::iconTouched));
    moneyIcon->addTouchEventListener(this, toucheventselector(TypeSelectPanel::iconTouched));
    towerIcon->addTouchEventListener(this, toucheventselector(TypeSelectPanel::iconTouched));
    bridgeIcon->addTouchEventListener(this, toucheventselector(TypeSelectPanel::iconTouched));

    placeBackButton(3);

}

void TypeSelectPanel::iconTouched(Object *sender, TouchEventType type) {
    auto icon = dynamic_cast<ui::Button*>(sender);
    if (type == TouchEventType::BEGAN) {
        icon->setScale(0.9);
    }
    else if (type == TouchEventType::ENDED
            || type == TouchEventType::CANCELED) {
        icon->setScale(1.0);
    }
    if (m_controller && type == TouchEventType::ENDED) {
        m_controller->upgradeTypeSelected(static_cast<UIUpgradeType>(icon->getTag()));
    }
}

BridgeUpgradePanel *BridgeUpgradePanel::create(GameUIController *controller) {
    BridgeUpgradePanel *widget = new BridgeUpgradePanel();
    if (widget && widget->init()) {
        widget->m_controller = controller;
        widget->setup();
        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

void BridgeUpgradePanel::setup() {
    setName("bridge");
    Size winSize = Director::getInstance()->getVisibleSize();
    m_upgradeBridgePanel = nullptr;
    m_scalable = Layout::create();
    addChild(m_scalable);

    auto bridgesBg = Sprite::createWithSpriteFrameName("gameScreen/ui/upgrades/tech/bridgesBack.png");
    m_scalable->addProtectedChild(bridgesBg, 4);
    bridgesBg->setPosition(Vec2(0.0, 360.0) + Tools::screenCenter());

    /*
    auto towersBg = Sprite::createWithSpriteFrameName("gameScreen/ui/upgrades/tech/towersBack.png");
    m_scalable->addProtectedChild(towersBg, 4);
    towersBg->setPosition(Vec2(0.0, -376.0) + Tools::screenCenter());


    //towerCopies
    for (int i = 0; i < 4; ++i) {
        auto cat = m_controller->m_config.controller->getCatapult(i);
        auto sprite = cat->m_sprite;
        auto clone = Sprite::createWithSpriteFrame(sprite->getSpriteFrame());
        clone->setAnchorPoint(sprite->getAnchorPoint());
        clone->setPosition(convertToNodeSpace(sprite->getParent()->convertToWorldSpace(sprite->getPosition())));
        m_scalable->addProtectedChild(clone, 5);
    }
    */
    float lift = 370.0;

    auto iconsHolder = Layout::create();
    m_scalable->addChild(iconsHolder, 5, 789);
    //bridgeUps
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            char path[50];
            sprintf(path, "Bridge%d%d", j, i);
            auto button = ui::Button::create();
            button->loadTextureNormal("gameScreen/ui/upgrades/tech/upArrow.png", plistTex);
            button->loadTexturePressed("gameScreen/ui/upgrades/tech/upArrow.png", plistTex);
            GD->ggc()->applyTransform(button, path);
            button->setTouchEnabled(true);
            button->setPosition(button->getPosition() + Vec2(0.0, lift));
            button->addTouchEventListener(this, toucheventselector(BridgeUpgradePanel::upTouched));
            button->setName("bridge");
            button->setTag(i*4+j);
            m_scalable->addChild(button, 10);

            //action
            button->runAction(RepeatForever::create(
                    Sequence::createWithTwoActions(
                            EaseSineOut::create(ScaleTo::create(0.4, 1.2)),
                            EaseSineIn::create(ScaleTo::create(0.4, 1.0))
                    )));

            int upgradeId = GD->m_model.getLevelProgress()->bridgeUpgradeIds[i*4+j];
            if (true) {
                char iconPath[64];
                sprintf(iconPath, "gameScreen/ui/upgrades/tech/bridge%02dPreview.png", upgradeId);
                auto icon = ImageView::create();
                icon->loadTexture(iconPath, plistTex);
                icon->setPosition(button->getPosition());
                iconsHolder->addChild(icon, 5, button->getTag());
            }

            //shield
            auto shieldSprite = Sprite::createWithSpriteFrameName("gameScreen/ui/upgrades/shieldIcon.png");
            m_scalable->addChild(shieldSprite, 4);
            shieldSprite->setPosition(button->getPosition() + Vec2(-26.0f, 50.0f));

            TTFConfig conf;
            conf.customGlyphs = "0123456789-";
            conf.fontFilePath = "IZH.ttf";
            conf.glyphs = GlyphCollection::CUSTOM;
            conf.fontSize = 25;
            ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(
                    StringUtils::format("bridgeUpgrades.upgrade%02d", upgradeId));

            auto label = Label::createWithTTF(conf,
                    StringUtils::format("%d", upgradeReader->fromPath("lifeMax")->toInt()), TextHAlignment::LEFT , 90);
            m_scalable->addChild(label,6);
            label->setColor(Color3B::BLACK);
            label->setPosition(button->getPosition() + Vec2(-10.0f, 40.0f));
            label->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
            label->setTag(i*4+j+200);

            //up id
            {
                std::string romeDigits[4] = {"", "(I)", "(II)", "(III)"};
                TTFConfig ttfConf;
                ttfConf.fontFilePath = "IZH.ttf";
                ttfConf.fontSize = 25;
                ttfConf.glyphs = GlyphCollection::CUSTOM;
                ttfConf.customGlyphs = "()IV";
                auto upLabel = Label::createWithTTF(ttfConf, romeDigits[upgradeId], TextHAlignment::CENTER, 200.0);
                m_scalable->addChild(upLabel, 6);
                upLabel->setPosition(button->getPosition() + Vec2(82.0, -30.0));
                upLabel->setColor(Color3B::BLACK);
                upLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
                upLabel->setTag(i*4+j+300);
            }
        }
    }
    /*
    for (int i = 0; i < 4; ++i) {
        char path[50];
        sprintf(path, "Catapult%dNode", i);
        auto button = ui::Button::create();
        button->loadTextureNormal("gameScreen/ui/upgrades/tech/upArrow.png", plistTex);
        button->loadTexturePressed("gameScreen/ui/upgrades/tech/upArrow.png", plistTex);
        GD->ggc()->applyTransform(button, path);
        button->setTouchEnabled(true);
        button->addTouchEventListener(this, toucheventselector(TechSelectPanel::upTouched));
        button->setName("tower");
        button->setTag(i);
        m_scalable->addChild(button, 10);
        //action
        button->runAction(RepeatForever::create(
                Sequence::createWithTwoActions(
                        EaseSineOut::create(ScaleTo::create(0.4, 1.2)),
                        EaseSineIn::create(ScaleTo::create(0.4, 1.0))
                )));

        {//bullet
            int upgradeId = GD->m_model.getLevelProgress()->bulletUpgradeIds[i];
            if (upgradeId > 0) {
                auto icon = ImageView::create();
                icon->loadTexture(getBulletFrameName(upgradeId).c_str(), plistTex);
                icon->setPosition(button->getPosition() + Point(50.0, 50.0));
                icon->setScale(0.5);
                m_scalable->addChild(icon, 9);
            }
        }
        {//guillotine
            int upgradeId = GD->m_model.getLevelProgress()->guillotineUpgradeIds[i];
            if (upgradeId > 0) {
                char iconPath[64];
                sprintf(iconPath, "gameScreen/ui/upgrades/tech/guillotine/item%02d.png", upgradeId - 1);
                auto icon = ImageView::create();
                icon->loadTexture(iconPath, plistTex);
                icon->setPosition(button->getPosition() + Point(50.0, -50.0));
                icon->setScale(0.5);
                m_scalable->addChild(icon, 9);
            }
        }

    } */
    placeBackButton(2);
    placeCloseButton(2);
    m_controller->m_currentTechIndex = -1;
    m_scalable->setScale(1.0f);
    m_scalable->setPosition(Vec2(0, -210.0));
    //showUpgradeBridge(3);
}

void BridgeUpgradePanel::upTouched(Object *sender, TouchEventType type) {
    auto icon = dynamic_cast<ui::Button*>(sender);
    if (type == TouchEventType::BEGAN) {
        icon->setScale(0.9);
    }
    else if (type == TouchEventType::ENDED
            || type == TouchEventType::CANCELED) {
        icon->setScale(1.0);
    }
    if (m_controller && type == TouchEventType::ENDED) {
        /*if (icon->getName() == "tower")
            m_controller->openUpgradeTower(icon->getTag());
        else */if (icon->getName() == "bridge")     {
            //m_controller->openUpgradeBridge(icon->getTag());
            if (icon->getTag() == m_controller->m_currentTechIndex && m_upgradeBridgePanel) {
                hideUpgradeBridge();
            }
            else {
                SimpleAudioEngine::getInstance()->playEffect("common/enter.wav", false, 1.0, 0.0, 1.0);
                showUpgradeBridge(icon->getTag());


            }
        }

    }

}


void BridgeUpgradePanel::showUpgradeBridge(int bridgeId) {
    int oldIndex = m_controller->m_currentTechIndex;
    m_controller->m_currentTechIndex = bridgeId;

    if (m_upgradeBridgePanel) {
        m_upgradeBridgePanel->removeFromParent();
        m_scalable->getChildByTag(oldIndex)->setVisible(true); // show old arrow
    }
    m_scalable->getChildByTag(bridgeId)->setVisible(false); // hide new arrow

    ImageView *greenBorder;
    if (m_scalable->getChildByTag(123))
        greenBorder = dynamic_cast<ImageView*>(m_scalable->getChildByTag(123));
    else {
        greenBorder = ImageView::create("gameScreen/ui/upgrades/tech/greenBorder.png", Widget::TextureResType::PLIST);
        m_scalable->addChild(greenBorder, 9, 123);
    }
    int col = m_controller->m_currentTechIndex % 4;
    int row = m_controller->m_currentTechIndex / 4;
    GD->ggc()->applyTransform(greenBorder, StringUtils::format("Bridge%d%d", col, row));
    greenBorder->setPosition(greenBorder->getPosition() + Vec2(0.0, 370.0));
    m_upgradeBridgePanel = Layout::create();
    m_upgradeBridgePanel->setPosition(Vec2(-188.0f, -810.0f));
    m_upgradeBridgePanel->setScale(1.5f); // issue #655
    m_buttons = Layout::create();
    m_upgradeBridgePanel->addChild(m_buttons, 2);
    m_scalable->addChild(m_upgradeBridgePanel, 20);
    auto background = ImageView::create();
    background->loadTexture("gameScreen/ui/upgrades/tech/panel1x3.png", plistTex);
    background->setPosition(Tools::screenCenter());
    background->setPositionY(820.0);
    //background->setScale(1.0f / m_upgradeBridgePanel->getScale()); // crazy shit!
    m_upgradeBridgePanel->addChild(background, 1);

    auto leftArrow  = ui::Button::create();
    auto rightArrow  = ui::Button::create();
    auto upArrow  = ui::Button::create();
    auto downArrow  = ui::Button::create();
    leftArrow->loadTextureNormal("gameScreen/ui/upgrades/tech/towerArrowLeft.png", plistTex);
    leftArrow->loadTexturePressed("gameScreen/ui/upgrades/tech/towerArrowLeft.png", plistTex);
    rightArrow->loadTextureNormal("gameScreen/ui/upgrades/tech/towerArrowLeft.png", plistTex);
    rightArrow->loadTexturePressed("gameScreen/ui/upgrades/tech/towerArrowLeft.png", plistTex);
    upArrow->loadTextureNormal("gameScreen/ui/upgrades/tech/towerArrowLeft.png", plistTex);
    upArrow->loadTexturePressed("gameScreen/ui/upgrades/tech/towerArrowLeft.png", plistTex);
    downArrow->loadTextureNormal("gameScreen/ui/upgrades/tech/towerArrowLeft.png", plistTex);
    downArrow->loadTexturePressed("gameScreen/ui/upgrades/tech/towerArrowLeft.png", plistTex);
    rightArrow->setFlippedX(true);
    upArrow->setRotation(90);
    downArrow->setRotation(-90);
    m_upgradeBridgePanel->addChild(leftArrow, 1);
    m_upgradeBridgePanel->addChild(rightArrow, 1);
    m_upgradeBridgePanel->addChild(upArrow, 1);
    m_upgradeBridgePanel->addChild(downArrow, 1);
    leftArrow->setPosition(Tools::screenCenter() + Point(-210.0, 300.0));
    rightArrow->setPosition(Tools::screenCenter() + Point(210.0, 300.0));
    upArrow->setPosition(Tools::screenCenter() + Point(0.0, 400.0));
    downArrow->setPosition(Tools::screenCenter() + Point(0.0, 200.0));
    leftArrow->addTouchEventListener(this, toucheventselector(BridgeUpgradePanel::buttonTouched));
    rightArrow->addTouchEventListener(this, toucheventselector(BridgeUpgradePanel::buttonTouched));
    upArrow->addTouchEventListener(this, toucheventselector(BridgeUpgradePanel::buttonTouched));
    downArrow->addTouchEventListener(this, toucheventselector(BridgeUpgradePanel::buttonTouched));
    leftArrow->setTouchEnabled(true);
    rightArrow->setTouchEnabled(true);
    upArrow->setTouchEnabled(true);
    downArrow->setTouchEnabled(true);
    leftArrow->setName("leftArrow");
    rightArrow->setName("rightArrow");
    upArrow->setName("upArrow");
    downArrow->setName("downArrow");

    //lowlight
    for (int i = 0; i < m_scalable->getChildByTag(789)->getChildren().size(); ++i) {
        auto icon = dynamic_cast<ImageView*>(m_scalable->getChildByTag(789)->getChildren().at(i));
        if (icon->getTag() == m_controller->m_currentTechIndex)
            icon->setColor(Color3B::WHITE);
        else
            icon->setColor(Color3B(70,70,70));
    }

    refreshItems();
}


void BridgeUpgradePanel::hideUpgradeBridge() {
    if (!m_upgradeBridgePanel) return;
    m_upgradeBridgePanel->removeFromParent();
    m_upgradeBridgePanel = nullptr;
    if (m_scalable->getChildByTag(123))
        dynamic_cast<ImageView*>(m_scalable->getChildByTag(123))->removeFromParent();
}


void BridgeUpgradePanel::refreshItems() {
    Panel::refreshItems();
    Point gap = Point(120.0,0.0);
    Point offset = Point(213.0, 762.0);
    m_buttons->removeAllChildren();
    for (int i = 0; i < 3; ++i) {
        char frameName[100];
        sprintf(frameName, "gameScreen/ui/upgrades/tech/bridges/item%02d.png", i);
        int upgradeIndex = i+1;
        char upgradeKey[100];
        sprintf(upgradeKey, "bridgeUpgrades.upgrade%02d", upgradeIndex);
        ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(upgradeKey);
        int priceGold = upgradeReader->fromPath("priceGold")->toInt();


        auto button = mgui::upgrades::Item::create(frameName, priceGold, 25.0*i);
        button->addGoldIcon();
        button->setPosition(Point(gap.x*i, gap.y*i)+offset);
        button->m_icon->setTouchEnabled(m_controller->canTouch(UpgradeType::bridge, upgradeIndex));
        button->m_icon->addTouchEventListener(this, toucheventselector(BridgeUpgradePanel::buttonTouched));
        button->m_icon->setName("bridge");
        button->setValue(StringUtils::format("%d",upgradeReader->fromPath("lifeMax")->toInt()),
                mgui::upgrades::Item::ValueType::defence);
        button->m_icon->setTag(upgradeIndex);
        if (m_controller->isEquiped(UpgradeType::bridge, upgradeIndex))
            button->setEquiped();
        if (!m_controller->isHighlighted(UpgradeType::bridge, upgradeIndex))
            //button->setColor(Color3B::GRAY);
            button->setHighlight(false);
        if (m_controller->isHighlighted(UpgradeType::bridge, upgradeIndex)
                && !m_controller->isEquiped(UpgradeType::bridge, upgradeIndex))
            button->addArrow();
        m_buttons->addChild(button, 2);
    }

    int upgradeId = GD->m_model.getLevelProgress()->bridgeUpgradeIds[m_controller->m_currentTechIndex];
    ImageView *icon = static_cast<ImageView*>(
            static_cast<Layout*>(m_scalable->getChildByTag(789))->getChildByTag(m_controller->m_currentTechIndex));
    icon->loadTexture(StringUtils::format("gameScreen/ui/upgrades/tech/bridge%02dPreview.png", upgradeId), Widget::TextureResType::PLIST);

    Label *upNumber = dynamic_cast<Label*>(m_scalable->getChildByTag(300+m_controller->m_currentTechIndex));
    std::string romeDigits[4] = {"", "(I)", "(II)", "(III)"};
    upNumber->setString(romeDigits[upgradeId]);

    Label *shieldLabel = dynamic_cast<Label*>(m_scalable->getChildByTag(200+m_controller->m_currentTechIndex));
    ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(
            StringUtils::format("bridgeUpgrades.upgrade%02d", upgradeId));

    shieldLabel->setString(StringUtils::format("%d", upgradeReader->fromPath("lifeMax")->toInt()));
}


void BridgeUpgradePanel::buttonTouched(Object *sender, TouchEventType type) {
    auto icon = dynamic_cast<ui::Button*>(sender);
    if (type == TouchEventType::BEGAN) {
        icon->setScale(0.9);
    }
    else if (type == TouchEventType::ENDED
            || type == TouchEventType::CANCELED) {
        icon->setScale(1.0);
    }
    if (m_controller && type == TouchEventType::ENDED) {
        if (icon->getName() == "tech")
            int a = 0; // do nothing
        else if (icon->getName() == "bridge")
        {
            m_controller->upgradeBridgeTo(icon->getTag());
        }
        else if (icon->getName() == "leftArrow")
            switchBridge(-1,0);
        else if (icon->getName() == "rightArrow")
            switchBridge(1,0);
        else if (icon->getName() == "upArrow")
            switchBridge(0,1);
        else if (icon->getName() == "downArrow")
            switchBridge(0,-1);
    }
}

void BridgeUpgradePanel::switchBridge(int offsetX, int offsetY) {
    SimpleAudioEngine::getInstance()->playEffect("common/enter.wav", false, 1.0, 0.0, 1.0);

    int row, col;
    col = m_controller->m_currentTechIndex % 4;
    row = (int)floorf((float)m_controller->m_currentTechIndex / 4);
    row += offsetX;
    col += offsetY;
    if (row > 3) row = 0;
    if (row < 0) row = 3;
    if (col > 3) col = 0;
    if (col < 0) col = 3;
    int newIndex = row * 4 + col;
    //log("new index is %d", newIndex);
    showUpgradeBridge(newIndex);
}

TowerUpgradePanel *TowerUpgradePanel::create(GameUIController *controller) {
    TowerUpgradePanel *widget = new TowerUpgradePanel();
    if (widget && widget->init()) {
        widget->m_controller = controller;
        widget->setup();
        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

void TowerUpgradePanel::setup() {
    setName("tower");

    m_scalable = Layout::create();
    addChild(m_scalable);

    m_scrollable = ScrollView::create();
    m_scrollable->setSize(Size(768.0f, 720.0f));
    m_scrollable->setPosition(Vec2(0.0f, 0.0f));
    m_scrollable->setTouchEnabled(true);

    m_scalable->addChild(m_scrollable, 2);

    auto towersHolder = Layout::create();
    m_scalable->addChild(towersHolder, 2 ,1122);

    auto towersBg = Sprite::createWithSpriteFrameName("gameScreen/ui/upgrades/tech/towersBack.png");
    towersHolder->addProtectedChild(towersBg, 4);
    towersBg->setPosition(Vec2(0.0, -376.0) + Tools::screenCenter());

    //towerCopies
    std::string romeDigits[4] = {"I", "II", "III", "IV"};
    TTFConfig ttfConf;
    ttfConf.fontFilePath = "IZH.ttf";
    ttfConf.fontSize = 40;
    for (int i = 0; i < 4; ++i) {
        auto cat = m_controller->m_config.controller->getCatapult(i);
        auto sprite = cat->m_sprite;
        auto clone = Sprite::createWithSpriteFrame(sprite->getSpriteFrame());
        clone->setAnchorPoint(sprite->getAnchorPoint());
        clone->setPosition(convertToNodeSpace(sprite->getParent()->convertToWorldSpace(sprite->getPosition())));
        towersHolder->addProtectedChild(clone, 5, i*10);
        if (i == m_controller->m_currentTechIndex) {
            ImageView *greenBorder = ImageView::create("gameScreen/ui/upgrades/tech/greenBorder.png", Widget::TextureResType::PLIST);
            towersHolder->addChild(greenBorder, 9);
            greenBorder->setPosition(clone->getPosition() +  Vec2(0.0, -10.0f));
            greenBorder->setScale(2.0);

        } else {
            //darkness
            clone->setColor(Color3B(70, 70, 70));
            //arrow
            ImageView *arrow = ui::ImageView::create();
            arrow->loadTexture("gameScreen/ui/upgrades/tech/upArrow.png", plistTex);
            arrow->setPosition(clone->getPosition());
            towersHolder->addProtectedChild(arrow, 11);
            arrow->runAction(RepeatForever::create(
                    Sequence::createWithTwoActions(
                            EaseSineOut::create(ScaleTo::create(0.4, 1.2)),
                            EaseSineIn::create(ScaleTo::create(0.4, 1.0))
                    )));
        }

        auto towerLabel = Label::createWithTTF(ttfConf, romeDigits[i], TextHAlignment::CENTER, 200.0f);
        towersHolder->addChild(towerLabel);
        towerLabel->setPosition(clone->getPosition() + Vec2(-75.0, 50.0));
        towerLabel->setColor(Color3B(246,215,29));

        auto button = ui::Layout::create();
        //button->loadTextureNormal("gameScreen/ui/upgrades/closeButton.png", plistTex);
        button->setPosition(clone->getPosition() - Vec2(50.0,50.0));
        button->setSize(Size(100.0,100.0));
        button->setContentSize(Size(100.0,100.0));
        button->setTouchEnabled(true);
        button->setTag(i);
        //button->setBackGroundColorType(BackGroundColorType::SOLID);
        //button->setBackGroundColor(Color3B::GREEN);
        int towerId = i;
        button->addTouchEventListener(this, toucheventselector(TowerUpgradePanel::buttonTouched));
        button->setName("towerId");
        towersHolder->addChild(button, 10);
    }

    towersHolder->setPosition(Vec2(0.0, 710.0f));
    towersHolder->setScale(1.0f);



    refreshItems();
    placeBackButton(3);
    placeCloseButton(3);


    m_scalable->setScale(1.0f);
}

void TowerUpgradePanel::buttonTouched(Object *sender, TouchEventType type) {
    auto icon = dynamic_cast<ui::Widget*>(sender);
    if (type == TouchEventType::BEGAN) {
        icon->setScale(0.9);
    }
    else if (type == TouchEventType::ENDED
            || type == TouchEventType::CANCELED) {
        icon->setScale(1.0);
    }
    if (m_controller && type == TouchEventType::ENDED) {
        if (icon->getName() == "tower")
            m_controller->upgradeTowerTo(icon->getTag());
        else if (icon->getName() == "bullet")
            m_controller->upgradeBulletTo(icon->getTag());
        else if (icon->getName() == "superBullet")
            m_controller->upgradeBulletToSuper(icon->getTag());
        else if (icon->getName() == "guillotine")
            m_controller->upgradeGuillotineTo(icon->getTag());
//        else if (icon->getName() == "leftArrow")
//            m_controller->switchTower(-1);
//        else if (icon->getName() == "rightArrow")
//            m_controller->switchTower(1);
        else if (icon->getName() == "towerId") {
            int index = icon->getTag();
            m_controller->switchTowerTo(index);
        }

    }
}
/*
void BridgeUpgradePanel::refreshItems() {
    Panel::refreshItems();
    Point gap = Point(120.0,0.0);
    Point offset = Point(213.0, 767.0);
    m_buttons->removeAllChildren();
    for (int i = 0; i < 3; ++i) {
        char frameName[100];
        sprintf(frameName, "gameScreen/ui/upgrades/tech/bridges/item%02d.png", i);
        int upgradeIndex = i+1;
        char upgradeKey[100];
        sprintf(upgradeKey, "bridgeUpgrades.upgrade%02d", upgradeIndex);
        ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(upgradeKey);
        int priceGold = upgradeReader->fromPath("priceGold")->toInt();


        auto button = mgui::upgrades::Item::create(frameName, priceGold, 25.0*i);
        button->setPosition(Point(gap.x*i, gap.y*i)+offset);
        button->m_icon->setTouchEnabled(m_controller->canTouch(UpgradeType::bridge, upgradeIndex));
        button->m_icon->addTouchEventListener(this, toucheventselector(BridgeUpgradePanel::buttonTouched));
        button->m_icon->setName("bridge");
        button->m_icon->setTag(upgradeIndex);
        if (m_controller->isEquiped(UpgradeType::bridge, upgradeIndex))
            button->setEquiped();
        if (!m_controller->isHighlighted(UpgradeType::bridge, upgradeIndex))
            //button->setColor(Color3B::GRAY);
            button->setHighlight(false);
        m_buttons->addChild(button, 2);
    }
}
*/
void TowerUpgradePanel::refreshItems() {
    Panel::refreshItems();
    m_scrollable->removeAllChildren();
    float perTowerGap = -500.0f;
    m_scrollable->setInnerContainerSize(Size(768.0f, fabs(perTowerGap) * 4 + 200.0f));
    Vec2 globalOffset = Vec2(0.0f, m_scrollable->getInnerContainerSize().height - 190.0f);
    float bulletGap = 180.0f;
    float towerFloor = 40.0f;
    Vec2 towerOffset = Vec2(300.0f, -towerFloor);
    Vec2 bulletOffset = Vec2(140.0f, -250.0f);
    Vec2 guillotineOffset = Vec2(570.0f, towerOffset.y + towerFloor);
    Vec2 superBulletOffset = Vec2(60.0f, towerOffset.y + towerFloor);

    const int arrCount = 8;

    Vec2 arrowOffsets[arrCount] = {Vec2(84, -337), Vec2(232,-496), Vec2(477, -465), Vec2(271,-235), Vec2(305, -354), Vec2(483, -354), Vec2(248, -106), Vec2(524,-103)};
    std::string arrowFrames[arrCount] = {"00","01","02","03","04","04","05","06"};

    //auto scroll to current tower
    int towerUpgrade = GD->m_model.getLevelProgress()->towerUpgradeIds[m_controller->m_currentTechIndex];
    if (towerUpgrade > 0)
        m_scrollable->getInnerContainer()->runAction(
            MoveTo::create(0.3f, Vec2(0.0f, - m_scrollable->getInnerContainerSize().height +
                    m_scrollable->getSize().height - perTowerGap * (towerUpgrade - 1))));

    for (int i = 0; i < 4; ++i) {
        //arrows
        for (int j = 0; j < arrCount; ++j) {
            if (i == 3 && (j == 0 || j == 1 || j == 6 || j ==2)) continue; // last tower doesnt contain superbullet
            auto arrow = Sprite::createWithSpriteFrameName(
                    StringUtils::format("gameScreen/ui/upgrades/tech/towerArrows/%s.png", arrowFrames[j].c_str()));
            arrow->setPosition(globalOffset + arrowOffsets[j] + Vec2(10, perTowerGap * i + 183));
            m_scrollable->addChild(arrow, 1);
        }


        //tower up
        {
            char frameName[100];
            sprintf(frameName, "gameScreen/ui/upgrades/tech/towers/item%02d.png", i);
            int upgradeIndex = i + 1;
            char upgradeKey[100];
            sprintf(upgradeKey, "towerUpgrades.upgrade%02d", upgradeIndex);
            ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(upgradeKey);
            int priceGold = upgradeReader->fromPath("priceGold")->toInt();

            auto button = mgui::upgrades::Item::create(frameName, priceGold, 25.0 * i);
            button->addGoldIcon();
            button->setPosition(Vec2(0.0f, perTowerGap * i) + towerOffset + globalOffset);
            button->m_icon->setTouchEnabled(m_controller->canTouch(UpgradeType::tower, upgradeIndex));
            button->m_icon->addTouchEventListener(this, toucheventselector(TowerUpgradePanel::buttonTouched));
            button->m_icon->setName("tower");
            button->setValue(StringUtils::format("reload:%.1f", upgradeReader->fromPath("attackTime")->toFloat()), Item::ValueType::attack);
            button->setScale(1.9f);
            button->m_icon->setTag(upgradeIndex);
            if (m_controller->isEquiped(UpgradeType::tower, upgradeIndex))
                button->setEquiped();
            if (!m_controller->isHighlighted(UpgradeType::tower, upgradeIndex))
                //button->setColor(Color3B::GRAY);
                button->setHighlight(false);
            if (m_controller->isHighlighted(UpgradeType::tower, upgradeIndex)
                    && !m_controller->isEquiped(UpgradeType::tower, upgradeIndex))
                button->addArrow();
            m_scrollable->addChild(button, 2);
        }
        //guillotine
        {
            char frameName[100];
            sprintf(frameName, "gameScreen/ui/upgrades/tech/guillotine/item%02d.png", i);
            int upgradeIndex = i+1;
            char upgradeKey[100];
            sprintf(upgradeKey, "guillotineUpgrades.upgrade%02d", upgradeIndex);
            ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(upgradeKey);
            int priceGold = upgradeReader->fromPath("priceGold")->toInt();


            int attackPower = upgradeReader->fromPath("attackPower")->toInt();

            auto button = mgui::upgrades::Item::create(frameName, priceGold, 25.0*i);
            button->addGoldIcon();
            button->setValue(StringUtils::format("%d-%d", attackPower, attackPower + 17));
            button->setPosition(Vec2(0.0f, perTowerGap * i) + guillotineOffset + globalOffset);
            button->m_icon->setTouchEnabled(m_controller->canTouch(UpgradeType::guillotine, upgradeIndex));
            button->m_icon->addTouchEventListener(this, toucheventselector(TowerUpgradePanel::buttonTouched));
            button->m_icon->setName("guillotine");
            button->setScale(1.5f);
            button->m_icon->setTag(upgradeIndex);
            if (m_controller->isEquiped(UpgradeType::guillotine, upgradeIndex))
                button->setEquiped();
            if (!m_controller->isHighlighted(UpgradeType::guillotine, upgradeIndex))
                //button->setColor(Color3B::GRAY);
                button->setHighlight(false);
            if (m_controller->isHighlighted(UpgradeType::guillotine, upgradeIndex)
                    && !m_controller->isEquiped(UpgradeType::guillotine, upgradeIndex))
                button->addArrow();

            m_scrollable->addChild(button, 2);
        }
        // regular bullets
        {
            for (int j = 0; j < 3; ++j) {
                char frameName[100];
                sprintf(frameName, "gameScreen/ui/upgrades/tech/bullets/item%02d.png", j + i * 3);
                int upgradeIndex = j + i * 3 + 1;
                char upgradeKey[100];
                sprintf(upgradeKey, "bulletUpgrades.upgrade%02d", upgradeIndex);
                ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(upgradeKey);
                int priceGold = upgradeReader->fromPath("priceGold")->toInt();

                int attackPower = upgradeReader->fromPath("attackPower")->toInt();
                auto button = mgui::upgrades::Item::create(frameName, priceGold, 25.0*j);
                button->addGoldIcon();
                button->setValue(StringUtils::format("%d-%d", attackPower, attackPower + 17));
                button->setPosition(Vec2(bulletGap * j, perTowerGap * i) + bulletOffset + globalOffset);
                button->m_icon->setTouchEnabled(m_controller->canTouch(UpgradeType::bullet, upgradeIndex));
                button->m_icon->addTouchEventListener(this, toucheventselector(TowerUpgradePanel::buttonTouched));
                button->m_icon->setName("bullet");
                button->setScale(1.5f);
                button->m_icon->setTag(upgradeIndex);
                if (m_controller->isEquiped(UpgradeType::bullet, upgradeIndex))
                    button->setEquiped();
                if (!m_controller->isHighlighted(UpgradeType::bullet, upgradeIndex))
                    button->setHighlight(false);
                if (m_controller->isHighlighted(UpgradeType::bullet, upgradeIndex)
                        && !m_controller->isEquiped(UpgradeType::bullet, upgradeIndex))
                    button->addArrow();
                m_scrollable->addChild(button, 3);
            }
        }
        // super bullet
        if (i < 3) {
            char frameName[100];
            sprintf(frameName, "gameScreen/ui/upgrades/tech/bullets/super/item%02d.png", i);
            int upgradeIndex = i + 13;
            char upgradeKey[100];
            sprintf(upgradeKey, "bulletUpgrades.upgrade%02d", upgradeIndex);
            ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(upgradeKey);
            int priceCrystal = upgradeReader->fromPath("priceCrystal")->toInt();

            int attackPower = upgradeReader->fromPath("attackPower")->toInt();
            int attackPowerMax = (upgradeReader->hasPath("attackPowerMax"))
                    ? upgradeReader->fromPath("attackPowerMax")->toInt()
                    : attackPower + 17;  // for display only

            auto button = mgui::upgrades::Item::create(frameName, priceCrystal, 25.0*i);
            button->setValue(StringUtils::format("%d-%d", attackPower, attackPowerMax));
            button->setPosition(Vec2(0.0f, perTowerGap * i) + superBulletOffset + globalOffset);
            button->m_icon->setTouchEnabled(m_controller->canTouch(UpgradeType::bullet, upgradeIndex));
            button->m_icon->addTouchEventListener(this, toucheventselector(TowerUpgradePanel::buttonTouched));
            button->m_icon->setName("superBullet");
            button->m_icon->setTag(upgradeIndex);
            button->setPriceBottomMargin(-20.0);
            //if (GD->m_model.getLevelProgress()->bulletUpgradeIds[m_controller->m_currentTechIndex] == upgradeIndex)
            if (m_controller->isEquiped(UpgradeType::bullet, upgradeIndex))
                button->setSuperEquiped();
            if (!m_controller->isHighlighted(UpgradeType::bullet, upgradeIndex))
                //button->setColor(Color3B::GRAY);
                button->setHighlight(false);
            if (m_controller->isHighlighted(UpgradeType::bullet, upgradeIndex)
                    && !m_controller->isEquiped(UpgradeType::bullet, upgradeIndex))
                button->addArrow();
            m_scrollable->addChild(button, 2);
            button->setScale(1.5f);
            button->addCrystalIcon();
        }
    }





    //tower copies
    Layout* towerHolder = dynamic_cast<Layout*>(m_scalable->getChildByTag(1122));
    for (int i = 0; i < 4; ++i) {
         auto sprite = dynamic_cast<Sprite*>(towerHolder->getProtectedChildByTag(i*10));
        sprite->setSpriteFrame(m_controller->m_config.controller->getCatapult(i)->m_sprite->getSpriteFrame());
    }
}


MoneyUpgradePanel *MoneyUpgradePanel::create(GameUIController *controller) {
    MoneyUpgradePanel *widget = new MoneyUpgradePanel();
    if (widget && widget->init()) {
        widget->m_controller = controller;
        widget->setup();
        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

void MoneyUpgradePanel::setup() {
    m_panelNode = ui::Layout::create();
    addChild(m_panelNode);
    setName("money");
    m_buttons = Layout::create();
    m_panelNode->addChild(m_buttons, 2);
    refreshItems();
    auto background = ImageView::create();
    background->loadTexture("gameScreen/ui/upgrades/money/panel.png", plistTex);
    background->setPosition(Tools::screenCenter());
    background->setPositionY(670.0);
    m_panelNode->addChild(background, 1);
    placeBackButton(3);
    placeCloseButton(3);

    // issue #432
    //m_panelNode->setScale(1.25f);
    // issue #673 ))
    m_panelNode->setScale(1.7f);
    m_panelNode->setPosition(Vec2(-268.0f, -650.0f));
}

void MoneyUpgradePanel::buttonTouched(Object *sender, TouchEventType type) {
    auto icon = dynamic_cast<ui::Button*>(sender);
    if (type == TouchEventType::BEGAN) {
        icon->setScale(0.9);
    }
    else if (type == TouchEventType::ENDED
            || type == TouchEventType::CANCELED) {
        icon->setScale(1.0);
    }
    if (m_controller && type == TouchEventType::ENDED) {
        m_controller->upgradeMoneyTo(icon->getTag());

    }
}

void MoneyUpgradePanel::refreshItems() {
    Panel::refreshItems();
    m_buttons->removeAllChildren();

    Point gap = Point(120.0,-130.0);
    Point offset = Point(220.0, 760.0);

    for (int i = 0; i < 9; ++i) {
        int colIndex = i % 3;
        int rowIndex = i / 3;
        int upgradeIndex = i;
        char upgradeKey[100];
        sprintf(upgradeKey, "moneyUpgrades.upgrade%02d", upgradeIndex);
        ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(upgradeKey);
        float priceDollar = upgradeReader->fromPath("priceDollar")->toFloat();

        auto button = mgui::upgrades::Item::create("gameScreen/ui/upgrades/money/item.png", priceDollar, 25.0*i, "%.2f$");
        button->setPosition(Point(gap.x*colIndex, gap.y*rowIndex)+offset);
        button->m_icon->setTouchEnabled(m_controller->canTouch(UpgradeType::money, upgradeIndex));
        button->m_icon->addTouchEventListener(this, toucheventselector(MoneyUpgradePanel::buttonTouched));
        button->m_icon->setTag(upgradeIndex);
        // bonus
        int bonusGold = upgradeReader->fromPath("bonusGold")->toInt();
        if (bonusGold > 0) {
            TTFConfig conf;
            conf.fontSize = 12;
            conf.fontFilePath = "IZH.ttf";
            conf.glyphs = GlyphCollection::ASCII;
            auto bonusLabel = Label::createWithTTF(conf, StringUtils::format("%d free!", bonusGold), TextHAlignment::LEFT, 100);
            button->m_icon->addProtectedChild(bonusLabel);
            bonusLabel->setPosition(Vec2(55.0, 10.0));
            bonusLabel->setColor(Color3B(244,206,0));
        }

        // crystalCount
        {
            auto bg = ui::ImageView::create("gameScreen/ui/upgrades/money/crystalBg.png", ui::TextureResType::PLIST);
            button->m_icon->addChild(bg,1);
            bg->setPosition(Vec2(57.0, 52.0));
            bg->setScaleX(0.7f);
            bg->setScaleY(0.6f);
            bg->setTouchEnabled(false);
            auto crystalLabel = ui::Text::create(
                    StringUtils::format("%d", upgradeReader->fromPath("crystal")->toInt()), "IZH.ttf", 15);
            crystalLabel->setColor(Color3B(5,42,252));
            crystalLabel->setTouchEnabled(false);
            button->m_icon->addChild(crystalLabel,2);

            crystalLabel->setPosition(Vec2(57.0, 52.0));
        }
        if (!m_controller->isHighlighted(UpgradeType::money, upgradeIndex))
            //button->setColor(Color3B::GRAY);
            button->setHighlight(false);
        m_buttons->addChild(button, 3);
    }
}

void Panel::placeBackButton(int zOrder) {
    auto button = ui::Button::create();
    button->loadTextureNormal("commonUI/backButton.png", TextureResType::LOCAL);
    button->setTouchEnabled(true);
    button->addTouchEventListener(this, toucheventselector(Panel::backButtonTouched));
    button->setPosition(Point(100.0, 950.0));
    button->setName("backButton");
    addChild(button, zOrder);
}

void Panel::replaceBackButton() {
    removeChildByName("backButton");
};

void Panel::backButtonTouched(Object *sender, TouchEventType type) {
    auto button = dynamic_cast<ui::Button*>(sender);
    if (type == TouchEventType::ENDED) {
        log("BACK BUTTON %s", getName().c_str());
        m_controller->backFromPanel();
    }
}

void Panel::placeCloseButton(int zOrder) {
    auto button = ui::Button::create();
    button->loadTextureNormal("gameScreen/ui/upgrades/closeButton.png", plistTex);
    button->setTouchEnabled(true);
    button->addTouchEventListener(this, toucheventselector(Panel::closeButtonTouched));
    button->setPosition(Point(700.0, 980.0));
    button->setName("closeButton");
    addChild(button, zOrder);
}

void Panel::replaceCloseButton() {
    removeChildByName("closeButton");

}

void Panel::closeButtonTouched(Object *sender, TouchEventType type) {
    auto button = dynamic_cast<ui::Button*>(sender);
    if (type == TouchEventType::ENDED) {
        m_controller->upgradesClose();
    }
}

void Item::setEquiped() {
    auto equipedMask = ImageView::create();
    equipedMask->loadTexture("gameScreen/ui/upgrades/tech/bullets/boughtMask.png", plistTex);
    m_icon->addChild(equipedMask, 3);
    equipedMask->setPosition(Point(49.0,51.0));
    equipedMask->setName("equipedMask");
}

void Item::setSuperEquiped() {
    auto equipedMask = ImageView::create();
    equipedMask->loadTexture("gameScreen/ui/upgrades/tech/bullets/super/boughtMask.png", plistTex);
    m_icon->addChild(equipedMask, 3);
    equipedMask->setPosition(Point(50.0,50.0));
    equipedMask->setName("equipedMask");
}

MagicUpgradePanel *MagicUpgradePanel::create(GameUIController *controller) {
    MagicUpgradePanel *widget = new MagicUpgradePanel();
    if (widget && widget->init()) {
        widget->m_controller = controller;
        widget->setup();
        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

void MagicUpgradePanel::setup() {
    setName("magic");
    m_scalable = ui::Layout::create();
    m_buttons = Layout::create();
    m_scalable->addChild(m_buttons, 2);
    addChild(m_scalable);
    refreshItems();
    auto background = ImageView::create();
    background->loadTexture("gameScreen/ui/upgrades/magic/panel.png", plistTex);
    background->setPosition(Tools::screenCenter());
    //background->setPositionY(670.0);
    m_scalable->addChild(background, 1);
    placeBackButton(3);
    placeCloseButton(3);

    //issue #676
    m_scalable->setScale(1.15f);
    m_scalable->setPosition(Vec2(-m_scalable->getScale() * 384.0f + 384, -112));
}

void MagicUpgradePanel::refreshItems() {
    Panel::refreshItems();
    m_buttons->removeAllChildren();

    Point gap = Point(150.0,-130.0);
    Point offset = Point(250.0, 670.0);

    std::string magicIds[4] = {"water", "earth", "fire", "air"};
    MagicType magicTypes[4] = {MagicType::water, MagicType::earth, MagicType::fire, MagicType::air};

    for (int m = 0; m < 4; ++m) {
        std::string magicId = magicIds[m];

        for (int i = 0; i < 3; ++i) {
            int rowIndex = m;
            int colIndex = i;
            int upgradeIndex = i+1;
            char upgradeKey[100];
            sprintf(upgradeKey, "magicUpgrades.upgrade%02d", upgradeIndex);
            ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(upgradeKey);
            float priceGold = upgradeReader->fromPath("priceGold")->toFloat();

            auto button = mgui::upgrades::Item::create(
                    StringUtils::format("gameScreen/ui/upgrades/magic/icons/%s.png", magicId.c_str()), priceGold, 000.0);
            ImageView *number = ui::ImageView::create(
                    StringUtils::format("gameScreen/ui/upgrades/magic/numbers/%s%d.png", magicId.c_str(), upgradeIndex), plistTex);
            m_buttons->addChild(number, 10);

            std::string settingsPath = "gameScreen/ui/upgrades/magic/settings+1+1.png";
            if (upgradeIndex == 1) settingsPath = "gameScreen/ui/upgrades/magic/settings1.png";
            //if (upgradeIndex == 2) settingsPath = "gameScreen/ui/upgrades/magic/settings+2+2.png";
            ImageView *settings = ui::ImageView::create(settingsPath, plistTex);
            m_buttons->addChild(settings, 10);
            button->addGoldIcon();
            button->setPosition(Point(gap.x*colIndex, gap.y*rowIndex)+offset);
            button->m_icon->setTouchEnabled(m_controller->canTouchMagic(magicTypes[m], upgradeIndex));
            button->m_icon->addTouchEventListener(this, toucheventselector(MagicUpgradePanel::buttonTouched));
            button->m_icon->setTag(upgradeIndex);
            button->m_icon->setName(magicId.c_str());
            if (!m_controller->isHighlightedMagic(magicTypes[m], upgradeIndex))
                //button->setColor(Color3B::GRAY);
                button->setHighlight(false);
            if (m_controller->isEquipedMagic(magicTypes[m], upgradeIndex)) {
                ImageView *boughtSign = ImageView::create("gameScreen/ui/upgrades/boughtSign.png", plistTex);
                button->m_icon->addChild(boughtSign);
                boughtSign->setPosition(Vec2(45.0, 28.0));
            }
            if (m_controller->isHighlightedMagic(magicTypes[m], upgradeIndex)
                    && !m_controller->isEquipedMagic(magicTypes[m], upgradeIndex)) {
                log("TULEI!!");
                button->addArrow();
            }
            m_buttons->addChild(button);

            number->setPosition(Point(18.0,91.0) + button->getPosition());
            settings->setPosition(Point(101.0,87.0) + button->getPosition());

        }
    }


}

void MagicUpgradePanel::buttonTouched(Object *sender, TouchEventType type) {
    auto icon = dynamic_cast<ui::Button*>(sender);
    switch (type) {
        case TouchEventType::BEGAN: icon->setScale(0.9); break;
        case TouchEventType::ENDED:
            m_controller->upgradeMagicTo(std::string(icon->getName()), icon->getTag());
        case TouchEventType::CANCELED:
            icon->setScale(1.0); break;
        default: break;
    }

}

MonsterCard *MonsterCard::create(std::string id, GameUIController *controller) {
    MonsterCard *widget = new MonsterCard();
    if (widget && widget->init()) {
        widget->m_controller = controller;
        widget->m_monsterId = id;
        widget->setup();
        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

void MonsterCard::setup() {
    setScale(1.2); // issue 433
    setPosition(Vec2(-70.0, -160.0));

    auto bg = ImageView::create();
    bg->setScale(1.2);
    auto monster = ImageView::create();
    char path[64];
    sprintf(path, "gameScreen/monsterCards/background.png");
    bg->loadTexture(path, TextureResType::LOCAL);
    sprintf(path, "gameScreen/monsterCards/%s.png", m_monsterId.c_str());
    monster->loadTexture(path, TextureResType::LOCAL);
    addChild(bg,-500);
    addChild(monster);
    bg->setPosition(Tools::screenCenter());
    bg->addTouchEventListener(this, toucheventselector(MonsterCard::buttonTouched));
    bg->setTouchEnabled(true);
    bg->runAction(FadeIn::create(0.2));
    monster->setPosition(bg->getPosition() + Point(-165.0, 6.0));

    // texts
    auto reader = GD->getConfig(Configs::monsterCards)->getSubConfigByPath(
            StringUtils::format("0.%s",m_monsterId.c_str()).c_str());
    auto monsterReader = GD->defaultsConfig()->getSubConfigByPath(
            StringUtils::format("monster.%s",m_monsterId.c_str()).c_str());

    TTFConfig conf;
    conf.fontSize = 18;
    conf.fontFilePath = "IZH.ttf";
    //conf.glyphs = GlyphCollection::DYNAMIC;

    // double it for bold font
    auto titleLabel = Label::createWithTTF(conf, _(reader->fromPath("title")->toString()->_string), TextHAlignment::CENTER, 300);
    auto titleLabel2 = Label::createWithTTF(conf, _(reader->fromPath("title")->toString()->_string), TextHAlignment::CENTER, 300);
    addProtectedChild(titleLabel);
    addProtectedChild(titleLabel2);
    titleLabel->setColor(Color3B::BLACK);
    titleLabel2->setColor(Color3B::BLACK);
    titleLabel->setPosition(bg->getPosition() + Point(105.0, 132.0));
    titleLabel2->setPosition(titleLabel->getPosition());
    titleLabel->setAnchorPoint(Point::ANCHOR_MIDDLE);
    titleLabel2->setAnchorPoint(Point::ANCHOR_MIDDLE);

    conf.fontSize = 14;

    ui::ScrollView *desc = ui::ScrollView::create();
    //desc->setBackGroundColorType(ui::Layout::BackGroundColorType::SOLID);
    desc->setTouchEnabled(true);

    addChild(desc);
    desc->setPosition(bg->getPosition() + Vec2(-15,-50));
    desc->setInnerContainerSize(Size(270,400));
    desc->setContentSize(Size(270,130));
    Node *descContent = Node::create();
    desc->addChild(descContent);
    auto descLabel = Label::createWithTTF(conf, _(reader->fromPath("description")->toString()->_string),
            TextHAlignment::LEFT , 264);
    descContent->addChild(descLabel);
    descLabel->setColor(Color3B::BLACK);
    descLabel->setPosition(Vec2(0, 0));
    descLabel->setAnchorPoint(Point::ANCHOR_TOP_LEFT);

    float descHeight = descLabel->getContentSize().height;
    if (reader->hasPath("specialSkill")) {
        conf.fontSize = 14;

        auto specTitleLabel = Label::createWithTTF(conf, _("Special opportunities:"), TextHAlignment::LEFT, 264);
        descContent->addChild(specTitleLabel);
        specTitleLabel->setColor(Color3B(55,8,8));
        specTitleLabel->setPosition(descLabel->getPosition() - Point(0.0, descLabel->getContentSize().height) + Point(0.0, 0.0));
        specTitleLabel->setAnchorPoint(Point::ANCHOR_TOP_LEFT);

        conf.fontSize = 12;

        auto specLabel = Label::createWithTTF(conf, _(reader->fromPath("specialSkill")->toString()->_string), TextHAlignment::LEFT, 264);
        descContent->addChild(specLabel);
        specLabel->setColor(Color3B::BLACK);
        specLabel->setPosition(specTitleLabel->getPosition() - Point(0.0, specTitleLabel->getContentSize().height) + Point(0.0, 0.0));
        specLabel->setAnchorPoint(Point::ANCHOR_TOP_LEFT);
        descHeight = specLabel->getContentSize().height + fabs(specLabel->getPosition().y);
    }
    if (descHeight < desc->getContentSize().height) descHeight = desc->getContentSize().height;
    descContent->setPosition(Vec2(0, descHeight));
    desc->setInnerContainerSize(Size(desc->getInnerContainerSize().width, descHeight));

    conf.fontSize = 16;

    std::string attackText;
    if (monsterReader->hasPath("attackPower"))
        attackText = StringUtils::format("%d-%d",
            monsterReader->fromPath("attackPower.min")->toInt(),
            monsterReader->fromPath("attackPower.max")->toInt());
    else if (monsterReader->hasPath("arhino"))
        attackText = StringUtils::format("%d-%d; %d-%d",
                monsterReader->fromPath("arhino.attackPower.min")->toInt(),
                monsterReader->fromPath("arhino.attackPower.max")->toInt(),
                monsterReader->fromPath("afoot.attackPower.max")->toInt(),
                monsterReader->fromPath("afoot.attackPower.max")->toInt());
    else //TODO: add slug exception
        attackText = "-";
    auto attackLabel = Label::createWithTTF(conf, attackText, TextHAlignment::LEFT , 123);
    addProtectedChild(attackLabel);
    attackLabel->setPosition(bg->getPosition() + Point(36.0, -120.0));
    attackLabel->setColor(Color3B::BLACK);
    attackLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);

    std::string defenceText = "-";
    if (monsterReader->hasPath("lifeMax"))
        defenceText = StringUtils::format("%d", monsterReader->fromPath("lifeMax")->toInt());
    else if (monsterReader->hasPath("arhino")) // rhinoRider
        defenceText = StringUtils::format("%d; %d",
                monsterReader->fromPath("arhino.lifeMax")->toInt(),
                monsterReader->fromPath("afoot.lifeMax")->toInt());
    if ( m_monsterId == "ginorMaster") defenceText = "?"; // very very special
    auto defenceLabel = Label::createWithTTF(conf, defenceText, TextHAlignment::LEFT , 123);
    addProtectedChild(defenceLabel);
    defenceLabel->setPosition(bg->getPosition() + Point(36.0, -90.0));
    defenceLabel->setColor(Color3B::BLACK);
    defenceLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);

    // mushroom
    if (m_monsterId == "mushroom") {
        auto goldIcon = Sprite::create("commonUI/goldIcon.png");
        addProtectedChild(goldIcon);
        goldIcon->setPosition(bg->getPosition() + Point(75.0, -105.0));

        auto goldLabel = Label::createWithTTF(conf, "100", TextHAlignment::LEFT , 123);
        addProtectedChild(goldLabel);
        goldLabel->setPosition(bg->getPosition() + Point(95.0, -115.0));
        goldLabel->setColor(Color3B::BLACK);
        goldLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);

    }

    //skip fake button
    auto skipLabel = Sprite::create("gameScreen/ui/tutorial/skip.png");
    skipLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    addProtectedChild(skipLabel);
    skipLabel->setPosition(bg->getPosition() + Point(178, -130));

    //close fake button
    auto close = Sprite::create("commonUI/closeButton.png");
    addProtectedChild(close);
    close->setPosition(bg->getPosition() + Vec2(260, 150));
}

void MonsterCard::buttonTouched(Object *sender, TouchEventType type) {
    if (m_controller) m_controller->closeMonsterCard(this);
    if (type == TouchEventType::ENDED && m_touchEndedCallback) m_touchEndedCallback();
}

void MonsterCard::setTouchEndedCallback(std::function<void()> callback) {
    m_touchEndedCallback = callback;
}

Sale *Sale::create(int id) {
    Sale *widget = new Sale();
    if (widget && widget->init()) {
        widget->m_saleId = id;
        widget->setup();
        widget->autorelease();
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

Sale *Sale::create(int id, std::function<void(int saleId)> onSaleClicked, std::function<void()> onCloseClicked) {
    Sale *widget = Sale::create(id);
    if (widget) {
        widget->m_onSaleClicked = onSaleClicked;
        widget->m_onCloseClicked = onCloseClicked;
        return widget;
    }
    CC_SAFE_DELETE(widget);
    return nullptr;
}

void Sale::setup() {
    using namespace ui;
    auto mask = Layout::create();
    mask->setBackGroundColor(Color3B::BLACK);
    mask->setBackGroundColorOpacity(123);
    mask->setBackGroundColorType(BackGroundColorType::SOLID);
    mask->setTouchEnabled(true);
    mask->setContentSize(Size(768, 1024));
    addChild(mask);

    auto saleBtn = ui::Button::create();
    int saleValue = GD->defaultsConfig()->fromPath(StringUtils::format("sales.sale%d.valueId", m_saleId))->toInt();
    saleBtn->loadTextureNormal(StringUtils::format("sales/%d.png", saleValue), Widget::TextureResType::LOCAL);
    saleBtn->setPosition(Tools::screenCenter());
    saleBtn->addClickEventListener([this](Ref*obj){
        m_onSaleClicked(m_saleId);
    });
    addChild(saleBtn);

    auto closeBtn = ui::Button::create();
    closeBtn->loadTextureNormal("commonUI/closeButton.png", Widget::TextureResType::LOCAL);
    closeBtn->setPosition(saleBtn->getPosition() + saleBtn->getContentSize() / 2 + Vec2(-10, -10));
    closeBtn->addClickEventListener([this](Ref*obj){
        m_onCloseClicked();
    });
    addChild(closeBtn);
}
