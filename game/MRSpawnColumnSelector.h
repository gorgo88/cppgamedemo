//
// Created by Admin on 22.05.14.
//


#ifndef __MRSpawnColumnSelector_H_
#define __MRSpawnColumnSelector_H_


namespace myth {
    class SpawnColumnSelector {
    public:

        SpawnColumnSelector() :
                m_lastMonsterCol(-1),
                m_spawnSameColCount(0) {}

        int getColumn();

        int m_lastMonsterCol;
        int m_spawnSameColCount;
    };

}


#endif //__MRSpawnColumnSelector_H_
