//
// Created by Admin on 28.01.14.
//


#include "cocos2d.h"

#ifndef __MRZoomedCircle_H_
#define __MRZoomedCircle_H_

namespace myth {
    using namespace cocos2d;

    struct ZoomedCirclePoint {
        float x;
        float y;
        float z;

        ZoomedCirclePoint():x(0.0), y(0.0), z(0.0) {
        }

        ZoomedCirclePoint(float x, float y, float z):x(x), y(y), z(z) {
        }
    };

    class ZoomedCircle : public cocos2d::Node {
    public:
        Texture2D *m_texture;
        cocos2d::Point m_centerTextureCoords;
        cocos2d::QuadCommand m_renderCommand;
        cocos2d::V3F_C4B_T2F_Quad m_quad[100];
        int m_quadCount;
        Tex2F m_tcDim;
        float m_radiusWorld;

        static ZoomedCircle *create(Texture2D *texture, float texRadius, float worldRadiu);

        void setup();

        virtual void draw(Renderer *renderer, const Mat4 &transform, uint32_t flags) override;

        virtual void draw(Renderer *renderer, const kmMat4& transform, bool transformUpdated);

        virtual ~ZoomedCircle();

        void render();

        void setZoomRegion(Tex2F point);
    };

}


#endif //__MRZoomedCircle_H_
