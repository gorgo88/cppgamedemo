//
// Created by Admin on 10.03.14.
//

#include "MRMonsterBat.h"
#include "MRGameController.h";

using namespace myth;

const string WALK_DOWN = "walkDown";
const string DEATH = "death";

void MonsterBat::fillAnimations(ConfigReader *reader) {
    m_animations = Dictionary::create();
    m_animations->retain();

    vector<string> animationNames;
    animationNames.push_back(DEATH);
    animationNames.push_back(WALK_DOWN);

    Animation *anim;
    for (int i = 0; i < animationNames.size(); ++i) {
        anim = Animation::create();
        char pattern[255];
        sprintf(pattern, "gameScreen/monsters/%s/%s/%s.png", m_config.data->id.c_str(), animationNames[i].c_str(), "%04d");
        Tools::addFramesToAnimation(anim, pattern, 16.0);
        m_animations->setObject(anim, animationNames[i]);
    }
    m_sprite->setScale(1.2);
}

void MonsterBat::fillData(ConfigReader *reader) {
    m_speed = reader->fromPath("speed")->toFloat();
    m_lifeMax = reader->fromPath("lifeMax")->toFloat();
}

void MonsterBat::update(float dt) {
    float ds = dt*m_speed;
    Point pos = m_node->getPosition();

    if (m_state == MonsterState::walkDown) {
        pos.y -= ds;
        m_node->setPosition(pos);

        if (pos.y <= m_config.controller->zones[0][0].center.y - 50.0) {
            m_config.controller->batAttack(this);
            //attack sound
            playSoundFromSet(attackSounds);
            dieMonster();
            return;
        }
    }
}

void MonsterBat::makeBloodPaddle() {
    return;
}

bool MonsterBat::isGoing() {
    return false;
}
