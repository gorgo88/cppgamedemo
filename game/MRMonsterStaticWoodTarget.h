//
// Created by Admin on 13.03.14.
//



#ifndef __MRMonsterStaticWoodTarget_H_
#define __MRMonsterStaticWoodTarget_H_

#include "MRWoodenTargetMonster.h"
#include "MRMonsterUnmovable.h"
#include "MRGameController.h"

namespace myth {

class GameContrller;

class MonsterStaticWoodTarget : public MonsterWoodTarget, public MonsterUnmovable {
public:

    virtual void fillAnimations(ConfigReader *reader) override;

    virtual void update(float dt) override;

    virtual void setup(ConfigReader *reader) override;


    virtual vector <Zone *>getCurrentZones();

    Zone *m_zone;
};

}

#endif //__MRMonsterStaticWoodTarget_H_
