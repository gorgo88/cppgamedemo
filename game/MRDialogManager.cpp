//
// Created by Admin on 06.03.14.
//


#include "MRDialogManager.h"
#include "strutil/strutil.h"
#include "ui/CocosGUI.h"

using namespace myth;
using namespace cocos2d;

static DialogManager* instance = nullptr;

//TODO: refactor all this shit. needs support for multiple dialogs simultaniously

DialogManager *DialogManager::getInstance() {
    if (!instance) instance = new DialogManager();
    return instance;
}

void DialogManager::setAutoCompleteMode(bool value) {
    m_autoCompleteMode = value;
}


void DialogManager::show(cocos2d::Node *parentNode, int zOrder) {
    show(parentNode, zOrder, Point::ZERO);
}

void DialogManager::show(cocos2d::Node *parentNode, int zOrder, Point point) {
    if (m_isTelling) return;
    m_node = Node::create();

    // issue #702
    if (GD->getDeviceFamily() == DeviceFamily::iphone
            || GD->getDeviceFamily() == DeviceFamily::iphone4) {
        m_node->setScale(1.4f);
        // issue #856
        // now magic is about to happen :)))
        if (point.x != 200) // tutorial's unusual case
            point.x *= 0.3f;
        else
            point.x = 0;

    }

    m_node->setPosition(point);
    auto bg = Sprite::create("gameScreen/ui/tutorial/talkerBackground.png");
    bg->setAnchorPoint(Point::ZERO);
    m_node->addChild(bg);
    Node* node = m_node;
    Point targetPosition = m_node->getPosition();
    m_node->setPosition(targetPosition - Point(900.0, 0.0));
    m_node->runAction(EaseSineOut::create(MoveTo::create(0.5, targetPosition)));
    m_label = nullptr;
    parentNode->addChild(m_node, zOrder);

    auto skipButton = ui::Layout::create();
    skipButton->setSize(bg->getContentSize());
    skipButton->setPosition(bg->getPosition());
    skipButton->setAnchorPoint(bg->getAnchorPoint());

    m_node->addChild(skipButton);
    skipButton->setTouchEnabled(true);
    using namespace ui;
    skipButton->addTouchEventListener(this, toucheventselector(DialogManager::buttonTouched));

    m_isTelling = false;


}

void DialogManager::buttonTouched(Ref *sender, ui::TouchEventType type) {
    if (type == ui::TouchEventType::TOUCH_EVENT_ENDED) {
        skip();
    }
}

void DialogManager::hide() {
    Node *node = m_node;
    m_node->runAction(Sequence::createWithTwoActions(
            EaseQuadraticActionIn::create(MoveTo::create(0.5, m_node->getPosition() - Point(900.0,0.0))),
            CallFunc::create([node](){node->removeFromParentAndCleanup(true);})
    ));
    m_node = nullptr;
    m_label = nullptr;
}

void DialogManager::createDialog(
        std::vector<std::string> statements,
        float delayScale,
        std::function<void()> callback) {
    if (m_isTelling) { // normally, there a no simultaneously dialogs. Maybe, in future, implement a queue
        callback();
        return;
    }
    m_currentStatements = statements;
    m_currentStatementIndex = 0;
    //delayScale *= 0.1;
    m_currentDelayScale = delayScale;
    m_currentCallback = callback;
    if (callback) m_currentCallbackCalled = false;
    m_isTelling = true;
    tellNextStatement(statements.size() > 1);
}

void DialogManager::refreshNextButton(DialogManager::NextButtonType type) {
    Node *old;
    if (m_node && (old = m_node->getChildByName("nextBtn")))
        old->removeFromParent();

    std::string nText = type == NextButtonType::skip ? "skip" : "next";
    auto skipLabel = Sprite::create(StringUtils::format("gameScreen/ui/tutorial/%s.png", nText.c_str()));

    skipLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    m_node->addChild(skipLabel);
    skipLabel->setName("nextBtn");
    skipLabel->setPosition(Point(462.0,24.0));
}


void DialogManager::moveTo(Point point) {
    // issue #702
    if (GD->getDeviceFamily() == DeviceFamily::iphone
            || GD->getDeviceFamily() == DeviceFamily::iphone4) {
        point.x = 0;
    }
    m_node->runAction(EaseQuadraticActionInOut::create(MoveTo::create(0.3, point)));
}

void DialogManager::skip() {
    if (m_label) {
        statementTellingComplete();
        if (!m_isTelling && !m_autoCompleteMode && !m_currentCallbackCalled) {
            m_currentCallbackCalled = true;
            m_currentCallback();
        }
    }
}

void DialogManager::skipAll() {
    if (m_label) {
        m_currentStatementIndex = 100500; // all this crap needs serious refactoring
        statementTellingComplete();
        if (!m_isTelling && !m_autoCompleteMode && !m_currentCallbackCalled) {
            m_currentCallbackCalled = true;
            m_currentCallback();
        }
    }
}


void DialogManager::tellNextStatement(bool hideAtTheEnd) {
    refreshNextButton(hideAtTheEnd ? NextButtonType::next : NextButtonType::skip);

    std::wstring wstr = str::to_wcs(m_currentStatements[m_currentStatementIndex]);

    TTFConfig conf;
    conf.fontFilePath = "IZH.ttf";
    conf.fontSize = 20;
    // issue #702
    if (GD->getDeviceFamily() == DeviceFamily::iphone || GD->getDeviceFamily() == DeviceFamily::iphone4)
        conf.fontSize = 18;
    //conf.customGlyphs = m_currentStatements[m_currentStatementIndex].c_str();
    conf.glyphs = GlyphCollection::DYNAMIC;

    auto label = Label::createWithTTF(conf, str::to_utf8(wstr), TextHAlignment::LEFT, 350);
    label->setColor(Color3B::BLACK);
    label->setPosition(Point(160.0,165.0));
    label->setAnchorPoint(Point(0.0,1.0));
    m_node->addChild(label);

    Vector<FiniteTimeAction*> actions;

    if (m_label) {
        Label *oldLabel = m_label;
        m_label->stopAllActions();
        m_label->runAction(Sequence::createWithTwoActions(
                FadeOut::create(0.2),
                CallFunc::create([oldLabel](){oldLabel->removeFromParentAndCleanup(true); })
        ));
        m_label = nullptr;
        actions.pushBack(DelayTime::create(0.2));
    }
    actions.pushBack(FadeIn::create(0.2));
    float stayDelay = m_currentStatements[m_currentStatementIndex].length()
            *m_currentDelayScale * 0.01;
    if (!m_autoCompleteMode) stayDelay *= 100.0;
    m_currentStayDelayAction = DelayTime::create(stayDelay);
    actions.pushBack(m_currentStayDelayAction);
    if (hideAtTheEnd) {
        actions.pushBack(FadeOut::create(0.2));
        actions.pushBack(CallFunc::create([label, this](){
            label->removeFromParentAndCleanup(true);
            m_label = nullptr;
            statementTellingComplete();
        }));
    }
    else {
        actions.pushBack(CallFunc::create([label, this](){
            statementTellingComplete();
        }));
    }
    label->setOpacity(0);
    label->runAction(Sequence::create(actions));
    label->setTag(101);
    m_label = label;
}

void DialogManager::statementTellingComplete() {

    m_currentStatementIndex++;
    if (!m_isTelling) return;
    if (m_currentStatementIndex >= m_currentStatements.size()) {
        m_isTelling = false;
        if (m_autoCompleteMode && !m_currentCallbackCalled) {
            m_currentCallbackCalled = true;
            m_currentCallback();
        }
    }
    else if (m_currentStatementIndex == m_currentStatements.size() - 1){
        tellNextStatement(false);
    }
    else tellNextStatement(true);

}

void DialogManager::setPaused(bool value) {
    //if (m_label) if (value) m_label->pause(); else m_label->resume();

}

void DialogManager::reset() {
    if (m_node && m_label) m_label->stopAllActions();
    if (m_node) {
        m_node->stopAllActions();
        m_node->removeFromParent();
    }
    m_node = nullptr;
    m_label = nullptr;
    m_isTelling = false;
}

