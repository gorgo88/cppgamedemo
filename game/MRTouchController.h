//
// Created by Admin on 20.03.14.
//


#ifndef __MRTouchController_H_
#define __MRTouchController_H_

namespace myth {


    class TouchController {
    public:
        virtual void allowTouches(int flags) = 0;

        virtual void denyTouches(int flags) = 0;
    };

}
#endif //__MRTouchController_H_
