//
// Created by Admin on 15.01.14.
//

#include "MRGameUIController.h"
#include "MRGameController.h"
#include "core/CheetahAtlasHelper.h"
#include "MRUIWidgets.h"
#include "MRPauseMenuPanel.h"
#include "audio/include/SimpleAudioEngine.h"
#include "MRTouchReceiver.h"


using namespace myth;
using namespace cocos2d;
using namespace ui;
using namespace CocosDenshion;


void GameUIController::upgradesBtnClicked(Object *sender, TouchEventType type) {
    if (!m_upgradeButtonTouchAllowed) return;
    if (type == TouchEventType::TOUCH_EVENT_BEGAN) {
        //dynamic_cast<Node*>(sender)->setScale(0.9);
    }
    if (type == TouchEventType::TOUCH_EVENT_ENDED) {
        //dynamic_cast<Node*>(sender)->setScale(1.0);
        if (m_upgradesMenuOpened) {
            upgradesClose();
        }
        else {
            upgradesOpen();
        }
    }
}


void GameUIController::upgradesOpen() {

    addShadow();
    m_config.controller->setPaused(true);

    openTypeSelect();
    m_upgradesMenuOpened = true;
}



void GameUIController::upgradesClose() {
    
    closeTypeSelect();
    closeBridgeUpgrades();
    //closeBridgeUpgrades();
    closeTowerUpgrades();
    closeMoneyUpgrades();
    closeMagicUpgrades();
    
    m_upgradesMenuOpened = false;
    
    if (m_askForBuyMagicianLifeQueriesCount > 0 && m_buingCrystals) {
        if (m_config.controller->isEnoughCrystalsForBuyLifes())
            m_buingCrystals = false;
    }
    else {
        m_config.controller->setPaused(false);
        removeShadow();
    }
    
}


void GameUIController::setup() {
    // test ui
    auto panelLeft = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/testBarLeft.png");
    auto panelRight = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/testBarRight.png");
    auto panelTopLeft = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/testBarTopLeft.png");
    auto panelTopRight = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/testBarTopRight.png");
    auto panelTop = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/testBarTop.png");
    GD->ggc()->applyTransformAP(panelLeft, "HudLeftPanel");
    GD->ggc()->applyTransformAP(panelRight, "HudRightPanel");
    GD->ggc()->applyTransformAP(panelTopLeft, "HudTopLeftPanel");
    GD->ggc()->applyTransformAP(panelTopRight, "HudTopRightPanel");
    GD->ggc()->applyTransformAP(panelTop, "HudTopPanel");
    m_config.parentNode->addChild(panelLeft);
    m_config.parentNode->addChild(panelRight);
    m_config.parentNode->addChild(panelTopLeft);
    m_config.parentNode->addChild(panelTopRight);
    m_config.parentNode->addChild(panelTop);


    auto upgradeButton = ui::Layout::create();
    auto ubSprite = Sprite::create();
    auto anim = Animation::create();
    //auto helpButton = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/helpButton.png");
    //m_config.parentNode->addChild(helpButton);
    //GD->ggc()->applyTransformAP(helpButton, "HudHelpButton");
    Tools::addFramesToAnimation(anim, "gameScreen/ui/hud/upgradeButtonAnimation/%04d.png", 12.0);
    anim->setLoops(-1);
    ubSprite->runAction(Animate::create(anim));
    ubSprite->setSpriteFrame(anim->getFrames().at(3)->getSpriteFrame());
    upgradeButton->setSize(ubSprite->getContentSize());
    ubSprite->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
    //upgradeButton->setAnchorPoint(Point::ANCHOR_MIDDLE);
    //upgradeButton->setContentSize(Size(200.0, 100.0));
    upgradeButton->addProtectedChild(ubSprite);
    upgradeButton->setTouchEnabled(true);
    upgradeButton->addTouchEventListener(this, toucheventselector(GameUIController::upgradesBtnClicked));
    m_config.parentNode->addChild(upgradeButton, 50);
    m_upgradeButton = upgradeButton;
    GD->ggc()->applyTransform(upgradeButton, "HudUpgradeButton");
    upgradeButton->setPosition(upgradeButton->getPosition() - Point(ubSprite->getContentSize()*0.5)); //stupid hack

    m_magicActivateButton = ui::Button::create();
    m_magicActivateButton->loadTextureNormal("gameScreen/ui/hud/magicButton.png",TextureResType::PLIST);
    m_magicActivateButton->setTag(1);
    m_magicActivateButton->setTouchEnabled(true);
    m_magicActivateButton->addTouchEventListener(this, toucheventselector(GameUIController::magicButtonClicked));
    GD->ggc()->applyTransform(m_magicActivateButton, "HudMagicButton");

    m_pauseButton = ui::Button::create();
    m_pauseButton->loadTextureNormal(
            "gameScreen/ui/hud/pauseButton.png",
     //       "gameScreen/ui/hud/pauseButton.png",
     //       "gameScreen/ui/hud/pauseButton.png",
    TextureResType::PLIST);
    m_pauseButton->setTouchEnabled(true);
    m_pauseButton->addTouchEventListener(this, toucheventselector(GameUIController::pauseButtonTouched));
    GD->ggc()->applyTransform(m_pauseButton, "PauseButton");


    m_config.parentNode->addChild(m_magicActivateButton,99);
    m_config.parentNode->addChild(m_pauseButton,99);

    if (GD->getDeviceFamily() == DeviceFamily::iphone) {
        m_pauseButton->setScale(1.3f);
        m_magicActivateButton->setScale(1.3f);
        m_upgradeButton->setScale(1.3f);
    }

    string labels[4] = {"Earth", "Water", "Air", "Fire"};
    for (int i = 0; i < 4; ++i) {
        auto sprite = Sprite::createWithSpriteFrameName(
                StringUtils::format("gameScreen/ui/hud/rune%s.png", labels[i].c_str()));
        m_config.parentNode->addChild(sprite, 10);
        GD->ggc()->applyTransformAP(sprite, StringUtils::format("Rune%s", labels[i].c_str()));
    }


    CheetahAtlasHelper::getInstance()->add("commonUI/dialog.atlas");
    CheetahAtlasHelper::getInstance()->add("gameScreen/ui/pauseMenu.atlas");
    CheetahAtlasHelper::getInstance()->add("gameScreen/ui/upgrades_1.atlas");
    CheetahAtlasHelper::getInstance()->add("gameScreen/ui/upgrades_2.atlas");
    CheetahAtlasHelper::getInstance()->add("gameScreen/ui/upgrades_3.atlas");

    if (GD->getDeviceFamily() != DeviceFamily::iphone) {

        Point labelOffset = Point(20.0, 10.0);

        auto goldIcon = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/goldIcon.png");
        GD->ggc()->applyTransformAP(goldIcon, "HudGoldIndicatorIcon");
        m_config.controller->m_uiNode->addChild(goldIcon, 200);
        TTFConfig conf;
        conf.fontFilePath = "IZH.ttf";
        conf.fontSize = 18;
        m_goldLabel = Label::createWithTTF(conf, "00", TextHAlignment::LEFT, 100);
        m_goldLabel->setAnchorPoint(Point::ANCHOR_TOP_LEFT);
        m_goldLabel->setPosition(goldIcon->getPosition() + labelOffset);
        m_config.controller->m_uiNode->addChild(m_goldLabel, 200);

        auto crystalIcon = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/crystalIcon.png");
        GD->ggc()->applyTransformAP(crystalIcon, "HudCrystalIndicatorIcon");
        m_config.controller->m_uiNode->addChild(crystalIcon, 200);
        m_crystalsLabel = Label::createWithTTF(conf, "00", TextHAlignment::LEFT, 100);
        m_crystalsLabel->setPosition(crystalIcon->getPosition() + labelOffset);
        m_crystalsLabel->setAnchorPoint(Point::ANCHOR_TOP_LEFT);
        m_config.controller->m_uiNode->addChild(m_crystalsLabel, 200);

        auto lifeIcon = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/heartIcon.png");
        GD->ggc()->applyTransformAP(lifeIcon, "HudHealthIndicatorIcon");
        m_config.controller->m_uiNode->addChild(lifeIcon, 200);
        m_lifeLabel = Label::createWithTTF(conf, "00", TextHAlignment::LEFT, 100);
        m_lifeLabel->setPosition(lifeIcon->getPosition() + labelOffset);
        m_lifeLabel->setAnchorPoint(Point::ANCHOR_TOP_LEFT);
        m_config.controller->m_uiNode->addChild(m_lifeLabel, 200);

        auto scoreLabel = Label::createWithTTF(conf, "0", TextHAlignment::LEFT, 200);
        m_config.controller->m_uiNode->addChild(scoreLabel, 200);
        GD->ggc()->applyTransform(scoreLabel, "HudScoreIndicatorIcon");
        scoreLabel->setAnchorPoint(Point::ANCHOR_MIDDLE_LEFT);
        //scoreLabel->setPosition(scoreLabel->getPosition()+Vec2(10.0,0.0));
        m_scoreLabel = scoreLabel;

        std::string levelStr = StringUtils::format(_("wave: %s").c_str(),
                Tools::getLevelTitle(GD->m_model.getCurrentSlot()->currentLevel).c_str());
        //(int)GD->m_model.temp.currentLocation + 1, (GD->m_model.getCurrentSlot()->currentLevel % 12) + 1);
        auto levelLabel = Label::createWithTTF(conf, levelStr,
                TextHAlignment::LEFT, 200);
        m_config.controller->m_uiNode->addChild(levelLabel, 200);
        GD->ggc()->applyTransformAP(levelLabel, "HudLevelIndicatorIcon");
        levelLabel->setAnchorPoint(Point::ANCHOR_MIDDLE_LEFT);
        //levelLabel->setPosition(levelLabel->getPosition() + labelOffset);

    }
    else {

        auto p = m_config.controller->m_uiNode;
        auto toolbarBg = Sprite::create("gameScreen/ui/toolbar.jpg");
        toolbarBg->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
        toolbarBg->setPosition(p->convertToNodeSpace(Vec2(0.0f, 1359.0f)));
        p->addChild(toolbarBg);


        auto footer = Sprite::create("gameScreen/ui/footer.jpg");
        footer->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
        footer->setPosition(p->convertToNodeSpace(Vec2(0.0f, 0.0f)));
        p->addChild(footer);

        Node *toolbarNode = Node::create();
        m_config.controller->m_uiNode->addChild(toolbarNode, 200);
        toolbarNode->setPosition(toolbarBg->getPosition());

        auto toolbarBg2 = Sprite::create("gameScreen/ui/toolbar2.png");
        GD->ggc()->applyTransformAP(toolbarBg2, "IphoneToolbar2");
        toolbarBg2->setPositionY(toolbarBg->getPositionY() + toolbarBg2->getPositionY()); // since IphoneToolbar2 from ggc is rel to toolbar
        p->addChild(toolbarBg2);


        TTFConfig conf;
        conf.fontFilePath = "IZH.ttf";
        conf.fontSize = 34;

        m_goldLabel = Label::createWithTTF(conf, "00", TextHAlignment::LEFT, 100);
        GD->ggc()->applyTransform(m_goldLabel, "IphoneToolbarGoldLabel");
        m_goldLabel->setAnchorPoint(Point::ANCHOR_MIDDLE);
        m_goldLabel->setColor(Color3B(246,226,143));
        toolbarNode->addChild(m_goldLabel, 1);

        m_crystalsLabel = Label::createWithTTF(conf, "00", TextHAlignment::LEFT, 100);
        GD->ggc()->applyTransform(m_crystalsLabel, "IphoneToolbarCrystalLabel");
        m_crystalsLabel->setAnchorPoint(Point::ANCHOR_MIDDLE);
        m_crystalsLabel->setColor(Color3B(148,195,247));
        toolbarNode->addChild(m_crystalsLabel, 1);

        conf.fontSize = 52;
        m_lifeLabel = Label::createWithTTF(conf, "00", TextHAlignment::LEFT, 100);
        GD->ggc()->applyTransform(m_lifeLabel, "IphoneToolbarHPLabel");
        m_lifeLabel->setAnchorPoint(Point::ANCHOR_MIDDLE);
        m_lifeLabel->setColor(Color3B(236,87,55));
        toolbarNode->addChild(m_lifeLabel, 1);
        conf.fontSize = 28;

        auto scoreLabel2 = Label::createWithTTF(conf, "0", TextHAlignment::LEFT, 200);
        GD->ggc()->applyTransform(scoreLabel2, "IphoneToolbarScoreLabel");
        scoreLabel2->setAnchorPoint(Point::ANCHOR_MIDDLE);
        scoreLabel2->setColor(Color3B(248,196,129));
        scoreLabel2->setString("SCORE: ");
        toolbarNode->addChild(scoreLabel2, 1);

        m_scoreLabel = Label::createWithTTF(conf, "0", TextHAlignment::LEFT, 200);
        GD->ggc()->applyTransform(m_scoreLabel, "IphoneToolbarScoreLabel2");
        m_scoreLabel->setAnchorPoint(Point::ANCHOR_MIDDLE);
        m_scoreLabel->setColor(Color3B(248,196,129));
        toolbarNode->addChild(m_scoreLabel, 1);

        std::string levelStr = StringUtils::format(_("WAVE:\n%s").c_str(),
                Tools::getLevelTitle(GD->m_model.getCurrentSlot()->currentLevel).c_str());
        auto levelLabel = Label::createWithTTF(conf, levelStr, TextHAlignment::CENTER, 200);
        GD->ggc()->applyTransform(levelLabel, "IphoneToolbarLevelLabel");
        levelLabel->setAnchorPoint(Point::ANCHOR_MIDDLE);
        levelLabel->setColor(Color3B(219,219,219));
        toolbarNode->addChild(levelLabel, 1);
    }

    auto airManaProgress = cocos2d::ui::LoadingBar::create();
    airManaProgress->loadTexture("gameScreen/ui/hud/progressBars/air.png", TextureResType::PLIST);
    GD->ggc()->applyTransformAP(airManaProgress, "HudAirProgressBar");
    m_config.parentNode->addChild(airManaProgress);
    m_magicProgessBars[static_cast<int>(MagicType::air)] = airManaProgress;

    auto earthManaProgress = cocos2d::ui::LoadingBar::create();
    earthManaProgress->loadTexture("gameScreen/ui/hud/progressBars/earth.png", TextureResType::PLIST);
    GD->ggc()->applyTransformAP(earthManaProgress, "HudEarthProgressBar");
    m_config.parentNode->addChild(earthManaProgress);
    m_magicProgessBars[static_cast<int>(MagicType::earth)] = earthManaProgress;

    auto waterManaProgress = cocos2d::ui::LoadingBar::create();
    waterManaProgress->loadTexture("gameScreen/ui/hud/progressBars/water.png", TextureResType::PLIST);
    GD->ggc()->applyTransformAP(waterManaProgress, "HudWaterProgressBar");
    m_config.parentNode->addChild(waterManaProgress);
    m_magicProgessBars[static_cast<int>(MagicType::water)] = waterManaProgress;

    auto fireManaProgress = cocos2d::ui::LoadingBar::create();
    fireManaProgress->loadTexture("gameScreen/ui/hud/progressBars/fire.png", TextureResType::PLIST);
    GD->ggc()->applyTransformAP(fireManaProgress, "HudFireProgressBar");
    m_config.parentNode->addChild(fireManaProgress);
    m_magicProgessBars[static_cast<int>(MagicType::fire)] = fireManaProgress;


    updateStats();

    m_magicButtonTouchAllowed = true;
    m_pauseButtonTouchAllowed = true;
    m_upgradeButtonTouchAllowed = true;


    // combo label
    /*TTFConfig conf;
    conf.customGlyphs = "0123456789X";
    conf.glyphs = GlyphCollection::CUSTOM;
    conf.fontFilePath = "IZH.ttf";
    conf.fontSize = 40;
    m_comboLabel = Label::createWithTTF(conf, "", TextHAlignment::LEFT , 100);
    m_comboLabel->setColor(Color3B::WHITE);
    m_comboLabel->setAnchorPoint(Point::ANCHOR_MIDDLE);
    */
    //m_comboLabel = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/combo/digits/0.png");
    m_comboLabel = Label::createWithBMFont("gameScreen/ui/comboDigitsFont/font.fnt", "x0", TextHAlignment::CENTER , 200, Vec2::ZERO);
    m_config.parentNode->addChild(m_comboLabel);
    GD->ggc()->applyTransform(m_comboLabel, "HudHelpButton");

}

void GameUIController::allowTouches(int flags) {
    if (flags & (int) TouchReceiverFlag::magicButton)
        m_magicButtonTouchAllowed = true;
    if (flags & (int) TouchReceiverFlag::pauseButton)
        m_pauseButtonTouchAllowed = true;
    if (flags & (int) TouchReceiverFlag::upgradeButton)
        m_upgradeButtonTouchAllowed = true;
}

void GameUIController::denyTouches(int flags) {
    if (flags & (int) TouchReceiverFlag::magicButton)
        m_magicButtonTouchAllowed = false;
    if (flags & (int) TouchReceiverFlag::pauseButton)
        m_pauseButtonTouchAllowed = false;
    if (flags & (int) TouchReceiverFlag::upgradeButton)
        m_upgradeButtonTouchAllowed = false;
}


void GameUIController::magicButtonClicked(Object *sender, TouchEventType type) {
    if (m_config.controller->isBossTime()) return;
    if (!m_magicButtonTouchAllowed) return;
    if (type == TouchEventType::TOUCH_EVENT_ENDED) {
        m_config.controller->activateMagic();
        SimpleAudioEngine::getInstance()->playEffect("gameScreen/magicActivate.wav", false, 1.0, 0.0, 1.0);
    }
}

void GameUIController::pauseButtonTouched(Object *sender, TouchEventType type) {
    if (!m_pauseButtonTouchAllowed) return;
    switch (type) {
        //default:
        //case TouchEventType::TOUCH_EVENT_BEGAN: button->setScale(0.9); break;
        case TouchEventType::TOUCH_EVENT_ENDED: {
            //button->setScale(1.0);
            openPauseMenu();
            Director::getInstance()->getTextureCache()->removeUnusedTextures();

            break;
        }
    }
}

void GameUIController::upgradeTypeSelected(UIUpgradeType type) {
    if (type == UIUpgradeType::bridge) {
        openUpgradeBridge();
    } else
    if (type == UIUpgradeType::tower) {
        openUpgradeTower(0);
    } else
    if (type == UIUpgradeType::money) {
        openUpgradeMoney();
    } else
    if (type == UIUpgradeType::magic) {
        openUpgradeMagic();
    }
    closeTypeSelect();
}

void GameUIController::openTypeSelect() {
    SimpleAudioEngine::getInstance()->playEffect("common/enter.wav", false, 1.0, 0.0, 1.0);
    m_uTypeSelectPanel = mgui::upgrades::TypeSelectPanel::create(this);
    m_uTypeSelectPanel->retain();
    m_config.parentNode->addChild(m_uTypeSelectPanel,101);
    m_currentPanel = m_uTypeSelectPanel;
}

void GameUIController::closeTypeSelect() {
    if (m_uTypeSelectPanel) {
        m_uTypeSelectPanel->release();
        m_config.parentNode->removeChild(m_uTypeSelectPanel, true);
        m_uTypeSelectPanel = nullptr;
    }
}

void GameUIController::openUpgradeBridge() {
    SimpleAudioEngine::getInstance()->playEffect("common/enter.wav", false, 1.0, 0.0, 1.0);

    m_bridgeUpgradePanel = mgui::upgrades::BridgeUpgradePanel::create(this);
    m_bridgeUpgradePanel->retain();
    m_config.parentNode->addChild(m_bridgeUpgradePanel, 101);
    //removeShadow();
    // game touch blocker
    m_currentPanel = m_bridgeUpgradePanel;
}

void GameUIController::addShadow() {
    auto maskLayer = LayerColor::create(Color4B::BLACK);
    maskLayer->setOpacity(180);
    maskLayer->setTag(3211);
    auto layout = ui::Layout::create();
    layout->setSize(maskLayer->getBoundingBox().size);
    layout->setTouchEnabled(true);
    maskLayer->addChild(layout);
    m_config.parentNode->addChild(maskLayer, 100);
}

void GameUIController::removeShadow() {
    m_config.parentNode->removeChildByTag(3211, true);


}
/*
void GameUIController::openUpgradeBridge(int index) {
    SimpleAudioEngine::getInstance()->playEffect("common/enter.wav", false, 1.0, 0.0, 1.0);

    m_currentTechType = TechType::bridge;
    m_currentTechIndex = index;
    closeBridgeUpgrades();
    m_bridgesUpdatePanel = mgui::upgrades::BridgeUpgradePanel::create(this);
    m_bridgesUpdatePanel->retain();
    m_bridgesUpdatePanel->setPosition(Point(0.0,0.0));
    m_config.parentNode->addChild(m_bridgesUpdatePanel, 101);
    m_currentPanel = m_bridgesUpdatePanel;

}  */

void GameUIController::openUpgradeTower(int index) {
    SimpleAudioEngine::getInstance()->playEffect("common/enter.wav", false, 1.0, 0.0, 1.0);

    m_currentTechType = TechType::tower;
    m_currentTechIndex = index;
    m_towersUpdatePanel = mgui::upgrades::TowerUpgradePanel::create(this);
    m_towersUpdatePanel->retain();
    m_towersUpdatePanel->setPosition(Point(0.0,0.0));
    m_config.parentNode->addChild(m_towersUpdatePanel, 101);
    m_currentPanel = m_towersUpdatePanel;
}

void GameUIController::closeBridgeUpgrades() {
    if (m_bridgeUpgradePanel) {
        m_config.parentNode->removeChild(m_bridgeUpgradePanel);
        m_bridgeUpgradePanel->release();
        m_bridgeUpgradePanel = nullptr;

    }
}

bool GameUIController::canTouch(UpgradeType type, int index) {
    char upgradeKey[100];
    sprintf(upgradeKey, "%sUpgrades.upgrade%02d", Tools::getUpgradeLabel(type).c_str(), index);
    ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(upgradeKey);
    if (OPEN_ALL) return true;
    if (type == UpgradeType::tower) {
        int minLevelComplete = upgradeReader->fromPath("minLevelComplete")->toInt();
        int currentTowerIndex = GD->m_model.getLevelProgress()->towerUpgradeIds[m_currentTechIndex];
        int bulletUpgradeId = GD->m_model.getLevelProgress()->bulletUpgradeIds[m_currentTechIndex];
        if (currentTowerIndex == index - 1
                && minLevelComplete - 1 <= GD->m_model.getCurrentSlot()->completeLevels
                && (/*last simple bullet*/index == 1 + bulletUpgradeId / 3
                || /*last super bullet*/(bulletUpgradeId >= 13 && index == 1 + bulletUpgradeId - 12))) return true; // very hard to understand
    } else
    if (type == UpgradeType::bullet) {
        int tower = upgradeReader->fromPath("tower")->toInt();
        int currentTowerIndex = GD->m_model.getLevelProgress()->towerUpgradeIds[m_currentTechIndex];
        int currentBulletIndex = GD->m_model.getLevelProgress()->bulletUpgradeIds[m_currentTechIndex];
        const int superBullet0Index = 13;
        const int bulletsPerTower = 3;
        int bulletNextToCurrentSuperBullet = (currentBulletIndex - (superBullet0Index - 1)) * bulletsPerTower + 1;
        if (currentTowerIndex == tower
                && (index >= superBullet0Index
                || (currentBulletIndex < superBullet0Index && index == currentBulletIndex + 1)
                || (currentBulletIndex >= superBullet0Index && index == bulletNextToCurrentSuperBullet))) return true;
    } else
    if (type == UpgradeType::guillotine) {
        int tower = upgradeReader->fromPath("tower")->toInt();
        if (GD->m_model.getLevelProgress()->towerUpgradeIds[m_currentTechIndex] >= tower
                && index == GD->m_model.getLevelProgress()->guillotineUpgradeIds[m_currentTechIndex] + 1) return true;
    } else
    if (type == UpgradeType::bridge) {
        if (index == GD->m_model.getLevelProgress()->bridgeUpgradeIds[m_currentTechIndex] + 1) return true;
    } else
    if (type == UpgradeType::money) {
        return true;
    }
    return false;
}

bool GameUIController::canTouchMagic(MagicType magicType, int index) {
    return isHighlightedMagic(magicType, index);
}

bool GameUIController::isHighlightedMagic(MagicType type, int index) {
    if (GD->m_model.getLevelProgress()->magicUpgradeIds[static_cast<int>(type)] + 1 == index) return true;
    return false;
}

bool GameUIController::isHighlighted(UpgradeType type, int index) {
    /*if (isEquiped(type, index)) return true;
    else */return canTouch(type, index);
}


void GameUIController::upgradeBridgeTo(int index) {
    m_currentUpgradeIndex = index;
    m_currentUpgradeType = UpgradeType::bridge;
    if (m_config.controller->canBuyUpgrade(UpgradeType::bridge , index))
        openAreYouSureDialog();
    else
        openBuyMoneyDialog();

}

void GameUIController::upgradeBulletTo(int i) {
    m_currentUpgradeIndex = i;
    m_currentUpgradeType = UpgradeType::bullet;
    if (m_config.controller->canBuyUpgrade(UpgradeType::bullet , i))
        openAreYouSureDialog();
    else
        openBuyMoneyDialog();
}

void GameUIController::upgradeBulletToSuper(int i) {
    m_currentUpgradeIndex = i;
    m_currentUpgradeType = UpgradeType::bullet;
    if (m_config.controller->canBuyUpgrade(UpgradeType::bullet , i))
        openAreYouSureDialog();
    else
        openBuyMoneyDialog();
}

void GameUIController::upgradeGuillotineTo(int i) {
    m_currentUpgradeIndex = i;
    m_currentUpgradeType = UpgradeType::guillotine;
    if (m_config.controller->canBuyUpgrade(UpgradeType::guillotine , i))
        openAreYouSureDialog(_("Combat knives shall be installed at the tower's base. They are launched at the enemy approach"));
    else
        openBuyMoneyDialog();
}

void GameUIController::upgradeTowerTo(int i) {
    m_currentUpgradeIndex = i;
    m_currentUpgradeType = UpgradeType::tower;
    if (m_config.controller->canBuyUpgrade(UpgradeType::tower , i))
        openAreYouSureDialog(_("This action increases the catapult firing rate"));
    else
        openBuyMoneyDialog();
}


void GameUIController::areYouSureTouched(Object *sender, TouchEventType type) {
    if (type == TouchEventType::TOUCH_EVENT_ENDED) {
        auto button = dynamic_cast<Button *> (sender);
        if (button->getTag() == 0) { //yes
            switch (m_currentUpgradeType) {
                case UpgradeType::bridge:
                    m_config.controller->upgradeBridge(m_currentTechIndex, m_currentUpgradeIndex); break;
                case UpgradeType::tower:
                    m_config.controller->upgradeTower(m_currentTechIndex, m_currentUpgradeIndex); break;
                case UpgradeType::bullet:
                    m_config.controller->upgradeBullet(m_currentTechIndex, m_currentUpgradeIndex); break;
                case UpgradeType::guillotine:
                    m_config.controller->upgradeGuillotine(m_currentTechIndex, m_currentUpgradeIndex); break;
                case UpgradeType::money: {
                    if (m_config.moneyUpgradeDelegate) // location screen, for example
                        m_config.moneyUpgradeDelegate->upgradeMoney(m_currentUpgradeIndex, nullptr, nullptr);
                    else
                        m_config.controller->upgradeMoney(m_currentUpgradeIndex, nullptr, nullptr);
                    break;
                }
                case UpgradeType::magic:
                    m_config.controller->upgradeMagic(m_currentUpgradeIndex, m_currentUpgradeMagicType); break;
            }
            m_areYouSureDialog->m_coveredPanel->refreshItems();
        }
        m_areYouSureDialog->m_coveredPanel->setEnabled(true);
        m_areYouSureDialog->removeFromParent();
        m_areYouSureDialog = nullptr;

    }
}

void GameUIController::buyMoneyDialogTouched(Object *sender, TouchEventType type) {
    if (type == TouchEventType::TOUCH_EVENT_ENDED) {
        auto button = dynamic_cast<Button *> (sender);
        if (button->getTag() == 0) { //yes
            m_currentPanel->removeFromParent();
            m_currentPanel = nullptr;
            openUpgradeMoney();
        }
        m_buyMoneyDialog->m_coveredPanel->setEnabled(true);
        m_buyMoneyDialog->removeFromParent();
        m_buyMoneyDialog = nullptr;

    }
}

/*
void GameUIController::closeBridgeUpgrades() {
    if (m_bridgesUpdatePanel) {
        m_bridgesUpdatePanel->removeFromParent();
        m_bridgesUpdatePanel->release();
        m_bridgesUpdatePanel = nullptr;
    }
}*/


void GameUIController::closeTowerUpgrades() {
    if (m_towersUpdatePanel) {
        m_towersUpdatePanel->removeFromParent();
        m_towersUpdatePanel->release();
        m_towersUpdatePanel = nullptr;
    }
}

void GameUIController::openAreYouSureDialog(std::string q) {
    SimpleAudioEngine::getInstance()->playEffect("common/enter.wav", false, 1.0, 0.0, 1.0);

    if (q == "") q = _("Want to buy?");
    if (q.size() < 50)
        m_areYouSureDialog = mgui::DialogBox::create(q, _("YES"), _("NO"));
    else
        m_areYouSureDialog = mgui::DialogBox::create(q, _("YES"), _("NO"), 20);
    m_config.parentNode->addChild(m_areYouSureDialog, 105);
    m_areYouSureDialog->setPosition(Tools::screenCenter() - Point(m_areYouSureDialog->getSize())*0.5);
    m_areYouSureDialog->m_okButton->addTouchEventListener(this, toucheventselector(GameUIController::areYouSureTouched));
    m_areYouSureDialog->m_cancelButton->addTouchEventListener(this, toucheventselector(GameUIController::areYouSureTouched));
    m_areYouSureDialog->m_coveredPanel = m_currentPanel;
    m_areYouSureDialog->m_coveredPanel->setEnabled(false);

}

void GameUIController::openBuyMoneyDialog() {
    SimpleAudioEngine::getInstance()->playEffect("common/enter.wav", false, 1.0, 0.0, 1.0);

    m_buyMoneyDialog = mgui::DialogBox::create(_("Not enough resources. Want to by crystals?"), _("YES"), _("NO"));
    m_config.parentNode->addChild(m_buyMoneyDialog, 105);
    m_buyMoneyDialog->setPosition(Tools::screenCenter() - Point(m_buyMoneyDialog->getSize())*0.5);
    m_buyMoneyDialog->m_okButton->addTouchEventListener(this, toucheventselector(GameUIController::buyMoneyDialogTouched));
    m_buyMoneyDialog->m_cancelButton->addTouchEventListener(this, toucheventselector(GameUIController::buyMoneyDialogTouched));
    m_buyMoneyDialog->m_coveredPanel = m_currentPanel;
    m_buyMoneyDialog->m_coveredPanel->setEnabled(false);

}

void GameUIController::openUpgradeMoney() {
    SimpleAudioEngine::getInstance()->playEffect("common/enter.wav", false, 1.0, 0.0, 1.0);

    m_moneyUpgradePanel = mgui::upgrades::MoneyUpgradePanel::create(this);
    m_moneyUpgradePanel->retain();
    m_config.parentNode->addChild(m_moneyUpgradePanel, 101);
    m_currentPanel = m_moneyUpgradePanel;
}

void GameUIController::upgradeMoneyTo(int i) {
    m_currentUpgradeIndex = i;
    m_currentUpgradeType = UpgradeType::money;
    openAreYouSureDialog();
}

void GameUIController::backFromPanel() {
    if (m_currentPanel->getName() == "bridge") {
        closeBridgeUpgrades();
        openTypeSelect();
    } else
    if (m_currentPanel->getName() == "tower") {
        closeTowerUpgrades();
        openTypeSelect();
    } else
    /*if (m_currentPanel->getName() == "bridge") {
        closeBridgeUpgrades();
        openUpgradeBridge();
    }  else  */
    if (m_currentPanel->getName() == "money") {
        if (m_buingCrystals) {
            if (m_config.controller->isEnoughCrystalsForBuyLifes())
                m_buingCrystals = false;
        }
        else {
            openTypeSelect();
        }
        closeMoneyUpgrades();
    } else
    if (m_currentPanel->getName() == "magic") {
        closeMagicUpgrades();
        openTypeSelect();
    } else
    if (m_currentPanel->getName() == "typeSelect") {
        closeTypeSelect();
        upgradesClose();
    }

}

void GameUIController::closeMoneyUpgrades() {
    if (m_moneyUpgradePanel) {
        m_moneyUpgradePanel->removeFromParent();
        m_moneyUpgradePanel->release();
        m_moneyUpgradePanel = nullptr;
    }
}

void GameUIController::updateStats() {
    char goldText[32], crystalsText[32], lifeText[32], scoreText[32];
    std::string goldStr = Tools::formatThousandSeparated(GD->m_model.getLevelProgress()->gold);
    std::string crystalsStr = Tools::formatThousandSeparated(GD->m_model.getLevelProgress()->crystals);
    std::string scoreStr = Tools::formatThousandSeparated(GD->m_model.getLevelProgress()->score);
    sprintf(goldText, "%s", goldStr.c_str());
    sprintf(crystalsText, "%s", crystalsStr.c_str());
    sprintf(lifeText, "%d", m_config.controller->m_life);
    if (GD->getDeviceFamily() != DeviceFamily::iphone)
        sprintf(scoreText, _("score: %s").c_str(), scoreStr.c_str());
    else
        sprintf(scoreText, "%s", scoreStr.c_str());
    m_goldLabel->setString(goldText);
    m_crystalsLabel->setString(crystalsText);
    m_lifeLabel->setString(lifeText);
    m_scoreLabel->setString(scoreText);

    for (int i = 0; i < 4; ++i) {
        m_magicProgessBars[i]->setPercent((int)m_config.controller->m_mana[i]);
    }
}


void GameUIController::openPauseMenu() {
    if (m_pauseMenuPanel) return;
    SimpleAudioEngine::getInstance()->playEffect("common/enter.wav", false, 1.0, 0.0, 1.0);

    m_pauseMenuPanel = mgui::PauseMenuPanel::create(this);
    m_config.parentNode->addChild(m_pauseMenuPanel, 101);
    m_config.controller->setPaused(true);
    addShadow();
}


void GameUIController::setBloodEnabled(bool value) {
    GD->m_model.isBloodEnabled = value;
}

void GameUIController::setSoundVolume(float value) {
    GD->m_model.soundVolume = value;
    CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(value);
}

void GameUIController::setMusicVolume(float value) {
    GD->m_model.musicVolume = value;
    CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(value);
}


void GameUIController::closePauseMenu() {
    m_pauseMenuPanel->removeFromParentAndCleanup(true);
    m_config.controller->setPaused(false);
    GD->m_model.dumpToDisk();
    m_pauseMenuPanel = nullptr;
    removeShadow();
}

void GameUIController::restartLevel() {
    //if (m_config.controller->m_tutorial) GD->m_model.temp.tutorialMode = true;
    GD->openScreen(Screens::game);
}

void GameUIController::exitToMainMenu() {
    SimpleAudioEngine::getInstance()->stopBackgroundMusic(false);
    GD->exitFromPauseMenu(m_config.controller->m_tutorial);
}

void GameUIController::getHelp() {
    CCLOG("getting help");
}


void GameUIController::openUpgradeMagic() {
    SimpleAudioEngine::getInstance()->playEffect("common/enter.wav", false, 1.0, 0.0, 1.0);

    m_magicUpgradePanel = mgui::upgrades::MagicUpgradePanel::create(this);
    m_magicUpgradePanel->retain();
    m_config.parentNode->addChild(m_magicUpgradePanel, 101);
    m_currentPanel = m_magicUpgradePanel;
}

void GameUIController::closeMagicUpgrades() {
    if (m_magicUpgradePanel) {
        m_magicUpgradePanel->removeFromParent();
        m_magicUpgradePanel->release();
        m_magicUpgradePanel = nullptr;
    }
}


void GameUIController::upgradeMagicTo(std::string magicName, int upgradeId) {
    m_currentUpgradeIndex = upgradeId;
    m_currentUpgradeType = UpgradeType::magic;
    MagicType type = MagicType::air;
    if ("air" == magicName)     type = MagicType::air; else
    if ("earth" == magicName)   type = MagicType::earth; else
    if ("fire" == magicName)    type = MagicType::fire; else
    if ("water" == magicName)   type = MagicType::water;
    m_currentUpgradeMagicType = type;
    if (m_config.controller->canBuyUpgrade(UpgradeType::magic , upgradeId))
        openAreYouSureDialog();
    else
        openBuyMoneyDialog();
}


void GameUIController::showMonsterCard(string id) {
    SimpleAudioEngine::getInstance()->playEffect("common/openManuscript.wav", false, 1.0, 0.0, 1.0);

    auto card = mgui::MonsterCard::create(id, this);
    m_monsterCards.push_back(card);
    m_config.controller->m_uiNode->addChild(card, 50);
    m_config.controller->setPaused(true);
    m_config.controller->dispatchDenyTouches(
            (int)TouchReceiverFlag::magicButton
            | (int)TouchReceiverFlag::catapult
            | (int)TouchReceiverFlag::upgradeButton

    );
}



void GameUIController::closeMonsterCard(mgui::MonsterCard *card) {
    SimpleAudioEngine::getInstance()->playEffect("common/closeManuscript.wav", false, 1.0, 0.0, 1.0);

    std::string id = card->m_monsterId;
    m_monsterCards.erase(std::find(m_monsterCards.begin(), m_monsterCards.end(), card));
    card->removeFromParentAndCleanup(true);

    if (m_monsterCards.size() == 0) {
        m_config.controller->setPaused(false);
        m_config.controller->dispatchAllowTouches(
                (int)TouchReceiverFlag::magicButton
                        | (int)TouchReceiverFlag::catapult
                        | (int)TouchReceiverFlag::pauseButton
                        | (int)TouchReceiverFlag::upgradeButton

        );

    }

    if (id == "ginor") m_config.controller->playGinorDialog();
    if (id == "devastator") m_config.controller->playDevastatorDialog();
}

void GameUIController::switchTower(int offset) {
//    SimpleAudioEngine::getInstance()->playEffect("common/enter.wav", false, 1.0, 0.0, 1.0);

    closeTowerUpgrades();
    m_currentTechIndex += offset;
    if (m_currentTechIndex < 0) m_currentTechIndex = 3;
    m_currentTechIndex = m_currentTechIndex % 4;
    log("new index is %d", m_currentTechIndex);
    openUpgradeTower(m_currentTechIndex);
}


void GameUIController::switchTowerTo(int towerId) {
    closeTowerUpgrades();
    m_currentTechIndex = towerId;
    if (m_currentTechIndex < 0) m_currentTechIndex = 3;
    m_currentTechIndex = m_currentTechIndex % 4;
    log("new index is %d", m_currentTechIndex);
    openUpgradeTower(m_currentTechIndex);
}




void GameUIController::displayComboCounter(int kills) {
    /*if (kills < 2) m_comboLabel->setVisible(false);
    else*/ {
        m_comboLabel->setString(StringUtils::format("x%d", kills));
        //m_comboLabel->setVisible(true);
    }
}

void GameUIController::askForBuyMagicianLife(function<void()> failureCallback, bool askForBuyCrystals) {
    m_buingCrystals = askForBuyCrystals;
    if (m_askForBuyMagicianLifeDialog) { // very specific situation: two or more monsters left the scene simultaneously
        m_askForBuyMagicianLifeQueriesCount ++;
        m_askForBuyMagicianLifeDialog->removeProtectedChildByTag(654); // remove question
        m_askForBuyMagicianLifeDialog->removeProtectedChildByTag(987); // remove tries label

    }
    else {
        addShadow();
        m_askForBuyMagicianLifeQueriesCount = 1;
        m_askForBuyMagicianLifeDialog = mgui::DialogBox::create("", _("YES"), _("NO"));
        m_config.controller->setPaused(true);
        m_config.parentNode->addChild(m_askForBuyMagicianLifeDialog, 101);
        m_askForBuyMagicianLifeDialog->setPosition(Tools::screenCenter() - Point(m_askForBuyMagicianLifeDialog->getSize())*0.50);
        m_askForBuyMagicianLifeDialog->m_okButton->addTouchEventListener(this, toucheventselector(GameUIController::askFouBuyMagicianLifeTouched));
        m_askForBuyMagicianLifeDialog->m_cancelButton->addTouchEventListener(this, toucheventselector(GameUIController::askFouBuyMagicianLifeTouched));
        m_config.controller->dispatchDenyTouches((int)TouchReceiverFlag::magicButton | (int)TouchReceiverFlag::upgradeButton);

    }
    RichText *question = RichText::create();
    int fontSize = 25;
    auto heartIcon = RichElementImage::create(1, Color3B::WHITE, 255, "commonUI/heartIcon.png");
    auto crystalIcon = RichElementImage::create(3, Color3B::WHITE, 255, "commonUI/crystalIcon.png");


    /// part of "Buy 1 heart for 50 crystals", where "heart" and "crystals" are pictures
    int lifesToBuy = m_askForBuyMagicianLifeQueriesCount;
    /*if (askForBuyCrystals) {
        int crystalBuyTransactionId = (lifesToBuy * LIFE_COST > 100) ? 225 : 100;
        question->pushBackElement(RichElementText::create(0, Color3B::WHITE, 255,
                StringUtils::format(_("Buy %d ").c_str(), crystalBuyTransactionId), "IZH.ttf", fontSize));
        question->pushBackElement(crystalIcon);
        question->pushBackElement(RichElementText::create(0, Color3B::WHITE, 255,
                StringUtils::format(_("and %d ").c_str(), lifesToBuy), "IZH.ttf", fontSize));
        question->pushBackElement(heartIcon);
        question->pushBackElement(RichElementText::create(4, Color3B::WHITE, 255, " ?", "IZH.ttf", fontSize));
    }
    else */{
        question->pushBackElement(RichElementText::create(0, Color3B::WHITE, 255,
                StringUtils::format(_("Buy %d").c_str(), lifesToBuy), "IZH.ttf", fontSize));
        question->pushBackElement(heartIcon);
        /// part of "Buy heart for 50 crystals", where "heart" and "crystals" are pictures
        question->pushBackElement(RichElementText::create(2, Color3B::WHITE, 255,
                StringUtils::format(_(" for %d ").c_str(), lifesToBuy * 50), "IZH.ttf", fontSize));
        question->pushBackElement(crystalIcon);
        question->pushBackElement(RichElementText::create(4, Color3B::WHITE, 255, " ?", "IZH.ttf", fontSize));

    }

    m_askForBuyMagicianLifeDialog->addProtectedChild(question, 100, 654);
    question->setPosition(m_askForBuyMagicianLifeDialog->m_titleField->getPosition() + Vec2(0, 20));
    //m_askForBuyMagicianLifeDialog->m_titleField->setPosition(m_askForBuyMagicianLifeDialog->m_titleField->getPosition() + Vec2(0.0, 30.0));
    //auto heartIcon = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/heartIcon.png");
//    heartIcon->setPosition(Vec2(
//            m_askForBuyMagicianLifeDialog->m_titleField->getPosition().x + (m_askForBuyMagicianLifeDialog->m_titleField->getContentSize()).width - 75.0,
//            m_askForBuyMagicianLifeDialog->m_titleField->getPosition().y - 5.0));
//    m_askForBuyMagicianLifeDialog->addProtectedChild(heartIcon,2);

    TTFConfig conf;
    conf.fontSize = 20;
    conf.fontFilePath = "IZH.ttf";
    auto triesLabel = Label::createWithTTF(conf, StringUtils::format(_("Only %d attempts left!").c_str(), m_config.controller->m_buyLifeTries+1),
            TextHAlignment::CENTER , 300);
    m_askForBuyMagicianLifeDialog->addProtectedChild(triesLabel, 2, 987);
    triesLabel->setPosition(Vec2(180.0, 85.0));
    triesLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);

    if (failureCallback) m_buyLifeFailureCallback = failureCallback;
    else m_buyLifeFailureCallback = nullptr;
}

void GameUIController::disappearAskForBuyLifeDialog() {
    if (m_askForBuyMagicianLifeDialog) {
        m_askForBuyMagicianLifeQueriesCount = 0;
        m_askForBuyMagicianLifeDialog->removeFromParent();
        m_askForBuyMagicianLifeDialog = nullptr;
        removeShadow();
        m_config.controller->setPaused(false);
        m_config.controller->dispatchAllowTouches((int)TouchReceiverFlag::magicButton | (int)TouchReceiverFlag::upgradeButton);

    }
}


void GameUIController::askFouBuyMagicianLifeTouched(Object *sender, TouchEventType type) {
    if (type == TouchEventType::TOUCH_EVENT_ENDED) {
        auto button = dynamic_cast<Button*> (sender);
        bool canceled = false;
        if (button->getTag() == 0) { //yes
            if (m_buingCrystals) {
                /*m_config.controller->upgradeMoney(m_askForBuyMagicianLifeQueriesCount * LIFE_COST > 100 ? 1 : 0,
                [this](){
                    m_config.controller->repair(m_askForBuyMagicianLifeQueriesCount);
                    openPauseMenu();
                }, [this](){m_buyLifeFailureCallback();});*/
                openUpgradeMoney();
            }
            else {
                m_config.controller->repair(m_askForBuyMagicianLifeQueriesCount);
                disappearAskForBuyLifeDialog();
            }

        }    else {
            canceled = true;
            disappearAskForBuyLifeDialog();
            if (canceled && m_buyLifeFailureCallback)
                m_buyLifeFailureCallback();
        }
    }
}


bool GameUIController::isEquiped(UpgradeType type, int index) {
    int currentEquiped;
    switch (type) {
        default:
        case UpgradeType::bridge: currentEquiped = GD->m_model.getLevelProgress()->bridgeUpgradeIds[m_currentTechIndex]; break;
        case UpgradeType::bullet: currentEquiped = GD->m_model.getLevelProgress()->bulletUpgradeIds[m_currentTechIndex]; break;
        case UpgradeType::guillotine: currentEquiped = GD->m_model.getLevelProgress()->guillotineUpgradeIds[m_currentTechIndex]; break;
        case UpgradeType::tower: currentEquiped = GD->m_model.getLevelProgress()->towerUpgradeIds[m_currentTechIndex]; break;
    }

    float filteredIndex = (float) index;
    float filteredEquipedIndex = (float) currentEquiped;
    if (type == UpgradeType::bullet && index >= 13) filteredIndex = (filteredIndex - 12) * 3 + 0.5;
    if (type == UpgradeType::bullet && currentEquiped >= 13) filteredEquipedIndex = (filteredEquipedIndex - 12) * 3 + 0.5;

    if (filteredIndex <= filteredEquipedIndex) return true;
    else return false;
}


bool GameUIController::isEquipedMagic(MagicType type, int upgradeIndex) {
    return (GD->m_model.getLevelProgress()->magicUpgradeIds[(int)type] >= upgradeIndex);
}



GameUIController::~GameUIController() {
    CheetahAtlasHelper::getInstance()->remove("commonUI/dialog.atlas");
    CheetahAtlasHelper::getInstance()->remove("gameScreen/ui/pauseMenu.atlas");
    CheetahAtlasHelper::getInstance()->remove("gameScreen/ui/upgrades_1.atlas");
    CheetahAtlasHelper::getInstance()->remove("gameScreen/ui/upgrades_2.atlas");
    CheetahAtlasHelper::getInstance()->remove("gameScreen/ui/upgrades_3.atlas");
    log("removed ui controller");
}

void GameUIController::placeSkipTutorialButton() {
    Button *levelBtn = Button::create();
    levelBtn->loadTextures(
            "mainMenuScreen/buttonActive.png",
            "mainMenuScreen/buttonInactive.png",
            "mainMenuScreen/buttonInactive.png");
    levelBtn->setPosition(Vec2(650.0, 870.0));
    //levelBtn->setTag((int)tags[i]);
    levelBtn->setTouchEnabled(true);
    levelBtn->setPressedActionEnabled(true);
    levelBtn->addTouchEventListener([this](Ref *target, Widget::TouchEventType type){
        if (type == Widget::TouchEventType::ENDED) {
            m_config.controller->quitFromTutorial();
        }
    });
    levelBtn->setScale(0.7);
    TTFConfig conf;
    conf.fontFilePath = "IZH.ttf";
    conf.fontSize = 30;
    Label *label = Label::createWithTTF(conf, _("Skip training"), TextHAlignment::CENTER , 300);
    levelBtn->addProtectedChild(label);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    label->setPosition(levelBtn->getSize() *0.5);
    m_config.parentNode->addChild(levelBtn,2);
}
