//
// Created by Admin on 13.01.14.
//



#ifndef __MRMonsterRhinoRider_H_
#define __MRMonsterRhinoRider_H_

#include "MRMonster.h"


namespace myth {

enum class RhinoRiderState {
    arhino,
    afoot
};

class MonsterRhinoRider : public Monster{

public:
    MonsterAttackRange m_afootAttackPower;
    float m_afootLifeMax;
    MonsterAttackRange m_arhinoAttackPower;
    float m_arhinoLifeMax;

    RhinoRiderState m_rhinoState;

    virtual void fillAnimations(ConfigReader *reader) override;

    virtual void fillData(ConfigReader *reader) override;

    virtual void fillPrice(ConfigReader *reader) override;

    virtual void dieMonster() override;

    void switchAnimations(RhinoRiderState state);

    virtual void deathAnimationComplete() override;

    MonsterState m_prevState;
    int m_arhinoPriceScore;

    virtual void fillSounds(ConfigReader *reader) override;

    MonsterSoundSet *m_afootAttackSounds;
    MonsterSoundSet *m_afootDeathSounds;
};

}
#endif //__MRMonsterRhinoRider_H_
