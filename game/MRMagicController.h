//
// Created by Admin on 25.12.13.
//



#ifndef __MagicController_H_
#define __MagicController_H_

#include "core/myth.h"
#include "MRMagicType.h"
#include "MRTouchController.h"
#include "ui/CocosGUI.h"

using namespace cocos2d;

using namespace std;
namespace myth {

class GameController;



struct MagicSegment {
    float sinus;
    float cosinus;

};

struct MagicInputData {
    cocos2d::Point point;
    float len;
};

class MagicNeuron {
public:
    string m_name;
    float m_weight;
    float m_threshold = 0.4;
    vector<MagicSegment> m_memory;
    float calculateWeight(vector<MagicSegment> *inputData, int dim) {
        m_weight = 0.0;
        for (int i = 0; i < dim; ++i) {
            float diff = fabs(inputData->at(i).cosinus - m_memory[i].cosinus) +
                    fabs(inputData->at(i).sinus - m_memory[i].sinus);
            if (diff < m_threshold) {
                m_weight += (1.0 / (diff + 1.0));
            }
        }
        return m_weight;
    };
};

class MagicControllerConfig {
public:
    int touchPriority;
    Node *parentNode;
    GameController *gameController;
    float successNeuronWeight = 2.0;
};

class MagicController : public TouchController {
    vector<MagicNeuron> m_neurons;
    vector<MagicSegment> m_inputSegments;
    vector<MagicInputData> m_inputPoints;
    Point m_inputPointMax;
    Point m_inputPointMin;

public:
    bool touchBegan(Touch *touch, Event *event);
    void touchMoved(Touch *touch, Event *event);
    void touchEnded(Touch *touch, Event *event);


    void activate();

    void setup();

    MagicControllerConfig m_config;
    bool m_active;
    bool m_capturing;

    void storeInputFromTouch(Touch *pTouch);

    void filterInputData();

    void recognizeGesture();

    int m_segmentCount;

    void normalizeInput();

    ParticleSystem *m_particles;
    EventListener *m_touchListener;

    virtual ~MagicController();

    cocos2d::ui::ImageView *m_mask;
    Node *m_targetsNode;
    bool m_selectingIslands;
    std::vector<std::pair<int,int>> m_selectedIslands;

    void selectIsland(int i, int j);

    MagicType m_currentType;
    int m_islandCountLimit;

    virtual void allowTouches(int flags);

    virtual void denyTouches(int flags);

    bool m_touchAllowed;

    void showTargets();

    void updateCounterLabel(int count);

    Node *getIslandCounter();

    void hideBackButton();

    ui::Button *m_backButton;

    void backButtonToucned();

    bool m_backButtonAllowed = true;

    void setIdleMode(bool mode);

    bool m_idleMode; // for tutorial, just draw a streak
    void startStreak(Vec2 pos);

    void updateStreak(Vec2 pos);

    void stopStreak(); // calls external

protected:
    void _stopStreak(); // calls internal

};
}

#endif //__MagicController_H_
