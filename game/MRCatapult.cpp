//
// Created by Admin on 10.12.13.
//


#include "MRCatapult.h"
#include "MRGameController.h"
#include "audio/include/SimpleAudioEngine.h"
#include "core/CheetahAtlasHelper.h"
#include "MRTouchReceiver.h"
#include "TestNode.h"

using namespace myth;
using namespace CocosDenshion;

void Catapult::setup() {
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("gameScreen/catapult/catapult.plist");
    CheetahAtlasHelper::getInstance()->add("gameScreen/towers/fog.atlas");
    CheetahAtlasHelper::getInstance()->add("gameScreen/guillotines/guillotines.atlas");
    m_forceTargetingVisible = false;
    m_node = Node::create();
    m_config.parentNode->addChild(m_node, 50);
    String *path = String::createWithFormat("Catapult%dNode", m_config.zoneCol);
    GD->ggc()->applyTransform(m_node, path->_string);

    m_bulletAnimation = nullptr;

    m_sprite = Sprite::create();
    m_node->addChild(m_sprite, 0);
    GD->ggc()->applyTransformAP(m_sprite, "Catapult0Node.TowerSprite");

    auto targetingMask = Sprite::create(m_config.targetingMaskTexture);
    m_targetingHandle = Sprite::create(m_config.targetingHandleTexture);
    m_targetingNode = Node::create();
    auto decorNode = Node::create();
    m_node->addChild(decorNode, 1);
    m_node->addChild(m_targetingNode, 3);
    m_targetingNode->addChild(targetingMask);
    m_targetingNode->addChild(m_targetingHandle);
    GD->ggc()->applyTransformAP(targetingMask, "Catapult0Node.MaskSprite");
    targetingMask->setOpacity(170);

    m_target = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/target.png");
    m_zoneNumbers = Node::create();
    auto d01 = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/zoneNumbers/01.png");
    auto d02 = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/zoneNumbers/02.png");
    auto d03 = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/zoneNumbers/03.png");
    auto d04 = Sprite::createWithSpriteFrameName("gameScreen/ui/hud/zoneNumbers/04.png");
    GD->ggc()->applyTransformAP(d01, "ZoneNumbers.ZoneNumber01");
    GD->ggc()->applyTransformAP(d02, "ZoneNumbers.ZoneNumber02");
    GD->ggc()->applyTransformAP(d03, "ZoneNumbers.ZoneNumber03");
    GD->ggc()->applyTransformAP(d04, "ZoneNumbers.ZoneNumber04");
    m_zoneNumbers->addChild(d01);
    m_zoneNumbers->addChild(d02);
    m_zoneNumbers->addChild(d03);
    m_zoneNumbers->addChild(d04);
    GD->ggc()->applyTransform(m_zoneNumbers, "ZoneNumbers");

    m_zoomer = ZoomedCircle::create(m_config.zoomerTexture, 0.04, 47);
    m_target->setVisible(false);
    m_zoomer->setVisible(false);
    m_zoneNumbers->setVisible(false);
    m_config.hudNode->addChild(m_zoomer, 1);

    m_config.hudNode->addChild(m_target, 2);
    m_config.hudNode->addChild(m_zoneNumbers, 3);
    m_target->setAnchorPoint(Point(0.5, 0.5));
    m_zoomer->setPosition(Point(0.0,0.0));
    auto numbersOffset = m_config.zoneCol == 3 ? Point(-80.0,0.0) : Point(80.0,0.0);
    m_zoneNumbers->setPositionX(m_node->getPosition().x + numbersOffset.x);

    //guillo
    m_guilloSprite = Sprite::createWithSpriteFrameName("gameScreen/guillotines/00/0000.png");
    m_node->addChild(m_guilloSprite, -1);
    GD->ggc()->applyTransformAP(m_guilloSprite, "Catapult0Node.GuillotineSprite");
    m_guilloInitPos = m_guilloSprite->getPosition();

    GD->ggc()->applyTransformAP(m_targetingHandle, "Catapult0Node.HandlerSprite");
    m_handleRect = GD->ggc()->getRect("Catapult0Node.HandlerRect");
    if (GD->getDeviceFamily() == DeviceFamily::ipad) {
        //m_handleRect.size.height = m_handleRect.size.height * 0.7;
    }


    m_targetingNode->setVisible(false);
    m_touchRect = GD->ggc()->getRect("Catapult0Node.TouchRect");

    m_touchListener = EventListenerTouchAllAtOnce::create();
    EventDispatcher *dispatcher = Director::getInstance()->getEventDispatcher();
    m_touchListener->onTouchesBegan = CC_CALLBACK_2(Catapult::onTouchesBegan, this);
    m_touchListener->onTouchesMoved = CC_CALLBACK_2(Catapult::onTouchesMoved, this);
    m_touchListener->onTouchesEnded = CC_CALLBACK_2(Catapult::onTouchesEnded, this);
    dispatcher->addEventListenerWithFixedPriority((EventListener *) m_touchListener, m_config.touchPriority);
    m_touchId = -1;

    m_touchesAllowed = true;

    if (AnimationCache::getInstance()->getAnimation("catapult") == NULL) {
        Animation* catapultShootAnimation = Animation::create();
        catapultShootAnimation->setRestoreOriginalFrame(true);
        Tools::addFramesToAnimation(catapultShootAnimation, "200%02d.png", 0, 48, 96.0);
        AnimationCache::getInstance()->addAnimation(catapultShootAnimation, "catapult");
    }

    m_fogSprite = Sprite::create();
    m_fogSprite->setVisible(false);
    m_node->addChild(m_fogSprite, 10);
    m_fogTime = 10.0;
    m_fogState = CatapultFogState::none;
    m_isBlocked = false;
    setIdleMode(false);
    if (AnimationCache::getInstance()->getAnimation("catapultFog") == NULL) {
        Animation* anim = Animation::create();
        Tools::addFramesToAnimation(anim, "gameScreen/fog/%04d.png", 10.0);
        anim->setLoops(-1);
        AnimationCache::getInstance()->addAnimation(anim, "catapultFog");
    }


    m_catapultSprite = Sprite::createWithSpriteFrameName("20000.png");
    m_node->addChild(m_catapultSprite, 2);
    GD->ggc()->applyTransformAP(m_catapultSprite, "Catapult0Node.CatapultSprite");

    calculateTargetingHandlePositions();


    // start upgrades
    char upgradeKey[100];
    sprintf(upgradeKey, "bulletUpgrades.upgrade%02d", m_config.startBulletUpgrade);
    ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(upgradeKey);
    applyBulletUpgrade(m_config.startBulletUpgrade, upgradeReader);

    sprintf(upgradeKey, "towerUpgrades.upgrade%02d", m_config.startTowerUpgrade);
    upgradeReader = GD->defaultsConfig()->getSubConfigByPath(upgradeKey);
    applyTowerUpgrade(m_config.startTowerUpgrade, upgradeReader);

    m_guillotineEnabled = false;
    sprintf(upgradeKey, "guillotineUpgrades.upgrade%02d", m_config.startTowerUpgrade);
    upgradeReader = GD->defaultsConfig()->getSubConfigByPath(upgradeKey);
    applyGuillotineUpgrade(m_config.startGuillotineUpgrade, upgradeReader);

    // fire progress bar
    auto fpb = ui::LoadingBar::create();
    fpb->loadTexture("gameScreen/ui/towerFireIndicator/full.png", ui::TextureResType::LOCAL);
    auto fpbb = Sprite::create("gameScreen/ui/towerFireIndicator/empty.png");
    auto fireIndicatorNode = Node::create();
    m_node->addChild(fireIndicatorNode);
    fireIndicatorNode->addChild(fpbb);
    fireIndicatorNode->addChild(fpb);
    m_fireIndicatorNode = fireIndicatorNode;
    m_fireIndicator = fpb;
    fireIndicatorNode->setPosition(Point(0.0, 100.0));
    fireIndicatorNode->setVisible(false);

    // decor // fock m brain!
    if (m_config.zoneCol == 0) {
        vector<string> textures;
        vector<string> labels;
        textures.push_back("wheel1");
        textures.push_back("bag");
        textures.push_back("wheels");
        labels.push_back("wheel1_00");
        labels.push_back("bag_00");
        labels.push_back("wheels_00");
        for (int i = 0; i < textures.size(); ++i) {
            char fPath[100];
            sprintf(fPath, "gameScreen/catapult/decor/%s.png", textures[i].c_str());
            auto sprite = Sprite::createWithSpriteFrameName(fPath);
            decorNode->addChild(sprite);
            char cPath[100];
            sprintf(cPath, "Catapult%dNode.%s", m_config.zoneCol, labels[i].c_str());
            GD->ggc()->applyTransformAP(sprite, cPath);

        }
    }
    if (m_config.zoneCol == 2) {
        vector<string> textures;
        vector<string> labels;
        textures.push_back("weapons4");
        textures.push_back("shield2");
        textures.push_back("wheels");
        textures.push_back("bags");
        labels.push_back("weapons4_01");
        labels.push_back("shield2_00");
        labels.push_back("wheels_01");
        labels.push_back("bags_00");
        for (int i = 0; i < textures.size(); ++i) {
            char fPath[100];
            sprintf(fPath, "gameScreen/catapult/decor/%s.png", textures[i].c_str());
            auto sprite = Sprite::createWithSpriteFrameName(fPath);
            decorNode->addChild(sprite);
            char cPath[100];
            sprintf(cPath, "Catapult%dNode.%s", m_config.zoneCol, labels[i].c_str());
            GD->ggc()->applyTransformAP(sprite, cPath);

        }
    }
    if (m_config.zoneCol == 1) {
        vector<string> textures;
        vector<string> labels;
        textures.push_back("weapons3");
        textures.push_back("bullets");
        textures.push_back("weapons1");
        labels.push_back("weapons3_00");
        labels.push_back("bullets_00");
        labels.push_back("weapons1_00");
        for (int i = 0; i < textures.size(); ++i) {
            char fPath[100];
            sprintf(fPath, "gameScreen/catapult/decor/%s.png", textures[i].c_str());
            auto sprite = Sprite::createWithSpriteFrameName(fPath);
            decorNode->addChild(sprite);
            char cPath[100];
            sprintf(cPath, "Catapult%dNode.%s", m_config.zoneCol, labels[i].c_str());
            GD->ggc()->applyTransformAP(sprite, cPath);

        }
    }
    if (m_config.zoneCol == 3) {
        vector<string> textures;
        vector<string> labels;
        textures.push_back("bullets");
        textures.push_back("weapons2");
        textures.push_back("shield1");
        labels.push_back("bullets_01");
        labels.push_back("weapons2_00");
        labels.push_back("shield1_00");
        for (int i = 0; i < textures.size(); ++i) {
            char fPath[100];
            sprintf(fPath, "gameScreen/catapult/decor/%s.png", textures[i].c_str());
            auto sprite = Sprite::createWithSpriteFrameName(fPath);
            decorNode->addChild(sprite);
            char cPath[100];
            sprintf(cPath, "Catapult%dNode.%s", m_config.zoneCol, labels[i].c_str());
            GD->ggc()->applyTransformAP(sprite, cPath);

        }
    }

    m_loading = false;
}


void Catapult::applyBulletUpgrade(int bulletUpgradeIndex, ConfigReader *upgradeReader) {
    m_attackPower = upgradeReader->fromPath("attackPower")->toFloat();
    //m_resetTime = upgradeReader->fromPath("attackTime")->toFloat(); // tower property now
    char pattern[50];
    sprintf(pattern, "gameScreen/bullets/%02d/%s.png", bulletUpgradeIndex, "%04d");
    if (m_bulletAnimation) m_bulletAnimation->release();
    m_bulletAnimation = Animation::create();
    Tools::addFramesToAnimation(m_bulletAnimation, pattern, 24.0);
    m_bulletAnimation->setLoops(-1);
    m_bulletAnimation->retain();
}

void Catapult::applyTowerUpgrade(int bulletUpgradeIndex, ConfigReader *upgradeReader) {
    char frameName[50];
    sprintf(frameName, "gameScreen/towers/%02d.png", bulletUpgradeIndex);
    m_sprite->setSpriteFrame(frameName);
    m_resetTime = upgradeReader->fromPath("attackTime")->toFloat();
}

void Catapult::applyGuillotineUpgrade(int bulletUpgradeIndex, ConfigReader *upgradeReader) {
    char frameName[50];
    int frameUpgIndex = (bulletUpgradeIndex > 0) ? bulletUpgradeIndex - 1 : 0;
    sprintf(frameName, "gameScreen/guillotines/%02d/0000.png", frameUpgIndex);
    m_guilloInterval = upgradeReader->fromPath("attackTime")->toFloat();
    m_guilloPower = upgradeReader->fromPath("attackPower")->toFloat();
    m_guilloSprite->setSpriteFrame(frameName);
    // check if animated
    if (SpriteFrameCache::getInstance()->getSpriteFrameByName(
            StringUtils::format("gameScreen/guillotines/%02d/0001.png", frameUpgIndex))) {
        Animation *anim = Animation::create();
        Tools::addFramesToAnimation(
                anim,
                StringUtils::format("gameScreen/guillotines/%02d/%s.png", frameUpgIndex, "%04d"), 16);
        anim->setLoops(-1);
        m_guilloSprite->runAction(Animate::create(anim));
    }
    m_guillotineEnabled = bulletUpgradeIndex != 0;
    m_guilloSprite->setVisible(m_guillotineEnabled);
}

void Catapult::onTouchesBegan(const std::vector<Touch*>& touches, Event  *event)
{
    if (!m_touchesAllowed) return;
    if (m_touchId != -1) return;
    for ( auto &item: touches )
    {
        Touch* touch = item;
        if (touchBegan(touch, event)) {
            m_touchId = touch->getID();
            break;
        }
    }
}

void Catapult::onTouchesMoved(const std::vector<Touch*>& touches, Event  *event)
{
    if (!m_touchesAllowed) return;

    for ( auto &item: touches )
    {
        Touch *touch = item;
        if (m_touchId == touch->getID()) {
            touchMoved(touch, event);
        }
    }
}

void Catapult::onTouchesEnded(const std::vector<Touch*>& touches, Event  *event)
{
    if (!m_touchesAllowed) return;

    for ( auto &item: touches )
    {
        Touch *touch = item;
        if (m_touchId == touch->getID()) {
            touchEnded(touch, event);
            m_touchId = -1;
        }
    }
}

bool Catapult::touchBegan(cocos2d::Touch* touch, cocos2d::Event* event) {
    return touchBegan(touch->getLocation());
}


bool Catapult::touchBegan(const Vec2 &worldPoint) {
    m_touchStartLocation = worldPoint;
    if (m_isBlocked || m_loading) return false;
    if (m_touchRect.containsPoint(m_node->convertToNodeSpace(worldPoint))) {
        calculateFingerZones(worldPoint);

        //if (!m_forceTargetingVisible)
        m_config.controller->catapultTouched(m_config.zoneCol);
        m_targetingNode->setVisible(true);
        touchMoved(worldPoint);
        return true;
    }
    return false;
}


void Catapult::calculateTargetingHandlePositions() {
    float dy = m_handleRect.size.height / 3;
    for (int i = 0; i < 4; ++i) {
        handlerZonePositions[i] = m_handleRect.size.height - i * dy + m_handleRect.origin.y;
    }
}

void Catapult::calculateFingerZones(Vec2 worldPoint) {
    float gap = 50.0f; // some gap from bottom of the game node

    Vec2 gameNodePoint = m_node->getParent()->convertToNodeSpace(worldPoint);
    float maxRange = GD->getDeviceFamily() == DeviceFamily::ipad
                     ? m_handleRect.size.height / 1.3f
                     : m_handleRect.size.height;

    float range = fminf(maxRange, gameNodePoint.y - gap);


    float dy = range / 3;// issue #847
    for (int i = 0; i < 4; ++i) {
        fingerZonePositions[i] = gameNodePoint.y /*m_handleRect.size.height*/ - i * dy;
    }
}


void Catapult::touchMoved(cocos2d::Touch* touch, cocos2d::Event* event) {
    touchMoved(touch->getLocation());
}
void Catapult::touchMoved(const Vec2 &worldPoint) {
    Vec2 gameNodePoint = m_node->getParent()->convertToNodeSpace(worldPoint);
    float minDistance = 10000.0; // why not
    int nearestZone = 0;
    for (int i = 0; i < 4; ++i) {
        float dist = fabsf(gameNodePoint.y - fingerZonePositions[i]);
        if (dist < minDistance) {
            nearestZone = i;
            minDistance = dist;
        }
    }
    m_targetedZone = nearestZone;
    m_targetingHandle->setPosition(Vec2(
            m_targetingHandle->getPositionX(),
            handlerZonePositions[nearestZone]));

    showTarget();
}

void Catapult::touchEnded(cocos2d::Touch* touch, cocos2d::Event* event) {
    touchEnded(touch->getLocation());
}

void Catapult::touchEnded() {
    //if (!m_forceTargetingVisible)
    m_targetingNode->setVisible(false);
    hideTarget();
    if (!m_idleMode) load();
}

void Catapult::touchEnded(const Vec2 &worldPoint) {
    touchEnded();
}


void Catapult::load() {
    hideTarget();

    Animation *animation = AnimationCache::getInstance()->getAnimation("catapult");
    Animate *action = Animate::create(animation);
    m_catapultSprite->runAction(action);
    m_catapultSprite->runAction(Sequence::createWithTwoActions(
            DelayTime::create(m_config.shootDelay + m_resetTime),
            CallFunc::create(CC_CALLBACK_0(Catapult::loadingComplete, this))
    ));

    m_catapultSprite->runAction(Sequence::createWithTwoActions(
            DelayTime::create(m_config.shootDelay),
            CallFunc::create(CC_CALLBACK_0(Catapult::shoot, this))
    ));
    m_loading = true;

    //sound
    SimpleAudioEngine::getInstance()->playEffect("gameScreen/catapultLoad.wav", false, 1.0, 0.0, m_config.soundLoadGain);

}

void Catapult::loadingComplete() {
    m_loading = false;
    SimpleAudioEngine::getInstance()->playEffect("gameScreen/catapultLoaded.wav", false, 1.0, 0.0, 0.6f		);

}

void Catapult::shoot() {
    CatapultBullet *freeBullet = NULL;
    for (auto _bullet = m_bullets.begin(); _bullet != m_bullets.end(); ++_bullet) {
        if (!(*_bullet)->m_active) {
            freeBullet = *_bullet;
            break;
        }
    }
    CatapultBullet *bullet;
    if (freeBullet == NULL) {
        bullet = new CatapultBullet();
        m_bullets.push_back(bullet);
    }
    else bullet = freeBullet;

    bullet->m_active = true;
    bullet->m_bulletSprite = Sprite::create();
    bullet->m_bulletSprite->runAction(Animate::create(m_bulletAnimation));

    m_config.bulletParentNode->addChild(bullet->m_bulletSprite, 60);
    bullet->m_targetZoneRow = m_targetedZone;
    bullet->m_targetZoneCol = m_config.zoneCol;
    bullet->m_sourceY = m_node->getPositionY();
    bullet->m_targetY = m_config.controller->zones[m_targetedZone][0].center.y;
    bullet->m_pathLength = bullet->m_targetY - bullet->m_sourceY;
    bullet->m_bulletSprite->setPosition(Point(m_node->getPositionX(), bullet->m_sourceY));
    bullet->m_ySpeed = bullet->m_pathLength / 1.5;
    bullet->m_attackPower = m_attackPower;

    //sound
    int sNum = Tools::randInt(1, 4);
    __String *s = __String::createWithFormat("gameScreen/bulletFlight%02d.wav", sNum);
    SimpleAudioEngine::getInstance()->playEffect(s->getCString(), false, 1.0, 0.0, m_config.soundFlightGain);
}

void Catapult::update(float dt) {

    for (auto _bullet = m_bullets.begin(); _bullet != m_bullets.end(); ++_bullet) {
        if ((*_bullet)->m_active) {
            Point pos = (*_bullet)->m_bulletSprite->getPosition();
            pos.y += dt * (*_bullet)->m_ySpeed;
            float pathScale = (pos.y - (*_bullet)->m_sourceY) / (*_bullet)->m_pathLength;
            (*_bullet)->m_bulletSprite->setScale(0.5 + sinf(pathScale * 3.14));
            (*_bullet)->m_bulletSprite->setPosition(pos);
            if (pos.y > (*_bullet)->m_targetY) {
                (*_bullet)->m_active = false;
                (*_bullet)->m_bulletSprite->removeFromParent();
                m_config.controller->bulletLanded(*_bullet);
            }
        }
    }

    if (m_fogState == CatapultFogState::still) {
        m_fogTimeout -=dt;
        m_fireIndicator->setPercent((int)(m_fogTimeout / m_fogTime * 100.0));
        if (m_fogTimeout <= 0.0)
            removeFog();
    }

    if (m_guillotineEnabled) {
        if (m_guilloTimer <= 0.0){
            if (m_config.controller->isAnyMonsterOppositeGuillotine(this)) {
                float halfDuration = 0.3;
                Vector<FiniteTimeAction*> actions;
                actions.pushBack(MoveTo::create(halfDuration, m_guilloInitPos + Point(0.0, 50.0)));
                actions.pushBack(CallFunc::create(CC_CALLBACK_0(Catapult::guilloAttackTime, this)));
                actions.pushBack(MoveTo::create(halfDuration, m_guilloInitPos));

                auto *action = Sequence::create(actions);
                //m_guilloSprite->stopAllActions(); // this stops animated guillotine
                m_guilloSprite->runAction(action);
                m_guilloTimer = m_guilloInterval;
            }
        }
        else {
            m_guilloTimer-=dt;
        }
    }



}

Rect Catapult::getGuillotineAttackRect() {
    float hWidth = 30.0, hHeight = 90.0;
    return Rect(
            m_node->getPositionX() - hWidth,
            m_node->getPositionY(),
            hWidth*2.0,
            hHeight*2.0
    );
}

void Catapult::guilloAttackTime() {
    m_config.controller->guillotineAttack(this);
}

void Catapult::recieveFog() {
    m_touchId = -1;
    m_isBlocked = true;
    m_targetingNode->setVisible(false);
    hideTarget();
    m_zoneNumbers->setVisible(false);

    if (m_fogState == CatapultFogState::still) {
        m_fogTimeout = m_fogTime;
    }
    else {
        if (m_fogState == CatapultFogState::none) {
            m_fogSprite->setScale(0.0);
            m_fogSprite->setVisible(true);
        }
        else {
            m_fogSprite->stopAllActions();
        }

        m_fogSprite->runAction(Animate::create(AnimationCache::getInstance()->getAnimation("catapultFog")));
        m_fogSprite->runAction(Sequence::createWithTwoActions(
                ScaleTo::create(0.3, 1.0),
                CallFunc::create(CC_CALLBACK_0(Catapult::fogActionComplete, this))
        ));
        m_fogState = CatapultFogState::entering;

    }

    m_fireIndicatorNode->setVisible(true);
}

void Catapult::showTarget() {
    Zone z = m_config.controller->zones[m_targetedZone][m_config.zoneCol];
    m_target->setVisible(true);
    m_zoomer->setVisible(true);
    m_target->setPosition(z.center);
    m_zoomer->setPosition(z.center);
    // some sheet
    m_zoomer->setZoomRegion(Tex2F(
            (z.center.x + m_config.controller->m_screen->rootNode->getPositionX() * 0.5f) / m_zoomer->m_texture->getContentSize().width,
            (z.center.y + m_config.controller->m_screen->rootNode->getPositionY() * 0.5f)/ m_zoomer->m_texture->getContentSize().height));
    m_zoneNumbers->setVisible(true);
}

void Catapult::hideTarget() {
    m_target->setVisible(false);
    m_zoomer->setVisible(false);
    m_zoneNumbers->setVisible(false);

}

void Catapult::fogActionComplete() {
    if (m_fogState == CatapultFogState::entering) {
        m_fogState = CatapultFogState::still;
        m_fogTimeout = m_fogTime;
    }
    else if (m_fogState == CatapultFogState::leaving) {
        m_fogSprite->stopAllActions();
        m_fogSprite->setVisible(false);
        m_isBlocked = false;
        m_fogState = CatapultFogState::none;
    }
}


void Catapult::removeFog() {
    m_fogSprite->runAction(Sequence::createWithTwoActions(
            ScaleTo::create(0.3, 0.0),
            CallFunc::create(CC_CALLBACK_0(Catapult::fogActionComplete, this))
    ));
    m_fireIndicatorNode->setVisible(false);
    m_fogState = CatapultFogState::leaving;
}


void Catapult::allowTouches(int flags) {
    if (flags & (int)TouchReceiverFlag::catapult)
        m_touchesAllowed = true;
}

void Catapult::denyTouches(int flags) {
    if (flags & (int)TouchReceiverFlag::catapult)
        m_touchesAllowed = false;
}

void Catapult::forceTargetingVisible(bool value) {
    m_forceTargetingVisible = value;
    m_targetingNode->setVisible(value);
}

Catapult::~Catapult() {
    if (m_bulletAnimation) m_bulletAnimation->release();
    Director::getInstance()->getEventDispatcher()->removeEventListener(m_touchListener);
    CheetahAtlasHelper::getInstance()->remove("gameScreen/towers/fog.atlas");
    CheetahAtlasHelper::getInstance()->remove("gameScreen/guillotines/guillotines.atlas");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("gameScreen/bullets/bullets.plist");
    SpriteFrameCache::getInstance()->removeSpriteFramesFromFile("gameScreen/catapult/catapult.plist");
}


void Catapult::setIdleMode(bool idle) {
    m_idleMode = idle;
}
