//
// Created by Admin on 16.01.14.
//


#ifndef __MRUIWidgets_H_
#define __MRUIWidgets_H_

#include "cocos2d.h"
#include "ui/CocosGUI.h"

using namespace cocos2d;

namespace myth {

    class GameUIController;
    enum class UIUpgradeType : int;
    namespace mgui {



        namespace upgrades {

            class Item : public cocos2d::ui::Layout {
            public:
                enum class ValueType {
                    attack,
                    defence,
                    none
                };

                cocos2d::ui::Button *m_icon;
                cocos2d::Label *m_priceGold;
                cocos2d::Label *m_priceCrystal;
                static Item *create(std::string iconFrameName, float priceGold, float priceCrystal);
                static Item *create(std::string iconFrameName, float priceGold, float priceCrystal, char const *priceGoldFormat);
                static Item *create();

                void setEquiped();

                void setSuperEquiped();
                void setPriceBottomMargin(float margin);
                void addCrystalIcon();
                void addGoldIcon();

                void setHighlight(bool isHighlighted);

                void setValue(std::string string);
                void setValue(std::string string, ValueType type);

                void addArrow();

            protected:
                void setup(std::string iconFrameName, float priceGold, float priceCrystal, char const *priceGoldFormat);
                void iconTouched(Object *sender, TouchEventType type);


                cocos2d::ui::ImageView *m_crystalIcon;
                cocos2d::ui::ImageView *m_goldIcon;
            };

            class Panel : public cocos2d::ui::Layout {
            public:
                GameUIController *m_controller;

                virtual void refreshItems() {};
                void placeBackButton(int zOrder);

                void backButtonTouched(cocos2d::Object *sender, TouchEventType type);

                void placeCloseButton(int zOrder);

                void closeButtonTouched(Object *sender, TouchEventType type);

                void replaceBackButton();

                void replaceCloseButton();
            };

            class TypeSelectPanel : public Panel {
            public:
                static TypeSelectPanel *create(GameUIController *controller);
            protected:
                void setup();
                void iconTouched(Object *sender, TouchEventType type);
            };

            class BridgeUpgradePanel : public Panel {
            public:
                static BridgeUpgradePanel *create(GameUIController *controller);
            protected:
                void setup();
                void upTouched(Object *sender, TouchEventType type);

                void showUpgradeBridge(int bridgeId);

                cocos2d::ui::Layout *m_upgradeBridgePanel;

                void buttonTouched(Object *sender, TouchEventType type);

                void switchBridge(int offsetX, int offsetY);


            public:
                virtual void refreshItems() override;

                cocos2d::ui::Layout *m_buttons;

                void hideUpgradeBridge();

                Layout *m_scalable;
            };
            /*
            class BridgeUpgradePanel : public Panel {
            public:
                cocos2d::ui::Layout *m_buttons;
                static BridgeUpgradePanel *create(GameUIController *controller);
            protected:
                void setup();
                void buttonTouched(Object *sender, TouchEventType type);

            public:
                virtual void refreshItems() override;


            };
            */
            class TowerUpgradePanel : public Panel {
            public:
                cocos2d::ui::ScrollView *m_scrollable;
                static TowerUpgradePanel *create(GameUIController *controller);
            protected:
                void setup();
                void buttonTouched(Object *sender, TouchEventType type);

            public:
                virtual void refreshItems() override;

                ui::Layout *m_scalable;
            };

            class MoneyUpgradePanel : public Panel {
            public:
                cocos2d::ui::Layout *m_buttons;
                std::string m_prevPanelName;
                static MoneyUpgradePanel *create(GameUIController *controller);
                virtual void refreshItems() override;
            protected:
                void setup();
                void buttonTouched(Object *sender, TouchEventType type);

                ui::Layout *m_panelNode;
            };


            class MagicUpgradePanel : public Panel {
            public:
                cocos2d::ui::Layout *m_buttons;

                static MagicUpgradePanel *create(GameUIController *controller);
                virtual void refreshItems() override;
            protected:
                void setup();
                void buttonTouched(Object *sender, TouchEventType type);

                Layout *m_scalable;
            };
        }


        class Button : public cocos2d::ui::Button {
        public:

            static mgui::Button* create(std::string label);
        protected:
            virtual bool init() override;

        public:
            void setTitleTextCustom(const std::string text);

            Label *m_label;
        };


        class DialogBox : public cocos2d::ui::Layout {
        public:
            mgui::Button *m_okButton;
            mgui::Button *m_cancelButton;
            cocos2d::Label *m_titleField;
            upgrades::Panel *m_coveredPanel;
            static DialogBox *create(std::string title, std::string okLabel, std::string cancelLabel);
            static DialogBox *create(std::string title, std::string okLabel, std::string cancelLabel, int textFontSize);
            static DialogBox *create(int textFontSize);
        protected:
            virtual bool init(int textFontSize);
        };

        class MonsterCard : public cocos2d::ui::Layout {
        public:
            static MonsterCard *create(std::string id, GameUIController *controller);
            void setTouchEndedCallback(std::function<void()> callback);
            std::string m_monsterId;

        protected:
            void setup();
            void buttonTouched(Object *sender, TouchEventType type);
            GameUIController *m_controller;
            std::function<void()> m_touchEndedCallback = nullptr;
        };

        class Sale : public cocos2d::ui::Layout {
        public:
            static Sale *create(int id);
            static Sale *create(int id, std::function<void(int saleId)> onSaleClicked, std::function<void()> onCloseClicked);
            int m_saleId;
            std::function<void(int saleId)> m_onSaleClicked;
            std::function<void()> m_onCloseClicked;

        protected:
            void setup();

        };
    }




}

#endif //__MRUIWidgets_H_
