//
// Created by Admin on 20.02.14.
//



#ifndef __MRMonsterMushroom_H_
#define __MRMonsterMushroom_H_

#include "MRMonster.h"
#include "MRMonsterUnmovable.h"

namespace myth {

class GameController;
class Zone;

    enum class MushroomState {
        hiding,
        entering,
        staying,
        leaving,
        leaved
    };

class MonsterMushroom : public Monster, public MonsterUnmovable  {
public:
    MushroomState m_mushroomState;


    virtual void update(float dt) override;

    virtual void fillAnimations(ConfigReader *reader) override;

    virtual void fillData(ConfigReader *reader) override;

    float m_stayTime;
    float m_stayTimer;

    virtual void animationComplete() override;

    virtual void setup(ConfigReader *reader) override;

    virtual void recieveDamage(float damage, DamageType type) override;

    virtual bool isActive() override;

    virtual std::vector<Zone *> getCurrentZones();

    Zone *m_lastZone;

    virtual bool isGoing() override;

};

}

#endif //__MRMonsterMushroom_H_
