//
// Created by Admin on 15.01.14.
//


#ifndef __MRGameUIController_H_
#define __MRGameUIController_H_

#include <core/MRMoneyUpgradeDelegate.h>
#include "cocos2d.h"
#include "MRUIWidgets.h"
#include "MRUpgradeType.h"
#include "MRPauseMenuPanel.h"
#include "MRMagicType.h"
#include "MRTouchController.h"


using namespace cocos2d;
using namespace cocos2d::ui;

namespace myth {

class GameController;

enum class UIUpgradeType : int {
    magic,
    money,
    tower,   //bridge|wall actually
    bridge
};

enum class TechType : int {
    tower,
    bridge
};

class GameUIControllerConfig {
public:

    GameUIControllerConfig() : moneyUpgradeDelegate(nullptr), controller(nullptr){
    }

    Node *parentNode;
    GameController *controller;
    MoneyUpgradeDelegate *moneyUpgradeDelegate;
};

class GameUIController : public Object, public TouchController {
public:
    GameUIControllerConfig m_config;

    GameUIController() :
            m_askForBuyMagicianLifeDialog(nullptr),
            m_askForBuyMagicianLifeQueriesCount(0),
            m_upgradesMenuOpened (false),
            m_buingCrystals (false),
            m_pauseMenuPanel (nullptr) {}

    void upgradesBtnClicked(Object *sender, TouchEventType type);
    void magicButtonClicked(Object *sender, TouchEventType type);
    bool m_upgradesMenuOpened;
    void upgradesClose();
    void upgradesOpen();
    void upgradeTypeSelected(UIUpgradeType type);

    void setup();

    Button *m_magicActivateButton;
    mgui::upgrades::TypeSelectPanel *m_uTypeSelectPanel = nullptr;

    void openTypeSelect();

    void closeTypeSelect();

    void openUpgradeBridge();

    mgui::upgrades::BridgeUpgradePanel *m_bridgeUpgradePanel = nullptr;

    void addShadow();

    void removeShadow();

    //void openUpgradeBridge(int index);

    void openUpgradeTower(int index);

    void closeBridgeUpgrades();


    TechType m_currentTechType;
    int m_currentTechIndex;

    void upgradeBridgeTo(int index);

    //mgui::upgrades::BridgeUpgradePanel *m_bridgesUpdatePanel = nullptr;
    mgui::upgrades::TowerUpgradePanel *m_towersUpdatePanel = nullptr;
    mgui::DialogBox *m_areYouSureDialog = nullptr;

    void upgradeBulletTo(int i);

    void upgradeBulletToSuper(int i);

    void upgradeGuillotineTo(int i);

    //void closeBridgeUpgrades();

    void closeTowerUpgrades();

    void upgradeTowerTo(int i);

    bool canTouch(UpgradeType type, int index);

    bool isHighlighted(UpgradeType type, int index);
    bool isHighlightedMagic(MagicType type, int index);

    void areYouSureTouched(Object *sender, TouchEventType type);

    int m_currentUpgradeIndex;
    UpgradeType m_currentUpgradeType;
    mgui::upgrades::Panel *m_currentPanel;

    void openAreYouSureDialog(std::string q = "");

    void openBuyMoneyDialog();

    mgui::DialogBox *m_buyMoneyDialog;

    void buyMoneyDialogTouched(Object *sender, TouchEventType type);

    void openUpgradeMoney();

    void upgradeMoneyTo(int i);

    mgui::upgrades::MoneyUpgradePanel *m_moneyUpgradePanel = nullptr;

    void backFromPanel();

    void closeMoneyUpgrades();

    Label *m_goldLabel;

    void updateStats();

    Label *m_crystalsLabel;

    Label *m_lifeLabel;
    LoadingBar *m_magicProgessBars[4];
    Button *m_pauseButton;

    void pauseButtonTouched(Object *sender, TouchEventType type);

    virtual ~GameUIController();

    mgui::PauseMenuPanel *m_pauseMenuPanel;

    void openPauseMenu();

    void setBloodEnabled(bool value);

    void setSoundVolume(float value);

    void setMusicVolume(float value);

    void closePauseMenu();

    void restartLevel();

    void exitToMainMenu();

    void getHelp();

    void openUpgradeMagic();

    void closeMagicUpgrades();

    mgui::upgrades::MagicUpgradePanel *m_magicUpgradePanel = nullptr;

    void upgradeMagicTo(std::string magicName, int upgradeId);

    MagicType m_currentUpgradeMagicType;

    bool canTouchMagic(MagicType magicType, int index);

    virtual void allowTouches(int flags);

    virtual void denyTouches(int flags);

    bool m_magicButtonTouchAllowed;
    bool m_pauseButtonTouchAllowed;
    bool m_upgradeButtonTouchAllowed;

    void showMonsterCard(std::string id);

    void closeMonsterCard(mgui::MonsterCard *card);

    std::vector<mgui::MonsterCard*> m_monsterCards;
    ui::Widget *m_upgradeButton;

    void switchTower(int offset);

    Label *m_scoreLabel;

    void displayComboCounter(int kills);

    Label *m_comboLabel;

    bool isEquiped(UpgradeType type, int index);

    std::function <void()> m_buyLifeFailureCallback;
    void askForBuyMagicianLife(std::function<void()> failureCallback, bool askForBuyCrystals);

    void askFouBuyMagicianLifeTouched(Object *sender, ui::TouchEventType type);

    mgui::DialogBox *m_askForBuyMagicianLifeDialog;

    void switchTowerTo(int towerId);

    int m_askForBuyMagicianLifeQueriesCount;

    void disappearAskForBuyLifeDialog();

    bool m_buingCrystals;

    void placeSkipTutorialButton();

    bool isEquipedMagic(MagicType type, int upgradeIndex);
};

}
#endif //__MRGameUIController_H_
