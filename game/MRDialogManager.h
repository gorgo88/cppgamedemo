//
// Created by Admin on 06.03.14.
//


#ifndef __MRDialogManager_H_
#define __MRDialogManager_H_

#include "core/myth.h"
#include "ui/CocosGUI.h"

namespace myth {

class DialogManager : public Ref {
    class Dialog {

    };
public:

    enum class NextButtonType {
        skip,
        next
    };

    Node *m_node;

    DialogManager() : m_node(nullptr), m_label(nullptr), m_nextBtnType(NextButtonType::skip) {
    }

    NextButtonType m_nextBtnType;

    static DialogManager *getInstance();
    void createDialog(std::vector<std::string> statements, float delayScale, std::function<void ()> callback);

    void show(cocos2d::Node *parentNode, int zOrder);
    void show(cocos2d::Node *parentNode, int zOrder, Point point);

    void hide();

    void moveTo(Point point);

    void skip();

    function<void()> m_currentCallback;
    vector<string> m_currentStatements;
    int m_currentStatementIndex;

    void tellNextStatement(bool hideAtTheEnd);

    float m_currentDelayScale;

    void statementTellingComplete();

    Label *m_label;

    void buttonTouched(Ref *sender, ui::TouchEventType type);

    DelayTime *m_currentStayDelayAction;
    bool m_isTelling;
    bool m_autoCompleteMode = true;

    void setAutoCompleteMode(bool value);

    bool m_currentCallbackCalled;

    void setPaused(bool value);

    void reset();

    void refreshNextButton(NextButtonType type);

    void skipAll();
};

}
#endif //__MRDialogManager_H_
