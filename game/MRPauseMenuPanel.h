//
// Created by Admin on 10.02.14.
//


#ifndef __MRPauseMenuPanel_H_
#define __MRPauseMenuPanel_H_

#include "cocos2d.h"
#include "ui/CocosGUI.h"

using namespace cocos2d;

namespace myth {
    class GameUIController;
    namespace mgui {
        class PauseMenuPanel: public cocos2d::Node {
        public:
            GameUIController *m_controller;
            static PauseMenuPanel *create(GameUIController *controller);
        protected:
            void setup();
            void buttonTouched(Object *sender, cocos2d::ui::TouchEventType type);

            void controlValueChanged(Ref *sender, cocos2d::ui::TouchEventType eventType);

            cocos2d::ui::Slider *m_soundSlider;
            cocos2d::ui::Slider *m_musicSlider;

        };
    }
}



#endif //__MRPauseMenuPanel_H_
