//
// Created by Admin on 10.12.13.
//



#ifndef __Bridge_H_
#define __Bridge_H_

#define ALLOWED_MONSTERS_TO_ATTACK 2

#include "core/myth.h"
#include "LifeBar.h"

namespace myth {

class GameController;
class Monster;

enum class BridgeState {
    up,
    down
};

class BridgeConfig {
public:
    int row;
    int col;
    Node *parentNode;
    int zOrder;
    float fallTimeout = 2.2;
    GameController *controller;
    int startUpgrade = 0;
};

class Bridge : protected EasyConfig {
public:
    float m_defencePower;
    float m_lifeMax;
    float m_life;

    BridgeConfig m_config;
    BridgeState m_state;
    void setup ();
    void applyUpgrade (int upgradeIndex, ConfigReader *reader);
    void fillAnimations(int upgradeIndex);
    Node *m_node;
    Sprite *m_sprite;

    void recieveDamage(float damage);
    bool isInAttackZone(cocos2d::Point point);

    void getDown();
    void getUp();

    void update(float dt);

    virtual ~Bridge();

    Animation *m_getUpAnimation;
    Animation *m_getDownAnimation;
    float m_fallTimeout;
    LifeBar *lifeBar;

    void updateLifeBar();

    float getLifeScale();

    Animation *m_damageAnimation;
    Monster *m_attackingMonsters[ALLOWED_MONSTERS_TO_ATTACK];
};

}
#endif //__Bridge_H_
