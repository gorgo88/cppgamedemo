//
// Created by Admin on 22.05.14.
//


#ifndef __MRMonsterData_H_
#define __MRMonsterData_H_

#include "cocos2d.h"
#include "Config/ConfigReader.h"

namespace myth {
    class MonsterData {
    public:
        MonsterData():price(-1), priceCrystal(-1), lifeMax(-1) {}
        std::string id;
        int count;
        int price;
        int priceCrystal;
        int lifeMax;
        float attackDelay;
        ConfigReader *configReader;
    };
}


#endif //__MRMonsterData_H_
