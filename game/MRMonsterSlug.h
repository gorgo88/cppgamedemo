//
// Created by Admin on 14.01.14.
//


#ifndef __MRMonsterSlug_H_
#define __MRMonsterSlug_H_

#include "MRMonster.h"

namespace myth {

class MonsterSlug : public Monster {
public:

    MonsterSlug() {
        m_isDuplicated = false;
        m_isClone = false;
        m_isJumping = false;
    }

    bool m_isDuplicated;
    virtual void recieveDamage(float damage) override;

    void duplicate();

    bool m_isClone;
    bool m_isJumping;

    void jump();
    void jumpComplete();

    virtual void update(float dt) override;

    virtual void fillData(ConfigReader *reader) override;

    virtual void fillPrice(ConfigReader *reader) override;

};

}

#endif //__MRMonsterSlug_H_
