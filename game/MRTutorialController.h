//
// Created by Admin on 13.02.14.
//


#ifndef __MRTutorialController_H_
#define __MRTutorialController_H_

#include "cocos2d.h"
#include "ui/CocosGUI.h"

using namespace cocos2d;

namespace myth {
    class GameController;

    class TutorialController : public cocos2d::Object {

    public:
        TutorialController (GameController *controller) {
            m_controller = controller;
            m_pseudoTarget = nullptr;
            m_magicSymbolDrawStarted = false;
        }

        virtual ~TutorialController();

        void update(float dt);
        GameController *m_controller;
        void setup();

        Vector<Node*> *m_pausableNodes;

        int m_currientLetter;
        float m_letterTimer;
        float m_letterTimeout;

        //void printLetter();

        cocos2d::Label *m_textField;
        bool m_readyToClose;
        cocos2d::EventListenerTouchOneByOne *m_touchBlocker;

        void touched(cocos2d::Object *sender, cocos2d::ui::TouchEventType type);

        int m_currentTextLen;
        std::wstring m_currentText;

        //void animationComplete();
        void pointCaves();

        void talkIslands();

        void pointIslands();

        void talkSlide();

        void pointSlide();

        void finishSlide();
        
        void talkTryStatic();

        void placeStaticTarget();

        void allMonstersKilled();

        int m_wave;

        void placeOneDynamicTargets();

        void takeTouches();

        void releaseTouches();

        void talkDynamicTarget();

        void talkGoodJob();

        void placeManyDynamicTargets();

        void talkWhatAboutMagic();

        void placeRestOfEnemies();

        void talkMagicScale();

        void highlihgtMagicScale();

        void talkDrawSymbol();

        void DrawSampleSymbol();

        void magicSymbolDrawn();

        void showMagicIslandsSelect();

        void talkActivateMagicMode();

        void highlightMagicButton();

        void talkManySymbols();

        void highlightAllMagicScales(bool finish);

        void talkWellDone();

        void skip();

        bool m_waitForDrawMagicSymbol;

        bool touchBegan(cocos2d::Touch * touch, cocos2d::Event * event);

        cocos2d::Sprite*m_pointerForCatapult;
        bool m_catapultSliding;
        cocos2d::Touch m_touch;

        cocos2d::Sprite *getHand();

        void drawSampleSymbolCaptureStart(cocos2d::Sprite * pSprite);

        void drawSampleSymbolCaptureStop();

        bool m_sampleSymbolCapturing;
        cocos2d::Sprite *m_sampleSymbolCaptureTarget;

        void talkCastIslandCount();

        void PointCastIslandCount();

        void talkTryYourMagic();

        bool m_waitForClickMagicButton;

        void magicButtonClicked();

        void talkCastIslandsDefine();

        void talkTryMakeAShoot();

        bool m_waitForCatapultTouched;

        void firstCatapultTouched();

        cocos2d::Vec2 m_glDiff;

        void setPaused(bool value);

        void stopPointIslands();

        bool m_waitForClickMagicButton2;

        void magicSymbolDrawStarted();

        void magicUserDrawSymbolStarted();

        void magicAppliedAndSomeoneIsAlive();

        void catapultShot();

        Sprite *m_pseudoTarget;
        bool m_slideHintShowed;
        bool m_magicSymbolDrawStarted;

        void monsterAttackedBridge();

        bool m_waitForFirstMonsterAttack;

        void waitJustABit(float time);

        float m_waitABitTimer;

        void waitJustABitComplete();

        bool m_magicStreakCapturing;
        cocos2d::Node *m_magicStreakTarget;
        bool m_magicFirstTime;

        void talkGoodCatapult();
    };

}

#endif //__MRTutorialController_H_
