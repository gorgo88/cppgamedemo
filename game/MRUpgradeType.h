#ifndef __MRUpgradeType_H_
#define __MRUpgradeType_H_

namespace myth {
    enum class UpgradeType : int {
        bridge,
        tower,
        bullet,
        guillotine,
        magic,
        money
    };
}
#endif