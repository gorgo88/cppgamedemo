//
// Created by Admin on 12.03.15.
//


#ifndef __MRMonsterDragon_H_
#define __MRMonsterDragon_H_

#include "MRMonster.h"
#include "MRMonsterUnmovable.h"
#include "MRGameController.h"
#include "MRMonsterMultiNode.h"
#include "MRIMonsterBoss.h"

namespace myth {
    class MonsterDragon : public Monster, public MonsterUnmovable, public MonsterMultiNode, public IMonsterBoss {
    struct Segment {
        Segment() :
                attackTimer(-1), node(nullptr), spriteHolder(nullptr), sprite(nullptr),
                idleTimer(-1), zone(nullptr), firstPlaced(false), headIsGoingOut(false),
                deafTimer(-1)
        {}
        float   attackTimer;
        float   attackDelayTimer;
        float   deafTimer;
        Node    *node;
        Node    *damageNode;
        Node    *spriteHolder;
        Sprite  *sprite;
        float   idleTimer;
        Zone *zone;
        bool firstPlaced;
        bool headIsGoingOut;
    };
    public:
        static const int SEGMENT_COUNT = 3;

        virtual ~MonsterDragon();

        virtual std::vector<Zone *>getCurrentZones();

        virtual void setup(ConfigReader *reader) override;

        virtual void update(float dt) override;

        virtual void fillData(ConfigReader *reader) override;

        virtual std::vector<std::string> getAllStandardAnimations() override;

        virtual void animationComplete() override;

        Segment m_segments[SEGMENT_COUNT];

        void headInSegment(int segmentIndex);

        int m_headSegmentIndex;

        int offsetSegmentIndex(int old, int offset);

        float m_idleTime;
        virtual std::vector<cocos2d::Node *> getNodes();

        bool isSegmentPlaced(int index);

        virtual void receiveDamage(float damage, cocos2d::Node *targetNode);

        bool m_vulnerabilityHead;
        bool m_vulnerabilityTail;

        void forceHeadOut(bool endless = false);

        virtual bool isActive() override;

        virtual void dieMonster() override;

        virtual bool shouldUseCommonDeathAnimation() override;

        virtual void deathAnimationComplete() override;


        virtual void winDefile() override;

        int m_isDefile;

        void stun();

        float m_deafTime;


        virtual void fillSounds(ConfigReader *reader) override;

        std::pair<std::string, float> m_attackSnd;
        std::pair<std::string, float> m_deafSnd;
        std::pair<std::string, float> m_damageSnd;
        int m_stunCount = -1;
    };
}

#endif //__MRMonsterDragon_H_
