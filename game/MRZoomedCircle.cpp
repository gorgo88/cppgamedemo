//
// Created by Admin on 28.01.14.
//

#include "MRZoomedCircle.h"

using namespace cocos2d;
using namespace myth;


ZoomedCircle *ZoomedCircle::create(Texture2D *texture, float texRadius, float worldRadius) {
    ZoomedCircle *node = new ZoomedCircle();
    if (node && node->init()) {
        node->m_texture = texture;
        texture->retain();
        float texRatio = (float)texture->getPixelsWide() / (float)texture->getPixelsHigh();
        node->m_tcDim = Tex2F(texRadius, texRadius * texRatio);
        node->m_radiusWorld = worldRadius;
        node->setup();
        node->autorelease();
        return node;
    }
    CC_SAFE_DELETE(node);
    return nullptr;
}

void ZoomedCircle::setup() {
    setShaderProgram(ShaderCache::getInstance()->getProgram(GLProgram::SHADER_NAME_POSITION_TEXTURE_COLOR_NO_MVP));

    float dim = m_radiusWorld;
    m_quadCount = 8;
    float da = (float)M_PI * 2 / m_quadCount;
    Tex2F tcOffset = Tex2F(0.5, 0.5);
    Tex2F tcDim = m_tcDim;
    for (int i = 0; i < m_quadCount; ++i) {
        float a = da * i;
        float a1 = a + da*0.5;
        float a2 = a1 + da*0.5;
        Color4B vertColor = Color4B(255,250,255,255);
        m_quad[i].bl.vertices = Vertex3F(0.0, 0.0, getVertexZ());
        m_quad[i].br.vertices = Vertex3F(cosf(a)*dim, sinf(a)*dim, getVertexZ());
        m_quad[i].tr.vertices = Vertex3F(cosf(a1)*dim, sinf(a1)*dim, getVertexZ());
        m_quad[i].tl.vertices = Vertex3F(cosf(a2)*dim, sinf(a2)*dim, getVertexZ());
        m_quad[i].bl.colors = vertColor;
        m_quad[i].br.colors = vertColor;
        m_quad[i].tr.colors = vertColor;
        m_quad[i].tl.colors = vertColor;
        m_quad[i].bl.texCoords = Tex2F(tcOffset.u, tcOffset.v);
        m_quad[i].br.texCoords = Tex2F(tcOffset.u + cosf(a)*tcDim.u, tcOffset.v + sinf(a)*tcDim.v);
        m_quad[i].tr.texCoords = Tex2F(tcOffset.u + cosf(a1)*tcDim.u, tcOffset.v + sinf(a1)*tcDim.v);
        m_quad[i].tl.texCoords = Tex2F(tcOffset.u + cosf(a2)*tcDim.u, tcOffset.v + sinf(a2)*tcDim.v);

    }

    //GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POSITION | GL::VERTEX_ATTRIB_FLAG_COLOR);
}

void ZoomedCircle::setZoomRegion(Tex2F tcOffset) {
    float da = (float)M_PI * 2 / m_quadCount;
    for (int i = 0; i < m_quadCount; ++i) {
        float a = da * i;
        float a1 = a + da*0.5;
        float a2 = a1 + da*0.5;
        m_quad[i].bl.texCoords = Tex2F(tcOffset.u, tcOffset.v);
        m_quad[i].br.texCoords = Tex2F(tcOffset.u + cosf(a)*m_tcDim.u, tcOffset.v + sinf(a)*m_tcDim.v);
        m_quad[i].tr.texCoords = Tex2F(tcOffset.u + cosf(a1)*m_tcDim.u, tcOffset.v + sinf(a1)*m_tcDim.v);
        m_quad[i].tl.texCoords = Tex2F(tcOffset.u + cosf(a2)*m_tcDim.u, tcOffset.v + sinf(a2)*m_tcDim.v);

    }
}

ZoomedCircle::~ZoomedCircle() {
    if (m_texture) m_texture->release();


}

void ZoomedCircle::draw(Renderer *renderer, const kmMat4& transform, bool transformUpdated) {
//    m_renderCommand.init(0, getVertexZ());
//    m_renderCommand.func = std::bind(&ZoomedCircle::render, this);
    
    m_renderCommand.init(getVertexZ(), m_texture->getName(), getGLProgramState(), BlendFunc::ALPHA_PREMULTIPLIED, m_quad, m_quadCount, _modelViewTransform);
    renderer->addCommand(&m_renderCommand);


}

void ZoomedCircle::render() {



    /*
    GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POSITION | GL::VERTEX_ATTRIB_FLAG_COLOR);
    _shaderProgram->use();


    glEnableVertexAttribArray(GL::VERTEX_ATTRIB_FLAG_POSITION);
    glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, 0, vertices);

    glEnableVertexAttribArray(GL::VERTEX_ATTRIB_FLAG_COLOR);
    glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, GL_TRUE, 0, colors);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, count);
    */

}

void ZoomedCircle::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags) {
    Node::draw(renderer, transform, flags);
    m_renderCommand.init(getVertexZ(), m_texture->getName(),getGLProgramState(), BlendFunc::ALPHA_PREMULTIPLIED, m_quad, m_quadCount,_modelViewTransform, flags);
    renderer->addCommand(&m_renderCommand);

}
