//
// Created by Admin on 21.03.14.
//


#ifndef __MRComixEnum_H_
#define __MRComixEnum_H_

namespace myth {

enum class ComixEnum : int {
    first,
    after1stLocation,
    after2ndLocation,
    after3rdLocation,
    after4thLocation
};

}
#endif //__MRComixEnum_H_
