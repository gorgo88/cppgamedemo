//
// Created by Admin on 21.05.14.
//


#include "Config/ConfigReader.h"
#include "MRMonsterData.h"

#ifndef __MRSpawnProgram_H_
#define __MRSpawnProgram_H_

namespace myth {

    class GameController;
    class SpawnProgram {

    public:
        class QueueItem {
        public:
            int monsterDataIndex;
            int total;
            int spawned;
        };


        enum class TimingType {absolute, relative};
        enum class State {notStarted, going, complete};
        enum class Tag {main, mainGinors, bosses, bossGinors, bossBats};

        TimingType m_type;
        State m_state;
        Tag m_tag;

        std::function<void(MonsterData *data, SpawnProgram *program)> m_spawnCallback;
        std::function<void(SpawnProgram *program)> m_completeCallback;

        std::string m_id;
        float m_interval;
        float m_delay;

        GameController *m_controller;

        std::vector<MonsterData> m_monsterDatas;
        std::vector<QueueItem> m_queue;


        SpawnProgram(std::string id) : m_id(id) {}

        void setup(ConfigReader *programReader);
        void update(float dt);

        void fillMonsterData(ConfigReader *reader);

        float getTotalTime();

        float getTotalMonsterCount();

        void fillQueue();

        void fillInterval(ConfigReader *reader);

        void fillDelay(ConfigReader *reader);

        void setController(GameController * controller);

        float m_spawnTimeout;

        void spawnTime();

        int m_queueItemIndex;

        void programComplete();

        void start();

        bool isGoing();

        std::string m_sectionId;

        bool isComplete();


        virtual ~SpawnProgram() {
            for (int i = 0; i < m_monsterDatas.size(); ++i)
                m_monsterDatas[i].configReader->release();
        }

        void forceComplete();
    };
}

#endif //__MRSpawnProgram_H_
