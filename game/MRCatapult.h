//
// Created by Admin on 10.12.13.
//



#ifndef __Catapult_H_
#define __Catapult_H_

#include "core/myth.h"
#include "MRZoomedCircle.h"
#include "MRTouchController.h"
#include "ui/CocosGUI.h"


namespace myth {

class GameController;

enum class CatapultFogState {
    entering,
    still,
    leaving,
    none
};

class CatapultConfig {
public:
    Node    *parentNode;
    Node    *bulletParentNode;
    int     touchPriority;
    std::string targetingMaskTexture;
    std::string targetingHandleTexture;
    GameController *controller;
    int     zoneCol;
    float   shootDelay = 0.4;
    float soundLoadGain = 1.0;
    float soundFlightGain = 1.0;
    int startTowerUpgrade = 0;
    int startBulletUpgrade = 0;
    int startGuillotineUpgrade = 0;
    Texture2D *zoomerTexture;
    Node *hudNode;
};

class CatapultBullet {
public:
    int     m_targetZoneRow;
    int     m_targetZoneCol;
    Sprite *m_bulletSprite;
    float   m_sourceY;
    float   m_targetY;
    float   m_pathLength;
    float   m_ySpeed;
    bool    m_active;
    float   m_attackPower;
};



class Catapult : public EasyConfig, public TouchController {
public:
    EventListenerTouchAllAtOnce *m_touchListener;
    CatapultConfig m_config;
    float m_attackPower;
    float m_resetTime;
    Node *m_node;
    Sprite *m_sprite;
    Node *m_targetingNode;
    Sprite *m_targetingHandle;
    Sprite *m_target;
    Rect m_touchRect;
    Rect m_handleRect;


    int m_targetedZone;

    std::vector<CatapultBullet*> m_bullets;
    Catapult() {
    }



    virtual ~Catapult();
    void setup ();
    void applyTowerUpgrade(int bulletUpgradeIndex, ConfigReader *upgradeReader);
    void applyBulletUpgrade(int bulletUpgradeIndex, ConfigReader *upgradeReader);
    void applyGuillotineUpgrade(int bulletUpgradeIndex, ConfigReader *upgradeReader);
    void onTouchesBegan(const std::vector<Touch*>& touches, Event  *event);
    void onTouchesMoved(const std::vector<Touch*>& touches, Event  *event);
    void onTouchesEnded(const std::vector<Touch*>& touches, Event  *event);
    bool touchBegan(Touch *touch, Event *event);
    void touchMoved(Touch *touch, Event *event);

    void touchEnded(Touch *touch, Event *event);
    void touchEnded();
    void shoot();
    void load();

    void update(float dt);

    void loadingComplete();


    int m_touchId;

    Sprite *m_catapultSprite;
    Animation *m_bulletAnimation;

    bool m_loading;

    void recieveFog();
    bool m_isBlocked;
    Sprite *m_fogSprite;
    CatapultFogState m_fogState;
    float m_fogTime;
    float m_fogTimeout;

    void fogActionComplete();

    void removeFog();

    Node *m_zoneNumbers;
    ZoomedCircle *m_zoomer;

    void hideTarget();

    void showTarget();

    Sprite *m_guilloSprite;

    void guilloAttackTime();

    float m_guilloTimer = 0.0;
    Point m_guilloInitPos;
    float m_guilloInterval;
    float m_guilloPower;

    Rect getGuillotineAttackRect();

    virtual void allowTouches(int flags);

    virtual void denyTouches(int flags);

    bool m_touchesAllowed;
    Node *m_fireIndicatorNode;
    ui::LoadingBar *m_fireIndicator;

    bool touchBegan(const Vec2 &worldPoint);
    void touchMoved(const Vec2 &worldPoint);
    void touchEnded(const Vec2 &worldPoint);

    Vec2 m_touchStartLocation;
    bool m_guillotineEnabled;
    bool m_forceTargetingVisible;

    void forceTargetingVisible(bool value);

    void setIdleMode(bool idle);

    bool m_idleMode;

    float handlerZonePositions[4]; // static
    float fingerZonePositions[4]; // recalculates every touch began depend on distance to screen edge
    void calculateFingerZones(Vec2 worldPoint);

    void calculateTargetingHandlePositions();
};
}


#endif //__Catapult_H_
