//
// Created by Admin on 20.08.14.
//

#include "MRMonsterDevastator.h"
#include "MRGameController.h"
using namespace myth;

const std::string WALK_DOWN = "walkDown";
const std::string WALK_LEFT = "walkLeft";
const std::string WALK_RIGHT = "walkRight";
const std::string DEATH = "death";

void MonsterDevastator::makeBloodPaddle() {
    return;
}

void MonsterDevastator::fillAnimations(ConfigReader *reader) {
    m_animations = Dictionary::create();
    m_animations->retain();

    vector<string> animationNames;
    animationNames.push_back(DEATH);
    animationNames.push_back(WALK_DOWN);
    animationNames.push_back(WALK_LEFT);
    animationNames.push_back(WALK_RIGHT);

    string textureId = m_config.data->id;
    if (reader->hasPath("textureId"))
        textureId = reader->fromPath("textureId")->toString()->_string;

    Animation *anim;
    for (int i = 0; i < animationNames.size(); ++i) {
        anim = Animation::create();
        char pattern[255];
        char fpsKey[255];
        sprintf(pattern, "gameScreen/monsters/%s/%s/%s.png", textureId.c_str(), animationNames[i].c_str(), "%04d");
        sprintf(fpsKey, "animation.%s.fps", animationNames[i].c_str());
        Tools::addFramesToAnimation(anim, pattern, reader->fromPath(fpsKey)->toFloat());
        m_animations->setObject(anim, animationNames[i]);
    }
}

void MonsterDevastator::update(float dt) {
    float ds = dt*m_speed;
    if (m_slowing) {
        m_speed += m_speed*0.05;
        if (m_speed >= m_initSpeed) {
            m_speed = m_initSpeed;
            m_slowing = false;
        }
    }
    Point pos = m_node->getPosition();
    if (!m_config.controller->canMonsterWalk(this)) {
        if (
                m_state == MonsterState::walkDown
                        ||m_state == MonsterState::walkLet
                        ||m_state == MonsterState::walkRight
                        ||m_state == MonsterState::walkDown2) m_bridge->recieveDamage(100500);
        return;
    }


    if (m_state == MonsterState::walkDown) {
        pos.y -= ds;
        m_node->setPosition(pos);

        if (pos.y <= m_config.tunt1Y) {
            if (m_config.column < 2) {
                m_state = MonsterState::walkRight;
                playAnimation(WALK_RIGHT, true);

            } else {
                m_state = MonsterState::walkLet;
                playAnimation(WALK_LEFT, true);

            }
            return;
        }
    }

    if (m_state == MonsterState::walkLet) {
        pos.x -= ds;
        m_node->setPosition(pos);

        if (pos.x <= m_config.turn2X) {
            m_state = MonsterState::walkDown2;
            playAnimation(WALK_DOWN, true);

            return;
        }
    }

    if (m_state == MonsterState::walkRight) {
        pos.x += ds;
        m_node->setPosition(pos);

        if (pos.x >= m_config.turn2X) {
            m_state = MonsterState::walkDown2;
            playAnimation(WALK_DOWN, true);

            return;
        }
    }

    if (m_state == MonsterState::walkDown2) {
        pos.y -= ds;
        m_node->setPosition(pos);

        if (pos.y <= m_config.endY) {
            m_state = MonsterState::dead;
            hasLeft();
            return;
        }
    }
}

void MonsterDevastator::recieveDamage(float damage, Monster::DamageType type) {
    if (type == DamageType::magic) return;
    Monster::recieveDamage(damage, type);
}
