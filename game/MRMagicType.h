#ifndef __MagicType_H_
#define __MagicType_H_

namespace myth {
    enum class MagicType :int {
        fire,
        air,
        water,
        earth,
        fail
    };
}

#endif