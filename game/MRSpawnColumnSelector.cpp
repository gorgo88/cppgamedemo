//
// Created by Admin on 22.05.14.
//

#include "MRSpawnColumnSelector.h"
#include "core/myth.h"

int myth::SpawnColumnSelector::getColumn() {
    int spawnCol = 0;
    while (true) {
        spawnCol = Tools::randInt(0, 3);
        if (spawnCol == m_lastMonsterCol && m_spawnSameColCount > 3)
            continue;
        else
            break;
    }
    if (spawnCol == m_lastMonsterCol) m_spawnSameColCount++;
    else {
        m_lastMonsterCol = spawnCol;
        m_spawnSameColCount = 1;
    }

    return spawnCol;
}
