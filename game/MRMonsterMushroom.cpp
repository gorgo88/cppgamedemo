//
// Created by Admin on 20.02.14.
//

#include "MRMonsterMushroom.h"
#include "MRGameController.h"

using namespace myth;

const string ENTER = "enter";
const string LEAVE = "leave";


void MonsterMushroom::setup(ConfigReader *reader) {
    m_speed = m_config.speed;
    fillData(reader);
    m_life = m_lifeMax;


    m_node = Node::create();
    m_spriteHolder = Node::create();
    m_sprite = Sprite::create();

    m_config.parentNode->addChild(m_node, m_config.zOrder);
    m_spriteHolder->addChild(m_sprite,1);
    m_node->addChild(m_spriteHolder);

    Zone *zone = m_config.controller->getRandFreeZone(std::pair<int,int>(1,2), std::pair<int,int>(0,3));
            //zones[Tools::randInt(1, 2)][Tools::randInt(0, 3)];

    m_node->setPosition(zone->center);
    m_lastZone = zone;

    m_state = MonsterState::walkDown;

    lifeBar = LifeBar::create();
    m_node->addChild(lifeBar);
    lifeBar->setPosition(-50.0, -50.0);
    lifeBar->setVisible(false);
    lifeBar->setRotation(-90.0);
    updateLifeBar();

    m_isDamageEmited = false;
    m_isAttackPlaying = false;


    fillAnimations(reader);
    fillSounds(reader);
    fillPrice(reader);

    m_mushroomState = MushroomState::entering;
    playAnimation(ENTER, false);
}

void MonsterMushroom::fillAnimations(ConfigReader *reader) {
    m_animations = Dictionary::create();
    m_animations->retain();

    Animation *anim;
    anim = Animation::create();
    Tools::addFramesToAnimation(anim, "gameScreen/monsters/mushroom/%04d.png", 15.0);
    m_animations->setObject(anim, ENTER);
    Vector<AnimationFrame*> frames = anim->getFrames();
    frames.reverse();
    anim = Animation::create();
    Tools::addFramesToAnimation(anim, "gameScreen/monsters/mushroom/%04d.png", frames.size()-1, 0, 15.0);
    m_animations->setObject(anim, LEAVE);

}

void MonsterMushroom::fillData(ConfigReader *reader) {
    m_lifeMax = 1.0;
    m_stayTime = reader->fromPath("stayDelay")->toFloat();
    m_stayTimer = m_stayTime;
}

void MonsterMushroom::update(float dt) {
    if (m_mushroomState == MushroomState::staying) {
        if (m_stayTimer <= 0.0) {
            m_mushroomState = MushroomState::leaving;
            m_state = MonsterState::dead;
            playAnimation(LEAVE, false);
        }
        else m_stayTimer -= dt;
    }

}


void MonsterMushroom::animationComplete() {
    if (m_mushroomState == MushroomState::entering) {
        m_mushroomState = MushroomState::staying;
    }
    else if (m_mushroomState == MushroomState::leaving) {
        m_mushroomState = MushroomState::leaved;
        m_config.controller->checkForCurrentSpawnSectionKilled();
    }
}

void MonsterMushroom::recieveDamage(float damage, DamageType type) {
    if (isActive() || m_mushroomState == MushroomState::leaving) { // issue #612 // don't know why I made comment in isActive()
        m_deathPosition = m_node->getPosition();
        m_node->removeFromParentAndCleanup(true);
        //lifeBar->removeFromParentAndCleanup(true);
        m_mushroomState = MushroomState::leaved;
        m_state = MonsterState::dead;
        m_config.controller->monsterWasKilled(this);

    }
}

bool MonsterMushroom::isActive() {
    //return m_mushroomState != MushroomState::leaved;
    return Monster::isActive() || MushroomState::leaving == m_mushroomState;
}

vector <Zone *> MonsterMushroom::getCurrentZones() {
    return vector<Zone*> {m_lastZone};
};

bool MonsterMushroom::isGoing() {
    return false;
}


