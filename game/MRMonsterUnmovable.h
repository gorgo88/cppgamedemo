//
// Created by Admin on 25.02.14.
//



#ifndef __MRMonsterUnmovable_H_
#define __MRMonsterUnmovable_H_

#include "cocos2d.h"

namespace myth {
class Zone;

class MonsterUnmovable {
public:

    virtual ~MonsterUnmovable() {}
    virtual std::vector<Zone *>getCurrentZones() = 0; // get zone adress in gameController.zones
};

}

#endif //__MRMonsterUnmovable_H_
