//
// Created by gorgo on 25.03.15.
//

#ifndef _MYTHRANDIA_MRMONSTERMULTINODE_H_
#define _MYTHRANDIA_MRMONSTERMULTINODE_H_

#include "cocos2d.h"

namespace myth {
    class MonsterMultiNode {
    public:
        virtual std::vector<cocos2d::Node*> getNodes() = 0;
        virtual void receiveDamage(float damage, cocos2d::Node* targetNode) = 0;
    };
}

#endif //_MYTHRANDIA_MRMONSTERMULTINODE_H_
