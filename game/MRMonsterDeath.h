//
// Created by Admin on 03.02.14.
//



#ifndef __MRMonsterDeath_H_
#define __MRMonsterDeath_H_

#include "MRMonster.h"
#include "MRMonsterUnmovable.h"
#include "MRIMonsterBoss.h"

namespace myth {
class GameController;
class Zone;

enum class DeathState {
    entering,
    leaving,
    waitingForAttack,
    attaking,
    hiding,
    goOut
};

class MonsterDeath : public Monster, public MonsterUnmovable, public IMonsterBoss {
public:
    DeathState m_deathState;
    float m_timeout;
    Zone *m_lastZone;


    virtual void fillData(ConfigReader *reader) override;

    virtual void fillAnimations(ConfigReader *reader) override;

    virtual void setup(ConfigReader *reader) override;

    virtual void update(float dt) override;

    void bossEnter();

    void bossAttack();

    void enterAnimationComplete();

    virtual void animationComplete() override;

    void attackAnimationComplete();

    void leaveAnimationComplete();

    float m_enterInterval;

    virtual void dieMonster() override;

    virtual void recieveDamage(float damage) override;

    virtual bool isActive() override;

    void leave();

    virtual void winDefile() override;

    bool m_isDefile = false;

    virtual std::vector<Zone *> getCurrentZones();

    virtual bool isGoing() override;
};

}
#endif //__MRMonsterDeath_H_
