	//
// Created by Admin on 19.12.13.
//


#include "CoordinateCache.h"
using namespace cocos2d;

cocos2d::Point CoordinateCache::getPoint(std::string path) {
    return this->fromPath(path+"._transform.position")->toPoint();
}

cocos2d::Rect CoordinateCache::getRect(std::string path) {
    return this->fromPath(path+"._rect")->toRect();
}

cocos2d::Rect CoordinateCache::getRectPos(std::string path) {
    cocos2d::Rect rect = getRect(path);
    rect.origin = getPoint(path);
    return rect;
}

void CoordinateCache::applyTransform(Node *node, std::string path) {
    auto position = getPoint(path);
    auto scale = fromPath(path+"._transform.scale")->toPoint();
    auto rotation = fromPath(path+"._transform.rotation")->toFloat();
    node->setPosition(position);
    node->setScale(scale.x, scale.y);
    node->setRotation(-rotation);

}

void CoordinateCache::applyTransformAP(Node *node, string path) {
    applyTransform(node, path);
    auto rect = getRect(path);
    node->setAnchorPoint(
            cocos2d::Point(
                    -rect.origin.x / rect.size.width,
                    -rect.origin.y / rect.size.height)
    );

}

cocos2d::Point CoordinateCache::getAnchorPoint(std::string path) {
    auto rect = getRect(path);
    auto ap = cocos2d::Point(
            -rect.origin.x / rect.size.width,
            -rect.origin.y / rect.size.height);
    return ap;
}

void CoordinateCache::readFromFile(std::string filePath) {
    ConfigReaderYAML::readFromFile(filePath);
    m_pConfig->autorelease();
    m_pConfig = ((Array*)m_pConfig)->getObjectAtIndex(0); // get 0 yaml document
    m_pConfig->retain();
}

CoordinateCache *CoordinateCache::createCache(string filePath) {
    CoordinateCache *instance = new CoordinateCache();
    instance->readFromFile(filePath);
    instance->autorelease();
    return instance;
}


