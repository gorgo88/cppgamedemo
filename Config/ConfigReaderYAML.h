//
// Created by Admin on 29.11.13.
//



#ifndef __ConfigReaderYAML_H_
#define __ConfigReaderYAML_H_

#include "ConfigReader.h"
#include "yaml-0.1.4/include/yaml.h"

class ConfigReaderYAML : public ConfigReader {

public:
    ConfigReaderYAML();
public:
    virtual ~ConfigReaderYAML();

    static ConfigReaderYAML *createWithFilePath(std::string filePath);

    virtual void readFromFile (std::string filePath);

    static ConfigReaderYAML *createWithString(std::string *inputString);

    void readFromString(std::string *inputString);

    void readFromParser(yaml_parser_s * parser);
};


#endif //__ConfigReaderYAML_H_
