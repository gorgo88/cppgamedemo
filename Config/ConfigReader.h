//
// Created by Admin on 29.11.13.
//

#ifndef __ConfigReader_h_
#define __ConfigReader_h_

#include "cocos2d.h"

using namespace cocos2d;


class ConfigReader : public Object {
protected:
    Object *m_pConfig;
    Object *tempStructure;
public:
    Object* getConfig ();
    ConfigReader *getSubConfigByPath(std::string path);
    std::string name;

    Object* getObjectByPath(std::string path);
    ConfigReader* fromPath(std::string path); // use only with below methods
    String*     toString();
    int         toInt();
    float       toFloat();
    Dictionary* toDictionary();
    Array*      toArray();
    cocos2d::Point       toPoint();
    cocos2d::Rect        toRect();

    ConfigReader() {m_pConfig = nullptr;}
    ConfigReader(Object* rootObject) { m_pConfig = rootObject; m_pConfig->retain();}


    virtual ~ConfigReader();

    bool hasPath(std::string path);
};

#endif