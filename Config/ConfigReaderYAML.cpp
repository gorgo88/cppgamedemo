//
// Created by Admin on 29.11.13.
//


#include "ConfigReaderYAML.h"


//constructor
ConfigReaderYAML::ConfigReaderYAML() : ConfigReader() {

}


ConfigReaderYAML *ConfigReaderYAML::createWithFilePath(std::string filePath) {
    auto instance = new ConfigReaderYAML();
    instance->autorelease();
    instance->readFromFile(filePath);
    return instance;
}

ConfigReaderYAML *ConfigReaderYAML::createWithString(std::string *inputString) {
    auto instance = new ConfigReaderYAML();
    instance->autorelease();
    instance->readFromString(inputString);
    return instance;
}

void ConfigReaderYAML::readFromFile(std::string filePath) {
    name = filePath; // debug
    auto str = FileUtils::getInstance()->getStringFromFile(FileUtils::getInstance()->fullPathForFilename(filePath));

    readFromString(&str);
}

void ConfigReaderYAML::readFromString(std::string *inputString) {
    yaml_parser_t parser;
    /* Initialize parser */
    if(!yaml_parser_initialize(&parser))
        fputs("Failed to initialize parser!\n", stderr);
    /* Set input file */
    yaml_parser_set_input_string(&parser, (unsigned char const *)((*inputString).c_str()), inputString->length());
    readFromParser(&parser);
}

void ConfigReaderYAML::readFromParser(yaml_parser_t *parser) {
    yaml_event_t  event;

    bool isKey = true;
    std::string lastKey;
    Array *structureStack = Array::create();
    structureStack->retain();
    structureStack->addObject(Array::create()); // documents array
    yaml_node_type_e currentStructureType = YAML_SEQUENCE_NODE;

    /* START new code */
    do {
        if (!yaml_parser_parse(parser, &event)) {
            printf("Parser error %d\n", parser->error);
            exit(EXIT_FAILURE);
        }

        switch(event.type)
        {
            case YAML_NO_EVENT: break;
                /* Stream start/end */
            case YAML_STREAM_START_EVENT: break;
            case YAML_STREAM_END_EVENT:   break;
                /* Block delimeters */
            case YAML_DOCUMENT_START_EVENT: {

                break;
            }
            case YAML_DOCUMENT_END_EVENT:   break;



            case YAML_SEQUENCE_START_EVENT: {
                Array *newSequence = Array::create();
                Object *currentStructure = structureStack->getLastObject();
                if (currentStructureType == YAML_SEQUENCE_NODE) {
                    ((Array *) currentStructure)->addObject(newSequence);
                }
                else if (currentStructureType == YAML_MAPPING_NODE) {
                    ((Dictionary *) currentStructure)->setObject(newSequence, lastKey);
                    isKey = !isKey;
                }
                structureStack->addObject(newSequence);
                currentStructureType = YAML_SEQUENCE_NODE;
                break;
            }

            case YAML_SEQUENCE_END_EVENT:   {
                structureStack->removeLastObject(true);
                Object *prevStructure = structureStack->getLastObject();
                if (typeid(*prevStructure) == typeid(Array))
                    currentStructureType = YAML_SEQUENCE_NODE;
                else if (typeid(*prevStructure) == typeid(Dictionary)) {
                    currentStructureType = YAML_MAPPING_NODE;
                }
                break;
            }



            case YAML_MAPPING_START_EVENT:  {
                Dictionary *newMapping = Dictionary::create();
                Object *currentStructure = structureStack->getLastObject();
                if (currentStructureType == YAML_SEQUENCE_NODE) {
                    ((Array *) currentStructure)->addObject(newMapping);
                }
                else if (currentStructureType == YAML_MAPPING_NODE) {
                    ((Dictionary *) currentStructure)->setObject(newMapping, lastKey);
                    isKey = !isKey;
                }
                structureStack->addObject(newMapping);
                currentStructureType = YAML_MAPPING_NODE;
                break;
            }

            case YAML_MAPPING_END_EVENT:   {
                structureStack->removeLastObject(true);
                Object *prevStructure = structureStack->getLastObject();
                if (typeid(*prevStructure) == typeid(Array))
                    currentStructureType = YAML_SEQUENCE_NODE;
                else if (typeid(*prevStructure) == typeid(Dictionary)) {
                    currentStructureType = YAML_MAPPING_NODE;
                }
                break;
            }
                /* Data */
            case YAML_ALIAS_EVENT: break;
            case YAML_SCALAR_EVENT: {
                Object *currentStructure = structureStack->getLastObject();
                if (currentStructureType == YAML_SEQUENCE_NODE) {
                    ((Array *) currentStructure)->
                            addObject(String::createWithData(
                            event.data.scalar.value,
                            event.data.scalar.length));
                }
                else if (currentStructureType == YAML_MAPPING_NODE) {
                    if (isKey) lastKey = std::string((char *) event.data.scalar.value);
                    else ((Dictionary *) currentStructure)->setObject(
                                String::createWithData(event.data.scalar.value,
                                        event.data.scalar.length), lastKey);
                    isKey = !isKey;
                }

                break;
            }
        }


        if(event.type != YAML_STREAM_END_EVENT)
            yaml_event_delete(&event);
    } while(event.type != YAML_STREAM_END_EVENT);
    yaml_event_delete(&event);
    /* END new code */

    /* Cleanup */
    yaml_parser_delete(parser);

    m_pConfig = structureStack->getObjectAtIndex(0);
    m_pConfig->retain();
    structureStack->release();
}

//destructor
ConfigReaderYAML::~ConfigReaderYAML() {

}
