//
// Created by Admin on 19.12.13.
//




#ifndef __CoordinateCache_H_
#define __CoordinateCache_H_

#include "ConfigReaderYAML.h"
#include "cocos2d.h"


using namespace cocos2d;
using namespace std;

class CoordinateCache : public ConfigReaderYAML {
protected:

public:
    static CoordinateCache *createCache(string filePath);


    cocos2d::Point  getPoint(string path);
    cocos2d::Rect   getRect(string path);
    void            applyTransform(Node *node, string path);
    void            applyTransformAP(Node *node, string path);
    cocos2d::Point  getAnchorPoint(string path);

    virtual void readFromFile(std::string filePath) override;

    cocos2d::Rect getRectPos(string path);
};


#endif //__CoordinateCache_H_
