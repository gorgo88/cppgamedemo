#include "ConfigReader.h"
#include "core/myth.h"

//
// Created by Admin on 29.11.13.
//
using namespace cocos2d;
using namespace myth;

Object* ConfigReader::getConfig() {
    return m_pConfig;
}

Object *ConfigReader::getObjectByPath(std::string path) {
    using namespace std;
    vector<string> parts = Tools::explode(path, '.');
    Object *currentStructure = m_pConfig;
    for (int i = 0; i < parts.size(); ++i) {
        string part = parts[i];
        bool isDigits = all_of(part.begin(), part.end(), ::isdigit);
        if (isDigits) {
            int index = stoi(part);
            if (dynamic_cast<Array*>(currentStructure)->count() <= index) {
                CCLOG("index %s does not exists", part.c_str());
                return nullptr;
            }
            currentStructure = ((Array*) currentStructure)->getObjectAtIndex(index);
        }
        else {
            currentStructure = ((Dictionary*)currentStructure)->objectForKey(part);
            if (currentStructure == NULL) {
                CCLOG("token %s does not exists", part.c_str());
                return nullptr;
            };
        }
    }
    return currentStructure;
}

ConfigReader *ConfigReader::fromPath(std::string path) {
    tempStructure = getObjectByPath(path);
    return this;
}

bool ConfigReader::hasPath(std::string path) {
    return getObjectByPath(path) != nullptr;
}

String *ConfigReader::toString() {
    return (String*) tempStructure;
}

int ConfigReader::toInt() {
    return toString()->intValue();
}

float ConfigReader::toFloat() {
    return toString()->floatValue();
}

Dictionary *ConfigReader::toDictionary() {
    return (Dictionary*) tempStructure;
}

Array *ConfigReader::toArray() {
    return (Array*) tempStructure;
}

cocos2d::Point ConfigReader::toPoint() {
    Dictionary *dict = toDictionary();
    cocos2d::Point point;
    point.x = ((String*)dict->objectForKey("x"))->floatValue();
    point.y = ((String*)dict->objectForKey("y"))->floatValue();
    return point;
}

cocos2d::Rect ConfigReader::toRect() {
    Dictionary *dict = toDictionary();
    cocos2d::Rect rect;
    rect.origin.x = ((String*)dict->objectForKey("x"))->floatValue();
    rect.origin.y = ((String*)dict->objectForKey("y"))->floatValue();
    rect.size.width = ((String*)dict->objectForKey("width"))->floatValue();
    rect.size.height = ((String*)dict->objectForKey("height"))->floatValue();
    return rect;
}

ConfigReader *ConfigReader::getSubConfigByPath(std::string path) {
    ConfigReader *subReader = new ConfigReader(getObjectByPath(path)); // todo: memory management!!!
    subReader->autorelease();
    return subReader;
}

ConfigReader::~ConfigReader() {
    if (m_pConfig) {
        int a = 90;
        //m_pConfig->release();
    }

}
