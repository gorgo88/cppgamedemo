//
// Created by Admin on 01.08.14.
//

#import <MacTypes.h>
#import <Foundation/Foundation.h>
#import "MRTapjoyController.h"

static MRTapjoyController *_instance;

@implementation MRTapjoyController {

}
+ (MRTapjoyController *)instance {
    if (!_instance) _instance = [MRTapjoyController new];
    return _instance;
}

- (void)sendEvent:(NSString *)name {
    TJEvent *evt = [TJEvent eventWithName:name delegate:self];

    // If presentAutomatically is set to YES, set the ViewController used to display content by calling:
    // [evt setPresentAutomatically:YES];
    // [evt setPresentationViewController:self];

    [evt send];

}

- (void)sendEvent:(NSString *)name value:(NSString *) value {
    TJEvent *evt = [TJEvent eventWithName:name value:value delegate:self];

    // If presentAutomatically is set to YES, set the ViewController used to display content by calling:
    // [evt setPresentAutomatically:YES];
    // [evt setPresentationViewController:self];

    [evt send];

}


- (void)sendEventComplete:(TJEvent *)event withContent:(BOOL)contentIsAvailable {
    NSLog(@"content is available %i for event %@ %@", contentIsAvailable, event.eventName, event.eventValue);
}

- (void)sendEventFail:(TJEvent *)event error:(NSError *)error {

}

- (void)contentIsReady:(TJEvent *)event withStatus:(int)status {

}

- (void)contentWillAppear:(TJEvent *)event {

}

- (void)contentDidAppear:(TJEvent *)event {

}

- (void)contentWillDisappear:(TJEvent *)event {

}

- (void)contentDidDisappear:(TJEvent *)event {

}

- (void)event:(TJEvent *)event didRequestAction:(TJEventRequest *)request {

}

- (void)didReceiveAd:(TJCAdView *)adView {
    [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:adView];
}

- (void)didFailWithMessage:(NSString *)msg {
    NSLog(@"tapjoy get ad error: %@", msg);
}

- (NSString *)adContentSize {
    return @"TJC_DISPLAY_AD_SIZE_320X50";
}

- (BOOL)shouldRefreshAd {
    return YES;
}


@end