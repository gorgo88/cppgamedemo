//
// Created by Admin on 01.08.14.
//

#import <Foundation/Foundation.h>
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    #import <Tapjoy/Tapjoy.h>
#endif


@interface MRTapjoyController : NSObject <TJEventDelegate, TJCAdDelegate >
+ (MRTapjoyController *) instance;
- (void)sendEvent: (NSString *) name;

- (void)sendEvent:(NSString *)name value:(NSString *)value;

@end