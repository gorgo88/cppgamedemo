//
// Created by gorgo on 21.03.15.
//

#ifndef _MYTHRANDIA_MRGADDELEGATE_H_
#define _MYTHRANDIA_MRGADDELEGATE_H_
namespace myth{
    class GADDelegate {
    public:
        virtual void interstitialIsReady() = 0;
        virtual void interstitialWasClosed() = 0;
    };
}

#endif //_MYTHRANDIA_MRGADDELEGATE_H_
