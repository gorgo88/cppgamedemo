//
// Created by Admin on 22.09.14.
//


#ifndef __MRGoogleADType_H_
#define __MRGoogleADType_H_


namespace myth {
    enum class GoogleADType {
        mainMenu,
        slotSelect,
        location,
        win,
        winTop,
        lose,
        loseTop,
        afterWin, //interstitials
        payed
    };
}


#endif //__MRGoogleADType_H_
