//
// Created by Admin on 22.01.15.
//

#import "MRGADInterstitialController.h"
#import <GoogleMobileAds/GoogleMobileAds.h>


@implementation MRGADInterstitialController {

}
- (instancetype)initWithPTools:(myth::PlatformToolsImplement *)aPTools {
    self = [super init];
    if (self) {
        pTools = aPTools;
    }

    return self;
}

- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    pTools->didReceiveAd(ad);
}

- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error {
    cocos2d::log("interstitial got error! %s", [error description].UTF8String);
}

- (void)interstitialWillPresentScreen:(GADInterstitial *)ad {
    cocos2d::Director::getInstance()->pause();
}

- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {

}

- (void)interstitialDidDismissScreen:(GADInterstitial *)ad {
    cocos2d::Director::getInstance()->resume();
    pTools->didDismissScreen(ad);
}

- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad {

}

+ (instancetype)controllerWithPTools:(myth::PlatformToolsImplement *)aPTools {
    return [[[self alloc] initWithPTools:aPTools] autorelease];
}

@end