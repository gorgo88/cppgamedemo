//
// Created by Admin on 25.06.14.
//



#ifndef __MRPlatformTools_H_
#define __MRPlatformTools_H_



#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
#include "MRPlatformToolsIOS.h"
#include "MRPurchaseErrorCode.h"
#include "MRGoogleADType.h"
#elif CC_TARGET_PLATFORM == CC_PLATFORM_LINUX
#include "MRPlatformToolsLinux.h"
#endif

#include "core/MRLeaderBoardItem.h"
#include "MRGADDelegate.h"

namespace myth {

    class PlatformTools {
    public:
        static void updateInterfaceRotation() {PlatformToolsImplement::updateInterfaceRotation();}
        static bool isGameCenterAuthenticated();
        static void authenticateGameCenter();

        static void postScoreToLeaderBoard(int score);

        static void getLeaderBoard(std::function<void(std::vector<LeaderBoardItem>, bool selfIncluded)> callback, int count);

        static bool isPurchaseAllowed();

        static void requestPurchase(int type);
        static void requestRestorePurchases();
        static void sendTapjoyEvent(std::string name);
        static void sendTapjoyEvent(std::string name, std::string value);

        static void postGAOpenScreen(std::string name);
        static void postGAEvent(std::string category, std::string action, std::string label, int value);
        static void postGATiming(std::string category, int value, std::string name, std::string label);

        static LeaderBoardItem getLocalPlayer(std::function<void(LeaderBoardItem localPlayer)> callback);

        static void setGAParameter(int id, std::string value);

        static void setupPaymentTransaction();

        static void openLink(std::string string);

        static void tapjoyShowOfferwall();

        static void tapjoyShowDisplayAd1();

        static void showGADBanner(GoogleADType type);

        static void hideGADBanner();

        static void preloadGADInterstitial(bool force = false);

        static void setGADDelegate(GADDelegate *delegate);

        static void clearGADDelegate();

        static void showGADInterstitialIfPossible(bool ignorePurchase = false);

        static void scheduleComeBackNotification(std::string text, int hours, int minutes);

        static void unscheduleComeBackNotifications();

        static int getDayId(int hours, int minutes);
        static int getMinuteId();
    };

}


#endif //__MRPlatformTools_H_
