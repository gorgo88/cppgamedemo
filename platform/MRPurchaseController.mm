//
// Created by Admin on 28.07.14.
//

#import <StoreKit/StoreKit.h>
#import "MRPurchaseController.h"
#include "MRGameDirector.h"
#include "MRTools.h"
#import "MRPlatformToolsIOS.h"
#import "Adjust.h"

MRPurchaseController *s_instance = nil;

@implementation MRPurchaseController {

@private
    NSArray *_products;
    double m_lastRequestedPrice;
}

@synthesize products = _products;

- (id)init {
    self = [super init];
    if (self) {
        //[self setup];
    }

    return self;
}

- (void) setup {
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
}

+ (MRPurchaseController *)sharedController {
    if (!s_instance) s_instance = [MRPurchaseController new];
    return s_instance;
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    NSLog(@"response %@", response);
    self.products = response.products;

    for (NSString *invalidIdentifier in response.invalidProductIdentifiers) {
        NSLog(@"invalid identifier: %@", invalidIdentifier);
        myth::GameDirector::getInstance()->paymentTransactionFailed(
                myth::PlatformToolsImplement::getInstance()->getPurchaseType([invalidIdentifier UTF8String]), 1);
        return;
    }

    for (SKProduct *product in response.products) {
        NSLog(@"product: %@", product);
        
    }
    SKMutablePayment *payment = [SKMutablePayment paymentWithProduct:response.products[0]];
    payment.quantity = 1;
    m_lastRequestedPrice = ((SKProduct *)response.products[0]).price.doubleValue;

    [[SKPaymentQueue defaultQueue] addPayment:payment];
    //[self displayStoreUI]; // Custom method
}



- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    NSLog(@"!!! IAP payment queue updated transactions, count = %d", transactions.count);
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            // Call the appropriate custom method.
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"purchasing");
                break;

            case SKPaymentTransactionStatePurchased:
                NSLog(@"purchase complete");
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                NSLog(@"purchase failed");
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"purchase restored");
                [self restoreTransaction:transaction];
            default:
                break;
        }
        NSLog(@"product id = %@", transaction.payment.productIdentifier);
    }
    
    
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"!!! IAP restoring transaction %@", transaction);
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    int purchaseType = myth::PlatformToolsImplement::getInstance()->
    getPurchaseType(transaction.payment.productIdentifier.UTF8String);
    int count = (int)transaction.payment.quantity;
    myth::GameDirector::getInstance()->paymentTransactionComplete(purchaseType, count);

}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    int purchaseType = myth::PlatformToolsImplement::getInstance()->
            getPurchaseType(transaction.payment.productIdentifier.UTF8String);
    int count = (int)transaction.payment.quantity;
    myth::GameDirector::getInstance()->paymentTransactionFailed(purchaseType, count);
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    int purchaseType = myth::PlatformToolsImplement::getInstance()->
            getPurchaseType(transaction.payment.productIdentifier.UTF8String);
    int count = transaction.payment.quantity;
    myth::GameDirector::getInstance()->paymentTransactionComplete(purchaseType, count);
    [Adjust trackRevenue:m_lastRequestedPrice * count forEvent:@"x00m9e"];
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    NSLog(@"!!! restore finished");
    myth::GameDirector::getInstance()->restoreCompletedIAPHandled();
}

- (void) paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
    NSLog(@"!!! restore failed with error: %@", error);
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:nil
                          message:[error localizedDescription]
                          delegate:self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil, nil];
    
    [alert show];
    [alert autorelease];
    
    myth::GameDirector::getInstance()->restoreCompletedIAPHandled();
    
}

- (void)dealloc {
    NSLog(@"dealloc purchase controller");
    [super dealloc];
}


- (void)requestPurchase:(int)type {
    NSString *productId = [NSString stringWithUTF8String:
            myth::PlatformToolsImplement::getInstance()->getPurchaseId(type).c_str()];
    SKProductsRequest *productsRequest = [[SKProductsRequest alloc]
            initWithProductIdentifiers:[NSSet setWithObject:productId]];
    productsRequest.delegate = self;
    [productsRequest start];
}

- (void)requestRestorePurchases {
    NSLog(@"!!! IAP requesting restore completed transactions");
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    NSLog(@"!!! IAP request did fail with error, %@", error);
    UIAlertView *alert = [[UIAlertView alloc]
            initWithTitle:[NSString stringWithUTF8String:_("In-App Store unavailable").c_str()]
                  message:[NSString stringWithUTF8String:_("The In-App Store is currently unavailable, please try again later.").c_str()]
                 delegate:self
        cancelButtonTitle:@"OK"
        otherButtonTitles:nil, nil];
    [alert show];
    [alert autorelease];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    myth::GameDirector::getInstance()->paymentTransactionFailed(-1, 1);
}

@end