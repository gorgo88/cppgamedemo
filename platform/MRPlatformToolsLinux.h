//
// Created by gorgo on 18.03.15.
//

#ifndef _MYGAME_MRPLATFORMTOOLSLINUX_H_
#define _MYGAME_MRPLATFORMTOOLSLINUX_H_

#include "cocos2d.h"
#include "core/MRLeaderBoardItem.h"
#include "MRPurchaseErrorCode.h"
#include "MRGoogleADType.h"
#include "MRGADDelegate.h"

namespace myth {
    class PlatformToolsImplement {
        PlatformToolsImplement();

    public:
        virtual ~PlatformToolsImplement();

        void setupPaymentTransaction();

        void openLink(std::string urlString);

        void tapjoyShowOfferwall();

        void tapjoyShowDisplayAd1();

        void showGADBanner(myth::GoogleADType type);

        void hideGADBanner();

        void preloadGADInterstitial(GoogleADType type);

        void didReceiveAd(void *interstitial);

        void setGADDelegate(GADDelegate *delegate);

        void clearGADDelegate();



        void showGADInterstitial();


        void sheduleComeBackNotification(std::string text, int hours, int minutes);

        void unscheduleComeBackNotifications();

    public:

        static myth::PlatformToolsImplement* getInstance();

        static void updateInterfaceRotation();

        bool isGameCenterAuthenticated();

        static void authenticateGameCenter();

        static void postScoreToLeaderBoard(int score);

        void getLeaderBoard(std::function<void(std::vector<LeaderBoardItem>, bool selfIncluded)> callback, int count);
        LeaderBoardItem getLocalPlayer(std::function<void(LeaderBoardItem localPlayer)> callback);

        bool isPurchaseAllowed();

        void requestPurchase(int type);
        void requestRestorePurchases();

        void sendTapjoyEvent(std::string name);
        void sendTapjoyEvent(std::string name, std::string value);

        void postGAOpenScreen(std::string name);
        void postGAEvent(std::string category, std::string action, std::string label, int value);
        void postGATiming(std::string category, int value, std::string name, std::string label);


        void setGAParameter(int id, std::string value);
        std::string getPurchaseId(int type);
        int getPurchaseType(std::string stringId);

        int getDayId(int hours, int minutes);
    protected:
        //NSArray *m_pTempScoresResult;
        std::function<void(std::vector<LeaderBoardItem>, bool selfIncluded)> m_pLeaderBoardCallback;
        void *m_gadBanner;
        void *m_gadInterstitial;
        GADDelegate *m_GADDelegate;
    };
}



#endif //_MYGAME_MRPLATFORMTOOLSLINUX_H_
