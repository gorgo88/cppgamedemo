//
// Created by Admin on 25.06.14.
//

#include "MRPlatformTools.h"
#import "MRGoogleADType.h"
#include "core/MRGameDirector.h"


void myth::PlatformTools::authenticateGameCenter() {
    PlatformToolsImplement::authenticateGameCenter();
}

void myth::PlatformTools::postScoreToLeaderBoard(int score) {
    PlatformToolsImplement::postScoreToLeaderBoard(score);
}

void myth::PlatformTools::getLeaderBoard(std::function<void(std::vector<LeaderBoardItem>, bool selfIncluded)> callback, int count) {
    PlatformToolsImplement::getInstance()->getLeaderBoard(callback, count);
}

bool myth::PlatformTools::isPurchaseAllowed() {
    return PlatformToolsImplement::getInstance()->isPurchaseAllowed();
}

void myth::PlatformTools::requestPurchase(int type) {
    PlatformToolsImplement::getInstance()->requestPurchase(type);
}

void myth::PlatformTools::requestRestorePurchases() {
    PlatformToolsImplement::getInstance()->requestRestorePurchases();
}

void myth::PlatformTools::sendTapjoyEvent(std::string name) {
    PlatformToolsImplement::getInstance()->sendTapjoyEvent(name);
}

void myth::PlatformTools::sendTapjoyEvent(std::string name, std::string value) {
    PlatformToolsImplement::getInstance()->sendTapjoyEvent(name, value);
}

void myth::PlatformTools::postGAOpenScreen(std::string name) {
    PlatformToolsImplement::getInstance()->postGAOpenScreen(name);
}

void myth::PlatformTools::postGAEvent(std::string category, std::string action, std::string label, int value) {
    PlatformToolsImplement::getInstance()->postGAEvent(category, action, label, value);
}

void myth::PlatformTools::postGATiming(std::string category, int value, std::string name, std::string label) {
    PlatformToolsImplement::getInstance()->postGATiming(category, value, name, label);
}

myth::LeaderBoardItem myth::PlatformTools::getLocalPlayer(std::function<void(LeaderBoardItem localPlayer)> callback) {
    return PlatformToolsImplement::getInstance()->getLocalPlayer(callback);
}

bool myth::PlatformTools::isGameCenterAuthenticated() {
    return PlatformToolsImplement::getInstance()->isGameCenterAuthenticated();
}

void myth::PlatformTools::setGAParameter(int id, std::string value) {
    PlatformToolsImplement::getInstance()->setGAParameter(id, value);
}

void myth::PlatformTools::setupPaymentTransaction() {
    PlatformToolsImplement::getInstance()->setupPaymentTransaction();
}

void myth::PlatformTools::openLink(std::string string) {
    PlatformToolsImplement::getInstance()->openLink(string);
}

void myth::PlatformTools::tapjoyShowOfferwall() {
    PlatformToolsImplement::getInstance()->tapjoyShowOfferwall();
}

void myth::PlatformTools::tapjoyShowDisplayAd1() {
    PlatformToolsImplement::getInstance()->tapjoyShowDisplayAd1();

}

void myth::PlatformTools::showGADBanner(GoogleADType type) {
    if (NO_AD || GD->m_model.isAdDisablePurchased) return;
    PlatformToolsImplement::getInstance()->showGADBanner(type);
}

void myth::PlatformTools::hideGADBanner() {
    PlatformToolsImplement::getInstance()->hideGADBanner();
}

void myth::PlatformTools::preloadGADInterstitial(bool ignorePurchase) {
    if (NO_AD || (!ignorePurchase && GD->m_model.isAdDisablePurchased)) return;
    GoogleADType _type = ignorePurchase ? GoogleADType::payed : GoogleADType::afterWin;
    PlatformToolsImplement::getInstance()->preloadGADInterstitial(_type);
}

void myth::PlatformTools::setGADDelegate(GADDelegate *delegate) {
    PlatformToolsImplement::getInstance()->setGADDelegate(delegate);
}

void myth::PlatformTools::clearGADDelegate() {
    PlatformToolsImplement::getInstance()->clearGADDelegate();
}

void myth::PlatformTools::showGADInterstitialIfPossible(bool ignorePurchase) {
    if (NO_AD || (!ignorePurchase && GD->m_model.isAdDisablePurchased)) return;
    PlatformToolsImplement::getInstance()->showGADInterstitial();
}

void myth::PlatformTools::scheduleComeBackNotification(std::string text, int hours, int minutes) {
    PlatformToolsImplement::getInstance()->sheduleComeBackNotification(text, hours, minutes);
}

void myth::PlatformTools::unscheduleComeBackNotifications() {
    PlatformToolsImplement::getInstance()->unscheduleComeBackNotifications();
}

int myth::PlatformTools::getDayId(int hours, int minutes) {
    std::time_t now = std::time(nullptr);
    std::tm tm1 = *std::localtime(&now);
    tm1.tm_year = 100;
    tm1.tm_mon = 0;
    tm1.tm_mday = 1; // some const date jan 1 2000
    tm1.tm_hour = hours;
    tm1.tm_min = minutes;
    tm1.tm_sec = 0;
    std::time_t startPoint = std::mktime(&tm1);
    int res = ((int)now - (int)startPoint) / 60 / 60 / 24;
    return res;
}

int myth::PlatformTools::getMinuteId() {
    std::time_t now = std::time(nullptr);
    return (int)now / 60;
}
