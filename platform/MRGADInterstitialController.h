//
// Created by Admin on 22.01.15.
//

#import <Foundation/Foundation.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "MRPlatformToolsIOS.h"

@interface MRGADInterstitialController : NSObject <GADInterstitialDelegate> {
    @public
    myth::PlatformToolsImplement *pTools;
}

- (instancetype)initWithPTools:(myth::PlatformToolsImplement *)aPTools;

+ (instancetype)controllerWithPTools:(myth::PlatformToolsImplement *)aPTools;



@end