//
// Created by Admin on 28.07.14.
//

//#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>


@interface MRPurchaseController : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver, UIAlertViewDelegate> {

}
@property(nonatomic) NSArray *products;


- (void)setup;

+ (MRPurchaseController *)sharedController;

- (void)requestPurchase:(int)type;
- (void)requestRestorePurchases;
@end