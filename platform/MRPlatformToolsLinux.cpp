//
// Created by gorgo on 18.03.15.
//

#include <core/MRGameDirector.h>
#include "MRPlatformToolsLinux.h"


static myth::PlatformToolsImplement *instance = nullptr;

void myth::PlatformToolsImplement::updateInterfaceRotation() {
}

bool myth::PlatformToolsImplement::isGameCenterAuthenticated() {
    return false;
}

void myth::PlatformToolsImplement::authenticateGameCenter() {
}

void myth::PlatformToolsImplement::postScoreToLeaderBoard(int score) {
}

void myth::PlatformToolsImplement::getLeaderBoard(std::function<void(std::vector<LeaderBoardItem>, bool selfIncluded)> callback, int count) {
}


myth::LeaderBoardItem myth::PlatformToolsImplement::getLocalPlayer(std::function<void(LeaderBoardItem localPlayer)> callback) {
    return myth::LeaderBoardItem();
}


myth::PlatformToolsImplement* myth::PlatformToolsImplement::getInstance() {
    if (!instance) instance = new PlatformToolsImplement();
    return instance;
}



void myth::PlatformToolsImplement::sendTapjoyEvent(std::string name) {
}

void myth::PlatformToolsImplement::sendTapjoyEvent(std::string name, std::string value) {
}

void myth::PlatformToolsImplement::postGAOpenScreen(std::string name) {
}

void myth::PlatformToolsImplement::postGAEvent(std::string category, std::string action, std::string label, int value) {
}

void myth::PlatformToolsImplement::postGATiming(std::string category, int value, std::string name, std::string label) {
}

void myth::PlatformToolsImplement::setGAParameter(int id, std::string value) {
}


bool myth::PlatformToolsImplement::isPurchaseAllowed() {
    return true;
}

void myth::PlatformToolsImplement::requestPurchase(int type) {
    GD->paymentTransactionComplete(type, 1);
}

void myth::PlatformToolsImplement::requestRestorePurchases() {


}


myth::PlatformToolsImplement::PlatformToolsImplement() : m_gadBanner(nullptr), m_gadInterstitial(nullptr) {

}

myth::PlatformToolsImplement::~PlatformToolsImplement() {
}

void myth::PlatformToolsImplement::setupPaymentTransaction() {
}

std::string myth::PlatformToolsImplement::getPurchaseId(int type) {
    return "";
}

int myth::PlatformToolsImplement::getPurchaseType(std::string stringId) {
}

void myth::PlatformToolsImplement::openLink(std::string urlString) {
}

void myth::PlatformToolsImplement::tapjoyShowOfferwall() {
}

void myth::PlatformToolsImplement::tapjoyShowDisplayAd1() {
}

void myth::PlatformToolsImplement::showGADBanner(myth::GoogleADType type) {
}


void myth::PlatformToolsImplement::hideGADBanner() {
}

void myth::PlatformToolsImplement::preloadGADInterstitial(myth::GoogleADType type) {
    if (m_GADDelegate) m_GADDelegate->interstitialIsReady();
}


void myth::PlatformToolsImplement::didReceiveAd(void *interstitial) {

}

void myth::PlatformToolsImplement::setGADDelegate(GADDelegate *delegate) {
    m_GADDelegate = delegate;
}

void myth::PlatformToolsImplement::clearGADDelegate() {
    m_GADDelegate = nullptr;
}

void myth::PlatformToolsImplement::showGADInterstitial() {
    if (m_GADDelegate) m_GADDelegate->interstitialWasClosed();
}

void myth::PlatformToolsImplement::sheduleComeBackNotification(std::string text, int hours, int minutes) {
}

void myth::PlatformToolsImplement::unscheduleComeBackNotifications() {
}


int myth::PlatformToolsImplement::getDayId(int hours, int minutes) {
    std::time_t now = std::time(nullptr);
    std::tm tm1 = *std::localtime(&now);
    tm1.tm_year = 100;
    tm1.tm_mon = 0;
    tm1.tm_mday = 1; // some const date jan 1 2000
    tm1.tm_hour = hours;
    tm1.tm_min = minutes;
    tm1.tm_sec = 0;
    std::time_t startPoint = std::mktime(&tm1);
    int res = ((int)now - (int)startPoint) / 60 / 60 / 24;
    return res;
}
