//
// Created by Admin on 27.05.14.
//

#include "MREncyclopediaScreen.h"
#include "core/myth.h"
#include "core/CheetahAtlasHelper.h"
#include "audio/include/SimpleAudioEngine.h"
#include "game/MRUIWidgets.h"

using namespace myth;
using namespace cocos2d;
using namespace CocosDenshion;
using namespace ui;

void EncyclopediaScreen::open() {
    Screen::open();
    schedulePreload();
}

void EncyclopediaScreen::close() {
    CheetahAtlasHelper::getInstance()->add("encyclopediaScreen/foreground.atlas");
    Screen::close();
}

void EncyclopediaScreen::preloadComplete() {
    SimpleAudioEngine::getInstance()->playBackgroundMusic("menuScreen/music.mp3", true);

    CheetahAtlasHelper::getInstance()->add("encyclopediaScreen/foreground.atlas");

    auto bg = Sprite::create("encyclopediaScreen/background.jpg");
    rootNode->addChild(bg, 1);
    bg->setPosition(Vec2(0.0, 1024.0));
    bg->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    bg->setScale(2.0);

    auto button = ui::Button::create();
    button->loadTextureNormal("commonUI/backButton.png", TextureResType::LOCAL);
    button->setTouchEnabled(true);
    button->addTouchEventListener(this, toucheventselector(EncyclopediaScreen::backButtonTouched));
    button->setPosition(Point(100.0, 950.0));
    rootNode->addChild(button, 10);

    auto title = Sprite::createWithSpriteFrameName("encyclopediaScreen/foreground/title.png");
    rootNode->addChild(title, 3);
    title->setPosition(Tools::screenCenter() + Vec2(0.0, 445.0));

    auto sv = ui::ScrollView::create();
    rootNode->addChild(sv, 2);
    sv->setPosition(Vec2(0.0, 50.0));
    sv->setTouchEnabled(true);
    sv->setSize(Size(768.0, 860.0));

    Array *cardsIndices = GD->getConfig(Configs::monsterCards)->fromPath("0")->toDictionary()->allKeys();
    int atRow = 3;
    float distance = 230.0;
    int lastRow = (int)floorf((float)cardsIndices->count() / (float)atRow);
    int lastRowCount = cardsIndices->count() - lastRow * atRow;
    float totalHeight = distance * (lastRow + 1);
    float fullRowXOffset = Tools::screenCenter().x - (atRow - 1) * distance * 0.5;
    float lastRowXOffset = Tools::screenCenter().x - (lastRowCount - 1) * distance * 0.5;
    for (int i = 0; i < cardsIndices->count(); ++i) {
        int row = (int)floorf(((float)i) / atRow);
        int col = i % atRow;
        string monsterId = dynamic_cast<String*>(cardsIndices->getObjectAtIndex(i))->_string;
        bool isUnknown = true;
        if (find(GD->m_model.getCurrentSlot()->shownMonsterCards.begin(),
                GD->m_model.getCurrentSlot()->shownMonsterCards.end(), monsterId) !=
                GD->m_model.getCurrentSlot()->shownMonsterCards.end())
            isUnknown = false;

        auto monsterButton = ui::Button::create();
        monsterButton->loadTextureNormal(
                isUnknown && !OPEN_ALL
                        ? "encyclopediaScreen/foreground/monsters/unknown.png"
                        : StringUtils::format("encyclopediaScreen/foreground/monsters/%s.png", monsterId.c_str()),
                ui::Widget::TextureResType::PLIST
        );
        sv->addChild(monsterButton);
        Vec2 pos;
        pos.x = col * distance + (row == lastRow ? lastRowXOffset : fullRowXOffset);
        pos.y = totalHeight - (row * distance + 10.0);

        monsterButton->setPosition(pos);

        if (!isUnknown || OPEN_ALL) {
            monsterButton->setTouchEnabled(true);
            monsterButton->setName(monsterId);
            using namespace ui;
            monsterButton->addTouchEventListener(this, toucheventselector(EncyclopediaScreen::monsterButtonTouched));
        }
    }
    sv->setInnerContainerSize(Size(sv->getSize().width, totalHeight  + 100.0));

}

void EncyclopediaScreen::monsterButtonTouched(Object *sender, ui::TouchEventType type) {
    auto button = static_cast<Button*>(sender);
    if (type == TouchEventType::TOUCH_EVENT_ENDED) {
        log ("monsterTouched = %s", button->getName().c_str());
        m_monsterCard = mgui::MonsterCard::create(button->getName(), nullptr);
        m_monsterCard->setTouchEnabled(true);
        rootNode->addChild(m_monsterCard, 6);
        m_monsterCard->setTouchEndedCallback([this](){
            closeMonsterCard();});
        m_shadow = ui::Layout::create();
        m_shadow->setSize(Size(768.0,1024.0));
        m_shadow->setBackGroundColor(Color3B::BLACK);
        m_shadow->setBackGroundColorType(ui::Layout::BackGroundColorType::SOLID);
        m_shadow->setBackGroundColorOpacity(128);
        m_shadow->setTouchEnabled(true);
        m_monsterCard->addTouchEventListener([this](Ref*target, ui::Widget::TouchEventType type){
            if (type == Widget::TouchEventType::ENDED) closeMonsterCard();
        });
        rootNode->addChild(m_shadow, 5);
        SimpleAudioEngine::getInstance()->playEffect("common/openManuscript.wav", false, 1.0, 0.0, 1.0);

    }
}

void EncyclopediaScreen::closeMonsterCard() {
    SimpleAudioEngine::getInstance()->playEffect("common/closeManuscript.wav", false, 1.0, 0.0, 1.0);
    m_monsterCard->removeFromParent();
    m_monsterCard = nullptr;
    m_shadow->removeFromParent();
    m_shadow = nullptr;
}


std::vector<Resource> EncyclopediaScreen::getPreloadResources() {
    return Screen::getPreloadResources();
}

void EncyclopediaScreen::backButtonTouched(Object *sender, ui::TouchEventType type) {
    auto button = static_cast<Button*>(sender);
    if (type == TouchEventType::TOUCH_EVENT_ENDED) {
        GD->openScreen(Screens::location);
    }
}
