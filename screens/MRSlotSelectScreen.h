//
// Created by Admin on 15.08.14.
//


#ifndef __MRSlotSelectScreen_H_
#define __MRSlotSelectScreen_H_

#include "core/myth.h"
#include "ui/CocosGUI.h"

namespace myth {
    class SlotSelectScreen : public Screen {
    public:
        SlotSelectScreen() : Screen("Slot Select Screen"), m_currentSlotIndex(-1) {}

        virtual void open() override;

        virtual void close() override;

        virtual ~SlotSelectScreen();

        void buttonHandle(Object *sender, cocos2d::ui::TouchEventType type);

        virtual void preloadComplete() override;

        virtual std::vector<Resource> getPreloadResources() override;

        void showSlots();

        void enterSlot(int index);

        int m_currentSlotIndex;

        void showHardDialog();
    };

}


#endif //__MRSlotSelectScreen_H_
