//
// Created by Admin on 04.12.13.
//



#ifndef __MGameScreen_H_
#define __MGameScreen_H_

#include "core/myth.h"
#include "game/MRGameController.h"

namespace myth {

class GameScreen : public Screen {

public:
    virtual ~GameScreen() {

    }


    GameScreen() : Screen("Game Screen") {

    }

    virtual void open() override;
private:
    GameController *gameController;

public:
    virtual void close();

    virtual std::vector<Resource> getPreloadResources() override;


    virtual void preloadComplete() override;
    GameController *getGameController();
protected:
    virtual void setupBordersIphone() override;
};

}

#endif //__MGameScreen_H_
