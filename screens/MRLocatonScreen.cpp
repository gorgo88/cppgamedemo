//
// Created by Admin on 20.02.14.
//

#include "MRLocatonScreen.h"
#include "game/MRLocations.h"
#include "game/MRDialogManager.h"
#include "ui/CocosGUI.h"
#include "game/MRUIWidgets.h"
#include "core/CheetahAtlasHelper.h"
#include "audio/include/SimpleAudioEngine.h"
#include "platform/MRPlatformTools.h"


using namespace myth;
using namespace cocos2d;
using namespace CocosDenshion;


void LocationScreen::open() {
    Screen::open();
    schedulePreload();

}


void LocationScreen::preloadComplete() {
    if (GD->m_model.temp.needsPlayNewLevelSound)
        SimpleAudioEngine::getInstance()->playEffect("levelScreen/newLevel.wav", false, 1.0, 0.0, 1.0);

    m_newGameDialog = false;
    SimpleAudioEngine::getInstance()->playBackgroundMusic("locationScreen/music.mp3", true);
    CheetahAtlasHelper::getInstance()->add("commonUI/dialog.atlas");
    CheetahAtlasHelper::getInstance()->add("locationScreen/ships.atlas");

    CheetahAtlasHelper::getInstance()->add("gameScreen/ui/upgrades_1.atlas");
    CheetahAtlasHelper::getInstance()->add("gameScreen/ui/upgrades_2.atlas");

    CheetahAtlasHelper::getInstance()->add("locationScreen/award.atlas");

    auto bg = Sprite::create("locationScreen/background.jpg");
    bg->setPosition(Tools::screenCenter());
    placeShips();
    rootNode->addChild(bg);
    auto tl = EventListenerTouchOneByOne::create();
    EventDispatcher *dispatcher = Director::getInstance()->getEventDispatcher();
    tl->onTouchBegan = CC_CALLBACK_2(LocationScreen::touchBegan, this);
    dispatcher->addEventListenerWithFixedPriority((EventListener *) tl, -10);
    m_touchListener = tl;
    m_dialogEnabled = false;

    using namespace ui;

    if (!GD->m_model.newGameDialogComplete || GD->m_model.temp.currentSlot == Model::NO_SLOT) {
        showNewGameDialog();
        return;
    }

    auto button = ui::Button::create();
    button->loadTextureNormal("commonUI/backButton.png", TextureResType::LOCAL);
    button->setTouchEnabled(true);
    button->setTag(888);
    button->addTouchEventListener(this, toucheventselector(LocationScreen::buttonHandle));
    button->setPosition(Point(125.0, 950.0));
    rootNode->addChild(button, 21);

    GD->recalculateLocation();
    int availableLocations = (GD->m_model.getCurrentSlot()->completeLevels+1) / GD->getLevelsPerLocation() + 1;
    if (availableLocations > 4) availableLocations = 4;
    if (OPEN_ALL) availableLocations = 4;
    for (int i = 0; i < availableLocations; ++i) {

        auto sign = Button::create();
        sign->loadTextureNormal("locationScreen/sign.png", TextureResType::LOCAL);
        sign->setTouchEnabled(true);
        char signKey[16];
        sprintf(signKey, "Sign%02d", i);
        GD->ggc()->applyTransformAP(sign, signKey);
        sign->setTag(111+i*111);
        sign->addTouchEventListener(this, toucheventselector(LocationScreen::buttonHandle));
        rootNode->addChild(sign);

        if (i == (int)GD->getCompleteLocations()) {
            //pulsing
            sign->runAction(RepeatForever::create(Sequence::createWithTwoActions(
                    ScaleTo::create(0.5, 1.0), ScaleTo::create(0.5, 0.8)
            )));
        }

        // achievements
        LevelAchievement flags[5] = {
                LevelAchievement::sniper,
                LevelAchievement::destroyer,
                LevelAchievement::mushroomer,
                LevelAchievement::lastLine,
                LevelAchievement::combo,
        };
        std::string strIds[5] = {"sniper", "destroyer", "mushroomer", "lastLine", "combo"};
        Node *achNode = Node::create();
        achNode->setPosition(sign->getPosition() + Vec2(80.0, 0.0));
        achNode->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
        rootNode->addChild(achNode);
        float lastY = 0.0;
        for (int j = 0; j < 5; ++j) {
            std::string frame;
            bool locAchieved = true;
            int lpl = LEVEL_COUNT / 4;
            for (int k = i * lpl; k < (i+1)*lpl; ++k ) {
                if (!GD->m_model.getCurrentSlot()->getLevelAchievement(k, flags[j])) {
                    locAchieved = false; break;
                }
            }
            if (locAchieved){
                auto sprite = Sprite::create(StringUtils::format("honorScreen/icons/%sIcon.png", strIds[j].c_str()));
                sprite->setPosition(Vec2(0.0, lastY));
                lastY += 80.0;
                achNode->addChild(sprite);
                sprite->setScale(0.4);
            }
        }
        achNode->setPosition(achNode->getPosition() + Vec2(0.0, -lastY*0.5));
    }
    // eniclopedia, achievements
    auto encButton = ui::Button::create();
    encButton->loadTextureNormal("locationScreen/encyclopediaButton.png", TextureResType::LOCAL);
    encButton->setTouchEnabled(true);
    encButton->setTag(999);
    encButton->addTouchEventListener(this, toucheventselector(LocationScreen::buttonHandle));
    GD->ggc()->applyTransformAP(encButton, "LocationEncyclopediaButton");
    rootNode->addChild(encButton, 10);

    auto achButton = ui::Button::create();
    achButton->loadTextureNormal("locationScreen/starButton.png", TextureResType::LOCAL);
    achButton->setTouchEnabled(true);
    achButton->setTag(1111);
    achButton->addTouchEventListener(this, toucheventselector(LocationScreen::buttonHandle));
    GD->ggc()->applyTransformAP(achButton, "LocationAchievementButton");
    rootNode->addChild(achButton, 10);

    if (GD->m_model.getCurrentSlot()->completeLevels == GD->getLevelsPerLocation() - 1
            && !GD->m_model.getCurrentSlot()->getFlag(FlagIndices::desertGreenDialogPlayed)) {
        GD->m_model.getCurrentSlot()->setFlag(FlagIndices::desertGreenDialogPlayed, true);
        GD->m_model.dumpToDisk();
        playDesertGreenDialog();
    }
    if (GD->m_model.getCurrentSlot()->completeLevels == GD->getLevelsPerLocation() * 2 - 1
            && !GD->m_model.getCurrentSlot()->getFlag(FlagIndices::greenSnowDialogPlayed)) {
        GD->m_model.getCurrentSlot()->setFlag(FlagIndices::greenSnowDialogPlayed, true);
        GD->m_model.dumpToDisk();
        playGreenSnowDialog();
    }
    if (GD->m_model.getCurrentSlot()->completeLevels == GD->getLevelsPerLocation() * 3 - 1
            && !GD->m_model.getCurrentSlot()->getFlag(FlagIndices::snowInfernoDialogPlayed)) {
        GD->m_model.getCurrentSlot()->setFlag(FlagIndices::snowInfernoDialogPlayed, true);
        GD->m_model.dumpToDisk();
        playSnowInfernoDialog();
    }

    //placeTutorialButton();
    placeStats();

    // difficulty
    std::string diffIconId = GD->m_model.getCurrentSlot()->difficulty == Difficulty::hard ? "hard" : "normal";
    auto diffIconSprite = Sprite::create(
            StringUtils::format("slotSelectScreen/%s.png", diffIconId.c_str()));
    rootNode->addChild(diffIconSprite, 10);
    diffIconSprite->setPosition(Vec2(690.0, 975.0));
    diffIconSprite->setScale(0.6f);

    if (GD->getDeviceFamily() != DeviceFamily::iphone4
            && GD->m_model.newGameDialogComplete)
        PlatformTools::showGADBanner(GoogleADType::location);

    placeShopButton();

    // workaround for money upgrades
    m_gameUIController = new GameUIController();
    m_gameUIController->m_config.controller = nullptr;
    m_gameUIController->m_config.moneyUpgradeDelegate = this;

    log ("day id = %d", PlatformTools::getDayId(20, 29));

    placeDailyAward();

}

bool LocationScreen::touchBegan(Touch* touch, Event *event) {
    return false; // old system
    /*if (m_dialogEnabled) return false;
    Point point = rootNode->convertTouchToNodeSpace(touch);
    Rect regularRect =  GD->ggc()->getRectPos("RegularLocationButton");
    Rect snowRect =     GD->ggc()->getRectPos("SnowLocationButton");
    Rect infernoRect =  GD->ggc()->getRectPos("InfernoLocationButton");
    Rect desertRect =   GD->ggc()->getRectPos("DesertLocationButton");

    CCLOG("efef, x=  %f, y = %f, rx = %f,%f,%f,%f", point.x, point.y, regularRect.origin.x, regularRect.origin.y, regularRect.size.width, regularRect.size.height);
    Location locationToGo = Location::regular;
    if (regularRect.containsPoint(point)) locationToGo = Location::regular;
    else if (snowRect.containsPoint(point)) locationToGo = Location::snow;
    else if (infernoRect.containsPoint(point)) locationToGo = Location::inferno;
    else if (desertRect.containsPoint(point)) locationToGo = Location::desert;
    else return false;
    GD->m_model.temp.currentLocation = locationToGo;
    GD->openScreen(Screens::level);
    return false;  */
}

void LocationScreen::close() {
    Screen::close();
    Director::getInstance()->getEventDispatcher()->removeEventListener(m_touchListener);
    CheetahAtlasHelper::getInstance()->remove("commonUI/dialog.atlas");
    CheetahAtlasHelper::getInstance()->remove("locationScreen/ships.atlas");

    CheetahAtlasHelper::getInstance()->remove("gameScreen/ui/upgrades_1.atlas");
    CheetahAtlasHelper::getInstance()->remove("gameScreen/ui/upgrades_2.atlas");

    CheetahAtlasHelper::getInstance()->remove("locationScreen/award.atlas");

    if (GD->getDeviceFamily() != DeviceFamily::iphone4) PlatformTools::hideGADBanner();

    if (m_gameUIController) delete m_gameUIController;
}

void LocationScreen::showNewGameDialog() {
    m_newGameDialog = true;
    GD->m_model.dumpToDisk();

    m_dialogEnabled = true;
    DialogManager::getInstance()->show(rootNode, 50, Point(0.0,440.0));
    DialogManager::getInstance()->setAutoCompleteMode(false);
    std::vector<std::string> statements;
    statements.push_back(_("Welcome, Hero! I am glad to greet you in the magic world of Mythrandia. I am Valenas, the Archimage of these lands."));
    statements.push_back(_("As it hasbecome known to you, an imminent threat is hanging over our island. Hordes of goblins and pirates are ready to attack our lands."));
    statements.push_back(_("The Council of Elders decided that you are the only one able to counter the threat."));
    statements.push_back(_("The Council authorized me to be your assistant and to deliver the defensive garrison into your charge. But at first I will tell you how to control the outpost."));

    DialogManager::getInstance()->createDialog(statements, 1.0, [this](){
        m_dialogEnabled = false;
        DialogManager::getInstance()->setAutoCompleteMode(true);
        DialogManager::getInstance()->hide();
        GD->m_model.newGameDialogComplete = true;
        GD->m_model.dumpToDisk();
        //GD->m_model.temp.tutorialMode = true;
        GD->openScreen(Screens::game);
        GD->m_model.getCurrentSlot()->currentLevel = 0;
    });

    fadeInShips();
}

void LocationScreen::showTutorialButtons() {
    /*std::string labels[2] = {_("Training"), _("Continue without training")};
    using namespace ui;

    for (int i = 0; i < 2; ++i) {
        Button *levelBtn = Button::create();
        levelBtn->loadTextures(
                "mainMenuScreen/buttonActive.png",
                "mainMenuScreen/buttonInactive.png",
                "mainMenuScreen/buttonInactive.png");
        levelBtn->setPosition(Point(200.0 + 200.0 * i, 100.0));
        levelBtn->setTag(i+10);
        //levelBtn->setTitleText(labels[i]);
        //levelBtn->setTitleFontSize(18);
        //levelBtn->setTitleFontName("fonts/IZH.ttf");
        levelBtn->setTouchEnabled(true);
        levelBtn->setPressedActionEnabled(true);
        levelBtn->addTouchEventListener(this, toucheventselector(LocationScreen::buttonHandle));
        TTFConfig conf;
        conf.glyphs = GlyphCollection::CUSTOM;
        conf.fontFilePath = "IZH.ttf";
        conf.fontSize = 20;
        conf.customGlyphs = "ОбучениеПродолжитьбезобучения ";
        Label *label = Label::createWithTTF(conf, labels[i], TextHAlignment::CENTER , 300);
        levelBtn->addProtectedChild(label);
        label->setAnchorPoint(Point::ANCHOR_MIDDLE);
        label->setPosition(levelBtn->getSize() * 0.5);
        rootNode->addChild(levelBtn,2);

    }     */
    DialogManager::getInstance()->hide();

}

void LocationScreen::buttonHandle(Object *sender, ui::TouchEventType type) {
    using namespace ui;
    Button *btn = dynamic_cast<Button*>(sender);

    if (type == TouchEventType::TOUCH_EVENT_ENDED) {
//        if (btn->getTag() == 10) { // what is that i dont remember :(
//            GD->m_model.temp.tutorialMode = true;
//            GD->openScreen(Screens::game);
//
//        }
//        else if (btn->getTag() == 11) {
//            m_dialogEnabled = false;
//            rootNode->removeChildByTag(10, true);
//            rootNode->removeChildByTag(11, true);
//        }


//        else
        if (btn->getTag() == 888) {
            if (rootNode->getChildByTag(20)) { // book menu
                rootNode->removeChildByTag(20);
            }
            else if (m_gameUIController && m_gameUIController->m_moneyUpgradePanel){
                m_gameUIController->closeMoneyUpgrades();
                m_gameUIController->removeShadow();
            }
            else {
                if (!m_newGameDialog) {
                    GD->openScreen(Screens::menu);
                }
            }
        }
        else if (btn->getTag() == 111) {
            if (!m_dialogEnabled) {
                GD->m_model.temp.currentLocation = Location::desert;
                GD->openScreen(Screens::level);
            }
        }
        else if (btn->getTag() == 222) {
            if (!m_dialogEnabled) {
                GD->m_model.temp.currentLocation = Location::regular;
                if (GD->m_model.getCurrentSlot()->getFlag(FlagIndices::regularLocationOpened) || FREE_STUFF) {
                    GD->openScreen(Screens::level);
                }
                else {
                    askForUnlockLocation(Location::regular);
                }
            }
        }
        else if (btn->getTag() == 333) {
            if (!m_dialogEnabled) {
                GD->m_model.temp.currentLocation = Location::snow;
                if (GD->m_model.getCurrentSlot()->getFlag(FlagIndices::snowLocationOpened) || FREE_STUFF) {
                    GD->openScreen(Screens::level);
                }
                else {
                    askForUnlockLocation(Location::snow);
                }
            }
        }
        else if (btn->getTag() == 444) {
            if (!m_dialogEnabled) {
                GD->m_model.temp.currentLocation = Location::inferno;
                if (GD->m_model.getCurrentSlot()->getFlag(FlagIndices::infernoLocationOpened) || FREE_STUFF) {
                    GD->openScreen(Screens::level);
                }
                else {
                    askForUnlockLocation(Location::inferno);
                }
            }
        }
        else if (btn->getTag() == 999 && !rootNode->getChildByTag(20)) {
            openBooksMenu();
        }
        else if (btn->getTag() == 1111) {
            GD->openScreen(Screens::achievementHelp);
        }
//        else if (btn->getTag() == 8893) { //tutorial
//            GD->m_model.temp.tutorialMode = true;
//            GD->openScreen(Screens::game);
//        }
        else if (btn->getName() == "bestiaryBookButton") {
            GD->openScreen(Screens::encyclopedia);
        }
        else if (btn->getName() == "faqBookButton") {
            GD->openScreen(Screens::faq);
        }else if (btn->getName() == "storyBookButton") {
            GD->openScreen(Screens::story);
            //rootNode->removeChildByTag(20);
            //PlatformTools::openLink("http://fantspace.com/mythrandia/");
        }
        // any case
    }
}

std::vector<Resource> LocationScreen::getPreloadResources() {
    vector<Resource> resources;
    resources.push_back(Resource(ResourceType::texture, "locationScreen/background.jpg"));
    resources.push_back(Resource(ResourceType::texture, "locationScreen/ships00.png"));
    resources.push_back(Resource(ResourceType::texture, "commonUI/backButton.png"));
    resources.push_back(Resource(ResourceType::texture, "mainMenuScreen/buttonActive.png"));
    resources.push_back(Resource(ResourceType::texture, "mainMenuScreen/buttonInactive.png"));
    resources.push_back(Resource(ResourceType::texture, "locationScreen/sign.png"));
    resources.push_back(Resource(ResourceType::texture, "gameScreen/bossPrice/digitFonts/crystals/font.png"));
    resources.push_back(Resource(ResourceType::texture, "gameScreen/bossPrice/digitFonts/gold/font.png"));
    resources.push_back(Resource(ResourceType::texture, "gameScreen/ui/upgrades_1.png"));
    resources.push_back(Resource(ResourceType::texture, "gameScreen/ui/upgrades_2.png"));
    resources.push_back(Resource(ResourceType::texture, "locationScreen/booksBack.jpg"));
    return resources;
}

void LocationScreen::playDesertGreenDialog() {
    DialogManager::getInstance()->show(rootNode, 60, Point(0.0, 480.0));
    vector<string> statements;
    statements.push_back(_("Hero, I have disappointing news. The enemy has committed a maneuver and curved the south fort."));
    statements.push_back(_("Our new task is to defend the west frontier and to prevent invasion of the enemy."));
    /// Unfortunately our treasury lacks crystals because of the siege and we don`t have money to hire the Master of dwarfs
    statements.push_back(_("Unfortunately our treasury lacks crystals because of the siege"));
    /// Unfortunately our treasury lacks crystals because of the siege and we don`t have money to hire the Master of dwarfs
    statements.push_back(_("and we don`t have money to hire the Master of dwarfs"));
    DialogManager::getInstance()->setAutoCompleteMode(false);
    DialogManager::getInstance()->createDialog(statements, 30.0, [this](){
        DialogManager::getInstance()->hide();
        DialogManager::getInstance()->setAutoCompleteMode(true);
    });
    fadeInShips();

}

void LocationScreen::playGreenSnowDialog() {
    DialogManager::getInstance()->show(rootNode, 60, Point(0.0, 480.0));
    vector<string> statements;
    statements.push_back(_("Since you had come we not only strengthened our defense but also boosted the morale of our defenders."));
    statements.push_back(_("We are endless thankful to you for all you`ve done for us. But it`s not the time to celebrate victory yet..."));
    statements.push_back(_("The enemies will not drop back so easily, their ships have already curved the island and are now next to the borders of the North outpost."));
    statements.push_back(_("We must immediately equip the outpost and defend the island."));
    DialogManager::getInstance()->setAutoCompleteMode(false);
    DialogManager::getInstance()->createDialog(statements, 30.0, [this](){
        DialogManager::getInstance()->hide();
        DialogManager::getInstance()->setAutoCompleteMode(true);
    });
    fadeInShips();
}


void LocationScreen::playSnowInfernoDialog() {
    DialogManager::getInstance()->show(rootNode, 60, Point(0.0, 480.0));
    vector<string> statements;
    statements.push_back(_("Hero, you`ve done is a priceless work, trade caravans and workers return to Mitrandia"));
    statements.push_back(_("now we have enough forces to give the decisive fight to the invaders………………"));
    statements.push_back(_("I have just received a terrible message!"));
    statements.push_back(_("Dark creatures have found a secret pass in the Ashes Mountains and are at the borders of the east outpost now."));
    statements.push_back(_("The enemy calls demons to serve him.Be careful, these creatures are much more dangerous than all thosehave been before"));
    DialogManager::getInstance()->setAutoCompleteMode(false);
    DialogManager::getInstance()->createDialog(statements, 30.0, [this](){
        DialogManager::getInstance()->hide();
        DialogManager::getInstance()->setAutoCompleteMode(true);
    });
}



void LocationScreen::askForUnlockLocation(Location location) {
    auto panel = mgui::DialogBox::create(_("Get 500 crystals for opening a map and transferring defense construction!"), _("YES"), _("NO"), 20);
    panel->setPosition(Tools::screenCenter() - Point(panel->getSize()*0.5));
    panel->m_okButton->setTouchEnabled(true);
    panel->m_okButton->setTag(1);
    using namespace ui;
    panel->m_okButton->addTouchEventListener(this, toucheventselector(LocationScreen::dialogButtonHandle));

    panel->m_cancelButton->setTouchEnabled(true);
    panel->m_cancelButton->setTag(2);
    panel->m_cancelButton->addTouchEventListener(this, toucheventselector(LocationScreen::dialogButtonHandle));
    panel->setTag(12345);

    m_currentUnlockLocation = location;

    rootNode->addChild(panel);
}

void LocationScreen::dialogButtonHandle(Object *sender, ui::TouchEventType type) {
    auto button = dynamic_cast<ui::Button*>(sender);
    if (type == ui::TouchEventType::TOUCH_EVENT_ENDED) {
        if (button->getTag() == 1) {
            rootNode->getChildByTag(12345)->removeFromParent();
            unlockLocation();
        }
        else if (button->getTag() == 2) {
            rootNode->getChildByTag(12345)->removeFromParent();
        }
        else if (button->getTag() == 3) {
            rootNode->getChildByTag(54321)->removeFromParent();
            buyCrystals();
        }
        else if (button->getTag() == 4) {
            rootNode->getChildByTag(54321)->removeFromParent();
        }
    }
}

void LocationScreen::unlockLocation() {
    FlagIndices flag;
    switch (m_currentUnlockLocation) {
        default:
        case  Location::regular:    flag = FlagIndices::regularLocationOpened; break;
        case  Location::snow:       flag = FlagIndices::snowLocationOpened; break;
        case  Location::inferno:    flag = FlagIndices::infernoLocationOpened; break;
    }
    const int unlockPrice = 500;
    if (!GD->m_model.getCurrentSlot()->getFlag(flag)) {
        if (GD->m_model.getCurrentSlot()->crystals >= unlockPrice) {
            GD->m_model.getCurrentSlot()->crystals -= unlockPrice;
            GD->m_model.getCurrentSlot()->setFlag(flag, true);
            GD->openScreen(Screens::level);
            GD->m_model.dumpToDisk();
        }
        else toggleShop();
    }
}

void LocationScreen::askForBuyCrystals() {
    auto panel = mgui::DialogBox::create(_("buy 100 diamonds for 0.99$ ?"), _("YES"), _("NO"), 20);
    panel->setPosition(Tools::screenCenter() - Point(panel->getSize()*0.5));
    panel->m_okButton->setTouchEnabled(true);
    panel->m_okButton->setTag(3);
    using namespace ui;
    panel->m_okButton->addTouchEventListener(this, toucheventselector(LocationScreen::dialogButtonHandle));

    panel->m_cancelButton->setTouchEnabled(true);
    panel->m_cancelButton->setTag(4);
    panel->m_cancelButton->addTouchEventListener(this, toucheventselector(LocationScreen::dialogButtonHandle));
    panel->setTag(54321);

    rootNode->addChild(panel);
}

void LocationScreen::buyCrystals() {
    // buyTransaction will be here

    GD->requestMoneyUpgrade(1, [this](){
        crystalsBought();
    }, nullptr);

}

void LocationScreen::crystalsBought() {
    //GD->m_model.getCurrentSlot()->crystals+=100;
    updateStats();
    unlockLocation();
}

void LocationScreen::placeShips() {
    int locId = GD->getCompleteLocations();
    if (locId >= 3) {m_ships = nullptr; return;}
    auto ships = Sprite::createWithSpriteFrameName(StringUtils::format("locationScreen/ships/%d.png", locId));
    ships->setPosition(Tools::screenCenter());
    rootNode->addChild(ships,2);
    m_ships = ships;
}

void LocationScreen::fadeInShips() {
    m_ships->setOpacity(0);
    m_ships->runAction(FadeIn::create(1.5));
}

void LocationScreen::placeTutorialButton() {
    using namespace ui;
    auto btn = mgui::Button::create(_("Training"));
    rootNode->addChild(btn);
    GD->ggc()->applyTransformAP(btn, "LocationTutorialButton");
    btn->setTouchEnabled(true);
    btn->addTouchEventListener(this, toucheventselector(LocationScreen::buttonHandle));
    btn->setTag(8893);
}

void LocationScreen::placeStats() {
//    TTFConfig conf;
//    conf.fontSize = 50;
//    conf.fontFilePath = "IZH.ttf";
//    conf.customGlyphs = "1023456789";
//    conf.glyphs = GlyphCollection::CUSTOM;

    //auto labelGold = Label::createWithTTF(conf,
    //        StringUtils::format("%d", GD->m_model.getCurrentSlot()->gold), TextHAlignment::CENTER, 100);
    auto labelGold = Label::createWithBMFont("gameScreen/bossPrice/digitFonts/gold/font.fnt",
            StringUtils::format("%d", GD->m_model.getCurrentSlot()->gold), TextHAlignment::CENTER, 200, Vec2::ZERO);

//    auto labelCrystal = Label::createWithTTF(conf,
//            StringUtils::format("%d", GD->m_model.getCurrentSlot()->crystals), TextHAlignment::CENTER, 100);
    auto labelCrystal = Label::createWithBMFont("gameScreen/bossPrice/digitFonts/crystals/font.fnt",
            StringUtils::format("%d", GD->m_model.getCurrentSlot()->crystals), TextHAlignment::CENTER, 200, Vec2::ZERO);
    labelGold->setPosition(Vec2(600.0, 70.0));
    labelCrystal->setPosition(Vec2(400.0, 70.0));
    labelGold->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    labelCrystal->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    labelGold->setName("labelGold");
    labelCrystal->setName("labelCrystal");
//    labelGold->setColor(Color3B(255,215,5));
//    labelCrystal->setColor(Color3B(2,38,255));
    rootNode->addChild(labelGold, 10);
    rootNode->addChild(labelCrystal, 10);
    auto iconGold = Sprite::create("locationScreen/goldIcon.png");
    auto iconCrystal = Sprite::create("locationScreen/crystalIcon.png");
    iconGold->setPosition(labelGold->getPosition() + Vec2(-20.0,0.0));
    iconCrystal->setPosition(labelCrystal->getPosition() + Vec2(-20.0,0.0));
    rootNode->addChild(iconGold, 10);
    rootNode->addChild(iconCrystal, 10);
}

void LocationScreen::openBooksMenu() {
    using namespace ui;
    auto node = Node::create();
    auto bg = ImageView::create("locationScreen/booksBack.jpg", ui::Widget::TextureResType::LOCAL);
    node->addChild(bg);
    bg->setOpacity(180);
    bg->setPosition(Tools::screenCenter());
    bg->setTouchEnabled(true);
    std::string btnNames[3] = {"faqBookButton", "bestiaryBookButton", "storyBookButton"};
    Vec2 btnPos[3] = {Vec2(475.0, 813.0), Vec2(457.0, 231.0), Vec2(261.0, 513.0)};
    for (int i = 0; i < 3; ++i) {
        auto button = ui::Button::create();
        button->loadTextureNormal(
                StringUtils::format("locationScreen/%s.png", btnNames[i].c_str()), ui::Widget::TextureResType::LOCAL);
        button->setPosition(btnPos[i]);
        button->setName(btnNames[i]);
        node->addChild(button);

        button->setTouchEnabled(true);
        button->addTouchEventListener(this, toucheventselector(LocationScreen::buttonHandle));

    }
    rootNode->addChild(node, 20, 20);
}

void LocationScreen::placeShopButton() {
    auto button = ui::Button::create();
    button->loadTextureNormal("locationScreen/shopButton.jpg", ui::Widget::TextureResType::LOCAL);
    button->addTouchEventListener([this](Ref *obj, ui::Widget::TouchEventType type){
        if (type == ui::Widget::TouchEventType::ENDED) {
            toggleShop();
        }
    });
    button->setPosition(Vec2(680, 265));
    rootNode->addChild(button, 5);
}

void LocationScreen::toggleShop() {
    auto node = Node::create();


    m_gameUIController->m_config.parentNode = node;
    m_gameUIController->addShadow();
    m_gameUIController->openUpgradeMoney();
    m_gameUIController->m_moneyUpgradePanel->replaceBackButton();
    m_gameUIController->m_moneyUpgradePanel->replaceCloseButton();

    rootNode->addChild(node, 10);
//    auto panel = mgui::upgrades::MoneyUpgradePanel::create(this);
//    m_moneyUpgradePanel->retain();
//    m_config.parentNode->addChild(m_moneyUpgradePanel, 101);
//    m_currentPanel = m_moneyUpgradePanel;
}

void LocationScreen::upgradeMoney(
        int upgradeIndex,
        std::function<void()> successCallback,
        std::function<void()> failureCallback) {
    GD->requestMoneyUpgrade(upgradeIndex, [this, successCallback](){
        SimpleAudioEngine::getInstance()->playEffect("gameScreen/buy.wav", false, 1.0, 0.0, 1.0);
        updateStats();
        if (successCallback) successCallback();
    }, failureCallback);
}

void LocationScreen::updateStats() {
    Label *labelGold = static_cast<Label*>(rootNode->getChildByName("labelGold"));
    Label *labelCrystal = static_cast<Label*>(rootNode->getChildByName("labelCrystal"));
    labelGold->setString(StringUtils::format("%d", GD->m_model.getCurrentSlot()->gold));
    labelCrystal->setString(StringUtils::format("%d", GD->m_model.getCurrentSlot()->crystals));
}

void LocationScreen::placeDailyAward() {
    int awards[5] = {15, 20, 25, 30, 40};
    int awardId = GD->getCurrentAwardId();
    log("award id = %d", awardId);
    if (awardId >= 0) {
        m_gameUIController->m_config.parentNode = rootNode;
        m_gameUIController->addShadow();

        bool newGame = false;
        if (GD->m_model.getCurrentSlot()->completeLevels < 1)
            newGame = true;
        if (!newGame && PlatformTools::isPurchaseAllowed()) {
            auto saleBtn = ui::Button::create();
            const int saleId = 6;
            int valueId = GD->defaultsConfig()->fromPath(StringUtils::format("sales.sale%d.valueId", saleId))->toInt();
            saleBtn->loadTextureNormal(StringUtils::format("sales/%d.png", valueId), Widget::TextureResType::LOCAL);
            saleBtn->setPosition(Tools::screenCenter() + Vec2(0,200));
            GD->m_model.getCurrentSlot()->setSaleShowed(saleId, true);
            int upgradeId = GD->defaultsConfig()->fromPath(StringUtils::format("sales.sale%d.moneyUpgradeId", saleId))->toInt();
            saleBtn->addClickEventListener([this, upgradeId, saleId](Ref *obj) {
                GD->requestMoneyUpgrade(upgradeId, [this, saleId]() {
                    SimpleAudioEngine::getInstance()->playEffect("gameScreen/buy.wav", false, 1.0, 0.0, 1.0);
                    GD->m_model.getCurrentSlot()->setSaleUsed(saleId, true);
                    updateStats();
                    rootNode->removeChildByName("saleButton");
                    rootNode->removeChildByName("saleCloseButton");
                }, nullptr);
            });
            rootNode->addChild(saleBtn, 100);
            saleBtn->setName("saleButton");
            
            auto closeBtn = ui::Button::create();
            closeBtn->loadTextureNormal("commonUI/closeButton.png", Widget::TextureResType::LOCAL);
            closeBtn->setPosition(saleBtn->getPosition() + saleBtn->getContentSize() / 2 + Vec2(-10, -10));
            closeBtn->addClickEventListener([this](Ref*obj){
                rootNode->removeChildByName("saleButton");
                rootNode->removeChildByName("saleCloseButton");
            });
            rootNode->addChild(closeBtn, 101);
            closeBtn->setName("saleCloseButton");
        }

        auto node = Node::create();
        auto bg = Sprite::createWithSpriteFrameName("locationScreen/award/bg.png");
        node->addChild(bg);
        node->setPosition(Tools::screenCenter() + Vec2(0, -200));

        auto active = Sprite::createWithSpriteFrameName(StringUtils::format("locationScreen/award/active%d.png", awardId));
        node->addChild(active);

        TTFConfig conf;
        conf.fontSize = 20;
        conf.fontFilePath = "IZH.ttf";
        Label *label = Label::createWithTTF(
                conf, _("Ежедневная торговля с гномами приносит в казну небольшое количество кристаллов"),
                TextHAlignment::CENTER, 570);
        label->setPosition(Vec2(-80, 147));
        label->setColor(Color3B(245, 155, 14));
        node->addChild(label);

        ui::Button *btn = ui::Button::create();
        btn->loadTextureNormal("locationScreen/award/btn.png", ui::Widget::TextureResType::PLIST);
        btn->setTitleFontName("IZH.ttf");
        btn->setTitleFontSize(20);
        btn->setTitleColor(Color3B(20,20,20));
        btn->setTitleText(_("GRAB"));
        btn->setPosition(Vec2(290,142));
        node->addChild(btn);
        btn->addClickEventListener([this](Ref*obj) {
            GD->grabDailyAward();
            updateStats();
            dynamic_cast<Node*>(obj)->getParent()->removeFromParent();
            m_gameUIController->removeShadow();
            SimpleAudioEngine::getInstance()->playEffect("locationScreen/gotCrystals.wav",false, 1, 0, 1);
        });

        rootNode->addChild(node, 100);
    }
    else
        GD->check35MinSale([this](){updateStats();});

}
