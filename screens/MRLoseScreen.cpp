//
// Created by Admin on 07.02.14.
//

#include "MRLoseScreen.h"
#include "audio/include/SimpleAudioEngine.h"
#include "core/CheetahAtlasHelper.h"
#include "platform/MRPlatformTools.h"
#include <game/MRUIWidgets.h>

using namespace myth;
using namespace cocos2d;
using namespace CocosDenshion;
using namespace ui;

void LoseScreen::open() {
    //m_isFade = true;
    Screen::open();

    ImageView *bg = ImageView::create();
    bg->loadTexture("looseScreen/background.png");
    bg->setScale(2.0);

    Size winSize = Director::getInstance()->getVisibleSize();
    bg->setPosition(Point(winSize.width*0.5f, winSize.height*0.6f));

    std::string labels[3] = {_("Menu"), _("Map"), _("Restart")};

    for (int i = 0; i < 3; ++i) {
        Button *levelBtn = Button::create();
        levelBtn->loadTextures(
                "mainMenuScreen/buttonActive.png",
                "mainMenuScreen/buttonInactive.png",
                "mainMenuScreen/buttonInactive.png");
        levelBtn->setPosition(Point(768.0/2.0, 150.0+100.0*i));
        levelBtn->setTag(i);
        TTFConfig conf;
        conf.fontFilePath = "IZH.ttf";
        conf.fontSize = 30;
        auto label = Label::createWithTTF(conf, labels[i], TextHAlignment::CENTER , 100);
        levelBtn->addProtectedChild(label);
        label->setPosition(levelBtn->getSize() * 0.5);
        levelBtn->setTouchEnabled(true);
        levelBtn->addTouchEventListener(this, toucheventselector(LoseScreen::buttonHandle));

        rootNode->addChild(levelBtn,2);

    }
    SimpleAudioEngine::getInstance()->playBackgroundMusic("looseScreen/music.mp3", false);
    this->rootNode->addChild(bg);

    //PlatformTools::showGADBanner(GoogleADType::lose);
    //if (GD->getDeviceFamily() == DeviceFamily::iphone)
    //    PlatformTools::showGADBanner(GoogleADType::loseTop);
    if (PlatformTools::isPurchaseAllowed())
        /*if ((GD->m_model.getCurrentSlot()->totalGameTimeSeconds / 60) > 35
            && ! GD->m_model.getCurrentSlot()->isSaleShowed(4)) {
            showSaleFullscreen(4);
        }*/ // deprecated, now in game director for any screen
        if (GD->m_model.getCurrentSlot()->currentLevel % 9 == 8) {
            showSaleFullscreen(7);
        }
        else if (GD->m_model.getCurrentSlot()->totalLoses >= 3
                 && !GD->m_model.getCurrentSlot()->isSaleShowed(1)) {
            showSaleFullscreen(1);
        }
        else if (GD->m_model.getCurrentSlot()->shouldShowAfterBossLoseSale) {
            GD->m_model.getCurrentSlot()->shouldShowAfterBossLoseSale = false;
            showSaleFullscreen(3, false);
        }
        else
            GD->check35MinSale();

}

void LoseScreen::showSaleFullscreen(int saleId, bool store) {
    mgui::Sale *panel = mgui::Sale::create(saleId);
    panel->setName("salePanel");

    if (store) {
        GD->m_model.getCurrentSlot()->setSaleShowed(saleId, true);
    }
    panel->m_onSaleClicked = [this](int _saleId) {
        saleClickedOk(_saleId);
    };
    panel->m_onCloseClicked = [this]() {
        saleClickedCancel();
    };
    rootNode->addChild(panel, 20);
}

void LoseScreen::buttonHandle(Object *sender, TouchEventType type) {
    Button *btn = dynamic_cast<Button*>(sender);
    if (type == TouchEventType::TOUCH_EVENT_ENDED) {
        if (btn->getTag() == 0)
            GD->openScreen(Screens::menu);
        else if (btn->getTag() == 1)
            GD->openScreen(Screens::location);
        else if (btn->getTag() == 2)
            GD->openScreen(Screens::game);
    }
}

void LoseScreen::saleClickedOk(int saleId) {
    rootNode->getChildByName("salePanel")->removeFromParent();

    int upgradeId = GD->defaultsConfig()->fromPath(StringUtils::format("sales.sale%d.moneyUpgradeId", saleId))->toInt();

    GD->requestMoneyUpgrade(upgradeId, [this, saleId](){
        SimpleAudioEngine::getInstance()->playEffect("gameScreen/buy.wav", false, 1.0, 0.0, 1.0);
        GD->m_model.getCurrentSlot()->setSaleUsed(saleId, true);
        GD->m_model.dumpToDisk();
        //GD->openScreen(Screens::level);
    }, nullptr);
}

void LoseScreen::saleClickedCancel() {
    GD->m_model.dumpToDisk();
    rootNode->getChildByName("salePanel")->removeFromParent();
}


void LoseScreen::close() {

    Screen::close();
    SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
    PlatformTools::hideGADBanner();

}

LoseScreen::~LoseScreen() {

}

