//
// Created by Admin on 08.09.14.
//

#include "MRStoryScreen.h"
#include "core/myth.h"

using namespace myth;
using namespace cocos2d;
using namespace ui;


void StoryScreen::open() {
    Screen::open();
    schedulePreload();

}

void StoryScreen::close() {
    Screen::close();
}

void StoryScreen::preloadComplete() {
    Screen::preloadComplete();
    auto bg = Sprite::create("storyScreen/bg.jpg");
    bg->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);


    Node *node = Node::create();

    auto sv = ui::ScrollView::create();
    sv->setPosition(Vec2(0.0, 0.0));
    sv->setTouchEnabled(true);
    sv->setSize(Size(768.0, 1024.0));
    sv->setInnerContainerSize(Size(768.0, bg->getContentSize().height));

    rootNode->addChild(sv, 1);
    sv->addChild(node);
    node->addChild(bg,1);


    auto button = ui::Button::create();
    button->loadTextureNormal("commonUI/backButton.png", TextureResType::LOCAL);
    button->setTouchEnabled(true);
    button->addTouchEventListener(this, toucheventselector(StoryScreen::backButtonTouched));
    button->setPosition(Point(100.0, 970.0));
    rootNode->addChild(button, 10);


}

std::vector<Resource> StoryScreen::getPreloadResources() {
    return Screen::getPreloadResources();
}

void StoryScreen::backButtonTouched(Object *sender, ui::TouchEventType type) {
    auto button = static_cast<Button*>(sender);
    if (type == TouchEventType::TOUCH_EVENT_ENDED) {
        GD->openScreen(Screens::location);
    }
}
