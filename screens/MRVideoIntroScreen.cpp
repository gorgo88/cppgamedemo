//
// Created by Admin on 29.05.14.
//

#include "MRVideoIntroScreen.h"
#include "cocos2d.h"
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

using namespace cocos2d::experimental::ui;

void myth::VideoIntroScreen::open() {
    m_videoPlayer = nullptr;
    Screen::open();
    auto videoPlayer = VideoPlayer::create();
    //rootNode->setPosition(Vec2::ZERO);
    //videoPlayer->setContentSize(Size(10.0,10.0));
    videoPlayer->setContentSize(Size(768.0,1024.0) );
    //GD->setForcedInterfaceOrientation(InterfaceOrientation::horizontal);
    rootNode->addChild(videoPlayer);
    Vec2 winCenter = Vec2(Director::getInstance()->getWinSize()*0.5);
    videoPlayer->setPosition(winCenter - rootNode->getPosition());
    videoPlayer->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    //if (GD->getDeviceFamily() == DeviceFamily::iphone4)
    //    videoPlayer->setPosition(Vec2(0.0f, 32.0f) + videoPlayer->getPosition()); // really magic number!
    //videoPlayer->setPosition(Vec2(0.0f,1000.0f));
    //videoPlayer->runAction(MoveTo::create(2.0f, Vec2(768.0f, 1024.0f)));
    videoPlayer->setKeepAspectRatioEnabled(false);
    videoPlayer->setFileName("videos/intro.mp4");
    videoPlayer->setFullScreenEnabled(false);
    videoPlayer->play();
    
    //videoPlayer->pause();

    videoPlayer->addEventListener(CC_CALLBACK_2(VideoIntroScreen::videoEventCallback, this));
    videoPlayer->setTag(1);
    m_videoPlayer = videoPlayer;
}


void myth::VideoIntroScreen::videoEventCallback(Ref* sender, VideoPlayer::EventType eventType) {
   if (eventType == VideoPlayer::EventType::COMPLETED) {
       log("video complete");
       dynamic_cast<VideoPlayer*>(rootNode->getChildByTag(1))->removeFromParent();
       m_videoPlayer = nullptr;
       //GD->setForcedInterfaceOrientation(InterfaceOrientation::vertical);
       GD->openScreen(Screens::menu);
   }
}

void myth::VideoIntroScreen::resumeVideo() {
    if (m_videoPlayer) m_videoPlayer->resume();
}
#else
void myth::VideoIntroScreen::resumeVideo() {
    // do nothing
}
void myth::VideoIntroScreen::open() {
    GD->openScreen(Screens::menu);
}
#endif

void myth::VideoIntroScreen::close() {
    Screen::close();
}
