//
// Created by Admin on 09.07.14.
//

#include "MRRatingScreen.h"
#include "platform/MRPlatformTools.h"
#include "ui/CocosGUI.h"

using namespace ui;

static const int PLAYER_RECORDS_COUNT = 60;

void myth::RatingScreen::open() {
    Screen::open();
    schedulePreload();
}

void myth::RatingScreen::close() {
    Screen::close();
}

void myth::RatingScreen::preloadComplete() {
    Screen::preloadComplete();
    m_leaderBoardLoaded = false;
    m_localPlayerLoaded = false;

    auto bg = Sprite::create("ratingScreen/background.jpg");
    rootNode->addChild(bg, 1);
    bg->setPosition(Vec2(0.0, 1024.0));
    bg->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);

    auto button = ui::Button::create();
    button->loadTextureNormal("commonUI/backButton.png", TextureResType::LOCAL);
    button->setTouchEnabled(true);
    button->addTouchEventListener(this, toucheventselector(RatingScreen::backButtonTouched));
    button->setPosition(Point(100.0, 980.0));
    rootNode->addChild(button, 10);

    TTFConfig conf;
    conf.fontFilePath = "IZH.ttf";
    conf.fontSize  = 30;

    auto loadingLabel = Label::createWithTTF(conf, "Loading...", TextHAlignment::CENTER , 500);
    rootNode->addChild(loadingLabel, 5);
    loadingLabel->setColor(Color3B::WHITE);
    loadingLabel->setPosition(Tools::screenCenter() + Vec2(0.0, 350.0));
    loadingLabel->setTag(456);


    if (!PlatformTools::isGameCenterAuthenticated()) {
        loadingLabel->setString(_("not authenticated"));
        return;
    }

    PlatformTools::getLeaderBoard([this](std::vector<LeaderBoardItem> leaderBoardItems, bool selfIncluded){
        log ("got leaderboard, %d items", leaderBoardItems.size());
        //drawLeaderBoard(leaderBoardItems, selfIncluded);
        m_items = leaderBoardItems;
        m_leaderBoardLoaded = true;
        checkAllLoaded();
    }, PLAYER_RECORDS_COUNT);

    PlatformTools::getLocalPlayer([this](LeaderBoardItem localPlayer){
        //drawLocalPlayer(localPlayer);
        m_localPlayer = localPlayer;
        m_localPlayerLoaded = true;
        checkAllLoaded();
    });


}

void myth::RatingScreen::backButtonTouched(Object *sender, ui::TouchEventType type) {
    auto button = static_cast<Button*>(sender);
    if (type == TouchEventType::TOUCH_EVENT_ENDED) {
        GD->openScreen(Screens::menu);
    }
}

vector<myth::Resource> myth::RatingScreen::getPreloadResources() {
    return Screen::getPreloadResources();
}

void myth::RatingScreen::drawLeaderBoard() {
    TTFConfig conf;
    conf.fontFilePath = "helvetica-neue-bold.ttf";
    conf.fontSize  = 25;

    Color3B colors[3] = {Color3B(242,199,98), Color3B(242,242,242), Color3B(206,101,3)};

    auto list = ui::ListView::create();
    rootNode->addChild(list, 5);
    list->setPosition(Vec2(40.0, 0.0));
    list->setSize(Size(768.0,780.0));

    list->setDirection(ui::ScrollView::Direction::VERTICAL);
    list->setBounceEnabled(true);

    m_list = list;

    //m_items.insert(m_items.begin(), m_localPlayer);

    //self
    ui::Layout *itemNode = ui::Layout::create();

    rootNode->addChild(itemNode, 6);
    itemNode->setPosition(Vec2(list->getPosition().x, 830.0));

    float labelYOffset = 24.0;

    Color3B *color;
    color = &colors[2];

    auto placeLabel = Label::createWithTTF(conf, StringUtils::format("%d", m_localPlayer.place), TextHAlignment::CENTER , 500);
    itemNode->addProtectedChild(placeLabel, 5);
    placeLabel->setColor(*color);
    placeLabel->setPosition(Vec2(49.0, labelYOffset));
    placeLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);

    auto nameLabel = Label::createWithTTF(conf, StringUtils::format("%s", m_localPlayer.username.c_str()), TextHAlignment::CENTER , 1000);
    itemNode->addProtectedChild(nameLabel, 5);
    nameLabel->setColor(*color);
    nameLabel->setPosition(Vec2(150.0, labelYOffset));
    nameLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);

    auto scoreLabel = Label::createWithTTF(conf, StringUtils::format("%d", m_localPlayer.score), TextHAlignment::CENTER , 500);
    itemNode->addProtectedChild(scoreLabel, 5);
    scoreLabel->setColor(*color);
    scoreLabel->setPosition(Vec2(624.0, labelYOffset));
    scoreLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);

    //itemNode->setPosition(Vec2(0.0, y));

    for (int i = 0; i < m_items.size(); ++i) {
        ui::Layout *itemNode = ui::Layout::create();
        //itemNode->setSize(Size(600.0, 50.0));

        itemNode->setBackGroundImage("ratingScreen/itemBack.png", Widget::TextureResType::LOCAL);
        itemNode->setSize(itemNode->getBackGroundImageTextureSize());
        //auto itemBg = Sprite::create("ratingScreen/itemBack.png");
        //itemBg->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
        list->pushBackCustomItem(itemNode);
        //list->addChild(itemNode);

        //itemNode->addChild(itemBg, 1);

        float labelYOffset = 27.0;

        Color3B *color;
        if (i < 10) color = &colors[0];
        else if (i < 20) color = &colors[1];
        else color =  &colors[2];

        float y = (float) (800.0 - i * 100.0);
        auto placeLabel = Label::createWithTTF(conf, StringUtils::format("%d", m_items[i].place), TextHAlignment::CENTER , 500);
        itemNode->addProtectedChild(placeLabel, 5);
        placeLabel->setColor(*color);
        placeLabel->setPosition(Vec2(49.0, labelYOffset));
        placeLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);

        auto nameLabel = Label::createWithTTF(conf, StringUtils::format("%s", m_items[i].username.c_str()), TextHAlignment::CENTER , 1000);
        itemNode->addProtectedChild(nameLabel, 5);
        nameLabel->setColor(*color);
        nameLabel->setPosition(Vec2(150.0, labelYOffset));
        nameLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);

        auto scoreLabel = Label::createWithTTF(conf, StringUtils::format("%d", m_items[i].score), TextHAlignment::CENTER , 500);
        itemNode->addProtectedChild(scoreLabel, 5);
        scoreLabel->setColor(*color);
        scoreLabel->setPosition(Vec2(624.0, labelYOffset));
        scoreLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);

        //itemNode->setPosition(Vec2(0.0, y));
    }

}

void myth::RatingScreen::checkAllLoaded() {
    if (m_leaderBoardLoaded && m_localPlayerLoaded) {
        if (rootNode->getChildByTag(456))
            rootNode->getChildByTag(456)->removeFromParent();
        drawLeaderBoard();
    }
}

void myth::RatingScreen::drawLocalPlayer(myth::LeaderBoardItem localPlayer) {
    TTFConfig conf;
    conf.fontFilePath = "helvetica-neue-bold.ttf";
    conf.fontSize  = 25;

    ui::Layout *itemNode = ui::Layout::create();

    itemNode->setBackGroundImage("ratingScreen/itemBack.png", Widget::TextureResType::LOCAL);
    itemNode->setSize(itemNode->getBackGroundImageTextureSize());

    float labelYOffset = 24.0;

    Color3B color = Color3B::WHITE;

    float y = 800.0;
    rootNode->addChild(itemNode);
    auto placeLabel = Label::createWithTTF(conf, StringUtils::format("%d", localPlayer.place), TextHAlignment::CENTER , 500);
    itemNode->addProtectedChild(placeLabel, 5);
    placeLabel->setColor(color);
    placeLabel->setPosition(Vec2(29.0, labelYOffset));
    placeLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE);

    auto nameLabel = Label::createWithTTF(conf, StringUtils::format("%s", localPlayer.username.c_str()), TextHAlignment::CENTER , 1000);
    itemNode->addProtectedChild(nameLabel, 5);
    nameLabel->setColor(color);
    nameLabel->setPosition(Vec2(160.0, labelYOffset));
    nameLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);

    auto scoreLabel = Label::createWithTTF(conf, StringUtils::format("%d", localPlayer.score), TextHAlignment::CENTER , 500);
    itemNode->addProtectedChild(scoreLabel, 5);
    scoreLabel->setColor(color);
    scoreLabel->setPosition(Vec2(624.0, labelYOffset));
    scoreLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_RIGHT);
}
