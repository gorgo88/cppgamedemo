//
// Created by Admin on 08.09.14.
//

#include "MRFaqScreen.h"
#include "core/myth.h"

using namespace myth;
using namespace cocos2d;
using namespace ui;


void FaqScreen::open() {
    Screen::open();
    schedulePreload();

}

void FaqScreen::close() {
    Screen::close();
}

void FaqScreen::preloadComplete() {
    Screen::preloadComplete();
    auto bg = Sprite::create("faqScreen/background.jpg");
    bg->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);


    Node *node = Node::create();

    auto sv = ui::ScrollView::create();
    sv->setPosition(Vec2(0.0, 0.0));
    sv->setTouchEnabled(true);
    sv->setSize(Size(768.0, 1024.0));
    sv->setInnerContainerSize(Size(768.0, bg->getContentSize().height));

    rootNode->addChild(sv, 1);
    sv->addChild(node);
    node->addChild(bg,1);

    std::string labels[9] = {
            _("If you need more gold to buy an upgrade but at the same time you have crystals, they could be automatically changed into gold at rate 1 to 4"),
            _("Walls upgrading significantly slows down the movement of monsters to the citadel"),
            _("Upgrading of towers increases the fire rate of catapults"),
            _("Defensive knives are a passive defensive device and they automatically shoot when enemies come closer"),
            _("Upgrade the skills of your magic assistants. Every next grade increases the number of lives and the amount of attacking magic spells"),
            _("The King of ginors and the Death attack your magic defender remotely"),
            _("If you've lost the level all your upgrades and earned money at this level are lost as well!"),
            _("If you replay the level the award and the points gained for defeating the enemy significantly decrease. Try to compete the level as successful as you can on your first try"),
            _("The player can always replay the level and improve the achievements")
    };

    TTFConfig conf;
    conf.fontSize = 18;
    conf.fontFilePath = "IZH.ttf";

    for (int i = 0; i < 9; ++i) {

        cocos2d::Rect rect = GD->ggc()->getRectPos(StringUtils::format("FaqRect_%02d", i+1));
        auto label = Label::createWithTTF(conf, labels[i].c_str(), TextHAlignment::LEFT , (int)rect.size.width - 20);
        label->setColor(Color3B::BLACK);
        label->setPosition(rect.origin + Vec2(10.0, bg->getContentSize().height - 3.0));
        label->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
        log("label x = %f, y = %f", label->getPosition().x, label->getPosition().y);
        node->addChild(label,2);

    }

    auto button = ui::Button::create();
    button->loadTextureNormal("commonUI/backButton.png", TextureResType::LOCAL);
    button->setTouchEnabled(true);
    button->addTouchEventListener(this, toucheventselector(FaqScreen::backButtonTouched));
    button->setPosition(Point(100.0, 970.0));
    rootNode->addChild(button, 10);


}

std::vector<Resource> FaqScreen::getPreloadResources() {
    std::vector<Resource> res;
    res.push_back(Resource(ResourceType::texture, "faqScreen/background.jpg"));
    return res;
}

void FaqScreen::backButtonTouched(Object *sender, ui::TouchEventType type) {
    auto button = static_cast<Button*>(sender);
    if (type == TouchEventType::TOUCH_EVENT_ENDED) {
        GD->openScreen(Screens::location);
    }
}
