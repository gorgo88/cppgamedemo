//
// Created by Admin on 29.05.14.
//


#ifndef __MRVideoIntroScreen_H_
#define __MRVideoIntroScreen_H_

#include "core/myth.h"
#include "ui/CocosGUI.h"

namespace myth {

    class VideoIntroScreen : public Screen {
    public:


        VideoIntroScreen() : Screen("Video Screen") {}

        void resumeVideo();

    private:
        virtual void open() override;

        virtual void close() override;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        void videoEventCallback(Ref *sender, cocos2d::experimental::ui::VideoPlayer::EventType eventType);

        cocos2d::experimental::ui::VideoPlayer *m_videoPlayer;
#endif
    };
}

#endif //__MRVideoIntroScreen_H_
