//
// Created by Admin on 09.07.14.
//


#ifndef __MRRatingScreen_H_
#define __MRRatingScreen_H_

#include "core/myth.h"
#include "ui/CocosGUI.h"
#include "core/MRLeaderBoardItem.h"

namespace myth {

    class RatingScreen : public Screen {
    public:


        RatingScreen() : Screen("Rating Screen") {}

        virtual void open() override;

        virtual void close() override;

        virtual void preloadComplete() override;

        virtual std::vector<Resource> getPreloadResources() override;
        void backButtonTouched(Object *sender, ui::TouchEventType type);

        void drawLeaderBoard();

        void checkAllLoaded();

        bool m_leaderBoardLoaded;
        bool m_localPlayerLoaded;

        void drawLocalPlayer(LeaderBoardItem localPlayer);

        ui::ListView *m_list;
        vector<LeaderBoardItem> m_items;
        LeaderBoardItem m_localPlayer;
    };

}
#endif //__MRRatingScreen_H_
