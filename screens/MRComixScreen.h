//
// Created by Admin on 06.03.14.
//



#ifndef __MRComixScreen_H_
#define __MRComixScreen_H_

#include "core/myth.h"
#include "ui/CocosGUI.h"

namespace myth {

class ComixScreen : public Screen {

public:


    ComixScreen() : Screen("Comix Screen") {}

    virtual void open() override;

    virtual void close() override;

    EventListener *m_touchListener;

    bool touchBegan(cocos2d::Touch *touch, cocos2d::Event *event);

    void gotoNext();

    virtual std::vector<Resource> getPreloadResources() override;

    virtual void preloadComplete() override;

    void doFireworks();

    int m_currentFrameIndex;

    void showNextFrame();

    int getFramesCount();

    Sprite *m_currentFrameSprite = nullptr;

    void placeSkipButton();

    void buttonHandle(Object *sender, cocos2d::ui::TouchEventType type);

};

}

#endif //__MRComixScreen_H_
