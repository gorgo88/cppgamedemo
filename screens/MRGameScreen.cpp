//
// Created by Admin on 04.12.13.
//


#include "MRGameScreen.h"
#include "game/MRGameController.h"
#import "audio/include/SimpleAudioEngine.h"
#include "strutil/strutil.h"

using namespace myth;
using namespace CocosDenshion;

void GameScreen::open() {
    m_isFade = false;
    Screen::open();
    schedulePreload();
}

void GameScreen::close() {
    gameController->onClose();
    gameController->release();
    gameController = NULL;
    Screen::close();
}


void GameScreen::preloadComplete() {
    if (GD->getDeviceFamily() == DeviceFamily::iphone) {
        rootNode->setPosition(Vec2(0.0f, 15.0f));
        rootNode->removeChildByTag(10);
        rootNode->removeChildByTag(11);
    }
    gameController = new GameController();
    gameController->setup(this);
}

std::vector<Resource> GameScreen::getPreloadResources() {
    std::vector<Resource> resources;
    ConfigReader *screenReader = GD->getScreenResourcesReader()->getSubConfigByPath("gameScreen");
    // textures
    Array *textures = screenReader->fromPath("textures")->toArray();
    for (int i = 0; i < textures->count(); ++i) {
        resources.push_back(Resource(
                ResourceType::texture,
                static_cast<String*>(textures->getObjectAtIndex(i))->_string));
    }
    /*char key[64];
    sprintf(key, "levels.level%02d.monsters", GD->m_model.getCurrentSlot()->currentLevel+1);
    Array *levelMonsters = GD->defaultsConfig()->fromPath(key)->toArray();
    for (int i = 0; i < levelMonsters->count(); ++i) {
        char texPath[64];
        String *monsterId = static_cast<String*>(
                static_cast<Dictionary*>(levelMonsters->getObjectAtIndex(i))->objectForKey("id"));
        sprintf(texPath, "gameScreen/monsters/%s.png", monsterId->getCString());
        resources.push_back(Resource(ResourceType::texture, std::string(texPath)));
    } */ //TODO: later
    if (GD->m_model.temp.currentLocation == Location::desert) {
        resources.push_back(Resource(ResourceType::texture, "gameScreen/backAnimations/scorpions.png"));
        resources.push_back(Resource(ResourceType::texture, "gameScreen/backAnimations/snake.png"));
        resources.push_back(Resource(ResourceType::texture, "gameScreen/backAnimations/fontain.png"));
        resources.push_back(Resource(ResourceType::texture, "gameScreen/backAnimations/eyes.png"));
    }
    if (GD->m_model.temp.currentLocation == Location::inferno) {
        resources.push_back(Resource(ResourceType::texture, "gameScreen/backAnimations/sparks.png"));
        resources.push_back(Resource(ResourceType::texture, "gameScreen/backAnimations/fireBoom.png"));
        resources.push_back(Resource(ResourceType::texture, "gameScreen/backAnimations/cracks.png"));
        resources.push_back(Resource(ResourceType::texture, "gameScreen/backAnimations/shine.png"));
        resources.push_back(Resource(ResourceType::texture, "gameScreen/backAnimations/bats.png"));
    }


    return resources;
}

void GameScreen::setupBordersIphone() {
    Screen::setupBordersIphone();
}

GameController * GameScreen::getGameController() {
    return gameController;
}
