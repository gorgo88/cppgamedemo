//
// Created by Admin on 09.01.14.
//

#include <game/MRUIWidgets.h>
#include "MRWinScreen.h"
#include "ui/CocosGUI.h"
#include "audio/include/SimpleAudioEngine.h"
#include "core/CheetahAtlasHelper.h"
#include "game/MRTutorialController.h"
#include "platform/MRPlatformTools.h"

using namespace myth;
using namespace cocos2d;
using namespace CocosDenshion;
using namespace ui;


void WinScreen::open() {
    //m_isFade = true;
    Screen::open();

    ImageView *bg = ImageView::create();
    bg->loadTexture("winScreen/background.png");

    Size winSize = Director::getInstance()->getVisibleSize();
    GD->ggc()->applyTransformAP(bg, "WinScreenLogo");
    bg->setScale(2.0);

    std::string labels[4] = {_("Menu"), _("Map"), _("Restart"), _("Play")};

    for (int i = 0; i < 4; ++i) {
        Button *levelBtn = Button::create();
        levelBtn->loadTextures(
                "mainMenuScreen/buttonActive.png",
                "mainMenuScreen/buttonInactive.png",
                "mainMenuScreen/buttonInactive.png");
        GD->ggc()->applyTransformAP(levelBtn, StringUtils::format("WinScreenButton%02d", 3-i));
        levelBtn->setTag(i);
        TTFConfig conf;
        conf.fontFilePath = "IZH.ttf";
        conf.fontSize = 30;
        auto label = Label::createWithTTF(conf, labels[i], TextHAlignment::CENTER , 100);
        levelBtn->addProtectedChild(label);
        label->setPosition(levelBtn->getSize() * 0.5);
        levelBtn->setTouchEnabled(true);
        levelBtn->addTouchEventListener(this, toucheventselector(WinScreen::buttonHandle));

        rootNode->addChild(levelBtn,2);

    }
    SimpleAudioEngine::getInstance()->playBackgroundMusic("winScreen/music.mp3", false);
    this->rootNode->addChild(bg);
    doFireworks();
    Director::getInstance()->getScheduler()->scheduleUpdateForTarget(this, 10, false);
    CheetahAtlasHelper::getInstance()->add("winScreen/fireworks.atlas");


    // achievements
    initAchievementAnimation();
    addTotalGoldLabel();

    PlatformTools::showGADBanner(GoogleADType::win);
    if (GD->getDeviceFamily() == DeviceFamily::iphone)
        PlatformTools::showGADBanner(GoogleADType::winTop);

}


void WinScreen::buttonHandle(Object *sender, TouchEventType type) {
    Button *btn = dynamic_cast<Button*>(sender);
    int currentLevel = GD->m_model.getCurrentSlot()->currentLevel;
    if (type == TouchEventType::TOUCH_EVENT_ENDED) {
        if (btn->getTag() == 0)    // to menu
            if (checkForShowComix(currentLevel)) return;
            else GD->openScreen(Screens::menu);
        else if (btn->getTag() == 1)  {    // to map
            if (checkForShowComix(currentLevel)) return;
            else GD->openScreen(Screens::location);
        }
        else if (btn->getTag() == 2)        // restart
            GD->openScreen(Screens::game);
        else {                                  // next
            if (checkForShowComix(currentLevel)) return;
            else {
//                GD->m_model.getCurrentSlot()->currentLevel++;
//                int completeLevels =  GD->m_model.getCurrentSlot()->completeLevels;
//                int targetLocation = (int)floorf(((float)completeLevels+1.0) / 12.0);
//                if (targetLocation > 3) targetLocation = 3;
//                Location targetLocationEnum = static_cast<Location>(targetLocation);
//                GD->m_model.temp.currentLocation = targetLocationEnum;
//
//                GD->openScreen(Screens::game);
                GD->gotoNextLevel();
            }


        }

    }
}

bool WinScreen::checkForShowComix(int completeLevel) {
    if (completeLevel == GD->getLevelsPerLocation() * 1 - 1) {
        GD->m_model.temp.currentComix = ComixEnum::after1stLocation;
        GD->openScreen(Screens::comix);
        return true;
    }
    else if (completeLevel == GD->getLevelsPerLocation() * 2 - 1) {
        GD->m_model.temp.currentComix = ComixEnum::after2ndLocation;
        GD->openScreen(Screens::comix);
        return true;
    }
    else if (completeLevel == GD->getLevelsPerLocation() * 3 - 1) {
        GD->m_model.temp.currentComix = ComixEnum::after3rdLocation;
        GD->openScreen(Screens::comix);
        return true;
    }
    else if (completeLevel == GD->getLevelsPerLocation() * 4                                                                     - 1) {
        GD->m_model.temp.currentComix = ComixEnum::after4thLocation;
        GD->openScreen(Screens::comix);
        return true;
    }
    return false;
}


void WinScreen::update(float dt) {
    m_fwTimer -= dt;
    if (m_fwTimer <= 0 && m_fwCount < 5) {
        doFireworks();
        m_fwTimer = 0.2;
        m_fwCount++;
    }

    if (m_totalScoreCountData.isCounting || m_totalCrystalCountData.isCounting) {
        m_totalScoreCountData.updateCurrents();
        m_totalCrystalCountData.updateCurrents();
        updateTotals();
        if (!m_totalScoreCountData.needsCounting()
                && !m_totalCrystalCountData.needsCounting()) {
            m_totalScoreCountData.isCounting = false;
            m_totalCrystalCountData.isCounting = false;
            SimpleAudioEngine::getInstance()->stopEffect(m_countingSoundId);
            endOfAnimations();
        }
    }
    if (m_livesCountData.isCounting || m_combosCountData.isCounting) {
        m_livesCountData.updateCurrents();
        m_combosCountData.updateCurrents();
        updateLivesCombos();
        if (!m_livesCountData.needsCounting()
                && !m_combosCountData.needsCounting()) {
            m_livesCountData.isCounting = false;
            m_combosCountData.isCounting = false;
            SimpleAudioEngine::getInstance()->stopEffect(m_countingSoundId);
            SimpleAudioEngine::getInstance()->playEffect("gameScreen/buy.wav", false, 1.0, 0.0, 1.0);
            animateTotals();
        }
    }


}

void WinScreen::close() {

    Screen::close();
    SimpleAudioEngine::getInstance()->stopBackgroundMusic(true);
    Director::getInstance()->getScheduler()->unscheduleUpdateForTarget(this);
    PlatformTools::hideGADBanner();

}

WinScreen::~WinScreen() {
    CheetahAtlasHelper::getInstance()->remove("winScreen/fireworks.atlas");
    CheetahAtlasHelper::getInstance()->remove("honorScreen/honorScreen.atlas");


}

void WinScreen::doFireworks() {
    Animation *anim = Animation::create();
    Tools::addFramesToAnimation(anim, "winScreen/fireworks/01/%04d.png", 24.0);
    anim->setLoops(-1);
    m_fwSprite = Sprite::create();
    m_fwSprite->runAction(Animate::create(anim));
    m_fwSprite->setPosition(Point(Tools::randFloat() * 300.0 + 200.0,Tools::randFloat() * 300.0 + 600.0));
    Color3B color = Color3B((GLubyte)Tools::randInt(100, 255), (GLubyte)Tools::randInt(100, 255), (GLubyte)Tools::randInt(100, 255));
    m_fwSprite->setColor(color);
    rootNode->addChild(m_fwSprite, 10);
}

void WinScreen::initAchievementAnimation() {
    /*LevelAchievement flags[5] = {
            LevelAchievement::sniper,
            LevelAchievement::destroyer,
            LevelAchievement::mushroomer,
            LevelAchievement::lastLine,
            LevelAchievement::combo,
    };
    std::string strIds[5] = {"sniper", "destroyer", "mushroomer", "lastLine", "combo"};
    float yPos = 900.0;
    float xPos = 768.0 * 0.5;
    for (int i = 0; i < 5; ++i) {
        if (GD->m_model.temp.getLevelAchievement(flags[i])) {
            auto sprite = Sprite::createWithSpriteFrameName(
                    StringUtils::format("honorScreen/%sIcon.png", strIds[i].c_str()));
            sprite->setPosition(Point(xPos, yPos));
            rootNode->addChild(sprite);
            //yPos += 150.0;
        }
    }      */
    auto bg = Sprite::create("winScreen/achievementsBg.png");
    rootNode->addChild(bg, 1, 8887);
    GD->ggc()->applyTransformAP(bg, "WinScreenAchievementsBg");

    // create score labels
    TTFConfig conf;
    conf.fontFilePath = "IZH.ttf";
    conf.fontSize = 20;

    LevelAchievement flags[5] = {
            LevelAchievement::sniper,
            LevelAchievement::destroyer,
            LevelAchievement::mushroomer,
            LevelAchievement::lastLine,
            LevelAchievement::combo,
    };

    std::string textIds[5] = {"sniper", "destroyer", "mushroomer", "lastLine", "combo"};

    for (int i = 0; i < 5; ++i) {
        if (GD->m_model.temp.getLevelAchievement(flags[i])) {

        }
        auto scoreLabel = Label::createWithTTF(conf, _("score = 0"), TextHAlignment::LEFT , 200);
        auto crystalLabel = Label::createWithTTF(conf, " = 0", TextHAlignment::LEFT , 200);
        auto crystalIcon = Sprite::create("winScreen/crystalIcon.png");
        rootNode->addChild(crystalIcon, 2);
        rootNode->addChild(scoreLabel, 2);
        rootNode->addChild(crystalLabel, 2);
        GD->ggc()->applyTransformAP(scoreLabel, StringUtils::format("AchScoreLabel_%s", textIds[i].c_str()));
        crystalIcon->setPosition(scoreLabel->getPosition() + Point(25, -30));
        crystalLabel->setPosition(crystalIcon->getPosition() + Point(20, -5));
        crystalLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
        crystalLabel->setColor(Color3B::RED);
        scoreLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
        ach[i].crystalLabel = crystalLabel;
        ach[i].scoreLabel = scoreLabel;

        log("levelAch %s is %d", textIds[i].c_str(), GD->m_model.temp.getLevelAchievement(flags[i]));

        if (!isJustAchieved(flags[i])
                && GD->m_model.getCurrentSlot()->getLevelAchievement(
                        GD->m_model.getCurrentSlot()->currentLevel, flags[i])) {
            auto icon = Sprite::create(StringUtils::format("honorScreen/icons/%sIcon.png", textIds[i].c_str()));
            auto iconNode = Node::create();
            iconNode->addChild(icon);
            rootNode->addChild(iconNode,3);
            auto pos = GD->ggc()->getPoint(StringUtils::format("WinScreenAchievementsBg.AchIcon_%s", textIds[i].c_str()));
            pos += rootNode->getChildByTag(8887)->getPosition();
            iconNode->setPosition(pos);

            if (textIds[i] == "combo") { // put a digit
                int achieveKills[4] = {5,6,7,8};
                int locId = GD->m_model.getCurrentSlot()->currentLevel / GD->getLevelsPerLocation();
                auto digit = Sprite::create(StringUtils::format("honorScreen/icons/combo%d.png", achieveKills[locId]));
                iconNode->addChild(digit, 1);
                digit->setPosition(Vec2(0, -20));
            }
        }
    }

    //totals
    m_totalCrystalCountData.init    = GD->m_model.getLevelProgress()->crystals;
    m_totalCrystalCountData.target  = GD->m_model.getCurrentSlot()->crystals;
    m_totalScoreCountData.init      = GD->m_model.getLevelProgress()->score;
    m_totalScoreCountData.target    = GD->m_model.getCurrentSlot()->score;
    m_totalCrystalCountData.increment = (int)ceilf((float)(m_totalCrystalCountData.target - m_totalCrystalCountData.init) / (3.0 / 1.0 * 60.0));
    m_totalScoreCountData.increment = (int)ceilf((float)(m_totalScoreCountData.target - m_totalScoreCountData.init) / (3.0 / 1.0 * 60.0));

    auto totalScoreLabel = Label::createWithTTF(conf,
            StringUtils::format(_("Total score = %d").c_str(), GD->m_model.getLevelProgress()->score), TextHAlignment::LEFT , 200);
    auto totalCrystalLabel = Label::createWithTTF(conf,
            StringUtils::format(" = %d", GD->m_model.getLevelProgress()->crystals), TextHAlignment::LEFT , 200);
    auto totalCrystalIcon = Sprite::create("winScreen/crystalIcon.png");
    rootNode->addChild(totalCrystalIcon, 2);
    rootNode->addChild(totalCrystalLabel, 2, 32145);
    rootNode->addChild(totalScoreLabel, 2, 45321);
    totalCrystalLabel->setColor(Color3B(247, 107, 12));
    totalScoreLabel->setColor(Color3B(247, 107, 12));
    GD->ggc()->applyTransformAP(totalScoreLabel, "TotalScoreLabel");
    totalCrystalIcon->setPosition(totalScoreLabel->getPosition() + Point(25.0, -30.0));
    totalCrystalLabel->setPosition(totalCrystalIcon->getPosition() + Point(20.0, -5.0));
    totalScoreLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    totalCrystalLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);


    m_livesCountData.init    = 0;
    m_livesCountData.target  = GD->m_model.temp.lastHeartPrice;
    m_combosCountData.init      = 0;
    m_combosCountData.target    = GD->m_model.temp.lastComboPrice;
    m_combosCountData.increment = (int)ceilf((float)(m_combosCountData.target - m_combosCountData.init) / (3.0 / 1.0 * 60.0));
    m_livesCountData.increment = (int)ceilf((float)(m_livesCountData.target - m_livesCountData.init) / (3.0 / 1.0 * 60.0));

    auto comboScoreLabel = Label::createWithTTF(conf,
            StringUtils::format(_("combo = %d").c_str(), 0), TextHAlignment::LEFT , 200);
    auto heartScoreLabel = Label::createWithTTF(conf,
            StringUtils::format("X%d = %d", 0, GD->m_model.temp.lastHeartPrice),
            TextHAlignment::LEFT , 200);
    auto heartScoreIcon = Sprite::create("winScreen/heartIcon.png");
    rootNode->addChild(comboScoreLabel, 2, 4564);
    rootNode->addChild(heartScoreLabel, 2, 9874);
    rootNode->addChild(heartScoreIcon, 2);
    comboScoreLabel->setColor(Color3B::WHITE);
    heartScoreLabel->setColor(Color3B::WHITE);
    comboScoreLabel->setPosition(totalCrystalLabel->getPosition() + Point(-245.0, 0.0));
    heartScoreLabel->setPosition(totalScoreLabel->getPosition() + Point(-170.0, 0.0));
    heartScoreIcon->setPosition(heartScoreLabel->getPosition() + Point(-20.0, 8.0));
    heartScoreLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    comboScoreLabel->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);

    m_currentAchStuff = 0;
    introNextAchievement();

}

void WinScreen::introNextAchievement() {
    LevelAchievement flags[5] = {
            LevelAchievement::sniper,
            LevelAchievement::destroyer,
            LevelAchievement::mushroomer,
            LevelAchievement::lastLine,
            LevelAchievement::combo,
    };
    std::string textIds[5] = {"sniper", "destroyer", "mushroomer", "lastLine", "combo"};

    if (m_currentAchStuff >= 5)
        //animateTotals();
        animateHeartCombo();
    else if (isJustAchieved(flags[m_currentAchStuff])) {
        auto iconNode = Node::create();
        auto achSprite = Sprite::create(StringUtils::format("honorScreen/icons/%sIcon.png", textIds[m_currentAchStuff].c_str()));
        rootNode->addChild(iconNode,3);
        iconNode->addChild(achSprite, 1);
        auto pos = GD->ggc()->getPoint(StringUtils::format("WinScreenAchievementsBg.AchIcon_%s", textIds[m_currentAchStuff].c_str()));
        pos += rootNode->getChildByTag(8887)->getPosition();
        if (textIds[m_currentAchStuff] == "combo") { // put a digit
            int achieveKills[4] = {5,6,7,8};
            int locId = GD->m_model.getCurrentSlot()->currentLevel / GD->getLevelsPerLocation();
            auto digit = Sprite::create(StringUtils::format("honorScreen/icons/combo%d.png", achieveKills[locId]));
            iconNode->addChild(digit, 2);
            digit->setPosition(Vec2(0, -20));
            digit->runAction(FadeIn::create(0.5));
        }
        iconNode->setPosition(pos);
        iconNode->setScale(0.1);
        achSprite->setOpacity(0);
        achSprite->runAction(FadeIn::create(0.5));
        Vector<FiniteTimeAction*> actions;
        actions.pushBack(ScaleTo::create(0.2, 1.5));
        actions.pushBack(ScaleTo::create(0.3, 1.0));
        actions.pushBack(DelayTime::create(0.1));
        actions.pushBack(CallFunc::create([this](){
            introNextAchievement();
        }));
        iconNode->runAction(Sequence::create(actions));
        ach[m_currentAchStuff].scoreLabel->setString(StringUtils::format(
                _("score = %d").c_str(), GD->getAchievementPrice(flags[m_currentAchStuff], "Score")));
        ach[m_currentAchStuff].crystalLabel->setString(StringUtils::format(
                " = %d", GD->getAchievementPrice(flags[m_currentAchStuff], "Crystal")));

        m_currentAchStuff++;
        SimpleAudioEngine::getInstance()->playEffect("winScreen/achievement.wav", false, 1.0, 0.0, 1.0);

    }
    else {
        m_currentAchStuff++;
        introNextAchievement();
    }

}

void WinScreen::animateTotals() {
    log("ready to total count");
    if (m_totalCrystalCountData.needsCounting())
        m_totalCrystalCountData.isCounting = true;
    if (m_totalScoreCountData.needsCounting())
        m_totalScoreCountData.isCounting = true;

    if (m_totalCrystalCountData.needsCounting() || m_totalScoreCountData.needsCounting())
        m_countingSoundId = SimpleAudioEngine::getInstance()->
            playEffect("winScreen/counting.wav", false, 1.0, 0.0, 1.0);

}


void WinScreen::animateHeartCombo() {
    log("ready to total count");
    if (m_livesCountData.needsCounting())
        m_livesCountData.isCounting = true;
    if (m_combosCountData.needsCounting())
        m_combosCountData.isCounting = true;

    if (m_combosCountData.needsCounting() || m_livesCountData.needsCounting())
        m_countingSoundId = SimpleAudioEngine::getInstance()->
                playEffect("winScreen/counting.wav", false, 1.0, 0.0, 1.0);

}


bool WinScreen::isJustAchieved(LevelAchievement ach) {
    return GD->m_model.temp.getLevelAchievement(ach);
}

bool WinScreen::CountingData::needsCounting() {
    return target > init;
}

void WinScreen::CountingData::updateCurrents() {
    if (init < target) init += increment;
    else init = target;
}

void WinScreen::updateTotals() {
    auto totalCrystalLabel = static_cast<Label*>(rootNode->getChildByTag(32145));
    auto totalScoreLabel = static_cast<Label*>(rootNode->getChildByTag(45321));
    totalCrystalLabel->setString(StringUtils::format(" = %d", m_totalCrystalCountData.init));
    totalScoreLabel->setString(StringUtils::format(_("Total score = %d").c_str(), m_totalScoreCountData.init));
}


void WinScreen::updateLivesCombos() {
    auto livesLabel = static_cast<Label*>(rootNode->getChildByTag(9874));
    auto comboLabel = static_cast<Label*>(rootNode->getChildByTag(4564));
    comboLabel->setString(StringUtils::format(_("combo = %d").c_str(), m_combosCountData.init));
    livesLabel->setString(StringUtils::format("X%d = %d", GD->m_model.temp.currentLevelSavedHearts, m_livesCountData.init));
}


void WinScreen::addTotalGoldLabel() {
    auto totalScoreLabel = static_cast<Label*>(rootNode->getChildByTag(45321));

    auto icon = Sprite::create("commonUI/goldIcon.png");
    rootNode->addChild(icon);
    TTFConfig conf;
    conf.fontFilePath = "IZH.ttf";
    conf.glyphs = GlyphCollection::ASCII;
    conf.fontSize = 20;
    auto label = Label::createWithTTF(conf,
            StringUtils::format("= %d", GD->m_model.getCurrentSlot()->gold), TextHAlignment::LEFT , 200);
    rootNode->addChild(label);
    label->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    label->setPosition(totalScoreLabel->getPosition() + Vec2(250.0, 0.0));
    label->setColor(Color3B::WHITE);
    icon->setPosition(label->getPosition() + Vec2(-10.0, 12.0));


}

void WinScreen::endOfAnimations() {
    //GD->check35MinSale(); f*ck this...

}

void WinScreen::saleClickedOk(int saleId) {

}

void WinScreen::saleClickedCancel() {

}
