//
// Created by Admin on 08.09.14.
//


#ifndef __MRFaqScreen_H_
#define __MRFaqScreen_H_

#include "core/MRScreen.h"
#include "ui/CocosGUI.h"

namespace myth {

    class StoryScreen : public Screen {
    public:
        StoryScreen() : Screen("Story Screen") {}

        virtual void open() override;

        virtual void close() override;

        virtual void preloadComplete() override;

        virtual std::vector<Resource> getPreloadResources() override;

        void backButtonTouched(Object *sender, ui::TouchEventType type);
    };

}

#endif //__MRFaqScreen_H_
