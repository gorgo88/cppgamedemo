//
// Created by Admin on 27.05.14.
//


#ifndef __MRAchievementHelpScreen_H_
#define __MRAchievementHelpScreen_H_

#include "core/MRScreen.h"
#include "ui/CocosGUI.h"

namespace myth {
    class AchievementHelpScreen : public Screen {
    public:

        AchievementHelpScreen() : Screen("Achievement Help Screen") {}

        virtual void open() override;

        virtual void close() override;

        virtual void preloadComplete() override;

        virtual std::vector<Resource> getPreloadResources() override;

        void backButtonTouched(Object *sender, ui::TouchEventType type);
    };
}

#endif //__MRAchievementHelpScreen_H_
