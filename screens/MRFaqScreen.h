//
// Created by Admin on 08.09.14.
//


#ifndef __MRFAQScreen_H_
#define __MRFAQScreen_H_

#include "core/MRScreen.h"
#include "ui/CocosGUI.h"

namespace myth {

    class FaqScreen : public Screen {
    public:
        FaqScreen() : Screen("Faq Screen") {}

        virtual void open() override;

        virtual void close() override;

        virtual void preloadComplete() override;

        virtual std::vector<Resource> getPreloadResources() override;

        void backButtonTouched(Object *sender, ui::TouchEventType type);
    };

}

#endif //__MRFAQScreen_H_
