//
// Created by Admin on 07.02.14.
//


#ifndef __MRLoseScreen_H_
#define __MRLoseScreen_H_

#include "core/myth.h"
#include "ui/CocosGUI.h"

namespace myth {
class LoseScreen : public Screen {

public:


    LoseScreen() : Screen("Lose Screen") {}

    virtual void open() override;

    virtual void close() override;

    virtual ~LoseScreen();

    void buttonHandle(Object *sender, cocos2d::ui::TouchEventType type);

    void showSaleFullscreen(int saleId, bool store = true);

    void saleClickedOk(int saleId);

    void saleClickedCancel();
};
}

#endif //__MRLoseScreen_H_
