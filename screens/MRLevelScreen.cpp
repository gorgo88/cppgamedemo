//
// Created by Admin on 20.02.14.
//

#include <game/MRUIWidgets.h>
#include "MRLevelScreen.h"
#include "ui/CocosGUI.h"
#import "audio/include/SimpleAudioEngine.h"
#include "core/CheetahAtlasHelper.h"

using namespace myth;
using namespace cocos2d;
using namespace ui;
using namespace CocosDenshion;

void LevelScreen::open() {
    Screen::open();
    schedulePreload();
}


void LevelScreen::preloadComplete() {
    SimpleAudioEngine::getInstance()->playBackgroundMusic("locationScreen/music.mp3", true);
    auto bg = Sprite::create(StringUtils::format("levelScreen/%02d.jpg", (int)GD->m_model.temp.currentLocation));
    bg->setPosition(Tools::screenCenter());
    bg->setScaleX(1.3);
    bg->setScaleY(1.15);
    rootNode->addChild(bg);

    float startX = 170.0, dx = 220.0, startY = 820.0, dy = -220.0;
    int locationIndex = static_cast<int>(GD->m_model.temp.currentLocation);
    int levelOffset = GD->getLevelsPerLocation() * locationIndex;

    CheetahAtlasHelper::getInstance()->add("levelScreen/button/button.atlas");

    Node *buttonHolder = Node::create();

    int levelsPerLocation = GD->getLevelsPerLocation();//LEVEL_COUNT / 4;
    for (int i = 0; i < levelsPerLocation; ++i) {
        int rowIndex = i % 3;
        int colIndex = i / 3;
        Point posButton = Point(startX + dx * rowIndex, startY + dy * colIndex);

        char levelDataKey[32];
        sprintf(levelDataKey, "levels.level%02d", getLevelId(i)+1);
        ConfigReader *levelDataReader = GD->defaultsConfig()->getSubConfigByPath(levelDataKey);

        bool enabled = (levelOffset + i <= GD->m_model.getCurrentSlot()->completeLevels + 1);

        enabled = enabled || OPEN_ALL;

        auto button = Button::create();


        std::string textureName = "";

        if (enabled) {
            if (i == bossLevel::LAST_BOSS)
                textureName = "levelScreen/button/buttonDeathBg.png";
            else if (i == bossLevel::SECOND_BOSS)
                textureName = "levelScreen/button/buttonDragonBg.png";
            else if (i == bossLevel::FIRST_BOSS)
                textureName = "levelScreen/button/buttonGinorBg.png";
            else {
                textureName = "levelScreen/button/buttonRegularBg.png";
            }
        }
        else {
            std::string locId = Tools::getLocationLabel(GD->m_model.temp.currentLocation);
            locId[0] = (char)toupper(locId[0]);
            textureName = StringUtils::format("levelScreen/button/buttonClosed%s.png", locId.c_str());
        }
        button->loadTextureNormal(textureName.c_str(), TextureResType::PLIST);
        button->loadTexturePressed(textureName.c_str(), TextureResType::PLIST);
        if (enabled)  {
            button->setTouchEnabled(true);
            button->addTouchEventListener(this, toucheventselector(LevelScreen::buttonTouched));
        }
        button->setTag(i);
        button->setPosition(posButton);
        buttonHolder->addChild(button);

        char text[8];
        sprintf(text, _("wave %d-%d").c_str(), locationIndex + 1, /*levelOffset + */i + 1);
        TTFConfig conf;
        conf.fontSize = 20;
        conf.fontFilePath = "IZH.ttf";
        conf.customGlyphs = "0123456789-";
        conf.glyphs = GlyphCollection::CUSTOM;
        auto label = Label::createWithTTF(conf, text, TextHAlignment::CENTER, 100);
        rootNode->addChild(label, 10);
        label->setAnchorPoint(Point::ANCHOR_MIDDLE);
        label->setPosition(posButton + Point(0, -110));
        if (levelOffset + i <= GD->m_model.getCurrentSlot()->completeLevels) label->setColor(Color3B::GREEN);

        if (enabled) {
            LevelAchievement flags[5] = {
                    LevelAchievement::sniper,
                    LevelAchievement::destroyer,
                    LevelAchievement::mushroomer,
                    LevelAchievement::lastLine,
                    LevelAchievement::combo,
            };
            std::string strIds[5] = {"sniper", "destroyer", "mushroomer", "lastLine", "combo"};
            for (int j = 0; j < 5; ++j) {
                std::string frame;
                if (GD->m_model.getCurrentSlot()->getLevelAchievement(getLevelId(i), flags[j]))
                    frame = StringUtils::format("levelScreen/button/%sOn.png", strIds[j].c_str());
                else
                    frame = StringUtils::format("levelScreen/button/%sOff.png", strIds[j].c_str());

                auto sprite = Sprite::createWithSpriteFrameName(frame);
                sprite->setPosition(posButton);
                buttonHolder->addChild(sprite);
            }
        }
    }

    rootNode->addChild(buttonHolder, 1);

    auto button = ui::Button::create();
    button->loadTextureNormal("commonUI/backButton.png", TextureResType::LOCAL);
    button->setTouchEnabled(true);
    button->addTouchEventListener(this, toucheventselector(LevelScreen::backButtonTouched));
    button->setPosition(Point(100.0, 970.0));
    rootNode->addChild(button, 10);

    placeStats();
    int interstitialInterval = GD->defaultsConfig()->fromPath("levelScreenADPayInterval")->toInt();
    // interstitial also checks internet connection
    if (PlatformTools::getMinuteId() - GD->m_model.temp.lastLevelScreenInterstitialMinute > interstitialInterval) {
        PlatformTools::setGADDelegate(this);
        PlatformTools::preloadGADInterstitial(true);
    } /*else if (PlatformTools::isPurchaseAllowed()) { // deprecated
        m_lastSaleBtnXIndex = 0;
        if (GD->m_model.getCurrentSlot()->isSaleShowed(0)
            && !GD->m_model.getCurrentSlot()->isSaleUsed(0)) {
            log("!!!wnaan show btn sale0");
            placeSaleThumbBtn(0);
        }
        if (GD->m_model.getCurrentSlot()->isSaleShowed(1)
            && !GD->m_model.getCurrentSlot()->isSaleUsed(1)) {
            placeSaleThumbBtn(1);
        }
    }*/
    if (PlatformTools::isPurchaseAllowed())
        /*if (((float)GD->m_model.getCurrentSlot()->totalGameTimeSeconds / 60.0f / 60.0f) > 2.0f
            && !GD->m_model.getCurrentSlot()->isSaleShowed(0)) { // 2 hours left
            showSaleFullscreen(0);
        }*/ // deprecated
        /*if (GD->m_model.getCurrentSlot()->shouldShowAfterBossWinSale) {
            GD->m_model.getCurrentSlot()->shouldShowAfterBossWinSale = false;
            showSaleFullscreen(2, false);
        }*/
        GD->check35MinSale([this](){updateStats();});
}


void LevelScreen::placeSaleThumbBtn(int saleId) {
    auto saleBtn = ui::Button::create();
    int saleValue = GD->defaultsConfig()->fromPath(StringUtils::format("sales.sale%d.valueId", saleId))->toInt();
    saleBtn->loadTextureNormal(StringUtils::format("sales/%d.png", saleValue), ui::Widget::TextureResType::LOCAL);
    saleBtn->addClickEventListener([this, saleId](Ref *sender){
        showSaleFullscreen(saleId);
    });

    rootNode->addChild(saleBtn, 10);
    saleBtn->setPosition(Vec2(768/4*m_lastSaleBtnXIndex + 768/8, 10));
    m_lastSaleBtnXIndex++;
    saleBtn->setScale(0.5f);
    saleBtn->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
}



void LevelScreen::showSaleFullscreen(int saleId, bool store) {
    mgui::Sale *panel = mgui::Sale::create(saleId);
    panel->setName("salePanel");

    if (store) {
        GD->m_model.getCurrentSlot()->setSaleShowed(saleId, true);
    }
    panel->m_onSaleClicked = [this](int _saleId) {
        saleClickedOk(_saleId);
    };
    panel->m_onCloseClicked = [this]() {
        saleClickedCancel();
    };
    rootNode->addChild(panel, 20);
}


void LevelScreen::placeStats() {
   auto labelGold = Label::createWithBMFont("gameScreen/bossPrice/digitFonts/gold/font.fnt",
            StringUtils::format("%d", GD->m_model.getCurrentSlot()->gold), TextHAlignment::CENTER, 200, Vec2::ZERO);

    auto labelCrystal = Label::createWithBMFont("gameScreen/bossPrice/digitFonts/crystals/font.fnt",
            StringUtils::format("%d", GD->m_model.getCurrentSlot()->crystals), TextHAlignment::CENTER, 200, Vec2::ZERO);
    labelGold->setPosition(Vec2(600.0, 960.0));
    labelCrystal->setPosition(Vec2(400.0, 960.0));
    labelGold->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    labelCrystal->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
    rootNode->addChild(labelGold, 10);
    rootNode->addChild(labelCrystal, 10);
    labelGold->setName("goldLabel");
    labelCrystal->setName("crystalLabel");
    auto iconGold = Sprite::create("locationScreen/goldIcon.png");
    auto iconCrystal = Sprite::create("locationScreen/crystalIcon.png");
    iconGold->setPosition(labelGold->getPosition() + Vec2(-20.0f,0.0));
    iconCrystal->setPosition(labelCrystal->getPosition() + Vec2(-20.0f,0.0));
    rootNode->addChild(iconGold, 10);
    rootNode->addChild(iconCrystal, 10);
}

void LevelScreen::buttonTouched(Object *sender, TouchEventType type) {
    auto button = static_cast<Button*>(sender);
    if (type == TouchEventType::TOUCH_EVENT_BEGAN) {
        button->setScale(1.1);
    }
    if (type == TouchEventType::TOUCH_EVENT_ENDED) {
        button->setScale(1.0);
        SimpleAudioEngine::getInstance()->playEffect("common/enter.wav", false, 1.0, 0.0, 1.0);
        GD->m_model.getCurrentSlot()->currentLevel = getLevelId(button->getTag());
        GD->openScreen(Screens::game);
    }
}
void LevelScreen::backButtonTouched(Object *sender, TouchEventType type) {
    auto button = static_cast<Button*>(sender);
    if (type == TouchEventType::TOUCH_EVENT_ENDED) {
        GD->openScreen(Screens::location);
    }
}

int LevelScreen::getLevelId(int buttonIndex) {
    int level = buttonIndex + GD->getLevelsPerLocation() * static_cast<int>(GD->m_model.temp.currentLocation);
    return level;
}

void LevelScreen::close() {
    PlatformTools::clearGADDelegate();
    CheetahAtlasHelper::getInstance()->remove("levelScreen/button/button.atlas");
    Screen::close();
}

std::vector<Resource> LevelScreen::getPreloadResources() {
    vector<Resource> resources;
    resources.push_back(Resource(ResourceType::texture,
            StringUtils::format("levelScreen/%02d.jpg", (int)GD->m_model.temp.currentLocation)));
    resources.push_back(Resource(ResourceType::texture, "commonUI/backButton.png"));
    resources.push_back(Resource(ResourceType::texture, "levelScreen/buttonRegular.png"));
    resources.push_back(Resource(ResourceType::texture, "levelScreen/buttonDeath.png"));
    resources.push_back(Resource(ResourceType::texture, "levelScreen/buttonGinor.png"));
    resources.push_back(Resource(ResourceType::sound, "gameScreen/explode01.wav"));
    return resources;
}

void LevelScreen::placeBanner() {
    auto button = ui::Button::create();
    button->loadTextureNormal("levelScreen/bannerBg.jpg", ui::Widget::TextureResType::LOCAL);
    button->addClickEventListener([button](Ref *sender){
        PlatformTools::showGADInterstitialIfPossible(true);
        button->removeFromParent();
    });

    // RichText
    auto richText = RichText::create();
    richText->ignoreContentAdaptWithSize(false);
    richText->setContentSize(Size((int)button->getContentSize().width - 20, 10));
    richText->ignoreContentAdaptWithSize(true);
    std::string html = _("/white/Watch a short video, and gain /yellow/100 gold/white/ and /blue/3 crystals");
    auto words = Tools::explode(html, '/');
    Color3B currentColor;
    for (auto word : words) {
        if (word == "yellow") currentColor = Color3B::YELLOW;
        else if (word == "blue") currentColor = Color3B::BLUE;
        else if (word == "white") currentColor = Color3B::WHITE;
        else {
            RichElementText* el = RichElementText::create(1, currentColor, 255, word, "IZH.ttf", 19);
            richText->pushBackElement(el);
        }
    }
    richText->setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);

    rootNode->addChild(button, 10);
    button->addChild(richText, 10);
    button->setPosition(Vec2(384, 0));
    richText->setPosition(Vec2(button->getContentSize().width / 2, 218));
    button->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);

}


void LevelScreen::interstitialIsReady() {
    placeBanner();
}

void LevelScreen::interstitialWasClosed() {
    GD->m_model.getCurrentSlot()->gold += GD->defaultsConfig()->fromPath("levelScreenADPayGold")->toInt();
    GD->m_model.getCurrentSlot()->crystals += GD->defaultsConfig()->fromPath("levelScreenADPayCrystals")->toInt();
    GD->m_model.dumpToDisk();
    updateStats();
    GD->m_model.temp.lastLevelScreenInterstitialMinute = PlatformTools::getMinuteId();
}

void LevelScreen::updateStats() {
    rootNode->getChildByName("goldLabel");
    auto goldLabel = dynamic_cast<Label*>(rootNode->getChildByName("goldLabel"));
    goldLabel->setString(StringUtils::format("%d", GD->m_model.getCurrentSlot()->gold));
    auto crystalLabel = dynamic_cast<Label*>(rootNode->getChildByName("crystalLabel"));
    crystalLabel->setString(StringUtils::format("%d", GD->m_model.getCurrentSlot()->crystals));

}

void LevelScreen::saleClickedOk(int saleId) {
    rootNode->getChildByName("salePanel")->removeFromParent();

    int upgradeId = GD->defaultsConfig()->fromPath(StringUtils::format("sales.sale%d.moneyUpgradeId", saleId))->toInt();

    m_lastSaleId = saleId;
    GD->requestMoneyUpgrade(upgradeId, [this](){
        SimpleAudioEngine::getInstance()->playEffect("gameScreen/buy.wav", false, 1.0, 0.0, 1.0);
        updateStats();
        GD->m_model.getCurrentSlot()->setSaleUsed(m_lastSaleId, true);
        GD->m_model.dumpToDisk();
        GD->openScreen(Screens::level);
    }, nullptr);
}

void LevelScreen::saleClickedCancel() {
    GD->m_model.dumpToDisk();
    rootNode->getChildByName("salePanel")->removeFromParent();
}

