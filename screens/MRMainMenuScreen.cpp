//
// Created by Admin on 09.01.14.
//

#include <game/MRUIWidgets.h>
#include "audio/include/SimpleAudioEngine.h"
#include "MRMainMenuScreen.h"
#include "ui/CocosGUI.h"
#include "core/CheetahAtlasHelper.h"
#include "platform/MRPlatformTools.h"
#include "platform/MRGoogleADType.h"

using namespace myth;
using namespace cocos2d;
using namespace CocosDenshion;
using namespace ui;


enum class ButtonTag : int {
    start, rating, reloadConfig, lang, clearModel
};

void MainMenuScreen::open() {
    //m_isFade = true;
    Screen::open();
    schedulePreload();

}


void MainMenuScreen::preloadComplete() {
    GD->m_model.temp.currentSlot = -1; // clear slot
    SimpleAudioEngine::getInstance()->playBackgroundMusic("menuScreen/music.mp3", true);
    ImageView *bg = ImageView::create();
    bg->loadTexture("mainMenuScreen/background.png");
    bg->setAnchorPoint(Point());

    std::string labels[2] = {_("Start"), _("Top score")};
    ButtonTag tags[2] = {ButtonTag::start, ButtonTag::rating};

    for (int i = 0; i < 2; ++i) {
        Button *levelBtn = Button::create();
        levelBtn->loadTextures(
                "mainMenuScreen/buttonActive.png",
                "mainMenuScreen/buttonInactive.png",
                "mainMenuScreen/buttonInactive.png");
        levelBtn->setPosition(Point(768.0/2.0, 600.0-90.0*i));
        levelBtn->setTag((int)tags[i]);
        levelBtn->setTouchEnabled(true);
        levelBtn->setTitleFontName("saef");
        levelBtn->setPressedActionEnabled(true);
        levelBtn->addTouchEventListener(this, toucheventselector(MainMenuScreen::buttonHandle));
        TTFConfig conf;
        conf.glyphs = GlyphCollection::CUSTOM;
        conf.fontFilePath = "IZH.ttf";
        conf.fontSize = 30;
        conf.customGlyphs = "New GameSettingsHelpClear model ";
        Label *label = Label::createWithTTF(conf, labels[i], TextHAlignment::CENTER , 300);
        levelBtn->addProtectedChild(label);
        label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
        label->setPosition(levelBtn->getSize() *0.5);
        rootNode->addChild(levelBtn,2);

    }

    this->rootNode->addChild(bg);

    GD->authenticateGameCenter();

    if (GD->m_model.newGameDialogComplete)
        PlatformTools::showGADBanner(GoogleADType::mainMenu);
    // lang
    /*Button *langBtn = Button::create();
    langBtn->loadTextures(
            "mainMenuScreen/buttonActive.png",
            "mainMenuScreen/buttonInactive.png",
            "mainMenuScreen/buttonInactive.png");
    langBtn->setPosition(Point(768.0/2.0, 600.0-90.0*4));
    langBtn->setTag((int)ButtonTag::lang);
    langBtn->setTouchEnabled(true);
    langBtn->setPressedActionEnabled(true);
    langBtn->addTouchEventListener(this, toucheventselector(MainMenuScreen::buttonHandle));
    TTFConfig conf;
    conf.fontFilePath = "IZH.ttf";
    conf.fontSize = 30;
    Label *label = Label::createWithTTF(conf, (GD->m_currentLanguage == "en") ? "Russian" : "English", TextHAlignment::CENTER , 300);
    langBtn->addProtectedChild(label);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    label->setPosition(langBtn->getSize() *0.5);
    rootNode->addChild(langBtn,2);    */

    if (!GD->m_model.isAdDisablePurchased) {
        placeDisableAdButton();
    }
}

std::vector<Resource> MainMenuScreen::getPreloadResources() {
    vector<Resource> resources;
    resources.push_back(Resource(ResourceType::texture, "mainMenuScreen/background.png"));
    resources.push_back(Resource(ResourceType::texture, "mainMenuScreen/buttonActive.png"));
    resources.push_back(Resource(ResourceType::texture, "mainMenuScreen/buttonInactive.png"));
    return resources;
}

void MainMenuScreen::buttonHandle(Object *sender, TouchEventType type) {
    Button *btn = dynamic_cast<Button*>(sender);



    if (type == TouchEventType::TOUCH_EVENT_ENDED) {

        if (btn->getTag() == (int)ButtonTag::lang) {
            GD->setupTranslation(GD->m_currentLanguage == "en" ? "ru_RU" : "en");
            GD->openScreen(Screens::menu);
        }

        if (btn->getTag() == (int)ButtonTag::rating) {
            GD->openScreen(Screens::rating);
        }

        else if (btn->getTag() == (int)ButtonTag::reloadConfig) {
            GameDirector::getInstance()->reloadConfig();
        }

        else if (btn->getTag() == (int)ButtonTag::start) {
            GD->openScreen(Screens::selectSlot);
        }

        else if (btn->getTag() == (int)ButtonTag::clearModel) {
            GD->m_model.clear();
            GD->m_model.dumpToDisk();
            GD->openScreen(Screens::menu);
        }

    }
}

void MainMenuScreen::close() {

    Screen::close();
    PlatformTools::hideGADBanner();
}

MainMenuScreen::~MainMenuScreen() {


}


void MainMenuScreen::placeDisableAdButton() {
    Button *btn = Button::create();
    btn->loadTextureNormal("mainMenuScreen/buttonSquare.png", Widget::TextureResType::LOCAL);
    btn->setPressedActionEnabled(true);
    TTFConfig conf;
    conf.glyphs = GlyphCollection::CUSTOM;
    conf.fontFilePath = "IZH.ttf";
    conf.fontSize = 25;
    conf.customGlyphs = "New GameSettingsHelpClear model ";
    Label *label = Label::createWithTTF(conf, _("ads off 2$"), TextHAlignment::CENTER , 210);
    btn->addProtectedChild(label);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    label->setPosition(btn->getSize() *0.5);
    rootNode->addChild(btn,2);
    btn->addClickEventListener([btn](Ref*obj){
        GD->requestDisableAd([btn](){
            SimpleAudioEngine::getInstance()->playEffect("gameScreen/buy.wav", false, 1.0, 0.0, 1.0);
            PlatformTools::hideGADBanner();
            btn->removeFromParent();
        }, nullptr);
    });
    btn->setPosition(Vec2(768/2, 600-90*3));
    btn->setName("disableAdButton");
    placeRestoreIAPButton();
}

void MainMenuScreen::placeRestoreIAPButton() {
    Button *btn = Button::create();
    btn->loadTextureNormal("mainMenuScreen/buttonSquare.png", Widget::TextureResType::LOCAL);
    btn->setPressedActionEnabled(true);
    TTFConfig conf;
    conf.fontFilePath = "IZH.ttf";
    conf.fontSize = 25;
    Label *label = Label::createWithTTF(conf, _("restore purchases"), TextHAlignment::CENTER , 210);
    btn->addProtectedChild(label);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    label->setPosition(btn->getSize() *0.5);
    rootNode->addChild(btn,2);
    btn->addClickEventListener([btn, this](Ref*obj){
        GD->requestRestoreCompletedIAP([btn, this](){
            if (GD->m_model.isAdDisablePurchased) {
                SimpleAudioEngine::getInstance()->playEffect("gameScreen/buy.wav", false, 1.0, 0.0, 1.0);
                PlatformTools::hideGADBanner();
                if (Node* dbtn = rootNode->getChildByName("disableAdButton"))
                    dbtn->removeFromParent();
            }
        });
        btn->removeFromParent();
    });
    btn->setPosition(Vec2(768/2, 600-90*5));

}
