//
// Created by Admin on 09.01.14.
//


#ifndef __MRMainMenuScreen_H_
#define __MRMainMenuScreen_H_

#include "core/myth.h"
#include "ui/CocosGUI.h"

namespace myth {
class MainMenuScreen : public Screen {

public:


    MainMenuScreen() : Screen("Main Menu Screen") {}

    virtual void open() override;

    virtual void close() override;

    virtual ~MainMenuScreen();

    void buttonHandle(Object *sender, cocos2d::ui::TouchEventType type);

    virtual void preloadComplete() override;

    virtual std::vector<Resource> getPreloadResources() override;

    void placeDisableAdButton();
    void placeRestoreIAPButton();
};
}

#endif //__MRMainMenuScreen_H_
