//
// Created by Admin on 15.08.14.
//

#include <audio/include/SimpleAudioEngine.h>
#include <core/CheetahAtlasHelper.h>
#include <game/MRUIWidgets.h>
#include "MRSlotSelectScreen.h"
#include "platform/MRPlatformTools.h"

using namespace myth;
using namespace cocos2d;
using namespace CocosDenshion;
using namespace ui;

enum class ButtonTag : int {
    slot1, slot2, slot3, clearSlot1, clearSlot2, clearSlot3, training, hard, normal, hardDialogOk, hardDialogCancel
};

void SlotSelectScreen::open() {
    Screen::open();
    schedulePreload();

}

void SlotSelectScreen::close() {
    CheetahAtlasHelper::getInstance()->remove("commonUI/dialog.atlas");
    PlatformTools::hideGADBanner();
    Screen::close();

}

SlotSelectScreen::~SlotSelectScreen() {

}

void SlotSelectScreen::buttonHandle(Object *sender, cocos2d::ui::TouchEventType type) {
    Button *btn = dynamic_cast<Button*>(sender);

    function<void (int)> clearSlot = [](int slotId){
        GD->m_model.slots[slotId].clear();
        GD->openScreen(Screens::selectSlot);
        GD->m_model.dumpToDisk();
    };

    if (type == TouchEventType::TOUCH_EVENT_ENDED) {
        if (btn->getTag() == (int) ButtonTag::slot1) enterSlot(0);
        else if (btn->getTag() == (int) ButtonTag::slot2) enterSlot(1);
        else if (btn->getTag() == (int) ButtonTag::slot3) enterSlot(2);
        else if (btn->getTag() == (int) ButtonTag::clearSlot1) clearSlot(0);
        else if (btn->getTag() == (int) ButtonTag::clearSlot2) clearSlot(1);
        else if (btn->getTag() == (int) ButtonTag::clearSlot3) clearSlot(2);
        else if (btn->getTag() == (int) ButtonTag::normal) GD->enterNewSlot(m_currentSlotIndex, Difficulty::normal);
        else if (btn->getTag() == (int) ButtonTag::hard) {
            //GD->enterNewSlot(m_currentSlotIndex, Difficulty::hard);
            showHardDialog();
        }
        else if (btn->getTag() == (int) ButtonTag::hardDialogCancel) rootNode->getChildByName("hardDialog")->removeFromParent();
        else if (btn->getTag() == (int) ButtonTag::hardDialogOk) GD->enterNewSlot(m_currentSlotIndex, Difficulty::hard);
        else if (btn->getTag() == 888) GD->openScreen(Screens::menu);
        else if (btn->getTag() == (int) ButtonTag::training) {
            GD->m_model.slots[0].setFlag(FlagIndices::newGameDialogComplete , false);
            GD->m_model.temp.currentSlot = Model::NO_SLOT;
            GD->openScreen(Screens::location);
        }
    }
}

void SlotSelectScreen::preloadComplete() {
    GD->m_model.temp.currentSlot = -1; // clear slot
    Screen::preloadComplete();
    CheetahAtlasHelper::getInstance()->add("commonUI/dialog.atlas");

    SimpleAudioEngine::getInstance()->playBackgroundMusic("menuScreen/music.mp3", true);

    ImageView *bg = ImageView::create();
    bg->loadTexture("mainMenuScreen/background.png");
    bg->setAnchorPoint(Point());
    this->rootNode->addChild(bg);


    auto button = ui::Button::create();
    button->loadTextureNormal("commonUI/backButton.png", TextureResType::LOCAL);
    button->setTouchEnabled(true);
    button->setTag(888);
    button->addTouchEventListener(this, toucheventselector(SlotSelectScreen::buttonHandle));
    button->setPosition(Point(100.0, 950.0));
    rootNode->addChild(button, 10);

    showSlots();

    if (GD->m_model.newGameDialogComplete)
        PlatformTools::showGADBanner(GoogleADType::slotSelect);

}

vector<Resource> SlotSelectScreen::getPreloadResources() {
    vector<Resource> resources;
    resources.push_back(Resource(ResourceType::texture, "mainMenuScreen/background.png"));
    resources.push_back(Resource(ResourceType::texture, "mainMenuScreen/buttonActive.png"));
    resources.push_back(Resource(ResourceType::texture, "mainMenuScreen/buttonInactive.png"));
    resources.push_back(Resource(ResourceType::texture, "commonUI/closeButton.png"));
    return resources;
}

void SlotSelectScreen::showSlots() {
    std::string labels[3] = {_("New game"), _("New Game"), _("New game")};
    ButtonTag tags[3] = {ButtonTag::slot1, ButtonTag::slot2, ButtonTag::slot3};
    ButtonTag clearTags[3] = {ButtonTag::clearSlot1, ButtonTag::clearSlot2, ButtonTag::clearSlot3};

    for (int i = 0; i < 3; ++i) {
        if (GD->m_model.slots[i].getFlag(FlagIndices::slotNewGameEntered)) {
            int level = GD->m_model.slots[i].completeLevels + 1;
            labels[i] = StringUtils::format(_("Lvl %s").c_str(),
                    Tools::getLevelTitle(GD->m_model.slots[i].completeLevels + 1).c_str());
        }
    }

    for (int i = 0; i < 3; ++i) {
        Button *trBtn = Button::create();
        trBtn->loadTextures(
                "mainMenuScreen/buttonActive.png",
                "mainMenuScreen/buttonInactive.png",
                "mainMenuScreen/buttonInactive.png");
        trBtn->setPosition(Point(768.0/2.0, 600.0-90.0*i));
        trBtn->setTag((int)tags[i]);
        trBtn->setTouchEnabled(true);
        trBtn->setTitleFontName("saef");
        trBtn->setPressedActionEnabled(true);
        trBtn->addTouchEventListener(this, toucheventselector(SlotSelectScreen::buttonHandle));
        TTFConfig conf;
        conf.glyphs = GlyphCollection::CUSTOM;
        conf.fontFilePath = "IZH.ttf";
        conf.fontSize = 30;
        conf.customGlyphs = "New GameSettingsHelpClear model ";
        Label *label = Label::createWithTTF(conf, labels[i], TextHAlignment::CENTER , 300);
        trBtn->addProtectedChild(label);
        label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
        label->setPosition(trBtn->getSize() *0.5);
        rootNode->addChild(trBtn,2);

        //clear button
        if (GD->m_model.slots[i].getFlag(FlagIndices::slotNewGameEntered)) {
            Button *clearBtn = Button::create();
            clearBtn->loadTextures(
                    "commonUI/closeButton.png",
                    "commonUI/closeButton.png",
                    "commonUI/closeButton.png");
            clearBtn->setPosition(trBtn->getPosition() + Vec2(170.0, 0.0));
            clearBtn->setTag((int)clearTags[i]);
            clearBtn->setTouchEnabled(true);
            clearBtn->setPressedActionEnabled(true);
            clearBtn->addTouchEventListener(this, toucheventselector(SlotSelectScreen::buttonHandle));
            rootNode->addChild(clearBtn,2);
        }

    }
    //training
    Button *trBtn = Button::create();
    trBtn->loadTextures(
            "mainMenuScreen/buttonActive.png",
            "mainMenuScreen/buttonInactive.png",
            "mainMenuScreen/buttonInactive.png");
    trBtn->setPosition(Point(768.0/2.0, 600.0-90.0*4));
    trBtn->setTag((int)ButtonTag::training);
    trBtn->setTouchEnabled(true);
    trBtn->setPressedActionEnabled(true);
    trBtn->addTouchEventListener(this, toucheventselector(SlotSelectScreen::buttonHandle));
    TTFConfig conf;
    conf.fontFilePath = "IZH.ttf";
    conf.fontSize = 30;
    Label *label = Label::createWithTTF(conf, _("Training"), TextHAlignment::CENTER , 300);
    trBtn->addProtectedChild(label);
    label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    label->setPosition(trBtn->getSize() *0.5);
    rootNode->addChild(trBtn,2);
}

void SlotSelectScreen::enterSlot(int index) {
    if (!GD->m_model.slots[index].getFlag(FlagIndices::slotNewGameEntered)) {
        if (m_currentSlotIndex >= 0) {
            auto button = dynamic_cast<Button*>(rootNode->getChildByTag(m_currentSlotIndex));
            button->setColor(Color3B::WHITE);
            button->setEnabled(true);
            rootNode->removeChildByTag((int)ButtonTag::hard);
            rootNode->removeChildByTag((int)ButtonTag::normal);
        }
        m_currentSlotIndex = index;
        auto button = dynamic_cast<Button*>(rootNode->getChildByTag(index));
        button->setColor(Color3B::GRAY);
        button->setEnabled(false);

        std::string icons[2] = {"normal", "hard"};
        ButtonTag tags[2] = {ButtonTag::normal, ButtonTag::hard};

        for (int i = 0; i < 2; ++i) {
            Button *modeBtn = Button::create();
            modeBtn->loadTextures(
                    StringUtils::format("slotSelectScreen/%s.png", icons[i].c_str()),
                    StringUtils::format("slotSelectScreen/%s.png", icons[i].c_str()),
                    StringUtils::format("slotSelectScreen/%s.png", icons[i].c_str()));
            modeBtn->setPosition(Point(768.0/2.0+(i*2-1)*130.0, 600.0-90.0*index));
            modeBtn->setTag((int)tags[i]);
            modeBtn->setTouchEnabled(true);
            modeBtn->setPressedActionEnabled(true);
            modeBtn->addTouchEventListener(this, toucheventselector(SlotSelectScreen::buttonHandle));
            /*TTFConfig conf;
            conf.fontFilePath = "IZH.ttf";
            conf.fontSize = 30;
            Label *label = Label::createWithTTF(conf, labels[i], TextHAlignment::CENTER , 300);
            modeBtn->addProtectedChild(label);
            label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
            label->setPosition(modeBtn->getSize() *0.5);*/
            rootNode->addChild(modeBtn,2);
//            ImageView *icon = ImageView::create(StringUtils::format("slotSelectScreen/%s.png", icons[i].c_str()),
//                    ui::TextureResType::LOCAL);
//            modeBtn->addProtectedChild(icon,2);
//            icon->setPosition(Vec2(modeBtn->getSize()*0.5));

        }
    }
    else {
        GD->enterSlot(index);
    }
}

void SlotSelectScreen::showHardDialog() {
    if (rootNode->getChildByName("hardDialog")) return;
    auto panel = mgui::DialogBox::create(_("Recommended for advanced players. More points in rating table."), _("YES"), _("NO"), 20);
    panel->setPosition(Tools::screenCenter() - Point(panel->getSize()*0.5));
    panel->m_okButton->setTouchEnabled(true);
    panel->m_okButton->setTag((int)ButtonTag::hardDialogOk);
    using namespace ui;
    panel->m_okButton->addTouchEventListener(this, toucheventselector(SlotSelectScreen::buttonHandle));

    panel->m_cancelButton->setTouchEnabled(true);
    panel->m_cancelButton->setTag((int)ButtonTag::hardDialogCancel);
    panel->m_cancelButton->addTouchEventListener(this, toucheventselector(SlotSelectScreen::buttonHandle));
    panel->setName("hardDialog");

    rootNode->addChild(panel, 20);
}
