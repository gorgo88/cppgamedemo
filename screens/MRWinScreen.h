//
// Created by Admin on 09.01.14.
//


#ifndef __MRWinScreen_H_
#define __MRWinScreen_H_

#include "core/myth.h"
#include "ui/CocosGUI.h"

namespace myth {
class WinScreen : public Screen {


public:


    WinScreen() : Screen("Win Screen") {}

    class CountingData {
    public:

        CountingData() : init(0), target(0), isCounting(false), increment(0){}
        int init;
        int target;
        bool isCounting;
        int increment;

        bool needsCounting();
        void updateCurrents();
    };

    class AchievementStuff {
    public:
        Label *scoreLabel;
        Label *crystalLabel;
    };


    virtual void open() override;

    virtual void close() override;

    virtual ~WinScreen();

    void buttonHandle(Object *sender, cocos2d::ui::TouchEventType type);

    void doFireworks();

    virtual void update(float dt);

    float m_fwTimer = 0.0;
    Sprite *m_fwSprite = nullptr;
    int m_fwCount = 0;

    bool checkForShowComix(int completeLevel);

    void initAchievementAnimation();

    AchievementStuff ach[5];
    int m_currentAchStuff;
    CountingData m_totalScoreCountData;
    CountingData m_totalCrystalCountData;
    CountingData m_livesCountData;
    CountingData m_combosCountData;
    void introNextAchievement();

    void animateTotals();

    bool isJustAchieved(LevelAchievement ach);

    void updateTotals();

    unsigned int m_countingSoundId;

    void addTotalGoldLabel();

    void animateHeartCombo();

    void updateLivesCombos();

    void endOfAnimations();

    void saleClickedOk(int saleId);

    void saleClickedCancel();
};
}

#endif //__MRWinScreen_H_
