//
// Created by Admin on 20.02.14.
//


#ifndef __MRLevelScreen_H_
#define __MRLevelScreen_H_

#include <core/myth.h>
#include "ui/CocosGUI.h"
#include <platform/MRPlatformTools.h>

namespace myth {

class LevelScreen : public Screen, public GADDelegate {
public:

    LevelScreen() : Screen("Level Screen") {}

    virtual void open() override;

    virtual void close() override;

    void buttonTouched(cocos2d::Object *sender, cocos2d::ui::TouchEventType type);

    int getLevelId(int buttonIndex);

    void backButtonTouched(Object *sender, ui::TouchEventType type);

    virtual void preloadComplete() override;

    virtual std::vector<Resource> getPreloadResources() override;

    void placeStats();

    void placeBanner();

    virtual void interstitialIsReady();

    virtual void interstitialWasClosed();

    void updateStats();

    void saleClickedOk(int saleId);
    void saleClickedCancel();

    int m_lastSaleId = -1;

    void showSaleFullscreen(int saleId, bool store = true);

    void placeSaleThumbBtn(int saleId);

    int m_lastSaleBtnXIndex;
};

}
#endif //__MRLevelScreen_H_
