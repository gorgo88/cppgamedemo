//
// Created by Admin on 07.10.14.
//


#ifndef __MRFinalTitlesScreen_H_
#define __MRFinalTitlesScreen_H_


#include "core/myth.h"
#include "ui/CocosGUI.h"

namespace myth {

    class FinalTitlesScreen : public Screen {
    public:
        FinalTitlesScreen() : Screen("Final Titles Screen"), m_touchAllowed(false), m_currentTitle(nullptr), m_side(false) {}

        virtual void open() override;

        virtual void close() override;

        bool touchBegan(Touch* touch, Event *event);

        EventListener *m_touchListener;

        bool m_touchAllowed;
        std::vector<std::string> m_titles;

        void showNextTitle();

        int m_currentTitleIndex;

        void finishTitles();

        Label *m_currentTitle;
        bool m_side;
    };

}

#endif //__MRFinalTitlesScreen_H_
