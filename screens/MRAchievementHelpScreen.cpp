//
// Created by Admin on 27.05.14.
//

#include "MRAchievementHelpScreen.h"
#include "core/myth.h"

using namespace myth;
using namespace cocos2d;
using namespace ui;

void AchievementHelpScreen::open() {
    Screen::open();
    schedulePreload();
}

void AchievementHelpScreen::close() {
    Screen::close();
}

void AchievementHelpScreen::preloadComplete() {
    auto bg = Sprite::create("encyclopediaScreen/background.jpg");
    rootNode->addChild(bg);
    bg->setPosition(Vec2(0.0, 1024.0));
    bg->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    bg->setScale(2.0);

    LevelAchievement achEnumIds[5] = {
            LevelAchievement::destroyer,
            LevelAchievement::mushroomer,
            LevelAchievement::combo,
            LevelAchievement::lastLine,
            LevelAchievement::sniper,
    };

    std::string achStringIds[5] = {"destroyer", "mushroomer", "combo", "lastLine", "sniper"};

    std::vector<std::string> descriptions;
    descriptions.push_back(_("Exterminator: This award is given for exterminating all the enemies at the current level."));
    descriptions.push_back(_("Mushroomer: This award is given at each level for three mushroom shots."));
    descriptions.push_back(_("You can become Master Combo if you destroy at least five enemy units. A series is counted if each subsequent shot destroys an enemy. Attacks of shots are summed up with magic attacks."));
    descriptions.push_back(_("The last frontier: This award is given at each level if the player hasn't allowed any monster to achieve the last line of defense on the islands."));
    // xgettext:no-c-format
    descriptions.push_back(_("Sniper: This award is given at every level for the high accuracy of shooting. Ninety percent of all shots must hit the target."));

    for (int i = 0; i < 5; ++i) {
        auto icon = Sprite::create(StringUtils::format("honorScreen/icons/%sIcon.png",achStringIds[i].c_str()));
        rootNode->addChild(icon);
        icon->setPosition(Vec2(100.0, 850-i*190));

        TTFConfig conf;
        conf.fontSize = 22;
        conf.fontFilePath = "IZH.ttf";
        auto label = Label::createWithTTF(conf, descriptions[i], TextHAlignment::LEFT, 510);
        rootNode->addChild(label);
        label->setPosition(icon->getPosition() + Vec2(100.0, 0.0));
        label->setAnchorPoint(Vec2::ANCHOR_MIDDLE_LEFT);
        label->setColor(Color3B::BLACK);
    }


    auto button = ui::Button::create();
    button->loadTextureNormal("commonUI/backButton.png", TextureResType::LOCAL);
    button->setTouchEnabled(true);
    button->addTouchEventListener(this, toucheventselector(AchievementHelpScreen::backButtonTouched));
    button->setPosition(Point(100.0, 970.0));
    rootNode->addChild(button, 10);
}

std::vector<Resource> AchievementHelpScreen::getPreloadResources() {
    return Screen::getPreloadResources();
}

void AchievementHelpScreen::backButtonTouched(Object *sender, ui::TouchEventType type) {
    auto button = static_cast<Button*>(sender);
    if (type == TouchEventType::TOUCH_EVENT_ENDED) {
        GD->openScreen(Screens::location);
    }
}
