//
// Created by Admin on 07.10.14.
//

#include "MRFinalTitlesScreen.h"
#include "core/CheetahAtlasHelper.h"
#include "game/MRUIWidgets.h"
#include "audio/include/SimpleAudioEngine.h"


using namespace myth;
using namespace CocosDenshion;


void myth::FinalTitlesScreen::open() {
    Screen::open();
    //Director::getInstance()->
    auto tl = EventListenerTouchOneByOne::create();
    EventDispatcher *dispatcher = Director::getInstance()->getEventDispatcher();
    tl->onTouchBegan = CC_CALLBACK_2(FinalTitlesScreen::touchBegan, this);
    dispatcher->addEventListenerWithFixedPriority((EventListener *) tl, -10);
    m_touchListener = tl;


    m_titles.clear();
    m_titles.push_back("Game designer - Yury Lukashev");
    m_titles.push_back("Programming - Dmitry Cheremukhin");
    m_titles.push_back("Painter - Konstantin Zolotov");
    m_titles.push_back("Painter - Evgeniy Marik");
    m_titles.push_back("Animation - Yurgen Kott");
    m_titles.push_back("3D modeling - Alexander Jlbey");
    m_titles.push_back("Alexander Zhalby");
    m_titles.push_back("Animation - Oleg Animation");
    TTFConfig conf;
    conf.fontSize = 40;
    conf.fontFilePath = "Square_rough.ttf";

    auto labelHeader = Label::createWithTTF(conf, "Project by:", TextHAlignment::RIGHT , 600);
    labelHeader->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    rootNode->addChild(labelHeader);

    labelHeader->setPosition(Vec2(384.0f, 900.0f));
//    auto action = Sequence::createWithTwoActions(MoveTo::create(1.0f, Vec2(750.0f, 800.0f)), CallFunc::create([this](){
//        m_touchAllowed = true;
//    }));
    labelHeader->runAction(FadeIn::create(0.5f));

    m_currentTitleIndex = 0;
    m_touchAllowed = true;
    showNextTitle();
    SimpleAudioEngine::getInstance()->playBackgroundMusic("gameScreen/music.mp3", true);
}


void myth::FinalTitlesScreen::close() {
    Screen::close();
    Director::getInstance()->getEventDispatcher()->removeEventListener(m_touchListener);
}

bool myth::FinalTitlesScreen::touchBegan(Touch* touch, Event *event) {
    if (m_touchAllowed) {
        showNextTitle();
    }
    return true;
}


void FinalTitlesScreen::showNextTitle() {
    if (m_currentTitleIndex >= m_titles.size()) {
        finishTitles();
        return;
    }

    if (m_currentTitle) {
        m_currentTitle->removeFromParent();
    }

    TTFConfig conf;
    conf.fontSize = 30;
    conf.fontFilePath = "Square_rough.ttf";

    auto labelTitle = Label::createWithTTF(conf, m_titles[m_currentTitleIndex], TextHAlignment::RIGHT , 768);

    labelTitle->setAnchorPoint(m_side ? Vec2::ANCHOR_TOP_RIGHT : Vec2::ANCHOR_TOP_LEFT);
    rootNode->addChild(labelTitle);
    Vec2 target = Vec2(m_side ? 750.0f : 18.0f, (float)Tools::randInt(600, 800));
    labelTitle->setPosition(target + Vec2(768.0f, 0.0f) * (m_side ? 1 : -1));
    Vector<FiniteTimeAction*> actions;
    actions.pushBack(EaseBackOut::create(MoveTo::create(1.0f, target)));
    actions.pushBack(DelayTime::create(3.0f));
    actions.pushBack(EaseBackIn::create(MoveTo::create(1.0f, labelTitle->getPosition())));
    if (m_currentTitleIndex == m_titles.size() - 1)
        actions.pushBack(DelayTime::create(3.0f));
    actions.pushBack(CallFunc::create([this](){
        showNextTitle();
    }));

    labelTitle->runAction(Sequence::create(actions));
    m_currentTitle = labelTitle;
    m_currentTitleIndex++;
    m_side = !m_side;
}

void FinalTitlesScreen::finishTitles() {
    GD->openScreen(Screens::menu);
}
