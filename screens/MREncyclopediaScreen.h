//
// Created by Admin on 27.05.14.
//


#ifndef __MREncyclopediaScreen_H_
#define __MREncyclopediaScreen_H_

#include "core/MRScreen.h"
#include "ui/CocosGUI.h"
#include "game/MRUIWidgets.h"

namespace myth {

    class EncyclopediaScreen : public Screen {

    public:


        EncyclopediaScreen() : Screen("Encyclopedia Screen") {}

        virtual void open() override;

        virtual void close() override;

        virtual void preloadComplete() override;

        virtual std::vector<Resource> getPreloadResources() override;

        void backButtonTouched(Object *sender, ui::TouchEventType type);

        void monsterButtonTouched(Object *sender, ui::TouchEventType type);

        mgui::MonsterCard *m_monsterCard;

        void monsterCardTouched(Object *sender, ui::TouchEventType type);

        void closeMonsterCard();

        ui::Layout *m_shadow;
    };

}
#endif //__MREncyclopediaScreen_H_
