//
// Created by Admin on 20.02.14.
//


#ifndef __MRLocatonScreen_H_
#define __MRLocatonScreen_H_

#include "core/myth.h"
#include "ui/CocosGUI.h"
#include "game/MRLocations.h"

#include <game/MRGameUIController.h>
#include <core/MRMoneyUpgradeDelegate.h>

namespace myth {

    class LocationScreen : public Screen, public MoneyUpgradeDelegate {
    public:

        LocationScreen() : Screen("Location Screen"), m_gameUIController(nullptr) {}

        virtual void open() override;

        virtual void close() override;

        EventListenerTouchOneByOne *m_touchListener;

        bool touchBegan(Touch *touch, Event *event);

        void showNewGameDialog();

        void showTutorialButtons();

        void buttonHandle(Object *sender, cocos2d::ui::TouchEventType type);

        bool m_dialogEnabled;

        virtual void preloadComplete() override;

        virtual std::vector<Resource> getPreloadResources() override;

        void playDesertGreenDialog();

        void playGreenSnowDialog();

        void playSnowInfernoDialog();

        void askForUnlockLocation(Location location);

        void dialogButtonHandle(Ref *sender, ui::TouchEventType type);

        void unlockLocation();

        Location m_currentUnlockLocation;

        void askForBuyCrystals();

        void buyCrystals();

        void crystalsBought();

        void placeShips();

        Sprite *m_ships;

        void fadeInShips();

        void placeTutorialButton();

        void placeStats();

        void openBooksMenu();

        bool m_newGameDialog;

        void placeShopButton();

        void toggleShop();

        virtual void upgradeMoney(int upgradeIndex,
                std::function<void()> successCallback,
                std::function<void()> failureCallback) override;

        GameUIController *m_gameUIController;

        void updateStats();

        void placeDailyAward();

    };

}
#endif //__MRLocatonScreen_H_
