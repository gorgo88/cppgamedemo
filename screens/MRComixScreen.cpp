//
// Created by Admin on 06.03.14.
//

#include "MRComixScreen.h"
#include "core/CheetahAtlasHelper.h"
#include "MRLocatonScreen.h"
#include "game/MRUIWidgets.h"
#include "audio/include/SimpleAudioEngine.h"


using namespace myth;
using namespace CocosDenshion;


void myth::ComixScreen::open() {
    Screen::open();
    //Director::getInstance()->
    schedulePreload();

}


void myth::ComixScreen::preloadComplete() {
    Screen::preloadComplete();
    Sprite *pics[4];
    char atlasPath[64];
    int comixIndex = (int)GD->m_model.temp.currentComix;



    if (GD->m_model.temp.currentComix == ComixEnum::after1stLocation
            ||GD->m_model.temp.currentComix == ComixEnum::after2ndLocation
            ||GD->m_model.temp.currentComix == ComixEnum::after4thLocation)
        SimpleAudioEngine::getInstance()->playBackgroundMusic("comixScreen/music235.mp3", false);
    else if (GD->m_model.temp.currentComix == ComixEnum::after3rdLocation
            ||GD->m_model.temp.currentComix == ComixEnum::first)
        SimpleAudioEngine::getInstance()->playBackgroundMusic("comixScreen/music4.mp3", true);
    sprintf(atlasPath, "comix/%02d.atlas", comixIndex);
    CheetahAtlasHelper::getInstance()->add(atlasPath);

    m_currentFrameIndex = 0;

    showNextFrame();

    auto tl = EventListenerTouchOneByOne::create();
    EventDispatcher *dispatcher = Director::getInstance()->getEventDispatcher();
    tl->onTouchBegan = CC_CALLBACK_2(ComixScreen::touchBegan, this);
    dispatcher->addEventListenerWithFixedPriority((EventListener *) tl, -10);
    m_touchListener = tl;

    CheetahAtlasHelper::getInstance()->add("winScreen/fireworks.atlas");
    if (GD->m_model.temp.currentComix == ComixEnum::after4thLocation) doFireworks();
}


void ComixScreen::showNextFrame() {
    if (m_currentFrameIndex >= getFramesCount()) {
//        gotoNext();
        return;
    }
    if (m_currentFrameIndex == getFramesCount() - 1) {
        placeSkipButton();
    }
    int comixIndex = (int)GD->m_model.temp.currentComix;
    char frame[16];
    sprintf(frame, "comix/%02d/%02d.png", comixIndex, m_currentFrameIndex);
    auto sprite = Sprite::createWithSpriteFrameName(frame);
    char geomKey[16];
    sprintf(geomKey, "Comix%02dPart%02d", comixIndex, m_currentFrameIndex);
    GD->ggc()->applyTransformAP(sprite, geomKey);
    sprite->setOpacity(0);
    Vector<FiniteTimeAction*> actions;
    actions.pushBack(FadeIn::create(1.0));
    float delay = (m_currentFrameIndex == getFramesCount() - 1) ? 6.0 : 1.5;
    actions.pushBack(DelayTime::create(delay));
    actions.pushBack(CallFunc::create([this](){
        showNextFrame();
    }));
    sprite->runAction(Sequence::create(actions));
    m_currentFrameSprite = sprite;
    rootNode->addChild(sprite);
    m_currentFrameIndex++;
}

void myth::ComixScreen::close() {
    Screen::close();
    Director::getInstance()->getEventDispatcher()->removeEventListener(m_touchListener);
    AnimationCache::getInstance()->removeAnimation("firework");
    CheetahAtlasHelper::getInstance()->remove("winScreen/fireworks.atlas");
    CheetahAtlasHelper::getInstance()->add("commonUI/dialog.atlas");
}

bool myth::ComixScreen::touchBegan(Touch* touch, Event *event) {
    if (m_currentFrameSprite) {
        m_currentFrameSprite->stopAllActions();
        m_currentFrameSprite->setOpacity(255);
        showNextFrame();
    }
    return true;
}

void myth::ComixScreen::gotoNext() {
    if (GD->m_model.temp.currentComix == ComixEnum::after4thLocation) {
        GD->openScreen(Screens::finalTitles);
    }
    else GD->openScreen(Screens::location);
}

std::vector<Resource> myth::ComixScreen::getPreloadResources() {
    using namespace std;

    char atlasPath[64];
    int comixIndex = (int)GD->m_model.temp.currentComix;
    sprintf(atlasPath, "comix/%02d.png", comixIndex);


    vector<Resource> resources;
    resources.push_back(Resource(ResourceType::texture, atlasPath));
    resources.push_back(Resource(ResourceType::texture, "winScreen/fireworks.png"));
    return resources;
}



void ComixScreen::doFireworks() {
    Animation *anim = Animation::create();
    Tools::addFramesToAnimation(anim, "winScreen/fireworks/01/%04d.png", 24.0);
    AnimationCache::getInstance()->addAnimation(anim, "firework");
    //anim->setLoops(-1);
    for (int i = 0; i < 5; ++i) {
        auto sprite = Sprite::create();
        auto action = RepeatForever::create(
                Sequence::createWithTwoActions(
                        DelayTime::create(Tools::randFloat() + 1.0),
                        CallFunc::create([sprite, anim](){
                            Color3B color = Color3B(
                                    (GLubyte)Tools::randInt(100, 255),
                                    (GLubyte)Tools::randInt(100, 255),
                                    (GLubyte)Tools::randInt(100, 255));
                            sprite->setColor(color);
                            sprite->runAction(Animate::create(AnimationCache::getInstance()->getAnimation("firework")));
                            sprite->setPosition(
                                    Point(Tools::randFloat() * 650.0 + 50.0,
                                            Tools::randFloat() * 900.0 + 50.0));
                        })
                )
        );
        sprite->runAction( action);
        rootNode->addChild(sprite, 100);
    }
}

int ComixScreen::getFramesCount() {
    int framesCount;
    switch (GD->m_model.temp.currentComix) {
        default:
        case ComixEnum::first: framesCount = 4; break;
        case ComixEnum::after1stLocation:
        case ComixEnum::after2ndLocation: framesCount = 2; break;
        case ComixEnum::after3rdLocation: framesCount = 3; break;
        case ComixEnum::after4thLocation: framesCount = 3; break;
    }
    return framesCount;
}

void ComixScreen::placeSkipButton() {
    CheetahAtlasHelper::getInstance()->add("commonUI/dialog.atlas");
    using namespace ui;
    auto btn = mgui::Button::create("Skip");
    rootNode->addChild(btn, 10);
    btn->setPosition(Vec2(600.0, 50.0));
    btn->setTouchEnabled(true);
    btn->addTouchEventListener(this, toucheventselector(ComixScreen::buttonHandle));
}


void ComixScreen::buttonHandle(Object *sender, ui::TouchEventType type) {
    using namespace ui;
    Button *btn = dynamic_cast<Button*>(sender);

    if (type == TouchEventType::TOUCH_EVENT_ENDED) {
        gotoNext();
    }
}
