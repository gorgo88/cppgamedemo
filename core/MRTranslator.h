//
// Created by Admin on 19.06.14.
//


#ifndef __MRTranslator_H_
#define __MRTranslator_H_

#include "cocos2d.h"

namespace myth {

    class Translator {
    public:
        static Translator *getInstance();
        std::string translate(std::string const &text);
    };

}
#endif //__MRTranslator_H_
