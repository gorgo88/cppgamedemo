//
// Created by Admin on 10.01.14.
//


#ifndef __MRStringPicker_H_
#define __MRStringPicker_H_

#include "myth.h"

namespace myth {
using namespace std;

class StringPicker {
public:
    int m_current;
    vector<string> m_strings;


    StringPicker() {
        m_current = 0;
    }

    void add(string str) {
        m_strings.push_back(str);
    }

    string pickRandom() {
        int index = Tools::randInt(0, m_strings.size() - 1);
        return m_strings[index];
    }

    string pickNext() {
        int index = m_current;
        m_current++;
        if (m_current >= m_strings.size()) m_current = 0;
        return m_strings[index];
    }
};

}

#endif //__MRStringPicker_H_
