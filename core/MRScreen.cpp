//
// Created by Admin on 04.12.13.
//


#include "MRScreen.h"
#include "MRTools.h"
#include "audio/include/SimpleAudioEngine.h"
#include "2d/CCFontAtlasCache.h"
#include "ui/UILoadingBar.h"
#include "CheetahAtlasHelper.h"
#include "platform/MRPlatformTools.h"
#include "game/MRDialogManager.h"

using namespace cocos2d;
using namespace myth;
using namespace CocosDenshion;

void Screen::replaceScene(bool fade) {
    Scene* scene = Scene::create();
    rootNode = scene;
    if (GD->getDeviceFamily() == DeviceFamily::iphone) setupBordersIphone();
    if (fade) scene = TransitionFade::create(FADE_TIME, scene);
    auto director = Director::getInstance();
    if (director->getRunningScene()) {
        Director::getInstance()->replaceScene(scene);

    }
    else director->runWithScene(scene);
}

void Screen::open() {
    log("open screen: %s", m_screenName.c_str());
    replaceScene(m_isFade);
    Director::getInstance()->getTextureCache()->removeUnusedTextures();

    if (m_screenName != "unnamedScreen") PlatformTools::postGAOpenScreen(m_screenName);
}

void Screen::close() {
    log("close screen: %s", m_screenName.c_str());
    SimpleAudioEngine::getInstance()->stopAllEffects();
    DialogManager::getInstance()->reset();
}




void Screen::startPreload(float dt) {
    if (m_screenName == "Game Screen")
        SimpleAudioEngine::getInstance()->playBackgroundMusic("preloadScreen/music.mp3", true);
    else
        SimpleAudioEngine::getInstance()->stopBackgroundMusic(false);
    CheetahAtlasHelper::getInstance()->add("preloaderScreen/bar/bar.atlas");

    Director::getInstance()->getScheduler()->unschedule("startPreload", this);
    Director::getInstance()->getTextureCache()->removeUnusedTextures();
    FontAtlasCache::purgeCachedData();
    bool isGameScreen = m_screenName == "Game Screen";
    Sprite *bg = (isGameScreen) ? GD->getPreloaderBackground() : Sprite::create("preloaderScreen/logo.jpg");
    bg->setPosition(Tools::screenCenter());
    rootNode->addChild(bg, 10, 222);

    auto barNode = Node::create();
    rootNode->addChild(barNode, 12, 333);
    barNode->setPosition(Vec2(0.0, 470.0) + Tools::screenCenter());

    barNode->setScale(0.7);

    auto bar = ui::LoadingBar::create();
    bar->loadTexture("preloaderScreen/bar/bar.png", ui::Widget::TextureResType::PLIST);
    barNode->addChild(bar, 2);
    bar->setPercent(0.0);
    bar->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    bar->setPosition(Vec2(0.0,1.0));


    auto barBack = Sprite::createWithSpriteFrameName("preloaderScreen/bar/back.png");
    barNode->addChild(barBack, 1);
    barBack->setAnchorPoint(Vec2::ANCHOR_MIDDLE);

    auto halo = Sprite::createWithSpriteFrameName("preloaderScreen/bar/halo.png");
    barNode->addChild(halo, 3);
    halo->setAnchorPoint(Vec2::ANCHOR_MIDDLE);

    auto bLabel = Label::createWithBMFont("preloaderScreen/font/font.fnt", "Loading 12%", TextHAlignment::CENTER , 500, Vec2());
    bLabel->setVerticalAlignment(TextVAlignment::CENTER);
    barNode->addChild(bLabel, 4);
    bLabel->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
    bLabel->setPosition(Vec2(0.0, -10.0));


    function<void (Node*, Node*, float)> updateHalo = [](Node* _bar, Node* _halo, float ratio){
        _halo->setPosition(Vec2(5 + _bar->getContentSize().width * ratio - _bar->getContentSize().width * _bar->getAnchorPoint().x, 0.0));

    };

    updateHalo(bar, halo, 0.0);

    //tip
    if (isGameScreen) {
        std::vector<std::string> texts;
        if (!GD->m_model.getCurrentSlot()->getFlag(FlagIndices::regularLocationOpened)
                && (GD->m_model.getCurrentSlot()->currentLevel == 2
                || GD->m_model.getCurrentSlot()->currentLevel == 5
                || GD->m_model.getCurrentSlot()->currentLevel == 8) ) {
            texts.push_back(_("Hero! Don't forget that 500 crystals are required to transfer defensive constructions to next outpost"));
        }
        else {
            texts.push_back(_("If you're short of gold for purchases, the system automatically makes an exchange of deficient amount into crystals at the rate of 1c = 4g."));
            texts.push_back(_("The substantive upgrading of walls increases the amount of time you'll block monsters."));
            texts.push_back(_("The use of magic spells doesn’t break combo series!"));
            texts.push_back(_("Magic doesn’t work while fighting the Boss, as all the magician’s forces go to support the barriers."));
            texts.push_back(_("The earth dragon and bats harm the magician during contact with the security barrier."));
            texts.push_back(_("Game play in \"HARD\" mode significantly increases the number of earned points, which is reflected in the ranking table of the main menu."));
            texts.push_back(_("Don’t forget to set defensive knives. This is the last chance to fend off a heavily wounded enemy."));
            texts.push_back(_("Some monsters can suffer damage even if they're underground."));
            texts.push_back(_("Hint: Create the longest-lasting combo sequence. It will generate game points for rating and additional income in gold."));
            texts.push_back(_("Изучение магических томов увеличивает количество очков заклинаний и здоровья мага"));
        }
        int i = Tools::randInt(0, (int)texts.size() - 1);

        TTFConfig conf;
        conf.fontFilePath = "IZH.ttf";
        conf.fontSize = 24;
        auto label = Label::createWithTTF(conf, texts[i], TextHAlignment::LEFT, 600);
        rootNode->addChild(label, 10, 888);
        label->setPosition(Vec2(384, 100));
        label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
        label->setColor(Color3B::BLACK);


    }

    Director::getInstance()->drawScene();



    auto resources = getPreloadResources();
    for (int i = 0; i < resources.size(); ++i) {
        //CCLOG("preloading resource path %s", resources[i].path.c_str());

        if (resources[i].type == ResourceType::texture)
            Director::getInstance()->getTextureCache()->addImage(resources[i].path.c_str());
        else
            SimpleAudioEngine::getInstance()->preloadEffect(resources[i].path.c_str());
        float done = ((float)(i+1)) / ((float)resources.size());

        bar->setPercent(done * 100.0f);
        updateHalo(bar, halo, done);
        bLabel->setString(StringUtils::format("Loading %d%%", (int)roundf(done*100.0)));

        Director::getInstance()->drawScene();
    }

    //usleep(100*1000);


    if (!isGameScreen)
        Director::getInstance()->getScheduler()->
            schedule(CC_CALLBACK_1(Screen::preloadCompleteDt, this), this, 5.1, 0, 0.01, false, "preloadComplete");
    else {
        // continue btn
        auto cBtn = ui::Button::create();
        cBtn->loadTextureNormal("mainMenuScreen/buttonActive.png", ui::Widget::TextureResType::LOCAL);
        cBtn->loadTexturePressed("mainMenuScreen/buttonInactive.png", ui::Widget::TextureResType::LOCAL);
        cBtn->addClickEventListener([this](Ref* obj){
            preloadCompleteDt(0);
        });
        rootNode->addChild(cBtn, 10);
        cBtn->setPosition(Vec2(Tools::screenCenter() + Vec2(0,0)));
        cBtn->runAction(RepeatForever::create(
                Sequence::createWithTwoActions(
                        ScaleTo::create(0.5f, 1.2f),
                        ScaleTo::create(0.5f, 1.0f)
                )
        ));
        cBtn->setTag(444);
        cBtn->setTitleFontName("IZH.ttf");
        cBtn->setTitleFontSize(30);
        cBtn->setTitleText(_("Continue"));
    }
    //preloadComplete();

}

void Screen::schedulePreload() {
    Director::getInstance()->getScheduler()->
            schedule(CC_CALLBACK_1(Screen::startPreload, this), this, 5.1, 0, 0.01, false, "startPreload");
    //startPreload(0.0);
}

void Screen::preloadCompleteDt(float dt) {
    rootNode->removeChildByTag(222, true);
    rootNode->removeChildByTag(333, true);
    if (rootNode->getChildByTag(888)) rootNode->removeChildByTag(888, true);
    if (rootNode->getChildByTag(444)) rootNode->removeChildByTag(444, true);
    CheetahAtlasHelper::getInstance()->remove("preloaderScreen/bar/bar.atlas");
    preloadComplete();
}


void Screen::preloadComplete() {

}

void Screen::setupBordersIphone() {
    rootNode->setPosition(Vec2(0.0f, 167.5f));
    auto footer = Sprite::create("commonUI/screenBorder.png");
    auto header = Sprite::create("commonUI/screenBorder.png");
    header->setFlippedY(true);
    rootNode->addChild(footer, 1, 10);
    rootNode->addChild(header, 1, 11);
    footer->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    header->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    footer->setPosition(rootNode->convertToNodeSpace(Vec2::ZERO));
    header->setPosition(rootNode->convertToNodeSpace(Vec2(0.0f, 1359.0f)));

}

std::string Screen::getScreenName() {
    return m_screenName;
}
