//
// Created by Admin on 12.01.14.
//


#ifndef __CheetahAtlasHelper_H_
#define __CheetahAtlasHelper_H_

#include "cocos2d.h"

class CheetahAtlasHelper {
    std::map<std::string,std::vector<std::string>>*  m_pLoadedAtlasNames;

    CheetahAtlasHelper();

public:
    static CheetahAtlasHelper *getInstance();
    void add(std::string path);
    void remove(std::string path);

};


#endif //__CheetahAtlasHelper_H_
