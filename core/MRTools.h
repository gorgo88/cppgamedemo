//
// Created by Admin on 16.12.13.
//



#ifndef __Tools_H_
#define __Tools_H_


#include "cocos2d.h"
#include "game/MRUpgradeType.h"
#include "game/MRMagicType.h"
#include "myth.h"

namespace myth {

class Tools {

public:
    static std::vector<std::string> explode(std::string const & s, char delim);
    static void addFramesToAnimation (cocos2d::Animation *anim, std::string pattern, int startFrame, int endFrame, float fps);
    static void addFramesToAnimation (cocos2d::Animation *anim, std::string pattern, float fps);
    static int randInt(int from, int to);
    static float randFloat();

    static std::string getUpgradeLabel(UpgradeType type);

    static cocos2d::Point screenCenter();

    static std::string getMagicId(MagicType type);
    static ConfigReader *getUpgradeReader(char const *prefix, int id);

    static string getLocationLabel(Location location);

    static string getAchievementLabel(LevelAchievement achievement);

    static string getLevelTitle(int level);


    static std::string formatThousandSeparated(int number);
};

}
#endif //__Tools_H_
