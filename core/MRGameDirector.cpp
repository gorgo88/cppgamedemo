//
// Created by Admin on 26.11.13.
//



#include "MRGameDirector.h"
#include "Config/ConfigReaderYAML.h"
#include "screens/MRGameScreen.h"
#include "screens/MRMainMenuScreen.h"
#include "screens/MRWinScreen.h"
#include "screens/MRLoseScreen.h"
#include "screens/MRLocatonScreen.h"
#include "audio/include/SimpleAudioEngine.h"
#include "screens/MRLevelScreen.h"
#include "network/HttpRequest.h"
#include "network/HttpClient.h"
#include "screens/MRComixScreen.h"
#include "game/MRComixEnum.h"
#include "screens/MREncyclopediaScreen.h"
#include "screens/MRAchievementHelpScreen.h"
#include "screens/MRVideoIntroScreen.h"
#include "platform/MRPlatformTools.h"
#include "screens/MRRatingScreen.h"
#include "moFileReader/moFileReader.h"
#include "screens/MRSlotSelectScreen.h"
#include "screens/MRFaqScreen.h"
#include "screens/MRStoryScreen.h"
#include "screens/MRFinalTitlesScreen.h"


using namespace cocos2d;
using namespace myth;
using namespace network;
using namespace CocosDenshion;

static GameDirector *s_instance = NULL;

GameDirector* GameDirector::getInstance() {
    if (s_instance == NULL) s_instance = new GameDirector();
    return s_instance;
}

void GameDirector::startPoint() {

    /*log (FileUtils::getInstance()->getStringFromFile(FileUtils::getInstance()->fullPathForFilename("locale/ru_RU/LC_MESSAGES/sample.po").c_str()).c_str());
    log (FileUtils::getInstance()->fullPathForFilename("locale/ru_RU/LC_MESSAGES/sample.po").c_str());
    //log (FileUtils::getInstance()->getFileUtils::getInstance()->fullPathForFilename("locale/ru_RU/LC_MESSAGES/sample.mo").c_str());
    log ("catopen = %d",
            catopen(FileUtils::getInstance()->fullPathForFilename("locale/ru_RU/LC_MESSAGES/sample.po").c_str(), 1));
    std::locale loc("ru_RU");
    log("con name = %s, LOCPATH = %s", loc.name().c_str(), getenv("LOCPATH"));
    setenv("LOCPATH", (FileUtils::getInstance()->fullPathForFilename("locale")+"/").c_str(), 1);
    log("con name = %s, LOCPATH = %s", loc.name().c_str(), getenv("LOCPATH"));
    auto& facet = std::use_facet<std::messages<char>>(loc);

    log("fullpath %s", FileUtils::getInstance()->fullPathForFilename("locale").c_str());
    auto cat = facet.open("sample", loc);
    if(cat < 0)
        log("no faset");
    else  {

        log("yes faset");
        log("yes faset: %s:", facet.get(cat, 0, 0, "No match").c_str());

    }
    facet.close(cat);
    */

    setupTranslation();
    //log("translate is %s", _("Hello world").c_str());
    //return;


    m_currentScreen = nullptr;
    m_pGameGeometryCache = nullptr;
    m_configs = Array::create();
    m_configs->retain();

    if (REMOTE_DEFAULTS) {
        HttpRequest* request = new HttpRequest();
        if (CONFIG_DEBUG)
            request->setUrl("https://docs.google.com/document/d/#####/export?hl=en&exportFormat=txt");
        else
            request->setUrl("https://docs.google.com/document/d/#####/export?hl=en&exportFormat=txt");
        request->setRequestType(HttpRequest::Type::GET);
        request->setResponseCallback(this, httpresponse_selector(GameDirector::onHttpRequestCompleted));
        request->setTag("GET test1");
        HttpClient::getInstance()->send(request);
        request->release();

        Scene *scene = Scene::create();
        if (!Director::getInstance()->getRunningScene())
            Director::getInstance()->runWithScene(scene);
        else
            Director::getInstance()->replaceScene(scene);
        TTFConfig conf;
        conf.glyphs = GlyphCollection::ASCII;
        conf.fontFilePath = "fonts/IZH.ttf";
        conf.fontSize = 50;
        Label *label = Label::createWithTTF(conf, "Loading config...", TextHAlignment::CENTER , 100);
        //scene->addChild(label);
        label->setPosition(Tools::screenCenter());
    }
    else {
        loadDefaultsLocal();
    }

}

void GameDirector::update(float dt) {
    ModelGameProgress *slot = m_model.getCurrentSlot();
    if (slot) slot->totalSlotTimeSeconds += dt;
}

void GameDirector::check35MinSale(std::function<void()> updateUICallback) {
    const int min35 = 2100;
    m_sale35MinUICallback = updateUICallback;
    ModelGameProgress *slot = m_model.getCurrentSlot();
    if (slot && slot->totalSlotTimeSeconds > min35 && !slot->isSaleShowed(4))
        show35MinSale();
}

void GameDirector::show35MinSale() {
    const int saleId = 4;
    mgui::Sale *panel = mgui::Sale::create(saleId);
    panel->setName("salePanel");

    m_model.getCurrentSlot()->setSaleShowed(saleId, true);

    panel->m_onSaleClicked = [this](int _saleId) {
        saleClickedOk(_saleId);
    };
    panel->m_onCloseClicked = [this]() {
        saleClickedCancel();
    };
    m_currentScreen->rootNode->addChild(panel, 2047); // very very top
    if (GameScreen *gs = dynamic_cast<GameScreen*>(m_currentScreen)) {
        gs->getGameController()->setPaused(true);
    }

}


void GameDirector::saleClickedOk(int saleId) {
    m_currentScreen->rootNode->getChildByName("salePanel")->removeFromParent();

    int upgradeId = GD->defaultsConfig()->fromPath(StringUtils::format("sales.sale%d.moneyUpgradeId", saleId))->toInt();

    requestMoneyUpgrade(upgradeId, [this, saleId](){
        SimpleAudioEngine::getInstance()->playEffect("gameScreen/buy.wav", false, 1.0, 0.0, 1.0);
        m_model.getCurrentSlot()->setSaleUsed(saleId, true);
        m_model.dumpToDisk();
        if (m_sale35MinUICallback) m_sale35MinUICallback();
        //GD->openScreen(Screens::level);
    }, nullptr);

    if (GameScreen *gs = dynamic_cast<GameScreen*>(m_currentScreen)) {
        gs->getGameController()->setPaused(false);
    }

}

void GameDirector::saleClickedCancel() {
    m_model.dumpToDisk();
    m_currentScreen->rootNode->getChildByName("salePanel")->removeFromParent();
    if (GameScreen *gs = dynamic_cast<GameScreen*>(m_currentScreen)) {
        gs->getGameController()->setPaused(false);
    }

}


void GameDirector::loadDefaultsLocal(){
    m_configs->insertObject(ConfigReaderYAML::createWithFilePath("data/defaultsData.yaml"), static_cast<int>(Configs::defaultsConfig));
    onDefaultsLoaded();
}

void GameDirector::onHttpRequestCompleted(HttpClient *sender, HttpResponse *response)
{
    if (!response) {
        return;
    }

    if (0 != strlen(response->getHttpRequest()->getTag())) {
        log("%s completed", response->getHttpRequest()->getTag());
    }

    int statusCode = response->getResponseCode();

    if (!response->isSucceed()) {
        log("response failed");
        log("error buffer: %s", response->getErrorBuffer());
        loadDefaultsLocal();
        return;
    }

    std::vector<char> *buffer = response->getResponseData();
    printf("Http Test, dump data: ");
    char responseString[131072];
    std::string responseStr;
    for (unsigned int i = 0; i < buffer->size(); i++) {
        responseString[i] = (*buffer)[i];
    }
    //printf(responseString);
    responseStr.assign(responseString);   // this is horrible, i am a noob
//    m_configs->insertObject(ConfigReaderYAML::createWithFilePath("data/defaultsData.yaml"), static_cast<int>(Configs::defaultsConfig));
    m_configs->insertObject(ConfigReaderYAML::createWithString(&responseStr), static_cast<int>(Configs::defaultsConfig));
    onDefaultsLoaded();
}

void GameDirector::onDefaultsLoaded() {
    m_configs->insertObject(ConfigReaderYAML::createWithFilePath("data/screenStaticResources.yaml"), static_cast<int>(Configs::screenResources));
    m_configs->insertObject(ConfigReaderYAML::createWithFilePath("data/monsterCards.yaml"), static_cast<int>(Configs::monsterCards));

    m_pDefaultsConfig0Document = getConfig(Configs::defaultsConfig)->getSubConfigByPath("0");
    m_pDefaultsConfig0Document->retain();

    m_pScreenResources0Document = getConfig(Configs::screenResources)->getSubConfigByPath("0");
    m_pScreenResources0Document->retain();

    if (defaultsConfig()->fromPath("resetModel")->toInt()) {
        m_model.clear();
        m_model.dumpToDisk();
    }
    else {
        m_model.restoreFromDisk();
    }

    if (defaultsConfig()->fromPath("displayPerformanceStats")->toInt())
        Director::getInstance()->setDisplayStats(true);
    else
        Director::getInstance()->setDisplayStats(false);
    setupSound();
    setupMultiResolutional();
    ggc();
    //playIntroVideo();

    PlatformTools::setupPaymentTransaction();

    if (TEST_ENV) {
        setupTestEnvironment();
    }
    else {
        this->openScreen(Screens::videoIntro);
    }

    // all sessions time counter
    Director::getInstance()->getScheduler()->scheduleUpdate<GameDirector>(this, 1, false);
}

ConfigReader* GameDirector::getConfig(Configs configIndex) {
    return (ConfigReader *) m_configs->getObjectAtIndex(static_cast<int>(configIndex));
}

Dictionary* GameDirector::getDefaults() {
    return (Dictionary*) ((Array*) getConfig(Configs::defaultsConfig)->getConfig())->getObjectAtIndex(0);
}

void GameDirector::openScreen(Screens screen) {
    if (m_currentScreen) {
        m_currentScreen->close();
        m_currentScreen->release();
        m_currentScreen = nullptr;
    }



    switch (screen) {
        default:
        case Screens::game:     m_currentScreen = new GameScreen(); break;
        case Screens::menu:     m_currentScreen = new MainMenuScreen(); break;
        case Screens::win:      m_currentScreen = new WinScreen(); break;
        case Screens::loose:    m_currentScreen = new LoseScreen(); break;
        case Screens::location: m_currentScreen = new LocationScreen(); break;
        case Screens::level:    m_currentScreen = new LevelScreen(); break;
        case Screens::comix:    m_currentScreen = new ComixScreen(); break;
        case Screens::encyclopedia:         m_currentScreen = new EncyclopediaScreen(); break;
        case Screens::achievementHelp:      m_currentScreen = new AchievementHelpScreen(); break;
        case Screens::videoIntro:           m_currentScreen = new VideoIntroScreen(); break;
        case Screens::rating:               m_currentScreen = new RatingScreen(); break;
        case Screens::selectSlot:           m_currentScreen = new SlotSelectScreen(); break;
        case Screens::faq:           m_currentScreen = new FaqScreen(); break;
        case Screens::story:         m_currentScreen = new StoryScreen(); break;
        case Screens::finalTitles:   m_currentScreen = new FinalTitlesScreen(); break;
    }
    m_currentScreen->open();


}

void GameDirector::setupMultiResolutional() {
    GLView *view = Director::getInstance()->getOpenGLView();
    Vec2 dr = getDeviceFamily() == DeviceFamily::iphone ? Vec2(768.0f, 1359.0f) : Vec2(768.0f, 1024.0f);
    view->setDesignResolutionSize(dr.x, dr.y, ResolutionPolicy::SHOW_ALL);

    auto fileUtils = FileUtils::getInstance();
    std::vector<std::string> paths;
    if (view->getFrameSize().height > 2000) {
        paths.push_back("images/ipadRetina");
        Director::getInstance()->setContentScaleFactor(
                1536 / view->getDesignResolutionSize().width);
    }
    else {
        paths.push_back("images/ipad");
        Director::getInstance()->setContentScaleFactor(
                768 / view->getDesignResolutionSize().width);
    }
    paths.push_back("data");
    paths.push_back("fonts");
    paths.push_back("sounds");

    fileUtils->setSearchPaths(paths);
}

CoordinateCache *GameDirector::ggc() {
    if (!m_pGameGeometryCache) {
        m_pGameGeometryCache = CoordinateCache::createCache("data/gameCoordinateCache.yaml");

        m_pGameGeometryCache->retain();
    }
    return m_pGameGeometryCache;
}

DeviceFamily GameDirector::getDeviceFamily() {
    if (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX) return DESKTOP_EMULATE_DEVICE_FAMILY;
    float winHeight = Director::getInstance()->getOpenGLView()->getFrameSize().height;
    float winWidth = Director::getInstance()->getOpenGLView()->getFrameSize().width;
    if ((winHeight == 640 && winWidth == 960) || (winHeight == 960 && winWidth == 640))
        return DeviceFamily::iphone4;
    else if (winHeight == 1024.0 || winHeight == 2048.0 || winWidth == 1024.0 || winWidth == 2048.0) // todo: refactor this shit
        return DeviceFamily::ipad;
    else
        return DeviceFamily::iphone;
}

void GameDirector::recalculateLocation () {
    /*int completeLevels =  m_model.getCurrentSlot()->completeLevels;
    int targetLocation = (int)floorf(((float)completeLevels+1.0) / 12.0);
    if (targetLocation > 3) targetLocation = 3;
    Location targetLocationEnum = static_cast<Location>(targetLocation);
    m_model.temp.currentLocation = targetLocationEnum;*/ // not need anymore
}

int GameDirector::getCompleteLocations() {
    int completeLevels =  m_model.getCurrentSlot()->completeLevels;
    int lpl = GD->getLevelsPerLocation();
    return (completeLevels+1) / lpl;
}

void GameDirector::levelComplete() {
    m_model.pushLevelProgress();
    m_model.filterLevelAchievements();
    payForAchievements();
    payForCombos();
    payForHearts();
    m_model.pushCurrentLevelAchievements();
    checkLocationAchievements();
    if (m_model.getCurrentSlot()->completeLevels < m_model.getCurrentSlot()->currentLevel)  {
        m_model.getCurrentSlot()->completeLevels = m_model.getCurrentSlot()->currentLevel;
        m_model.temp.needsPlayNewLevelSound = true;
    }
    m_model.dumpToDisk();
    recalculateLocation();
    postScoreToLeaderBoard(m_model.getCurrentSlot()->score);
    PlatformTools::showGADInterstitialIfPossible();

    openScreen(Screens::win);

    PlatformTools::sendTapjoyEvent("levelComplete", StringUtils::format("%d", m_model.getCurrentSlot()->currentLevel));
    PlatformTools::setGAParameter(googleAnalytics::PARAMETER_LAST_COMPLETE_LEVEL,
            StringUtils::format("%d", m_model.getCurrentSlot()->completeLevels));

    int curLvl = m_model.getCurrentSlot()->currentLevel;
    if (curLvl % 3 == 2 && curLvl != 2)
        m_model.getCurrentSlot()->shouldShowAfterBossWinSale = true;
}

void GameDirector::levelFailed() {
    m_model.getCurrentSlot()->totalLoses ++;
    m_model.dumpToDisk();
    openScreen(Screens::loose);
    PlatformTools::sendTapjoyEvent("levelFailed", StringUtils::format("%d", m_model.getCurrentSlot()->currentLevel));
    PlatformTools::setGAParameter(googleAnalytics::PARAMETER_LAST_FAILED_LEVEL,
            StringUtils::format("%d", m_model.getCurrentSlot()->currentLevel));

    int curLvl = m_model.getCurrentSlot()->currentLevel;
    log("cur lvl %d, cond = %i", curLvl, curLvl % 3 == 2 && curLvl != 2);
    if (curLvl % 3 == 2 && curLvl != 2)
        m_model.getCurrentSlot()->shouldShowAfterBossLoseSale = true;

}

void GameDirector::gotoNextLevel() {
    GD->m_model.getCurrentSlot()->currentLevel++;
    GD->openScreen(Screens::level);
}

ConfigReader *GameDirector::defaultsConfig() {
    return m_pDefaultsConfig0Document;
}

ConfigReader *GameDirector::getScreenResourcesReader() {
    return m_pScreenResources0Document;
}


void GameDirector::setupSound() {
    CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(m_model.soundVolume);
    CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(m_model.musicVolume);
}

void GameDirector::playIntroVideo() {
    //VideoPlayer::playFullscreenVideo("videos/intro.mp4");


}


void GameDirector::enterNewSlot(int slotIndex, Difficulty difficulty) {
    m_model.temp.currentSlot = slotIndex;
    if (!m_model.getCurrentSlot()->getFlag(FlagIndices::slotNewGameEntered)) {
        m_model.getCurrentSlot()->setFlag(FlagIndices::slotNewGameEntered, true);
        m_model.dumpToDisk();
        m_model.getCurrentSlot()->difficulty = difficulty;
        m_model.temp.currentComix = ComixEnum::first;
        openScreen(Screens::comix);
    }
}

void GameDirector::enterSlot(int slotIndex) {
    m_model.temp.currentSlot = slotIndex;
    openScreen(Screens::location);
}


GameDirector::~GameDirector() {
    m_configs->release();
    m_pDefaultsConfig0Document->release();
    m_pScreenResources0Document->release();
}

cocos2d::Sprite *GameDirector::getPreloaderBackground() {
    if (!m_preloaderBgSprite){
        m_preloaderBgSprite = Sprite::create("preloader/background.png");
        auto tipBg = Sprite::create("preloaderScreen/tipBg.png");
        m_preloaderBgSprite->addChild(tipBg);
        tipBg->setPosition(Vec2(384, 100));
        m_preloaderBgSprite->retain();
    }
    return m_preloaderBgSprite;
}



void GameDirector::payForAchievements() {
    LevelAchievement items[5] = {
            LevelAchievement::sniper,
            LevelAchievement::destroyer,
            LevelAchievement::mushroomer,
            LevelAchievement::lastLine,
            LevelAchievement::combo,
    };
    std::string achTxt[5] = {"sniper", "destroyer", "mushroomer", "lastLine", "combo"};
    std::string loc = Tools::getLocationLabel(m_model.temp.currentLocation);

    for (int i = 0; i < 5; ++i) {
        if (m_model.temp.getLevelAchievement(items[i])
                && !m_model.getCurrentSlot()->getLevelAchievement(m_model.getCurrentSlot()->currentLevel, items[i])) {
            int priceCrystal = getAchievementPrice(items[i], "Crystal");
            int priceScore = getAchievementPrice(items[i], "Score");
            m_model.getCurrentSlot()->crystals += priceCrystal;
            m_model.getCurrentSlot()->score += priceScore;
        }
    }
}

int GameDirector::getAchievementPrice(LevelAchievement achievement, std::string kind) {
    std::string loc = Tools::getLocationLabel(m_model.temp.currentLocation);
    return defaultsConfig()->fromPath(StringUtils::format(
            "achievements.%s.byWavePrices.%s.price%s",
            Tools::getAchievementLabel(achievement).c_str(),
            loc.c_str(),
            kind.c_str()
    ))->toInt();
}

void GameDirector::checkLocationAchievements() {
    int lpl = LEVEL_COUNT / 4;
    int currentLocation = (int)m_model.temp.currentLocation;
    LevelAchievement ach [5] = {LevelAchievement::destroyer, LevelAchievement::lastLine,
        LevelAchievement::mushroomer, LevelAchievement::combo, LevelAchievement::sniper };
    for (int a = 0; a < 5; ++a) {
        bool res = true;
        for (int i = currentLocation * lpl; i < lpl * (currentLocation + 1); ++i) {
            if (!m_model.getCurrentSlot()->getLevelAchievement(i, ach[a])) {
                res = false;
                break;
            }
        }
        if (res && !m_model.getCurrentSlot()->getLocationAchievement(currentLocation, ach[a])) {
            m_model.getCurrentSlot()->setLocationAchievement(currentLocation, ach[a], true);
            //int priceCrystal = getAchievementPrice(ach[a], "Crystal") * 10; // issue #645
            int priceScore = getAchievementPrice(ach[a], "Score") * 10;
            //m_model.getCurrentSlot()->crystals += priceCrystal;
            m_model.getCurrentSlot()->score += priceScore;
        }
    }
}

void GameDirector::payForCombos() {
    int scoreTotal = 0;
    for (int i = 0; i < m_model.temp.levelCombos.size(); ++i) {
        int combo = m_model.temp.levelCombos[i];
        int score = getComboScore(combo);
        scoreTotal += score;
    }
    m_model.getCurrentSlot()->score += scoreTotal;
    m_model.temp.lastComboPrice = scoreTotal;
}

int GameDirector::getComboScore(int comboKills) {
    int priceCombo = 100;
    //  100 * kills + 100 * (kills - 1) * 0.03
    if (m_model.getCurrentSlot()->currentLevel <= m_model.getCurrentSlot()->completeLevels) priceCombo = 1;
    return priceCombo * (comboKills) + static_cast<int>(static_cast<double>(priceCombo * (comboKills - 1)) * 0.03);
}

int GameDirector::getComboGold(int comboKills) {
    int custom[11] = {0,0,1,3,5,10,15,23,30,35,45};
    return comboKills <= 10 ? custom[comboKills] : custom[10] + (comboKills - 10) * 5;
}

void GameDirector::payForHearts() {
    int priceForHeart = 250;
    if (m_model.getCurrentSlot()->currentLevel <= m_model.getCurrentSlot()->completeLevels) priceForHeart = 1;
    int score = m_model.temp.currentLevelSavedHearts * priceForHeart;
    m_model.getCurrentSlot()->score += score;
    m_model.temp.lastHeartPrice = score;
}

void GameDirector::setForcedInterfaceOrientation(InterfaceOrientation const &orient) {
    m_forcedInterfaceOrientation = orient;
    PlatformTools::updateInterfaceRotation();
}

void GameDirector::setupTestEnvironment() {
//    m_model.temp.currentSlot = 0;
//    m_model.getCurrentSlot()->currentLevel = 0;
//
//    openScreen(Screens::game);
    //Tapjoy
    //PlatformTools::tapjoyShowOfferwall();
    //PlatformTools::tapjoyShowDisplayAd1();
    //openScreen(Screens::finalTitles);
    // win screen
    m_model.temp.currentSlot = 0;
    m_model.getCurrentSlot()->clear();
    m_model.getLevelProgress()->clear();
    m_model.getCurrentSlot()->score = 12345;
    m_model.getLevelProgress()->score = 123;
    m_model.getCurrentSlot()->crystals = 345;
    m_model.getLevelProgress()->crystals = 14;
    m_model.getLevelProgress()->levelAchievements[0] = {0};
    m_model.getCurrentSlot()->levelAchievements[0] = {0};
    m_model.temp.currentLevelAchievements = 0xffff;
    m_model.temp.levelCombos = {2,4,5,6,7,2};
    m_model.temp.currentLevelSavedHearts = 5;
    m_model.temp.lastComboPrice = 5648;
    m_model.temp.lastHeartPrice = 568;
    m_model.getCurrentSlot()->currentLevel = 0;
    m_model.getCurrentSlot()->gold = 5445;
    openScreen(Screens::win);
}

void GameDirector::authenticateGameCenter() {
    PlatformTools::authenticateGameCenter();
}

void GameDirector::postScoreToLeaderBoard(int score) {
    PlatformTools::postScoreToLeaderBoard(score);
}

void GameDirector::reloadConfig() {
    m_configs->release();
    m_configs = nullptr;
    startPoint();
}

void GameDirector::setupTranslation() {
    using namespace std;
    LanguageType lang = Application::getInstance()->getCurrentLanguage();
    string langId = "en";
    switch (lang) {
        default:
        case LanguageType::ENGLISH: break;
        case LanguageType::RUSSIAN: langId = "ru_RU"; break;
    }
    langId = "ru_RU";

    setupTranslation(langId);
}

void GameDirector::setupTranslation(std::string language) {
    using namespace std;
    m_currentLanguage = language;

    string messageCatalogPath = StringUtils::format("locale/%s/LC_MESSAGES/default.mo", m_currentLanguage.c_str());

    if (FileUtils::getInstance()->isFileExist(messageCatalogPath)) {
        messageCatalogPath = FileUtils::getInstance()->fullPathForFilename(messageCatalogPath);
        auto code = moFileReaderSingleton::GetInstance().ReadFile(messageCatalogPath.c_str());
        if ( code != moFileLib::moFileReader::EC_SUCCESS ) {
            log("mo file load error, %d, %i", (int)code, code == moFileLib::moFileReader::EC_MAGICNUMBER_NOMATCH);
        }
    }
}


std::string GameDirector::getTranslation(std::string msgId) {
    /*if (m_messageCatalog.empty() || m_messageCatalog.find(msgId) == m_messageCatalog.end()) return msgId;
    return m_messageCatalog[msgId];*/
    return msgId;
}

//void GameDirector::tutorialComplete() {
//    if (m_model.temp.currentSlot == 3) openScreen(Screens::menu);
//    else openScreen(Screens::location);
//}

void GameDirector::exitFromPauseMenu(bool isTutorial) {

    if (isTutorial)     openScreen(Screens::menu);
    else                openScreen(Screens::location);

}

void GameDirector::paymentTransactionComplete(int type, int count) {
    if (type == (int)PurchaseSpecialTypes::disable_ad)
        disableAd();
    else upgradeMoney(type, count);

    if (m_moneyUpgradeSuccessCallback) m_moneyUpgradeSuccessCallback();
    m_moneyUpgradeSuccessCallback = nullptr;
    Director::getInstance()->resume();
    Director::getInstance()->getEventDispatcher()->setEnabled(true);
    if (Director::getInstance()->getRunningScene()->getChildByTag(123))
        Director::getInstance()->getRunningScene()->getChildByTag(123)->removeFromParent();
}

void GameDirector::upgradeMoney(int id, int count) {
    char upgradeKey[100];
    sprintf(upgradeKey, "moneyUpgrades.upgrade%02d", id);
    ConfigReader *upgradeReader = GD->defaultsConfig()->getSubConfigByPath(upgradeKey);
    GD->m_model.getCurrentSlot()->crystals += upgradeReader->fromPath("crystal")->toInt();
    GD->m_model.dumpToDisk();
    GD->m_model.getLevelProgress()->crystals += upgradeReader->fromPath("crystal")->toInt();

    //PlatformTools::requestPurchase(PurchaseType::crystals_100, [](){}, [](PurchaseErrorCode code){});
    PlatformTools::postGAEvent("upgrades", "bought_crystals", "",
                               upgradeReader->fromPath("crystal")->toInt());
}

void GameDirector::disableAd() {
    m_model.isAdDisablePurchased = true;
    GD->m_model.dumpToDisk();
}


void GameDirector::paymentTransactionFailed(int type, int count) {

    if (m_moneyUpgradeFailureCallback) m_moneyUpgradeFailureCallback();
    m_moneyUpgradeFailureCallback = nullptr;
    Director::getInstance()->resume();
    Director::getInstance()->getEventDispatcher()->setEnabled(true);
    if (Director::getInstance()->getRunningScene()->getChildByTag(123))
        Director::getInstance()->getRunningScene()->getChildByTag(123)->removeFromParent();

}

void GameDirector::requestMoneyUpgrade(int type, function<void()> successCallback, function<void()> failureCallback) {
    m_moneyUpgradeSuccessCallback = successCallback;
    m_moneyUpgradeFailureCallback = failureCallback;
    Director::getInstance()->pause();
    auto maskLayer = LayerColor::create(Color4B(0,0,0,128));
    Director::getInstance()->getRunningScene()->addChild(maskLayer, 10, 123);
    Director::getInstance()->getEventDispatcher()->setEnabled(false);

    if (FREE_STUFF) {
        paymentTransactionComplete(type, 1);
        return;
    }
    if (!PlatformTools::isPurchaseAllowed()) {
        paymentTransactionFailed(type, 1);
        return;
    }
    PlatformTools::requestPurchase(type);
}

void GameDirector::requestDisableAd(function<void()> successCallback, function<void()> failureCallback) {
    int type = (int)PurchaseSpecialTypes::disable_ad;
    requestMoneyUpgrade(type, successCallback, failureCallback);
}


void GameDirector::requestRestoreCompletedIAP(function<void()> successCallback) {
    m_restoreIAPHandledCallback = successCallback;
    PlatformTools::requestRestorePurchases();
    Director::getInstance()->pause();
    auto maskLayer = LayerColor::create(Color4B(0,0,0,128));
    Director::getInstance()->getRunningScene()->addChild(maskLayer, 10, 123);
    Director::getInstance()->getEventDispatcher()->setEnabled(false);

}

void GameDirector::restoreCompletedIAPHandled() {
    
    if (m_restoreIAPHandledCallback) {
        m_restoreIAPHandledCallback();
        m_restoreIAPHandledCallback = nullptr;
    }
    Director::getInstance()->resume();
    Director::getInstance()->getEventDispatcher()->setEnabled(true);
    if (Director::getInstance()->getRunningScene()->getChildByTag(123))
        Director::getInstance()->getRunningScene()->getChildByTag(123)->removeFromParent();

}

int GameDirector::getLevelsPerLocation() {
    return LEVEL_COUNT / 4;
}

Screen *GameDirector::getCurrentScreen() {
    return m_currentScreen;
}

void GameDirector::resumeVideo() {
    if (getCurrentScreen() && getCurrentScreen()->getScreenName() == "Video Screen")
        dynamic_cast<VideoIntroScreen*>(getCurrentScreen())->resumeVideo();
}

void GameDirector::scheduleComeBackNotification() {
    std::vector<std::string> texts;
    texts.push_back(_("Commander! Omin###########"));
    texts.push_back(_("Gnomes fill#############"));
    texts.push_back(_("Commander, ############3!"));
    texts.push_back(_("The army of #########"));
    int textId = Tools::randInt(0, texts.size() - 1);
    for (int i = 0; i < 3; ++i) { // issue #691
        int awardId = GD->getAwardId(i, PlatformTools::getDayId(20, 29) + 1);
        if (awardId == 1) {
            textId = 1;
            break;
        }
    }
    PlatformTools::scheduleComeBackNotification(texts[textId], 20, 30);
}

void GameDirector::unscheduleComeBackNotification() {
    PlatformTools::unscheduleComeBackNotifications();
}

int GameDirector::getCurrentAwardId() {
    return getCurrentAwardId(m_model.temp.currentSlot);
}

int GameDirector::getCurrentAwardId(int slotId) {
    return getAwardId(slotId, PlatformTools::getDayId(20, 29));
}

int GameDirector::getAwardId(int slotId, int dayId) {
    ModelGameProgress *slot = &m_model.slots[slotId];
    const int COUNT = 5;
    int awards[COUNT] = {15, 20, 25, 30, 40};
    int currentAwardId = -1;
    int currentDayId = dayId;
    int lastDayId = slot->dailyAwardLastGrabedDayId;
    if (currentDayId > lastDayId) {
        if (currentDayId - lastDayId == 1) {
            int lastAwardId = slot->dailyAwardLastGrabedAwardId;
            currentAwardId = lastAwardId + 1;
            if (currentAwardId >= COUNT) currentAwardId = COUNT - 1;
        }
        else {
            currentAwardId = 0;
        }
    }
    return currentAwardId;
}

void GameDirector::grabDailyAward() {
    const int COUNT = 5;
    int awards[COUNT] = {15, 20, 25, 30, 40};
    int awardId = getCurrentAwardId();
    m_model.getCurrentSlot()->crystals += awards[awardId];
    m_model.getCurrentSlot()->dailyAwardLastGrabedAwardId = awardId;
    m_model.getCurrentSlot()->dailyAwardLastGrabedDayId = PlatformTools::getDayId(20, 29);
    m_model.dumpToDisk();
}

void GameDirector::setGamePausedWhileEnterBackground() {
    if (GameScreen *gs = dynamic_cast<GameScreen*>(m_currentScreen)) {
        gs->getGameController()->getUIController()->pauseButtonTouched(nullptr, cocos2d::ui::TouchEventType::TOUCH_EVENT_ENDED);
    }
}
