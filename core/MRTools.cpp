//
// Created by Admin on 16.12.13.
//


#include "MRTools.h"
#include "game/MRUpgradeType.h"
#include "game/MRMagicController.h"

using namespace cocos2d;
using namespace std;



void myth::Tools::addFramesToAnimation(cocos2d::Animation *anim, std::string pattern, int startFrame, int endFrame, float fps) {
    anim->setDelayPerUnit(1.0 / fps);
    int increment = startFrame < endFrame ? 1 : -1;
    for (int i = startFrame; i != endFrame+increment; i+=increment) {
        cocos2d::String *frameName = cocos2d::String::createWithFormat(pattern.c_str(), i);
        cocos2d::SpriteFrame *frame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(frameName->getCString());
        CCASSERT(frame != NULL, cocos2d::String::createWithFormat("frame %s not found", frameName->getCString())->getCString() );
        anim->addSpriteFrame(frame);
    }

}

void myth::Tools::addFramesToAnimation(cocos2d::Animation *anim, std::string pattern, float fps) {
    anim->setDelayPerUnit(1.0 / fps);
    int number = 0;
    cocos2d::SpriteFrame *frame = nullptr;
    do {
        char frameName[100];
        sprintf(frameName, pattern.c_str(), number);
        cocos2d::SpriteFrame *frame = cocos2d::SpriteFrameCache::getInstance()->getSpriteFrameByName(frameName);
        if (frame) anim->addSpriteFrame(frame);
        else {
            CCLOG("frame %s not found", frameName);
            break;
        };
        number++;
    } while (!frame);

}


std::vector<std::string> myth::Tools::explode(std::string const &s, char delim) {
    std::vector<std::string> result;
    std::istringstream iss(s);

    for (std::string token; std::getline(iss, token, delim); )
    {
        result.push_back(std::move(token));
    }

    return result;
}

int myth::Tools::randInt(int from, int to) {
    return static_cast<int>(round(static_cast<float>(from) + static_cast<float>(to - from) * randFloat()));
}

float myth::Tools::randFloat() {
    #if CC_TARGET_PLATFORM == CC_PLATFORM_LINUX
    //srand ( (u_int)time(NULL) );
    return ((float)rand()/(float)(RAND_MAX));
    #else
    double arc4RandMax = 0x100000000;
    double r = ((double)arc4random() / arc4RandMax);
    return static_cast<float>(r);
    #endif
}

string myth::Tools::getUpgradeLabel(UpgradeType type) {
    switch (type) {
        case UpgradeType::tower:    return "tower";
        case UpgradeType::bridge:   return "bridge";
        case UpgradeType::bullet:   return "bullet";
        case UpgradeType::guillotine: return "guillotine";
        case UpgradeType::magic:    return "magic";
        case UpgradeType::money:    return "money";
        default:
            return std::string();
    }
}

cocos2d::Point myth::Tools::screenCenter() {
    return Vec2(384.0f, 512.0f);//Point(cocos2d::Director::getInstance()->getVisibleSize())*0.5;
}

string myth::Tools::getMagicId(MagicType type) {
    switch (type) {
        case MagicType::air: return "air";
        case MagicType::earth: return "earth";
        case MagicType::water: return "water";
        case MagicType::fire: return "fire";
        default: break;
    }
    return "";
}

ConfigReader *myth::Tools::getUpgradeReader(char const *prefix, int id) {
    char key[32];
    sprintf(key, "%sUpgrades.upgrade%02d", prefix, id);
    return GD->defaultsConfig()->getSubConfigByPath(key);
}

string myth::Tools::getLocationLabel(myth::Location location) {
    switch (location) {
        case Location::desert:  return "desert";
        case Location::regular: return "regular";
        case Location::snow:    return "snow";
        case Location::inferno: return "inferno";
        default: break;
    }
    return "";
}

string myth::Tools::getAchievementLabel(myth::LevelAchievement achievement) {
    switch (achievement) {
        case LevelAchievement::sniper:          return "sniper";
        case LevelAchievement::lastLine:        return "lastLine";
        case LevelAchievement::destroyer:       return "destroyer";
        case LevelAchievement::mushroomer:      return "mushroomer";
        case LevelAchievement::combo:         return "combo";
        default: break;
    }
    return "";
}

string myth::Tools::getLevelTitle(int level) {
    int lpl = GD->getLevelsPerLocation();
    return StringUtils::format("%d-%d", (level) / lpl + 1, (level % lpl) + 1);
}

std::string myth::Tools::formatThousandSeparated(int number) {
    std::string str = to_string(number);
    int insertPosition = str.length() - 3;
    while (insertPosition > 0) {
        str.insert(insertPosition, " ");
        insertPosition-=3;
    }
    return str.c_str();
}
