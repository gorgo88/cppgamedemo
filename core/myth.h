#ifndef __myth_h_
#define __myth_h_


#include "MRGameDirector.h"
#include "MRScreen.h"
#include "MREasyConfig.h"
#include "MRTools.h"
#include "MRTranslator.h"


#include "moFileReader/moFileReader.h"

namespace googleAnalytics {
    static const int PARAMETER_LAST_COMPLETE_LEVEL = 1;
    static const int PARAMETER_LAST_FAILED_LEVEL = 2;
}

#endif //__myth_h_
using namespace moFileLib;
