//
// Created by Admin on 10.12.13.
//


#include "MREasyConfig.h"

void EasyConfig::setConfigDict(Dictionary *configDict) {
    m_pConfigDict = configDict;
}

EasyConfig *EasyConfig::conf(std::string key) {
    m_pCurrentRawValue = m_pConfigDict->objectForKey(key);
    return this;
}

int EasyConfig::intValue() {
    return ((String *) m_pCurrentRawValue)->intValue();
}

float EasyConfig::floatValue() {
    return ((String *) m_pCurrentRawValue)->floatValue();
}

String* EasyConfig::toString() {
    return ((String *) m_pCurrentRawValue);
}

Array* EasyConfig::toArray() {
    return ((Array *) m_pCurrentRawValue);
}

Dictionary* EasyConfig::toDictionary() {
    return ((Dictionary *) m_pCurrentRawValue);
}



