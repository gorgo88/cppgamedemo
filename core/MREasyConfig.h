//
// Created by Admin on 10.12.13.
//



#ifndef __EasyConfig_H_
#define __EasyConfig_H_

#include "myth.h"

class EasyConfig {
public:
    Dictionary *m_pConfigDict;
    Object *m_pCurrentRawValue;
    void setConfigDict(Dictionary *configDict);
    EasyConfig *conf(std::string key);
    int intValue();
    float floatValue();
    Array *toArray();
    String *toString();
    Dictionary *toDictionary();
};


#endif //__EasyConfig_H_
