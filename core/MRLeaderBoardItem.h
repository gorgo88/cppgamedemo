//
// Created by Admin on 09.07.14.
//


#ifndef __MRLeaderBoardItem_H_
#define __MRLeaderBoardItem_H_

#include "cocos2d.h"

namespace myth {
   class LeaderBoardItem {
    public :

       LeaderBoardItem() : isSelf(false), score(0), place(0), username("") {}

       int place;
       std::string username;
       int score;
       bool isSelf;
   };
}
#endif //__MRLeaderBoardItem_H_
