#ifndef __MEMTEST_SCENE_H__
#define __MEMTEST_SCENE_H__

#include "cocos2d.h"
#include "Config/ConfigReader.h"
#include "MRScreen.h"
#include "Config/CoordinateCache.h"
#include "game/MRModel.h"
#include "network/HttpClient.h"
#include "network/HttpResponse.h"
#include "moFileReader/moFileReader.h"

#define DEFAULTS ((ConfigReader*) GameDirector::getInstance()->m_configs->getObjectAtIndex(Configs::defaultsConfig))
#define GD GameDirector::getInstance()
#define REMOTE_DEFAULTS false
#define TEST_ENV false
#define OPEN_ALL false
#define FREE_STUFF false
#define IMMORTAL false
#define NO_AD false
#define CONFIG_DEBUG false
#define DESKTOP_EMULATE_DEVICE_FAMILY DeviceFamily::ipad
//#define _(str) myth::GameDirector::getInstance()->getTranslation(str)

namespace myth {

enum class Configs : int {
    defaultsConfig = 0,
    screenResources = 1,
    monsterCards = 2,
    settingsConfig
};

enum class Screens {
    intro,
    menu,
    game,
    loose,
    win,
    location,
    level,
    comix,
    encyclopedia,
    achievementHelp,
    videoIntro,
    rating,
    selectSlot,
    faq,
    story,
    finalTitles,
};

enum class DeviceFamily {
    ipad,
    iphone,
    iphone4
};

enum class PurchaseSpecialTypes : int {
    disable_ad = 100
};

namespace bossLevel {
    static const int FIRST_BOSS     = 2;
    static const int SECOND_BOSS    = 5;
    static const int LAST_BOSS      = 8;
};

enum class InterfaceOrientation {
    horizontal,
    vertical
};

class GameDirector  : public cocos2d::Object
{
    cocos2d::Array *m_configs;
    Screen *m_currentScreen;
    CoordinateCache *m_pGameGeometryCache = NULL;
    InterfaceOrientation m_forcedInterfaceOrientation;

public:
    virtual ~GameDirector();

    GameDirector()
            : m_forcedInterfaceOrientation(InterfaceOrientation::vertical),
                m_gameStarted(false), m_moneyUpgradeSuccessCallback(nullptr),
                m_moneyUpgradeFailureCallback(nullptr),
                m_currentScreen(nullptr){
    }

public:
    static GameDirector* getInstance();
    Screen *getCurrentScreen();
    void startPoint();
    Model m_model;
    InterfaceOrientation getForcedInterfaceOrientation() {return m_forcedInterfaceOrientation;}
    void setForcedInterfaceOrientation(InterfaceOrientation const &orient);
    cocos2d::EventListenerTouchOneByOne *touchListener;
    ConfigReader *m_pDefaultsConfig0Document;
    void openScreen(Screens screen);

    ConfigReader *getConfig(Configs configIndex);

    Dictionary *getDefaults();
    ConfigReader *defaultsConfig();

    void setupMultiResolutional();

    CoordinateCache *ggc();

    DeviceFamily getDeviceFamily();

    void levelComplete();

    void setupSound();

    ConfigReader *getScreenResourcesReader();

    ConfigReader *m_pScreenResources0Document;

    void onHttpRequestCompleted(cocos2d::network::HttpClient * sender, cocos2d::network::HttpResponse * response);

    void playIntroVideo();

    void enterNewSlot(int slotIndex, Difficulty difficulty);
    void enterSlot(int slotIndex);

    void loadDefaultsLocal();
    void onDefaultsLoaded();


    void levelFailed();

    void gotoNextLevel();

    void recalculateLocation();

    cocos2d::Sprite *getPreloaderBackground();

    cocos2d::Sprite *m_preloaderBgSprite = nullptr;

    void payForAchievements();

    int getAchievementPrice(LevelAchievement achievement, std::string kind);

    void checkLocationAchievements();

    void payForCombos();

    void payForHearts();

    int getCompleteLocations(); // last complete locations count: 0,1,2,3,4
    void setupTestEnvironment();

    int getComboScore(int comboKills);
    int getComboGold(int comboKills);

    void authenticateGameCenter();

    void postScoreToLeaderBoard(int score);

    void reloadConfig();

    bool m_gameStarted;

    void setupTranslation();

    //std::map<std::string, std::string> m_messageCatalog;
    string getTranslation(string msgId);

    string m_currentLanguage;

    void setupTranslation(string langId);

    //void tutorialComplete();

    void exitFromPauseMenu(bool isTutorial = false);

    void requestMoneyUpgrade(int type, function<void()> successCallback, function<void()> failureCallback);
    void requestDisableAd(function<void()> successCallback, function<void()> failureCallback);
    void requestRestoreCompletedIAP(function<void()> successCallback);
    void restoreCompletedIAPHandled();
    void paymentTransactionComplete(int type, int count);   // moneyUpdate type and count
    void paymentTransactionFailed(int type, int count);

    std::function<void()> m_moneyUpgradeSuccessCallback;
    std::function<void()> m_moneyUpgradeFailureCallback;
    std::function<void()> m_restoreIAPHandledCallback;

    int getLevelsPerLocation();

    void resumeVideo();

    void scheduleComeBackNotification();

    void unscheduleComeBackNotification();

    int getCurrentAwardId();
    int getCurrentAwardId(int slot);

    void grabDailyAward();

    int getAwardId(int slotId, int dayId);

    void disableAd();

    void upgradeMoney(int id, int count);
    void setGamePausedWhileEnterBackground();

    void update(float dt);
    std::function<void()> m_sale35MinUICallback = nullptr;
    void show35MinSale();

    void saleClickedOk(int saleId);

    void saleClickedCancel();

    void check35MinSale(std::function<void()> updateUICallback = nullptr);
};

}
#endif