//
// Created by gorgo on 06.04.15.
//

#ifndef _MYTHRANDIA_MRMONEYUPGRADEDELEGATE_H_
#define _MYTHRANDIA_MRMONEYUPGRADEDELEGATE_H_

#include "cocos2d.h"

namespace myth {
    class MoneyUpgradeDelegate {
    public:
        virtual void upgradeMoney(int upgradeIndex,
                std::function<void()> successCallback,
                std::function<void()> failureCallback) = 0;
    };
}

#endif //_MYTHRANDIA_MRMONEYUPGRADEDELEGATE_H_
