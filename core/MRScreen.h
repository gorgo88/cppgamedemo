//
// Created by Admin on 04.12.13.
//



#ifndef __MRScreen_H_
#define __MRScreen_H_

#include "cocos2d.h"
using namespace cocos2d;
namespace myth {


enum class ResourceType {texture,sound};


struct Resource {
    Resource(myth::ResourceType const &type, const std::string &path):type(type), path(path) {
    }

    ResourceType type;
    std::string path;
};

class Screen : public Object {
    const float FADE_TIME = 1.0f;

public:
    Node *rootNode;


    Screen() : m_screenName("unnamedScreen") {}

    Screen(std::string m_screenName)
            : m_screenName(m_screenName) {
    }

    virtual void open();
    virtual void close();

    void schedulePreload();
    void startPreload(float dt);
    std::string getScreenName();
    virtual void preloadComplete();

    virtual std::vector<Resource> getPreloadResources() {return std::vector<Resource>();};

    virtual ~Screen() {

    }

protected:
    void replaceScene(bool fade);
    std::string m_screenName;

    bool m_isFade = false;
private:
    void preloadCompleteDt(float dt);
protected:
    virtual void setupBordersIphone();
};

}
#endif //__MRScreen_H_
