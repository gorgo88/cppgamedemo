//
// Created by Admin on 12.01.14.
//

#include "CheetahAtlasHelper.h"
#include "MRTools.h"

using namespace cocos2d;
using namespace std;

static CheetahAtlasHelper *s_instance = nullptr;

CheetahAtlasHelper* CheetahAtlasHelper::getInstance() {
    if (!s_instance) s_instance = new CheetahAtlasHelper();
    return s_instance;
}


CheetahAtlasHelper::CheetahAtlasHelper() {
    m_pLoadedAtlasNames = new map<string, vector<string>>();
}

void CheetahAtlasHelper::add(string path) {

    string fullpath = FileUtils::getInstance()->fullPathForFilename(path);

    if (m_pLoadedAtlasNames->find(fullpath) != m_pLoadedAtlasNames->end()) {
        CCLOG("atlas %s already loaded ", fullpath.c_str());
        return;
    }

    string texturePath = path;
    SpriteFrameCache *frameCache = SpriteFrameCache::getInstance();

    // remove .xxx
    size_t startPos = texturePath.find_last_of(".");
    texturePath = texturePath.erase(startPos);

    // append .png
    texturePath = texturePath.append(".png");

    CCLOG("myth::Tools: Trying to use file %s as texture", texturePath.c_str());

    Texture2D *texture = Director::getInstance()->getTextureCache()->addImage(texturePath.c_str());

    if (!texture) {
        CCLOG("cocos2d: SpriteFrameCache: Couldn't load texture");
        return;
    }

    String *contents = String::createWithContentsOfFile(fullpath.c_str());

    CCASSERT(contents, "CCBMFontConfiguration::parseConfigFile | Open file error.");

    if (!contents)
    {
        CCLOG("cocos2d: Error parsing atlas file %s", path.c_str());
        return;
    }

    // parse spacing / padding
    string line;
    string strLeft = contents->getCString();
    vector<string> frameNames;
    while (strLeft.length() > 0)
    {
        size_t pos = strLeft.find('\n');

        if (pos != std::string::npos) {
            // the data is more than a line.get one line
            line = strLeft.substr(0, pos);
            strLeft = strLeft.substr(pos + 1);
        }
        else {
            // get the left data
            line = strLeft;
            strLeft.erase();
        }

        if(line.substr(0,strlen("textures:")) == "textures:") {
            //do nothing, hope  never use multiple textures per single atlas
        }
        else {
            vector<string> words = myth::Tools::explode(line, '\t');

            float  chX = (float)atof(words[1].c_str());
            float  chY = (float)atof(words[2].c_str());
            float  chWidth = (float)atof(words[3].c_str());
            float  chHeight = (float)atof(words[4].c_str());
            float  chOffsetX = (float)atof(words[5].c_str());
            float  chOffsetY = (float)atof(words[6].c_str());
            float  chOrigWidth = (float)atof(words[7].c_str());
            float  chOrigHeight = (float)atof(words[8].c_str());
            bool chRotated = words.size() == 10 && words[9] == "r";

            Point offsetUnRotated = chRotated
                    ? Point(chOffsetY, chOrigHeight - chWidth - chOffsetX)
                    : Point(chOffsetX, chOffsetY);
            Size frameSize = chRotated
                    ? Size(chHeight, chWidth)
                    : Size(chWidth, chHeight);

            Point offsetCenter = Point(
                    offsetUnRotated.x + frameSize.width*0.5 - chOrigWidth*0.5,
                    (- offsetUnRotated.y - frameSize.height*0.5 + chOrigHeight*0.5) * (chRotated ? -1.0 : 1.0) // explodes my brain a little
            );

            SpriteFrame *spriteFrame = SpriteFrame::createWithTexture(
                    texture,
                    Rect(chX, chY, frameSize.width, frameSize.height),
                    chRotated,
                    offsetCenter,
                    Size(chOrigWidth, chOrigHeight));

            // add sprite frame
            frameCache->addSpriteFrame(spriteFrame, words[0]);
            frameNames.push_back(words[0]);

        }
    }

    m_pLoadedAtlasNames->insert(pair<std::string, std::vector<string>>(fullpath, frameNames));
}

void CheetahAtlasHelper::remove(string path) {
    string fullpath = FileUtils::getInstance()->fullPathForFilename(path);

    if (m_pLoadedAtlasNames->find(fullpath) == m_pLoadedAtlasNames->end()) {
        CCLOG("atlas %s was not loaded", fullpath.c_str());
        return;
    }
    for (int i = 0; i < m_pLoadedAtlasNames->at(fullpath).size(); ++i) {
        string frameName = m_pLoadedAtlasNames->at(fullpath).at(i);
        cocos2d::SpriteFrameCache::getInstance()->removeSpriteFrameByName(frameName);
    }
    m_pLoadedAtlasNames->erase(fullpath);
}

